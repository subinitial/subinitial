# Subinitial Stacks Library

For more information visit this library's wiki at:
https://bitbucket.org/subinitial/subinitial/wiki/Home

Subinitial Stacks documentation webpage:
http://subinitial.com/stacks/docs

Git repository:
https://bitbucket.org/subinitial/subinitial.git


## Introduction

The Subinitial Stacks Library provides interface code and firmware for Stacks products.
All 2.x and 3.x versions share a common API, documentation, and example code.
Please email support@subinitial.com for comments or questions


[Python API documentation is available at subinitial.com](http://subinitial.com/misc/doc/index.html)


## Requirements
- Python 3.5+ or 2.7+
- **python** and **pip3** or **pip2** on your system PATH. 


Use the commands below in a command prompt to verify your installed versions:

```
python --version
python3 --version
pip3 --version
pip2 --version
```

## Installation
Install all requirements, then open a command prompt and run the **pip3** command below
```
pip3 install --user git+https://bitbucket.org/subinitial/subinitial.git
```
NOTES:

- Use `pip2` instead of `pip3` to install the library into your Python 2 packages
- pip's `--user` flag allows you to install this package without admin privileges,
  If you'd like to install the package system-wide then omit the `--user` flag e.g. `pip3 install git+https://bitbucket.org/`...
- You can use virtualenv to make a localized Python environment for a particular project then
  pip install all required packages as needed with the virtualenv activated. Omit the `--user`
  flag when installing this package inside a virtualenv.
- You can distribute a specific version of the library with your Python code easily 
  by using the command below. The command below creates the **subinitial-stacks** package from the 
  git tag **v1.13.0** in the directory "**.**" which is the current working directory (CWD)
```
pip3 install git+https://bitbucket.org/subinitial/subinitial.git@v1.13.0 --target="."
```

## Verify Installation
For a Stacks Core configured to IP address 192.168.1.49:
```
#!python
import subinitial.stacks as stacks
core = stacks.Core(host="192.168.1.49")
```

For more detailed installation instructions and help getting started refer to the [Getting Started](https://bitbucket.org/subinitial/subinitial/wiki/Getting_Started) page.
