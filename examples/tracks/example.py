# -*- coding: UTF-8 -*-
# Tracks Accessory Example
# © 2012-2018 Subinitial LLC. All Rights Reserved
# Redistribution and use in source and binary forms, with or without modification, are permitted provided that the
# following conditions are met: Redistributions of source code and binary forms must retain the above copyright notice,
# these conditions, and the disclaimer below all in plain text. The name Subinitial may not be used to endorse or
# promote products derived from this software without prior written consent from Subinitial LLC. This software may only
# be redistributed and used with a Subinitial Stacks product.

# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
# warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. Refer to the included LICENSE.txt for more details.

import subinitial.stacks as stacks

core = stacks.Core(host="192.168.1.49")  # Default host IP
tracksacc = stacks.Tracks(core, bus_address=8)  # Default Tracks bus address


""" BASIC USAGE """

core.dutpower5v.engage()  # If you power your accessories off of the 5V dut power be sure to enable it first!
import time
time.sleep(0.5)  # Wait some time for tracks to power up

tracksacc.cmd(0)  # command relay A0 on, all others off
tracksacc.cmd("A1", 4)  # command relay A1 and B0 on, all others off
tracksacc.engage(mask=0xFF000000)  # engage all relays in channels G and H using a bitmask
print("Get commanded state for all tracks relays: {0}".format(tracksacc.get_cmd()))
tracksacc.disengage(mask=0xF)  # disengage all relays in channel A using a bitmask

tracksacc.get_cmd(cmdtype=0)
