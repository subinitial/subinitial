# -*- coding: UTF-8 -*-
# Stacks Core CAN Example
# © 2012-2018 Subinitial LLC. All Rights Reserved
# Redistribution and use in source and binary forms, with or without modification, are permitted provided that the
# following conditions are met: Redistributions of source code and binary forms must retain the above copyright notice,
# these conditions, and the disclaimer below all in plain text. The name Subinitial may not be used to endorse or
# promote products derived from this software without prior written consent from Subinitial LLC. This software may only
# be redistributed and used with a Subinitial Stacks product.

# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
# warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. Refer to the included LICENSE.txt for more details.

import time
import subinitial.stacks as stacks

core = stacks.Core(host="192.168.1.49")  # Default host IP


""" BASIC USAGE """

# Set up default baud timing parameters to give 1MHz baud rate
core.can.set_baudrate(baudrate_hz=1e6)

""" Transmit """
tx_mailbox = core.can.mailbox[0]
tx_mailbox.set_config(tx_mailbox.MODE_TRANSMIT, message_id=1)

# Transmit a 3 byte payload
tx_mailbox.transmit(1, 2, 3)

# Transmit an 8 byte payload using a bytearray
tx_mailbox.transmit(b'abcdefgh')

""" Receive """

rx_mailbox = core.can.mailbox[1]

# Configure the rx_mailbox to receive the first message on the bus with an id==1
rx_mailbox.set_config(rx_mailbox.MODE_RECEIVE, message_id=1)

# Delay some time to wait for a message
time.sleep(1)

rxdata = rx_mailbox.get_received()

print("Received CAN Message[ is_valid:{0}, payload bytes:{1}, overflow occured:{2} ]"
      .format(rxdata.is_valid, rxdata, rxdata.was_overflow))


""" ADVANCED USAGE """

def listen_in_on_any_messages():
    wiretap_mailbox = core.can.mailbox[2]
    wiretap_mailbox.set_config(wiretap_mailbox.MODE_RECEIVE_WITH_OVERWRITE,  # Keep the most recent message
                               message_id=0,
                               acceptance_mask=wiretap_mailbox.ACCEPTANCE_MASK_PROMISCUOUS)  # Accept any message id

    while True:
        rxdata = wiretap_mailbox.get_received()

        if rxdata.is_valid:
            note = "  Some messages were ignored." if rxdata.was_overflow else ""
            print("Sniffed CAN Payload: {0}{1}".format(rxdata, note))

        time.sleep(0.01)  # check around 10 Hz
