# -*- coding: UTF-8 -*-
# Stacks Core Dio Example
# © 2012-2018 Subinitial LLC. All Rights Reserved
# Redistribution and use in source and binary forms, with or without modification, are permitted provided that the
# following conditions are met: Redistributions of source code and binary forms must retain the above copyright notice,
# these conditions, and the disclaimer below all in plain text. The name Subinitial may not be used to endorse or
# promote products derived from this software without prior written consent from Subinitial LLC. This software may only
# be redistributed and used with a Subinitial Stacks product.

# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
# warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. Refer to the included LICENSE.txt for more details.

import subinitial.stacks as stacks

core = stacks.Core(host="192.168.1.49")  # Default host IP


""" BASIC USAGE """

# Sets pins DIO0 and DIO3 to be used as a DIO output (Using a list of pin indices)
core.dio.set_config(0, 3, direction=core.dio.OUTPUT)
core.dio.set(0, 3)  # Sets pins 0 and 3 (output high)
core.dio.clear(0)  # Clears pin 0 (output low)

# Sets DIO3 to be used as a DIO input (This time using a bitmask to specify DIO pins)
core.dio.set_config(mask=0b1000, direction=core.dio.INPUT)
dio3_state = core.dio.get_status()[3]  # Reads the pin's state
print("DIO3's state is: {0}".format(dio3_state))  # Prints the pin's state


""" ADVANCED USAGE """

dio_pinmask = 0b000101011  # Pin mask for DIOs 0, 1, 3, 5
# Sets DIO 0, 1, 3, 5 to inputs with internal pull-down
core.dio.set_config(mask=dio_pinmask, pullupdown=core.dio.PULL_DOWN)
pin_states = core.dio.get_status()  # Gets levels of all DIO pins

core.dio.set_config(0b11, direction=core.dio.OUTPUT)
core.dio.set_config(0b1100, direction=core.dio.INPUT, pullupdown=core.dio.PULL_UP)

print("DIO Input Status: {0:b}".format(core.dio.get_status() & 0b1100))  # reads status of dio pins 2 and 3
core.dio.cmd(0xF)  # turns on dio pins 0 and 1 since they are configured as outputs

core.dio.set_config(mask=0b1100, direction=core.dio.INPUT, pullupdown=core.dio.PULL_DOWN)
