# -*- coding: UTF-8 -*-
# Stacks Core Timer Example
# © 2012-2018 Subinitial LLC. All Rights Reserved
# Redistribution and use in source and binary forms, with or without modification, are permitted provided that the
# following conditions are met: Redistributions of source code and binary forms must retain the above copyright notice,
# these conditions, and the disclaimer below all in plain text. The name Subinitial may not be used to endorse or
# promote products derived from this software without prior written consent from Subinitial LLC. This software may only
# be redistributed and used with a Subinitial Stacks product.

# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
# warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. Refer to the included LICENSE.txt for more details.

import subinitial.stacks as stacks

core = stacks.Core(host="192.168.1.49")  # Default host IP



""" BASIC USAGE """

# Enable the event counter pin (TCLK0)
core.timer.enable_counter()

# Set the required number of events and the timeout for the frequency measurement to improve accuracy
print("Measured Frequency: {0}".format(core.timer.measure_frequency(min_events=1000000, timeout=2.0)))


""" ADVANCED USAGE """

# Restart the timer clock and the event counter
core.timer.restart()

# DELAY SOME AMOUNT OF TIME
import time
time.sleep(0.5)

clock, count = core.timer.get_clock_and_counter_values()
print("Clock Ticks since restart: {0}, Events Counted: {1}".format(clock, count))
