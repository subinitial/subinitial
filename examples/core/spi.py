# -*- coding: UTF-8 -*-
# Stacks Core SPI Example
# © 2012-2018 Subinitial LLC. All Rights Reserved
# Redistribution and use in source and binary forms, with or without modification, are permitted provided that the
# following conditions are met: Redistributions of source code and binary forms must retain the above copyright notice,
# these conditions, and the disclaimer below all in plain text. The name Subinitial may not be used to endorse or
# promote products derived from this software without prior written consent from Subinitial LLC. This software may only
# be redistributed and used with a Subinitial Stacks product.

# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
# warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. Refer to the included LICENSE.txt for more details.

import subinitial.stacks as stacks

core = stacks.Core(host="192.168.1.49")  # Default host IP


""" USAGE """

# Example communication with a device on SPI Chip Select 1
#   NOTE: You can manually control a DIO as a chip select instead of
#         using the channel's hardware SPI Chip Select if desired

# Enable the spi channel controller and proper D-sub pin MUX'ing
core.spi1.enable()

# Setup the channel configuration: SCLK baudrate, and data polarity edges, etc.
core.spi1.set_config(baudrate=1.0e6, cpolarity=0, ncphase=1)

# Transmit a single byte
core.spi1.transmit(0x12)

# Transmit several bytes
core.spi1.transmit(0x12, 0x34, 0x56)

# Transmit a byte array
core.spi1.transmit(b'123456')

# Transmit an ascii string
core.spi1.transmit("xyz123")

# Read all cached bytes from the spi channel's receive FIFO
# (bytes from most recent transmit() are stored in receive FIFO, and cleared from FIFO on receive())
rxdata = core.spi1.receive()
print("Received {0} bytes, Byte0: {1}, All Bytes: {2}".format(len(rxdata), hex(rxdata[0]), rxdata))

# Transmit two bytes of data and read the two bytes received during this transmission
txrxdata = core.spi1.transmit_receive(0x12, 0x34)
print("Received {0} bytes, Byte0: {1}, All Bytes: {2}".format(len(txrxdata), hex(txrxdata[0]), txrxdata))
