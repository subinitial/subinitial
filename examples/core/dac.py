# -*- coding: UTF-8 -*-
# Stacks Core Dac Example
# © 2012-2018 Subinitial LLC. All Rights Reserved
# Redistribution and use in source and binary forms, with or without modification, are permitted provided that the
# following conditions are met: Redistributions of source code and binary forms must retain the above copyright notice,
# these conditions, and the disclaimer below all in plain text. The name Subinitial may not be used to endorse or
# promote products derived from this software without prior written consent from Subinitial LLC. This software may only
# be redistributed and used with a Subinitial Stacks product.

# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
# warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. Refer to the included LICENSE.txt for more details.

import subinitial.stacks as stacks

core = stacks.Core(host="192.168.1.49")  # Default host IP


""" BASIC USAGE """

core.dac.set_dc(3.1, -0.35)  # Set the Core's DAC0 to 3.1V DC and DAC1 to -0.35V
print("The DAC is set to {0}".format(core.dac.get_dc()))


""" ADVANCED USAGE """

def program_arbitrary_waveform():
    left_waveform = [-2, 2] * 5 # square waveform
    right_waveform = [-3, -1.5, 0, 1.5, 3, 1.5, 0, -1.5] * 1 # stepping waveform
    core.dac.update_waveform(left_values=left_waveform, right_values=right_waveform, samplerate_hz=1e3, run_time_s=2)

    core.dac.set_control(mode=core.dac.MODE_WAVETRIGGERED)

# program_arbitrary_waveform()
