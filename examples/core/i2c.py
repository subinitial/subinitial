# -*- coding: UTF-8 -*-
# Stacks Core I2C Example
# © 2012-2018 Subinitial LLC. All Rights Reserved
# Redistribution and use in source and binary forms, with or without modification, are permitted provided that the
# following conditions are met: Redistributions of source code and binary forms must retain the above copyright notice,
# these conditions, and the disclaimer below all in plain text. The name Subinitial may not be used to endorse or
# promote products derived from this software without prior written consent from Subinitial LLC. This software may only
# be redistributed and used with a Subinitial Stacks product.

# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
# warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. Refer to the included LICENSE.txt for more details.

import subinitial.stacks as stacks

core = stacks.Core(host="192.168.1.49")  # Default host IP


""" USAGE """

# Enable the I2C peripheral and pullups
core.i2c.enable()

# Write one byte: 0x12 to I2C device 0x50 @ 400kHz baud
core.i2c.write(0x50, 0x12)

# Write multiple bytes to I2C device 0x50
core.i2c.write(0x50, 0x12, 0x34, 0x56)

# Alternate format to write multiple bytes to I2C device 0x50
list_of_bytes = [1, 2, 3, 4]
core.i2c.write(0x50, *list_of_bytes)

# Write two bytes 0x1234 to I2C device 0x50 @ 100kHz baud
core.i2c.write(0x50, b'1234', speed=core.i2c.SPEED_100KHZ)

# Read two bytes, from I2C device 0x50, using an optional SMBus style command: 0x20 @ 100kHz baud
rxdata = core.i2c.write(0x50, 2, command=0x20, command_size=1, speed=core.i2c.SPEED_400KHZ)
print(rxdata)
print("Individual Bytes: {0}, {1}".format(rxdata[0], rxdata[1]))


def smbus_usage():
    """Example usage if the device implements an SM Bus interface (virtual registers/address space)"""
    core.i2c.write(0x50, b'1234', command=0x20, command_size=1)
    rxsmbus = core.i2c.read(0x50, 2, command=0x20, command_size=1)
    print("Rxdata should be b'1234' if the device has a RW register at command location 0x20: {0}".format(rxsmbus))


def slavedevice_helper():
    """Create a virtual device to conveniently communicate with an I2C slave"""
    # Start by defining a core.i2c.Device that captures the bus configuration
    mychip = core.i2c.Device(0x50, 1, speed=core.i2c.SPEED_100KHZ)

    # Then use this mychip variable to read/write i2c data
    mychip.write(b'1234', command=0x20)
    rxsmbus = mychip.read(2, command=0x20)
