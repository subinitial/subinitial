# -*- coding: UTF-8 -*-
# Stacks Core UART Example
# © 2012-2018 Subinitial LLC. All Rights Reserved
# Redistribution and use in source and binary forms, with or without modification, are permitted provided that the
# following conditions are met: Redistributions of source code and binary forms must retain the above copyright notice,
# these conditions, and the disclaimer below all in plain text. The name Subinitial may not be used to endorse or
# promote products derived from this software without prior written consent from Subinitial LLC. This software may only
# be redistributed and used with a Subinitial Stacks product.

# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
# warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. Refer to the included LICENSE.txt for more details.

import subinitial.stacks as stacks
import time

core = stacks.Core(host="192.168.1.49")  # Default host IP


# set up default baud timing parameters to give 115200Hz baud rate
uart = core.uart
uart.set_config(115200, uart.PARITY_NONE)

# transmit string as bytes
txdata = input("UART Transmit> ")
uart.transmit(txdata.encode())

# wait a while for some data to be receivd
time.sleep(1)

# receive bytes and print as string
rxdata = uart.receive()
print("UART Received>", rxdata.decode("utf-8"))

# disable the user controlled uart so that it once again acts as a Stacks Core debug console
uart.disable()
