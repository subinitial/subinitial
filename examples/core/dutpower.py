# -*- coding: UTF-8 -*-
# Stacks Core DutPower Example
# © 2012-2018 Subinitial LLC. All Rights Reserved
# Redistribution and use in source and binary forms, with or without modification, are permitted provided that the
# following conditions are met: Redistributions of source code and binary forms must retain the above copyright notice,
# these conditions, and the disclaimer below all in plain text. The name Subinitial may not be used to endorse or
# promote products derived from this software without prior written consent from Subinitial LLC. This software may only
# be redistributed and used with a Subinitial Stacks product.

# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
# warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. Refer to the included LICENSE.txt for more details.

import subinitial.stacks as stacks

core = stacks.Core(host="192.168.1.49")  # Default host IP


""" BASIC USAGE """

core.dutpower5v.engage()  # Turns on 5V DUT power
is_5v_engaged, is_5v_faulted = core.dutpower5v.get_status()
print("Dut Power 5V On: {0}, Faulted: {1}".format(is_5v_engaged, is_5v_faulted))

core.dutpower12v.engage()  # Turns on 12V DUT power

core.dutpower5v.disengage()  # Turns off 5V DUT power
print("Dut Power 5V On: {0}, Faulted: {1}".format(*core.dutpower5v.get_status()))



""" ADVANCED USAGE """

# Setting DUT power current limits and fault behavior
core.dutpower12v.set_config(core.dutpower12v.ILIMIT12V_2A5)
core.dutpower5v.set_config(core.dutpower5v.ILIMIT_0A75)
