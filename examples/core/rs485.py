# -*- coding: UTF-8 -*-
# Stacks Core RS485 UART Example
# © 2012-2018 Subinitial LLC. All Rights Reserved
# Redistribution and use in source and binary forms, with or without modification, are permitted provided that the
# following conditions are met: Redistributions of source code and binary forms must retain the above copyright notice,
# these conditions, and the disclaimer below all in plain text. The name Subinitial may not be used to endorse or
# promote products derived from this software without prior written consent from Subinitial LLC. This software may only
# be redistributed and used with a Subinitial Stacks product.

# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
# warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. Refer to the included LICENSE.txt for more details.

import subinitial.stacks as stacks
import time

core = stacks.Core(host="192.168.1.49")  # Default host IP


# set up default baud timing parameters to give 1MHz baud rate
uart485 = core.rs485
uart485.set_config(1000000, uart485.PARITY_NONE)

# transmit string as bytes
txdata = input("RS485 Transmit> ")
uart485.transmit(txdata.encode())

# wait a while for some data to be received
time.sleep(1)

# receive bytes and print as string
rxdata = uart485.receive()
print("RS485 Received>", rxdata.decode("utf-8"))

# disable the RS485 uart so it can once again be used when communicating with RS485 based Tracks products
uart485.disable()
