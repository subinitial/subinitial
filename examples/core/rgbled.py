# -*- coding: UTF-8 -*-
# Stacks Core RgbLed Example
# © 2012-2018 Subinitial LLC. All Rights Reserved
# Redistribution and use in source and binary forms, with or without modification, are permitted provided that the
# following conditions are met: Redistributions of source code and binary forms must retain the above copyright notice,
# these conditions, and the disclaimer below all in plain text. The name Subinitial may not be used to endorse or
# promote products derived from this software without prior written consent from Subinitial LLC. This software may only
# be redistributed and used with a Subinitial Stacks product.

# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
# warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. Refer to the included LICENSE.txt for more details.

import subinitial.stacks as stacks

core = stacks.Core(host="192.168.1.49")  # Default host IP


""" BASIC USAGE """

core.rgbled.set(core.rgbled.COLOR_GREEN)  # Set the RGBLED to green
core.rgbled.set(core.rgbled.COLOR_BLUE)  # Set the RGBLED to blue


""" ADVANCED USAGE """

core.rgbled.set(0xFF0000FF)  # Set the RGBLED to very bright red
core.rgbled.set(0x00AA0010)  # Set the RGBLED to dim green
print("0x{0:X}".format(core.rgbled.get()))
