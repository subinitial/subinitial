# -*- coding: UTF-8 -*-
# Stacks Core Pwm Example
# © 2012-2018 Subinitial LLC. All Rights Reserved
# Redistribution and use in source and binary forms, with or without modification, are permitted provided that the
# following conditions are met: Redistributions of source code and binary forms must retain the above copyright notice,
# these conditions, and the disclaimer below all in plain text. The name Subinitial may not be used to endorse or
# promote products derived from this software without prior written consent from Subinitial LLC. This software may only
# be redistributed and used with a Subinitial Stacks product.

# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
# warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. Refer to the included LICENSE.txt for more details.

import subinitial.stacks as stacks

core = stacks.Core(host="192.168.1.49")  # Default host IP


""" BASIC USAGE """

core.pwm.set_frequency(0, 100e3)  # Set frequency to 100kHz for the entire CH 0
core.pwm.set_dutycycle(0, 0.453)  # Set duty cycle to 45.3 percent
core.pwm.enable(0)  # Enables both PWM Channel 0 pins High and Low (PWMH0 & PWML0)
core.pwm.disable(core.pwm.CH_PWMH0)  # Disables just PWM Channel 0 pin HIGH (PWMH0)

core.pwm.set_frequency(2, 32768)    # Set PWM channel 2 frequency to 32.768kHz
core.pwm.set_dutycycle(2, 0.50)     # Set PWM channel 2 duty cycle to 50 percent
core.pwm.enable(2)                  # Enable PWM Channel 2 pins (PWMH2)

print("CH0 Duty: {0}, Frequency: {1}".format(core.pwm.get_dutycycle(core.pwm.CH_PWMH0),
                                             core.pwm.get_frequency(core.pwm.CH_PWMH0)))

# Disabling PWMs
core.pwm.disable(core.pwm.CH_PWMH0, core.pwm.CH_PWML0, core.pwm.CH_PWMH2)
