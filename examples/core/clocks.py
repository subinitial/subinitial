# -*- coding: UTF-8 -*-
# Stacks Core Clocks Example
# © 2012-2018 Subinitial LLC. All Rights Reserved
# Redistribution and use in source and binary forms, with or without modification, are permitted provided that the
# following conditions are met: Redistributions of source code and binary forms must retain the above copyright notice,
# these conditions, and the disclaimer below all in plain text. The name Subinitial may not be used to endorse or
# promote products derived from this software without prior written consent from Subinitial LLC. This software may only
# be redistributed and used with a Subinitial Stacks product.

# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
# warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. Refer to the included LICENSE.txt for more details.

import subinitial.stacks as stacks

core = stacks.Core(host="192.168.1.49")  # Default host IP


""" BASIC USAGE """

core.clock1.set_config(core.clock1.HZ_1M5)  # Configure clock1 to output 1.5MHz clock waveform
core.clock1.enable()  # Enable clock pin channel 1

core.clock2.set_config(core.clock2.HZ_32K768)  # Configure clock2 to output 32.768kHz clock waveform
core.clock2.enable()

freq_setpoint = core.clock1.get_frequency()  # Reads the set frequency in Hz
print("Clock frequency channel 1 set to {0} Hz".format(freq_setpoint))

core.clock1.disable()  # Disable clock pin channel 1
core.clock2.disable()

