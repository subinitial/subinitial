# -*- coding: UTF-8 -*-
# Stacks Core Clocks Example
# © 2012-2018 Subinitial LLC. All Rights Reserved
# Redistribution and use in source and binary forms, with or without modification, are permitted provided that the
# following conditions are met: Redistributions of source code and binary forms must retain the above copyright notice,
# these conditions, and the disclaimer below all in plain text. The name Subinitial may not be used to endorse or
# promote products derived from this software without prior written consent from Subinitial LLC. This software may only
# be redistributed and used with a Subinitial Stacks product.

# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
# warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. Refer to the included LICENSE.txt for more details.

import subinitial.stacks as stacks

core = stacks.Core(host="192.168.1.49")  # Default host IP


""" BASIC USAGE """

core.leds.set(0, 1)  # Turns on user LED0 and LED1 using a list of led indices
core.leds.toggle(1)  # Toggles user LED0
core.leds.clear(0)  # Turns off user LED1


""" ADVANCED USAGE """

core.leds.toggle(mask=0b01)  # Toggle rear user LED0 using a bitmask
core.leds.cmd(mask=0b01)  # command rear user LED0 on, and 1 off

print(core.leds.get_cmd())
