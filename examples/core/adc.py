# -*- coding: UTF-8 -*-
# Stacks Core Adc Example
# © 2012-2018 Subinitial LLC. All Rights Reserved
# Redistribution and use in source and binary forms, with or without modification, are permitted provided that the
# following conditions are met: Redistributions of source code and binary forms must retain the above copyright notice,
# these conditions, and the disclaimer below all in plain text. The name Subinitial may not be used to endorse or
# promote products derived from this software without prior written consent from Subinitial LLC. This software may only
# be redistributed and used with a Subinitial Stacks product.

# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
# warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. Refer to the included LICENSE.txt for more details.

import time
import subinitial.stacks as stacks

core = stacks.Core(host="192.168.1.49")  # Default host IP


""" BASIC USAGE """

core.adc.enable(0)  # Enable ADC channel 0
time.sleep(0.05)  # wait a few milliseconds for a sample to occur
voltage_at_pin = core.adc.get(0)  # Get the most recent adc pin voltage reading
print("ADC channel 0 was {0}V at its input pin".format(voltage_at_pin))


""" ADVANCED USAGE """

# Max results
maxresults = core.adc.get_all_max()  # get all max results since last max-reset
print("Channel 0 Max Result: {0}".format(maxresults[0]))
core.adc.reset_all_max()  # reset all max results
