# -*- coding: UTF-8 -*-
# Analog Deck RgbLed Example
# © 2012-2015 Subinitial LLC. All Rights Reserved
# Redistribution and use in source and binary forms, with or without modification, are permitted provided that the
# following conditions are met: Redistributions of source code and binary forms must retain the above copyright notice,
# these conditions, and the disclaimer below all in plain text. The name Subinitial may not be used to endorse or
# promote products derived from this software without prior written consent from Subinitial LLC. This software may only
# be redistributed and used with a Subinitial Stacks product.

# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
# warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. Refer to the included LICENSE.txt for more details.

import subinitial.stacks as stacks

core = stacks.Core(host="192.168.1.49")  # Default host IP
analogdeck = stacks.AnalogDeck(core, bus_address=2)  # Default Analog Deck bus address


""" BASIC USAGE """

analogdeck.dio.set_config(dio0_3_innotout=True, dio4_7_innotout=False)  # Set 0-3 as inputs, 4-7 as outputs

print("Pin input status for DIOs 0-3: 0x{0:X}".format(analogdeck.dio.get_status() & 0xF))

analogdeck.dio.set(0, 1, 2, 3)  # Command Dio pins 0-3 to output high
analogdeck.dio.cmd(mask=0xF0)  # Command Dio pins 4-7 to output high using a bitmask


analogdeck.dio.clear(7)  # Clear pin 7's output command

print("Get commanded state for all Dio pins: 0x{0:X}".format(analogdeck.dio.get_cmd()))


""" ADVANCED USAGE """

# Configure DIO to 5V logic
# analogdeck.dio.set_config(dio0_3_innotout=True, dio4_7_innotout=False, logic5v_not3v3=True)
