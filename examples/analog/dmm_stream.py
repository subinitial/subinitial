# -*- coding: UTF-8 -*-
# Analog Deck Dmm Stream Example
# © 2012-2018 Subinitial LLC. All Rights Reserved
# Redistribution and use in source and binary forms, with or without modification, are permitted provided that the
# following conditions are met: Redistributions of source code and binary forms must retain the above copyright notice,
# these conditions, and the disclaimer below all in plain text. The name Subinitial may not be used to endorse or
# promote products derived from this software without prior written consent from Subinitial LLC. This software may only
# be redistributed and used with a Subinitial Stacks product.

# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
# warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. Refer to the included LICENSE.txt for more details.


import subinitial.stacks as stacks


def streamdmm():
    core = stacks.Core(host="192.168.1.49")  # Default host IP
    analogdeck = stacks.AnalogDeck(core, bus_address=2)  # Default Analog Deck bus address

    dmm = analogdeck.dmm
    while True:
        # Stream arm configures the dmm for streaming on one channel at a pre-specified samplerate and range (Auto-Range not allowed)
        # Note: auto-offset cal will be suspended for the duration of the stream.
        dmm.stream_arm(0, samplerate=dmm.SAMPLERATE_3600HZ, range_=dmm.RANGE_MED_25V)

        # After arming, we can trigger the stream and capture samples as long as needed
        # The analogdeck will begin buffering samples after this call.
        # Triggering the stream explicitly is optional. If the dmm is not already triggered,
        # then the dmm.stream() call will trigger it for you.
        dmm.stream_trigger()
        count = 1
        for sample in dmm.stream():
            # dmm.stream() yields samples as they become available over the network.
            #   The samples are measured at the samplerate specified in dmm.stream_arm with a typical sample-period accuracy of 0.3%
            #   The dmm can store up to 4500 samples before its internal buffer becomes full.
            #   dmm.stream() must read samples often enough to prevent the buffer from becoming full if a continuous stream is desired.
            #
            # dmm.stream() takes two optional parameters:
            #   pollingdelay=200, units are in sample-periods
            #   The polling delay represents the minimum time between reads for available buffered samples.
            #   The minimum value of pollingdelay is limitted by your network latency.
            #   Typical roundtrip request/response on a small LAN will take between 1 and 10ms.
            #
            #   timeout=1000,  units are in sample-periods
            #   The timeout represents the maximum time to wait for new samples to be available. If this timeout is exceeded
            #   a SubinitialTimeoutError is raised. The Dmm will stop sampling new stream data if the stream buffer fills up
            #   (due to samples accumulating without being read) or if the user calls dmm.stream_stop().
            print("sample: {}, measurement: {}V".format(count, sample))
            count += 1
            if count > 10000:  # arbitrarily stop streaming after 10k samples
                break

        # Stop the stream process, call this before performing additional dmm measurements
        # Performing a dmm.measure or a dmm.freerun call will implicitly stop the stream process as well.
        dmm.stream_stop()
        # if auto-offsetcal was enabled prior to the stream, then it will resume after the stream is stopped

streamdmm()