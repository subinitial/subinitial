# -*- coding: UTF-8 -*-
# Analog Deck Dmm Example
# © 2012-2018 Subinitial LLC. All Rights Reserved
# Redistribution and use in source and binary forms, with or without modification, are permitted provided that the
# following conditions are met: Redistributions of source code and binary forms must retain the above copyright notice,
# these conditions, and the disclaimer below all in plain text. The name Subinitial may not be used to endorse or
# promote products derived from this software without prior written consent from Subinitial LLC. This software may only
# be redistributed and used with a Subinitial Stacks product.

# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
# warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. Refer to the included LICENSE.txt for more details.

import subinitial.stacks as stacks

core = stacks.Core(host="192.168.1.49")  # Default host IP
analogdeck = stacks.AnalogDeck(core, bus_address=2)  # Default Analog Deck bus address


""" BASIC USAGE """

# The dmm starts from powerup with channels 0-6 being measured continuously.
results = analogdeck.dmm.get_results()  # You can get all the most recent channel data using this method
print("CH1 Volts: {0}".format(results[1]))
print("CH6 Amps: {0}".format(results[6]))

# To explicitly trigger a single measurement of a set of channels 'measure' method:
results = analogdeck.dmm.measure(0, 2, 3)  # Measure channels 0, 2, and 3 and return the results
print("CH0 Volts: {0}".format(results[0]))
print("CH2 Volts: {0}".format(results[2]))
print("CH3 Volts: {0}".format(results[3]))

# After a triggered measure the DMM is stopped and no longer freerunning.
# analogdeck.dmm.get_results() will return the last triggered value.
# To resume the DMM freerunning use the dmm.freerun method
analogdeck.dmm.freerun(mask=0x7F)  # freerun on channels 0-6 using a bitmask to specify


""" ADVANCED USAGE AND EXAMPLES """

# You can store a peripheral object into a local variable to make dealing with that variable
# more straightforward as seen below.
dmm = analogdeck.dmm


def controlling_dmm_operation():
    # By default, the Dmm is configured in free-run mode with channels 0-6 enabled and auto-zero cal on.
    # Alternatively, you can trigger a measurement to several channels (They are measured in succession)
    dmm.trigger(0, 1)  # This triggers the start of a measurement result on channels 0 and 1

    # The dmm's cycle counter will increments after each full cycle of enabled channels is measured. In freerun mode
    # this count is always increasing, in trigger mode it only increments by one per trigger.
    print("Present Dmm Cycle-Count: {0}".format(dmm.get_cyclecounter()))

    # After a trigger is complete and the cycle counter has incremented, the dmm will be stopped and results
    # will be available with dmm.get_results(). Subsequent calls to dmm.trigger or dmm.measure will update
    # the result values.

    # You can reconfigure the dmm back to freerun on different channels as shown below
    dmm.freerun(3, 4, 5)  # This sets the dmm to free-run mode with only channels 3, 4, & 5 enabled

    # NOTE: When passing your selected channels to trigger and freerun methods you can use a bitmask instead of a list
    # by using the 'mask' parameter.
    dmm.freerun(mask=0b111000)  # this for example is equivalent to the call to dmm.freerun(3, 4, 5)


def retrieve_most_recent_channel_results():
    # dmm.get_results retrieves all of the most recent measurement results for each channel on the DMM.
    # In free-run mode these results are constantly being updated as the DMM performs a measurement on
    # each channel in sequence. In trigger mode, these results reflect the last measurement obtained
    # from the last time they were triggered.
    channel_results = dmm.get_results()

    # dmm.get_next_results waits for at least one entire cycle count before returning the results value.
    # This method blocks while waiting and can timeout if the analogdeck.dmm is not running in freerun mode.
    # Note: Use this method when the dmm is configured in freerun mode, you've changed the input signal, and you
    #       want the next dmm result that will reflect the new input signal data.
    next_channel_results = dmm.get_next_results()

    # Print voltage channel results
    for i in range(0, 6):
        print("Channel {0}: {1}V".format(i, channel_results[i]))

    # Print current channel result
    print("Channel {0}: {1}A".format(6, channel_results[6]))

    # Print High Voltage channel result
    print("Channel {0}: {1}V".format(7, channel_results[7]))

    # Print cycle counter, this number increments after all enabled channels have completed one measurement result
    print("Cycle Count: {0}".format(channel_results.cyclecount))

    # Print out-of-range error flags
    # Note: Any bit in this value that is set indicates the last result obtained was out-of-range.
    # Note: Bit 0 represents channel 0 (1 << 0), bit 7 represents channel 7 (1 << 7)
    print("Out of range channels bitmask: {0}".format(channel_results.out_of_range))

    print("Channel 2 result is out of range?: {0}".format(channel_results.out_of_range[2]))


def configuring_dmm_channel_ranges():
    # By default each channel is set to minimum sample rate, auto-range, no absolute value results, and no averaging
    dmm.set_channelranges(range0=dmm.RANGE_HIGH_250V,  # Specify range selection for channel 0
                          range2=dmm.RANGE_MED_25V,    # Specify range selection for channel 2
                          range5=dmm.RANGE_LOW_2V5)    # Specify range selection for channel 5
                                                       # The remaining channels will be configured as RANGE_AUTO

    dmm.set_ranges(1, 3, 4, range_=dmm.RANGE_AUTO)  # Set several channels to the same range
    # You can see what each range is set to with the dmm.get_ranges method
    channel0_range = dmm.get_ranges()[0]
    print("Channel 0 range: {0}".format(["Auto", "Low", "Med", "High"][channel0_range]))

    # If you want to see the present range setting for all channels including auto-range channels pass False to the get_ranges method
    print("A list of present range settings:", dmm.get_ranges(autorange_not_actualrange=False))

