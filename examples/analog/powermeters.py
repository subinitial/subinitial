# -*- coding: UTF-8 -*-
# Analog Deck Power Meter Example
# © 2012-2018 Subinitial LLC. All Rights Reserved
# Redistribution and use in source and binary forms, with or without modification, are permitted provided that the
# following conditions are met: Redistributions of source code and binary forms must retain the above copyright notice,
# these conditions, and the disclaimer below all in plain text. The name Subinitial may not be used to endorse or
# promote products derived from this software without prior written consent from Subinitial LLC. This software may only
# be redistributed and used with a Subinitial Stacks product.

# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
# warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. Refer to the included LICENSE.txt for more details.

import subinitial.stacks as stacks

core = stacks.Core(host="192.168.1.49")  # Default host IP
analogdeck = stacks.AnalogDeck(core, bus_address=2)  # Default Analog Deck bus address


""" BASIC USAGE """

print("Show the measured telemetry for PowerMeter 0:")
print("\tVoltage (V): {0}".format(analogdeck.powermeter0.get_voltage()))
print("\tCurrent (A): {0}".format(analogdeck.powermeter0.get_current()))

print("Show the measured telemetry for PowerMeter 1:")
print("\tVoltage (V): {0}".format(analogdeck.powermeter1.get_voltage()))
print("\tCurrent (A): {0}".format(analogdeck.powermeter1.get_current()))
