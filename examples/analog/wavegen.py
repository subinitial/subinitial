# -*- coding: UTF-8 -*-
# Analog Deck Waveform Generator Example
# © 2012-2018 Subinitial LLC. All Rights Reserved
# Redistribution and use in source and binary forms, with or without modification, are permitted provided that the
# following conditions are met: Redistributions of source code and binary forms must retain the above copyright notice,
# these conditions, and the disclaimer below all in plain text. The name Subinitial may not be used to endorse or
# promote products derived from this software without prior written consent from Subinitial LLC. This software may only
# be redistributed and used with a Subinitial Stacks product.

# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
# warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. Refer to the included LICENSE.txt for more details.

import subinitial.stacks as stacks

core = stacks.Core(host="192.168.1.49")  # Default host IP
analogdeck = stacks.AnalogDeck(core, bus_address=2)  # Default Analog Deck bus address


""" BASIC USAGE """

analogdeck.wavegen.set_dc(3.3)  # Set the waveform generator to 3.3V DC
print("This should show 3.3V: {0}".format(analogdeck.wavegen.get_dc()))


""" ADVANCED USAGE AND EXAMPLES """

def arbitrary_waveform():
    # Set the waveform to an arbitrary list of voltage samples at a specified sample-rate
    analogdeck.wavegen.update_waveform(samplerate_hz=300e3,
                                       samples=[-5, -4, -3, -2, -1, 0, 1, 2, 3, 4, 5] * 30)

    # Turn on waveform mode
    analogdeck.wavegen.set_control(analogdeck.wavegen.MODE_WAVEFREERUN)

    # Set the wavegen back to DC only mode
    analogdeck.wavegen.set_control(analogdeck.wavegen.MODE_DC)

# arbitrary_waveform()
