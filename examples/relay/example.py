# -*- coding: UTF-8 -*-
# Relay Deck Example
# © 2012-2018 Subinitial LLC. All Rights Reserved
# Redistribution and use in source and binary forms, with or without modification, are permitted provided that the
# following conditions are met: Redistributions of source code and binary forms must retain the above copyright notice,
# these conditions, and the disclaimer below all in plain text. The name Subinitial may not be used to endorse or
# promote products derived from this software without prior written consent from Subinitial LLC. This software may only
# be redistributed and used with a Subinitial Stacks product.

# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
# warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. Refer to the included LICENSE.txt for more details.

import subinitial.stacks as stacks

core = stacks.Core(host="192.168.1.49")  # Default host IP
relaydeck = stacks.RelayDeck(core, bus_address=4)  # Default Relay Deck bus address


""" BASIC USAGE """

relaydeck.relay0.engage()  # engage SPDT relay 0's coil

# Get the current reading, fuse status, and max recorded current from relay 0
i0 = relaydeck.relay0.get_rms_current()
imax0 = relaydeck.relay0.get_max_current()
fusefault0 = relaydeck.relay0.get_fusefault()
print("Relay 0 Current: {0} ({1} max), Fuse Fault: {2}".format(i0, imax0, fusefault0))

# reset the max current reading
relaydeck.relay0.reset_max_current()

# disengage solid state relay 4
relaydeck.relay4.disengage()
# get the on/off status of the relay
print("Relay 4 state: {0}".format(relaydeck.relay4.get_status()))

# set the rgb led to a dim Blue
relaydeck.rgbled.set(0x0000FF11)