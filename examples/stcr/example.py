# -*- coding: UTF-8 -*-
# Subinitial Thermocouple Reader Example
# © 2012-2018 Subinitial LLC. All Rights Reserved
# Redistribution and use in source and binary forms, with or without modification, are permitted provided that the
# following conditions are met: Redistributions of source code and binary forms must retain the above copyright notice,
# these conditions, and the disclaimer below all in plain text. The name Subinitial may not be used to endorse or
# promote products derived from this software without prior written consent from Subinitial LLC. This software may only
# be redistributed and used with a Subinitial Stacks product.

# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
# warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. Refer to the included LICENSE.txt for more details.

import subinitial.stacks as stacks

stcr = stacks.Stcr("192.168.1.50" or "stcr000440" or "stcr000440.local")  # connect via ip, wins, or mdns


""" BASIC USAGE """

res = stcr.ch3.measure()
print("Channel 3 Temperature: {0}C".format(res))

multiple = stcr.measure(1, 4)
print("Channel 1 Temperature: {0}C, Channel 4 Temperature: {1}C".format(multiple[1], multiple[4]))


""" ADVANCED USAGE """

# Acquiring temperature data faster by:
#   lowering the measurement precision (faster sample rate)
#   and disabling open detection (faster sample rate)
fastresult = stcr.ch0.measure(precision=stcr.PRECISION_16BIT, opendetect=False)
print("Fast Result {0}".format(fastresult))

# Checking the timestamp and error (if an error occured)
print("\tTimeStamp: {0}s, Error: {1}".format(fastresult.timestamp, fastresult.error))
