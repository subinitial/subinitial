# -*- coding: UTF-8 -*-
# Tracks Accessory interface
# © 2012-2018 Subinitial LLC. All Rights Reserved
# Redistribution and use in source and binary forms, with or without modification, are permitted provided that the
# following conditions are met: Redistributions of source code and binary forms must retain the above copyright notice,
# these conditions, and the disclaimer below all in plain text. The name Subinitial may not be used to endorse or
# promote products derived from this software without prior written consent from Subinitial LLC. This software may only
# be redistributed and used with a Subinitial Stacks product.

# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
# warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. Refer to the included LICENSE.txt for more details.

from subinitial.stacks import VERSION_STACKS
from subinitial.py3.stacks import *
from subinitial.stacks.utils import digest_indices, mask_to_bits, Bitmask

VERSION_MAJOR = 1



_channel_tostr = ('A0', 'A1', 'A2', 'A3', 'B0', 'B1', 'B2', 'B3', 'C0', 'C1', 'C2', 'C3', 'D0', 'D1', 'D2', 'D3',
                  'E0', 'E1', 'E2', 'E3', 'F0', 'F1', 'F2', 'F3', 'G0', 'G1', 'G2', 'G3', 'H0', 'H1', 'H2', 'H3')

_channelweight = {'A': 0, 'B': 4, 'C': 8, 'D': 12, 'E': 16, 'F': 20, 'G': 24, 'H': 28}


# def _relays_tostr(relays):
#     return stacks.bits_to_csv("Relays: ", relays, "Relays: none", _channel_tostr)


def _idx_to_mask(idx):
    if type(idx) == str:  # Decode string types
        if len(idx) != 2:
            return 0
        id_ = _channelweight.get(idx[0].upper(), None)
        if id_ is None:
            return 0
        id_ += ord(idx[1]) - ord('0')
        idx = id_ if id_ >= 0 else 0
    if idx < 32:
        return 1 << idx
    return 0


class Tracks(Device):
    ASSEMBLY = "SA13755"
    FIRMWARE_MIN = "v1.9.0"

    def __init__(self, core_deck, bus_address, verify_connection=True):
        """Construct a Tracks Accessory instance.

        .. code-block:: python

            # Python Example
            tracks = subinitial.stacks.Tracks(core, bus_address=8)

        :param core_deck: Stacks Core instance that is controlling this Device
        :type core_deck: subinitial.py3.core1.Core
        :param bus_address: bus address from 2-31
        :type bus_address: int
        """
        Device.__init__(self, core_deck, bus_address, verify_connection=verify_connection)
        self.regs = RegisterSet(self)

    def __enter__(self):
        return self.regs, self.r, self.control, self.request.queue()

    def __exit__(self, type, value, traceback):
        self.request.submit()

    def _get_devicetype(self):
        return self.TYPE_ACCESSORY

    def cmd(self, *channels, mask=0):
        """Command specified channels on (and disengage omitted channels).

        :param channels: arbitrary list of indices. e.g. 0, 1, 12, 13, "F3", "A2"
        :type channels: tuple[int|str]
        :param mask: optional bitmask of indices. e.g. 0b11
            See `Specifying Indices or Masks <http://subinitial.com/stacks/docs/indices>`_
        :type mask: int
        """
        bits = digest_indices(channels, mask, idx2mask=_idx_to_mask)
        self.regs.RELAYS_CMD.write(bits)

    def engage(self, *channels, mask=0):
        """Engage relay channels.

        :param channels: arbitrary list of indices. e.g. 0, 1, 12, 13, "F3", "A2"
        :type channels: tuple[int|str]
        :param mask: optional bitmask of indices. e.g. 0b11
            See `Specifying Indices or Masks <http://subinitial.com/stacks/docs/indices>`_
        :type mask: int
        """
        bits = digest_indices(channels, mask, idx2mask=_idx_to_mask)
        self.regs.RELAYS_ENGAGE.write(bits)

    def disengage(self, *channels, mask=0):
        """Disengage relay channels.

        :param channels: arbitrary list of indices. e.g. 0, 1, 12, 13, "F3", "A2"
        :type channels: tuple[int|str]
        :param mask: optional bitmask of indices. e.g. 0b11
            See `Specifying Indices or Masks <http://subinitial.com/stacks/docs/indices>`_
        :type mask: int
        """
        bits = digest_indices(channels, mask, idx2mask=_idx_to_mask)
        self.regs.RELAYS_DISENGAGE.write(bits)

    def toggle(self, *channels, mask=0):
        """toggle relay channels

        :param channels: arbitrary list of indices. e.g. 0, 1, 12, 13, "F3", "A2"
        :type channels: tuple[int|str]
        :param mask: optional bitmask of indices. e.g. 0b11
            See `Specifying Indices or Masks <http://subinitial.com/stacks/docs/indices>`_
        :type mask: int
        """
        bits = digest_indices(channels, mask, idx2mask=_idx_to_mask)
        self.regs.RELAYS_TOGGLE.write(bits)

    CMDTYPE_MASK = 0
    CMDTYPE_NAMES = 1
    CMDTYPE_INDICES = 2

    def get_cmd(self, cmdtype=CMDTYPE_MASK):
        """Get commanded (engaged) channels.

        :param cmdtype: selects return type.

            * 0: bitmask
            * 1: list of names
            * 2: list of indices

        :type cmdtype: int
        :return: command channels as bitmask, names, or indices
        :rtype: int | Bitmask | list[str] | list[int]
        """
        cmdmask = self.regs.RELAYS_CMD.read()
        if cmdtype == self.CMDTYPE_MASK:
            return Bitmask(cmdmask)
        cmdbits = mask_to_bits(cmdmask)
        if cmdtype == self.CMDTYPE_INDICES:
            return cmdbits
        return list(_channel_tostr[i] for i in cmdbits)

    @staticmethod
    def get_channel_mask(channel):
        """Get bitmask by channel name or index

        :param channel: index or name e.g. 0, 1, 12, 13, "F3", "A2"
        :type channel: int | str
        :return: bitmask of channel (1 << index, 32-bit)
        :rtype: int
        """
        return _idx_to_mask(channel)

    @staticmethod
    def get_channel_name(index):
        """Get name of channel by index

        :param index: channel index (0-31)
        :type index: int
        :return: channel name ("A1" through "H3")
        :rtype: str
        """
        return _channel_tostr[index]


class RTypes:
#/*%regdef%*/
    class Bitmask32(RegisterU32):
        @staticmethod
        def convert(val):
            return Bitmask(val)
    
        @staticmethod
        def revert(val):
            return val
    
#/*%end_regdef%*/

class RegisterSet(RegisterSetBase):
    Addresses = {
        #/*%regaddr%*/
        0: "REG0",
        1: "KEY",
        2: "SERIALNUMBER",
        4: "FIRMWARE",
        6: "ASSEMBLY_NUMBER",
        7: "BUSADDRESS",
        8: "CONTROL",
        9: "STATUS",
        10: "CONFIG_ADDRESS",
        12: "CONFIG",
        14: "UPTIME",
        16: "BROADCAST_STATUS",
        17: "REQUEST_STATUS",
        18: "GUID",
        19: "SESSION",
        20: "GPR0",
        21: "GPR1",
        22: "GPR2",
        23: "GPR3",
        24: "TRIGGER",
        48: "RELAYS_CMD",
        50: "RELAYS_ENGAGE",
        52: "RELAYS_DISENGAGE",
        54: "RELAYS_TOGGLE",
        56: "RELAYS_MAKEBREAK",
        60: "RELAYS_BREAKMAKE",
        #/*%end_regaddr%*/
    }

    def __init__(self, device):
        self.dev = device
        #/*%reginst%*/
        self.REG0 = Register(0, device)
        self.KEY = Register(1, device)
        self.SERIALNUMBER = RegisterU32(2, device)
        self.FIRMWARE = RegisterU32(4, device)
        self.ASSEMBLY_NUMBER = Register(6, device)
        self.BUSADDRESS = Register(7, device)
        self.CONTROL = Register(8, device)
        self.STATUS = Register(9, device)
        self.CONFIG_ADDRESS = RegisterU32(10, device)
        self.CONFIG = RegisterU32(12, device)
        self.UPTIME = RegisterU32(14, device)
        self.BROADCAST_STATUS = Register(16, device)
        self.REQUEST_STATUS = Register(17, device)
        self.GUID = RegisterBytes(18, device)
        self.SESSION = RegisterBytes(19, device)
        self.GPR0 = Register(20, device)
        self.GPR1 = Register(21, device)
        self.GPR2 = Register(22, device)
        self.GPR3 = Register(23, device)
        self.TRIGGER = Register(24, device)
        self.RELAYS_CMD = RTypes.Bitmask32(48, device)
        self.RELAYS_ENGAGE = RegisterU32(50, device)
        self.RELAYS_DISENGAGE = RegisterU32(52, device)
        self.RELAYS_TOGGLE = RegisterU32(54, device)
        self.RELAYS_MAKEBREAK = RegisterU64(56, device)
        self.RELAYS_BREAKMAKE = RegisterU64(60, device)
        #/*%end_reginst%*/

    def __getitem__(self, addr):
        if type(addr) == int:
            addr = RegisterSet.Addresses[addr]
        return getattr(self, addr)


def _tobool(str):
    return not (str == "0" or str.lower() == "false")


class LanTracks:
    ASSEMBLY = "SA13980"
    FIRMWARE_MIN = (1, 0, 0)

    FAILUREMODE_RECONNECT = 0
    FAILUREMODE_SESSIONID_CHANGED = 1
    FAILUREMODE_NO_RETRIES_REMAIN = 2

    class WarnLogger:
        def warning(self, str):
            sys.stderr.write(str)
            sys.stderr.write("\n")

    def __init__(self, host, port=9221, autoconnect=True,
                 lan_retries=3, lan_timeout=5, lan_handler=None, verify_connection=True, logger=None):
        """Construct a Stacks Lan Tracks instance.

        .. code-block:: python

            # Python Example connect to two different STCR devices
            tracks0 = subinitial.stacks.LanTracks("192.168.1.50")
            # Connecting to lantracks serial# 440 using its default hostname.
            # The hostname is configurable with set_hostname(...)
            tracks1 = subinitial.stacks.LanTracks("lantracks000440")

        :param host: hostname or ip address of the device
        :type host: str

        :param port: tcp port of the device server
        :type port: int

        :param autoconnect: if True a TCP connection is made to the device on object initialization
        :type autoconnect: bool

        :param lan_retries: number of reconnection attempts (per each request) to perform after a network failure
            with the device. Set this to None for an infinite number of retries
        :type lan_retries: int

        :param lan_timeout: time in seconds after which a socket operation is considered timed out thereby prompting a
            reconnection attempt or raising a SubinitialNetworkException if no retries remain
        :type lan_timeout: float

        :param lan_handler: An optional event handler which is called during network failures to allow user
            intervention and custom fault tolerant applications

                * Param 1 (NetworkConnection): A reference to the failing network connection is supplied.
                  Use this to modify the NetworkConnection host, port, or timeout fields before continuing with
                  reconnection attempts.
                * Param 2 (int) FailureMode: A flag that notifies what failure mode is to be handled.

                    * Mode 0: A network timeout has occured and a reconnection attempt is about to be made.

                        * return None to continue recovering from the network failure
                        * return 0 to cancel the attempt and raise a SubinitialNetworkException
                        * return 1+ to specify the number of additional attempts remaining, thereby prolonging the
                          reconnection attempt procedure.

                    * Mode 1: A reconnection attempt has succeeded, but the device's session ID has changed
                      changed indicating possible state resets.

                        * return None to continue recovering from the network failure
                        * return 0 to cancel the attempt and raise a SubinitialNetworkException

                    * Mode 2: No reconnection attempts remain.

                        * return None to proceed with raising a SubinitialNetworkException
                        * return 1+ to specify a number of additional reconnection attempts, thereby prolonging the
                          reconnection attempt procedure.
        :type lan_handler: (NetworkConnection, int)->int|None
        """
        self.retries = lan_retries
        self.timeout = lan_timeout
        self.verify_on_connect = verify_connection
        self.on_networkfailure = lan_handler
        self.host = host
        self.port = port
        self._is_open = False
        self._last_contime = {}
        self._last_contime_lock = threading.Lock()
        self.logger = logger
        if self.logger is None:
            self.logger = self.WarnLogger()
        # self.raise_response_exceptions = raise_response_exceptions

        self.socket = None
        self._buffer_size = 2048

        if autoconnect:
            self.connect()

        self._methodname = ""

    def __enter__(self):
        return self

    def __exit__(self, type_, value, traceback):
        self.close()

    @property
    def is_connected(self):
        return self._is_open

    def connect(self, host=None):
        host = host or self.host
        if self._is_open:
            if host == self.host:
                return
            else:
                self.close()
        self.host = host

        with self._last_contime_lock:
            # self._limit_connections_per_second()

            self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            # Don't set a socket timeout here
            self.socket.settimeout(self.timeout)
            try:
                resp = self.socket.connect((self.host, self.port))
            except:
                raise SubinitialTimeoutError("Failed to connect to {} at host: \"{}\","
                                             " timed out after {} seconds".format(self.__class__.__name__,
                                                                                  self.host, self.timeout))
            self._is_open = True
            if self.verify_on_connect:
                if not self.verify_connection():
                    self.close()
                    raise SubinitialException("device verification failed")

    def _reconnect(self, tries=1):
        # self._request_lock.release()
        while True:
            if self.on_networkfailure:
                retval = self.on_networkfailure(self, self.FAILUREMODE_RECONNECT)
                if type(retval) == int:
                    tries = retval
                if tries == 0:
                    return 0
            try:
                self.close()
                self.connect()
                return tries
            except Exception as e:
                self.logger.warning("Reconnecting to {} failed: {0}".format(self.__class__.__name__, e))
                if tries is None:
                    continue
                tries -= 1
                if tries <= 0:
                    return 0

    def close(self):
        if self._is_open:
            self._is_open = False
            if self.socket:
                self.socket.shutdown(socket.SHUT_RDWR)
                self.socket.close()
                self.socket = None
            return True
        return False

    def _txrx(self, msg):
        # First Attempt
        try:
            self.socket.send(msg)
            return self.socket.recv(self._buffer_size)
        except Exception as e:
            self.logger.warning("{} transmission failed: {0}".format(self.__class__.__name__, e))
            ex = e

        # Retries
        retries = self.retries
        while True:

            # Try to reconnect if retries remain
            if retries is None or retries > 0:
                self.logger.warning("Reconnecting to {}...".format(self.__class__.__name__))
                retries = self._reconnect(retries)  # reconnecting may have consumed some retries

            # Notify upper layer of network failure
            if retries <= 0 and self.on_networkfailure:
                retval = self.on_networkfailure(self, self.FAILUREMODE_NO_RETRIES_REMAIN)
                if type(retval) == int:
                    retries = retval
                retries = self._reconnect(retries)
                if retries:  # if the handler has modified the retry count then try to reconnect
                    continue

            # We have run out of retries, time to quit
            if retries <= 0:
                break

            # Now that we are reconnected retry the transmission
            try:
                self.socket.send(msg)
                return self.socket.recv(self._buffer_size)
            except Exception as e:
                self.logger.warning("{} transmission failed: {}".format(self.__class__.__name__, e))
                ex = e
            retries -= 1  # the above transmission failed, consume one retry

        raise ex  # SubinitialException("network connection failed")

    def request(self, req):
        if type(req) != str:
            req = json.dumps(req)
        msg = req.encode('utf-8')
        rsp = self._txrx(msg)
        if rsp and len(rsp) and rsp != b'_':  # "_" character denotes empty responses
            resp = rsp.decode()
            if resp.startswith("error: "):
                raise Exception(resp[7:])
            if resp.startswith("{"):
                return json.loads(resp)
            return resp

    def verify_connection(self):
        """Verifies network connection to the device
        Also verifies if firmware version and library version are compatible.
        This prompts the user when incompatibilities are detected.

        :returns: True if connection verified, False if connection could not be verified
        :rtype: bool
        """
        try:
            version = self.request("version?")

            if version["assembly"] != self.ASSEMBLY:
                print("\nWARNING: Python is expecting {} with part-number: {}\nDevice is reporting part-number: {}"
                      .format(self.__class__.__name__, self.ASSEMBLY, version["assembly"]))
                time.sleep(0.01)
                resp = input("Continue anyway? (y or n):")
                if 'y' in resp.lower():
                    return True
                else:
                    return False

            if tuple(version["firmware"]) < self.FIRMWARE_MIN:
                vstr = "v{}.{}.{}".format(*version["firmware"])
                fminstr = "v{}.{}.{}".format(*self.FIRMWARE_MIN)
                print("\nWARNING: {}\nFirmware should be updated to version >= {}".format(
                    self.__class__.__name__, fminstr))
                time.sleep(0.01)
                resp = input("Update this device's firmware from {} to {}? (y or n):".format(vstr, fminstr))
                if 'y' in resp.lower():
                    if not self._upgrade(fminstr):
                        print("ERROR: firmware upgrade failed")
                        return False
                    version = self.request("version?")

            if VERSION_STACKS < tuple(version["lib_min"]):
                vstr = "v{}.{}.{}".format(*VERSION_STACKS)
                fminstr = "v{}.{}.{}".format(*version["lib_min"])
                print("\nWARNING: {}\nStacks Python API should be updated from {} to version >= {}.".format(
                    self.__class__.__name__, vstr, fminstr))
                time.sleep(0.01)
                resp = input("Continue anyway? (y or n):")
                if 'y' in resp.lower():
                    pass
                else:
                    return False
        except Exception as ex:
            print("An error occured while verifying connection to {}:\n\t{}".format(self.__class__.__name__, ex))
            return False
        return True

    def set_hostname(self, hostname="tracks"):
        """Set the device's hostname, netbios name, and zeroconf/mdns name

        :param hostname: desired hostname
        :type: str
        """
        self.request("hostname {}".format(hostname))

    def get_hostname(self):
        """Get the device's hostname

        :return: device hostname
        :rtype: str
        """
        return self.request("hostname?")

    def get_idn(self):
        """Get the device's IDN

        :return: device IDN
        :rtype: str
        """
        return self.request("idn?")

    def reset(self):
        """Perform a complete system reset, the device will go offline momentarily and Python will
            attempt reconnection as it becomes available"""
        return self.request("reboot")

    def _shutdown(self):
        """Shutdown device, after which a power-cycle will be required for the device to resume operations"""
        return self.request("shutdown")

    def _upgrade(self, version=""):
        """Perform a software upgrade"""
        self.socket.settimeout(10)
        retval = self.request("upgrade {}".format(version))
        etc = retval["etc"]
        if retval["error"]:
            self.close()
            raise Exception(retval["error"])
        if etc:
            print("closing")
            self.close()
            print("waiting {}s".format(etc))
            time.sleep(etc)
            print("reconnecting")
            self.connect()
            print("connection succeeded")

        return retval

    def configuration_reset(self):
        """Reset the device configuration to factory defaults. This is equivalent to holding the RESET button on the device."""
        pass

    def set_netconfig(self, dhcp=True, ip="192.168.1.51", subnet="255.255.255.0", gateway="192.168.1.1", dns="8.8.8.8"):
        """Set the device's network configuration to use automatic DHCP or static IP/Subnet/Gateway/DNS server
            the configuration will be applied immediately, and Python will attempt to connect to the new configuration
            parameters. If dhcp==True then Python will look for the hostname using mDNS and netbios
        :param dhcp: if True, use DHCP, if False, use static ip/subnet/gateway
        :type dhcp: bool
        :param ip: ipv4 ip address
        :type ip: str
        :param subnet: ipv4 subnet mask
        :type subnet: str
        :param gateway: gateway ipv4 address
        :type gateway: str
        :param dns: dns ip address(es), e.g. "8.8.8.8 8.8.4.4"
        :type dns: str
        """
        self.request("netconfig {} {} {} {} {}".format(dhcp, ip, subnet, gateway, dns))

    def get_netconfig(self):
        dhcp, ip, subnet, gateway, dns = self.request("netconfig?")
        return {"ip": ip, "subnet": subnet, "gateway": gateway, "dhcp": _tobool(dhcp), "dns": dns}

    def get_netstatus(self):
        dhcp, ip, subnet, gateway, dns = self.request("netconfig?")
        return {"ip": ip, "subnet": subnet, "gateway": gateway, "dhcp": _tobool(dhcp), "dns": dns}

    USB_NONE = "none"
    USB_MULTI = "g_multi"
    USB_ETHERNET = "g_ether"
    USB_MASS_STORAGE = "g_mass_storage"
    USB_SERIAL = "g_serial"

    def set_usbmode(self, mode=USB_MULTI, ip="192.168.9.9", dhcp_server=True):
        """Set the device's network configuration to use automatic DHCP or static IP/Subnet/Gateway
        :param mode: usb device operating mode
            USB_NONE: the device will not enumerate as any usb device on your PC
            USB_MULTI: the device will enumerate as a usb-serial device, usb-ethernet device, and usb-mass-storage device
            USB_ETHERNET: the device will enumerate as just a usb-ethernet device
            USB_MASS_STORAGE: the device will enumerate as just a usb-mass-storage device
            USB_SERIAL: the device will enumeraet as just a usb-serial device
        :type mode: str
        :param ip: the device's ipv4 ip address will be configured to this value on the USB0 ethernet interface
                (this value is only used if mode == USB_MULTI or USB_ETHERNET)
        :type ip: str
        :param dhcp_server: if True, the device will host a DHCP server that will assign your PC an ip address within
            the same subnet as the configured ip parameter (used if mode == USB_MULTI or USB_ETHERNET)
        :type dhcp_server: bool
        """
        self.request("usb {} {} {}".format(mode, ip, dhcp_server))

    def get_usbmode(self):
        ip, subnet, gateway, dhcp = self.request("netconfig?")
        return {"ip": ip, "subnet": subnet, "gateway": gateway, "dhcp": _tobool(dhcp)}

    def _get_status(self):
        """Retrieve any errors or warnings concerning the device's operation.
            An empty dictionary is a nominal status result. Any error conditions will have
            a key "error" with a string value describing the error condition. Additional key:values
            may be provided to help define the error/warning."""
        pass

    def set_channelgroups(self, *channelgroups):
        """Defines a set of ChannelGroups on this Tracks device.

        Any previous ChannelGroup configuration on Tracks will be overwritten by this configuration.
        All channels associated with a ChannelGroup will be disengaged immediately.
        ChannelGroups are cleared between power-cycles/reboots.

        :param channelgroups: Each argument is a 32-bit integer whose bits represent a collection of channels on the
            Tracks device. One ChannelGroup will be registered on Tracks for each argument supplied. Only one
            channel within a ChannelGroup may be engaged at a time. An attempt to engage more than one channel within
            a ChannelGroup will result in a Python Exception 'ChannelGroup violation'.
            For convenience, use the _select_ method to ensure only one channel within a ChannelGroup is engaged at a
            time.
        :type channelgroups: int
        """
        self.request("set_channelgroups {}".format(" ".join(str(x) for x in channelgroups)))

    def get_channelgroups(self):
        """Get all active ChannelGroups configured for this TracksDevice.

        :return: channelmasks for each active ChannelGroup configured on this Tracks device. An empty list indicates
        no active ChannelGroups have been defined.
        :rtype: list[int]
        """
        resp = self.request("channelgroups?")
        if resp:
            return list(int(x) for x in resp.split(","))
        return []

    def select(self, channel):
        """Engage only one channel at a time with respect to the channel's ChannelGroup membership.

        :param channel: channel to be engaged.
            If this channel is a ChannelGroup member then its ChannelGroup channels are disengaged completely before
            this channel is engaged. If the channel provided is not a ChannelGroup member then all other channels
            not part of a ChannelGroup are disengaged completely before this channel is engaged.
        :type channel: int
        """
        self.request("select {}".format(channel))

    def set(self, *channels, mask=0):
        self.engage(*channels, mask=mask)

    def clear(self, *channels, mask=0):
        self.disengage(*channels, mask=mask)

    def cmd(self, *channels, mask=0):
        """Command specified channels on (and disengage omitted channels).

        :param channels: arbitrary list of indices. e.g. 0, 1, 12, 13, "F3", "A2"
        :type channels: tuple[int|str]
        :param mask: optional bitmask of indices. e.g. 0b11
            See `Specifying Indices or Masks <http://subinitial.com/stacks/docs/indices>`_
        :type mask: int
        """
        bits = digest_indices(channels, mask, idx2mask=_idx_to_mask)
        self.request("cmd {}".format(bits))

    def engage(self, *channels, mask=0):
        """Engage relay channels.

        :param channels: arbitrary list of indices. e.g. 0, 1, 12, 13, "F3", "A2"
        :type channels: tuple[int|str]
        :param mask: optional bitmask of indices. e.g. 0b11
            See `Specifying Indices or Masks <http://subinitial.com/stacks/docs/indices>`_
        :type mask: int
        """
        bits = digest_indices(channels, mask, idx2mask=_idx_to_mask)
        self.request("set {}".format(bits))

    def disengage(self, *channels, mask=0):
        """Disengage relay channels.

        :param channels: arbitrary list of indices. e.g. 0, 1, 12, 13, "F3", "A2"
        :type channels: tuple[int|str]
        :param mask: optional bitmask of indices. e.g. 0b11
            See `Specifying Indices or Masks <http://subinitial.com/stacks/docs/indices>`_
        :type mask: int
        """
        bits = digest_indices(channels, mask, idx2mask=_idx_to_mask)
        self.request("clear {}".format(bits))

    def toggle(self, *channels, mask=0):
        """toggle relay channels

        :param channels: arbitrary list of indices. e.g. 0, 1, 12, 13, "F3", "A2"
        :type channels: tuple[int|str]
        :param mask: optional bitmask of indices. e.g. 0b11
            See `Specifying Indices or Masks <http://subinitial.com/stacks/docs/indices>`_
        :type mask: int
        """
        bits = digest_indices(channels, mask, idx2mask=_idx_to_mask)
        self.request("toggle {}".format(bits))

    CMDTYPE_MASK = 0
    CMDTYPE_NAMES = 1
    CMDTYPE_INDICES = 2

    def get_cmd(self, cmdtype=CMDTYPE_MASK):
        """Get commanded (engaged) channels.

        :param cmdtype: selects return type.

            * 0: bitmask
            * 1: list of names
            * 2: list of indices

        :type cmdtype: int
        :return: command channels as bitmask, names, or indices
        :rtype: int | Bitmask | list[str] | list[int]
        """
        cmdmask = int(self.request("cmd?"))
        if cmdtype == self.CMDTYPE_MASK:
            return Bitmask(cmdmask)
        cmdbits = mask_to_bits(cmdmask)
        if cmdtype == self.CMDTYPE_INDICES:
            return cmdbits
        return list(_channel_tostr[i] for i in cmdbits)

    @staticmethod
    def get_channel_mask(channel):
        """Get bitmask by channel name or index

        :param channel: index or name e.g. 0, 1, 12, 13, "F3", "A2"
        :type channel: int | str
        :return: bitmask of channel (1 << index, 32-bit)
        :rtype: int
        """
        return _idx_to_mask(channel)

    @staticmethod
    def get_channel_name(index):
        """Get name of channel by index

        :param index: channel index (0-31)
        :type index: int
        :return: channel name ("A1" through "H3")
        :rtype: str
        """
        return _channel_tostr[index]
