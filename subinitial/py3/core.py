# -*- coding: UTF-8 -*-
# Stacks Core interface
# © 2012-2018 Subinitial LLC. All Rights Reserved
# Redistribution and use in source and binary forms, with or without modification, are permitted provided that the
# following conditions are met: Redistributions of source code and binary forms must retain the above copyright notice,
# these conditions, and the disclaimer below all in plain text. The name Subinitial may not be used to endorse or
# promote products derived from this software without prior written consent from Subinitial LLC. This software may only
# be redistributed and used with a Subinitial Stacks product.

# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
# warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. Refer to the included LICENSE.txt for more details.

from subinitial.py3.stacks import *


class Core(Device):
    """Construct a Core Deck instance

    .. code-block:: python

        # Python Example
        core = subinitial.stacks.Core(host="192.168.1.49")

    :param host: hostname or ip address of Stacks Core
    :type host: str

    :param port: Stacks Core ModbusTCP port (Default 502)
    :type port: int

    :param autoconnect: if True a TCP connection is made to the Stacks Core on object initialization
    :type autoconnect: bool

    :param raise_response_exceptions: if True any Modbus Exceptions are raised in Python as
        SubinitialModbusExceptions
    :type raise_response_exceptions: bool

    :param lan_retries: number of reconnection attempts (per each request) to perform after a network failure
        with Stacks. Set this to None for an infinite number of retries
    :type lan_retries: int

    :param lan_timeout: time in seconds after which a socket operation is considered timed out thereby prompting a
        reconnection attempt or raising a SubinitialNetworkException if no retries remain
    :type lan_timeout: float

    :param lan_handler: An optional event handler which is called during network failures to allow user
        intervention and custom fault tolerant applications

            * Param 1 (NetworkConnection): A reference to the failing network connection is supplied.
              Use this to modify the NetworkConnection host, port, or timeout fields before continuing with
              reconnection attempts.
            * Param 2 (int) FailureMode: A flag that notifies what failure mode is to be handled.

                * Mode 0: A network timeout has occured and a reconnection attempt is about to be made.

                    * return None to continue recovering from the network failure
                    * return 0 to cancel the attempt and raise a SubinitialNetworkException
                    * return 1+ to specify the number of additional attempts remaining, thereby prolonging the
                      reconnection attempt procedure.

                * Mode 1: A reconnection attempt has succeeded, but one or more Stack Devices' session IDs have
                  changed indicating possible state resets.

                    * return None to continue recovering from the network failure
                    * return 0 to cancel the attempt and raise a SubinitialNetworkException

                * Mode 2: No reconnection attempts remain.

                    * return None to proceed with raising a SubinitialNetworkException
                    * return 1+ to specify a number of additional reconnection attempts, thereby prolonging the
                      reconnection attempt procedure.
    :type lan_handler: (NetworkConnection, int)->int|None
    """
    ASSEMBLY = "SA13729"
    FIRMWARE_MIN = "v1.11.1"

    def __init__(self, host="192.168.1.49", port=502, autoconnect=True, raise_response_exceptions=True,
                 lan_retries=3, lan_timeout=5, lan_handler=None, verify_connection=True):
        self.host = host
        self.port = port
        self.connection = Connection(host, port, lan_retries, lan_timeout, lan_handler)

        Device.__init__(self, self, 1, verify_connection=False)
        self.regs = RegisterSet(self)
        """:type: RegisterSet"""

        self.dutpower12v = DutPower(self, DutPower.ID_12V)
        """:type: DutPower"""
        self.dutpower5v = DutPower(self, DutPower.ID_5V)
        """:type: DutPower"""
        self.dutpower3v3 = DutPower(self, DutPower.ID_3V3)
        """:type: DutPower"""
        self.dutpowerneg12v = DutPower(self, DutPower.ID_neg12V)
        """:type: DutPower"""
        self.dio = Dio(self)
        """:type: Dio"""
        self.rgbled = RgbLed(self)
        """:type: RgbLed"""
        self.spi1 = SpiChannel(self, 1)
        """:type: SpiChannel"""
        self.spi2 = SpiChannel(self, 2)
        """:type: SpiChannel"""
        self.spi3 = SpiChannel(self, 3)
        """:type: SpiChannel"""
        self.spi = Spi(self)
        """:type: Spi"""
        self.uart = Uart(self)
        """:type: Uart"""
        self.rs485 = Uart485(self)
        """:type: Uart485"""
        self.adc = Adc(self)
        """:type: Adc"""
        self.dac = Dac(self)
        """:type: Dac"""
        self.power = Power(self)
        """:type: Power"""
        self.pwm = Pwm(self)
        """:type: Pwm"""
        self.timer = Timer(self)
        """:type: Timer"""
        self.clock1 = Clock(self, channel=1)
        """:type: Clock"""
        self.clock2 = Clock(self, channel=2)
        """:type: Clock"""
        self.i2c = I2C(self)
        """:type: I2C"""
        self.can = Can(self)
        """:type: Can"""
        self.leds = Leds(self)
        """:type: Leds"""
        self.fs = FileSystem(self)
        """:type: FileSystem"""
        self.updater = Updater(self)
        """:type: Updater"""

        if autoconnect:
            self.connect()
        if verify_connection:
            if autoconnect:
                self.verify_connection()
            else:
                self.request.opener = self.verify_connection

    def connect(self, host=None):
        """If autoconnect is false on initialization, this will create a connection to the Core

            :rtype: bool
            :returns: True if connection occured, False if connection was already open
        """
        if host is not None and host != self.host:
            self.close()
            self.connection.host = self.host = host
        elif self.connection.is_connected:
            return False
        self.connection.connect()
        return True

    def close(self):
        """Disconnects from the Core

            :rtype: bool
            :returns: True if connection was closed, False if connection was not open
        """
        return self.connection.kill()

    def _reset(self):
        self.print_console("reset")
        self.close()
        time.sleep(0.5)
        self.connect()

    _bsock = None
    _baddr = None
    _bdest = None

    class _BResponse(str):
        def _set_addr(self, addr):
            self.address = addr
            return self

    def get_mselapsed(self):
        return self.regs.TIMESTAMP.read()

    def reset_mselapsed(self):
        return self.regs.TIMESTAMP.write(0)

    @staticmethod
    def broadcast(message, print_=False):
        if print_:
            print(">{}".format(message.strip()))
        if Core._bsock is None:
            import socket
            Core._baddr = (str(socket.INADDR_ANY), 33334)  # host, incoming port
            Core._bdest = ('255.255.255.255', 33333)  # host, outgoing port
            s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM, socket.IPPROTO_UDP)
            s.settimeout(1)
            s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
            s.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)
            s.bind(Core._baddr)
            Core._bsock = s
        msg = "{}\n".format(message.strip()).encode()
        Core._bsock.sendto(msg, Core._bdest)
        data, addr = Core._bsock.recvfrom(1024)
        text = data.decode()
        if print_:
            print(text.strip())
        return Core._BResponse(text)._set_addr(addr[0])

    def __enter__(self):
        return self.regs, self.r, self.control, self.request.queue()

    def __exit__(self, type, value, traceback):
        self.request.submit()

    def update_busaddress(self, new_busaddress):
        raise Exception("Core bus_address is fixed at 1")


class RTypes:
#/*%regdef%*/
    class AdcRegister(Register):
        @staticmethod
        def convert(val):
            return 3.8147554741741054e-05 * val
    
        @staticmethod
        def revert(val):
            return round(val / 3.8147554741741054e-05)
    
    class Div1000(Register):
        @staticmethod
        def convert(val):
            return 0.001 * val
    
        @staticmethod
        def revert(val):
            return round(val / 0.001)
    
    class Celcius(Register):
        @staticmethod
        def convert(val):
            return 0.008116501008881075 * val + -279.3829787234042
    
        @staticmethod
        def revert(val):
            return round((val - -279.3829787234042) / 0.008116501008881075)
    
    class Div100(Register):
        @staticmethod
        def convert(val):
            return 0.01 * val
    
        @staticmethod
        def revert(val):
            return round(val / 0.01)
    
    class Neg12(RegisterI16):
        @staticmethod
        def convert(val):
            return 0.001 * val
    
        @staticmethod
        def revert(val):
            return round(val / 0.001)
    
    class DacDC(Register):
        @staticmethod
        def convert(val):
            return 0.0016117216117216117 * val + -3.3
    
        @staticmethod
        def revert(val):
            return round(min(max((val - -3.3) / 0.0016117216117216117, 0), 4095))
    
    class DacWave(RegisterU16Array):
        @staticmethod
        def convert(val):
            return 0.0016117216117216117 * val + -3.3
    
        @staticmethod
        def revert(val):
            return round(min(max((val - -3.3) / 0.0016117216117216117, 0), 4095))
    
    class Mul1000(Register):
        @staticmethod
        def convert(val):
            return 0.001 * val
    
        @staticmethod
        def revert(val):
            return round(val / 0.001)
    
    class U32Div1000(RegisterU32):
        @staticmethod
        def convert(val):
            return 0.001 * val
    
        @staticmethod
        def revert(val):
            return round(val / 0.001)
    
    class Percentage(Register):
        @staticmethod
        def convert(val):
            return 3.0517578125e-05 * val
    
        @staticmethod
        def revert(val):
            return round(val / 3.0517578125e-05)
    
    class UartBaud(Register):
        @staticmethod
        def convert(val):
            return round(7500000.0 / val)
    
        @staticmethod
        def revert(val):
            return round(val / 7500000.0)
    
#/*%end_regdef%*/


class RegisterSet(RegisterSetBase):
    Addresses = {
        #/*%regaddr%*/
        0: "REG0",
        1: "KEY",
        2: "SERIALNUMBER",
        4: "FIRMWARE",
        6: "ASSEMBLY_NUMBER",
        7: "BUSADDRESS",
        8: "CONTROL",
        9: "STATUS",
        10: "CONFIG_ADDRESS",
        12: "CONFIG",
        14: "UPTIME",
        16: "BROADCAST_STATUS",
        17: "REQUEST_STATUS",
        18: "GUID",
        19: "SESSION",
        20: "GPR0",
        21: "GPR1",
        22: "GPR2",
        23: "GPR3",
        24: "TRIGGER",
        25: "FILE_PATH0",
        26: "FILE_PATH1",
        27: "FILE_CURSOR",
        29: "FILE_CONTROL",
        30: "FILE_BUFFER",
        32: "TIMESTAMP",
        34: "TIMESTAMP_PS",
        40: "DUTPOWER12V_CURRENT",
        41: "DUTPOWER5V_CURRENT",
        42: "DUTPOWER3V3_CURRENT",
        43: "DUTPOWERNEG12V_CURRENT",
        44: "ADC_CH0",
        45: "ADC_CH1",
        46: "ADC_CH2",
        47: "ADC_CH3",
        48: "POWERSUPPLY_CORE_TEMP",
        49: "DUTPOWER_CONTROL",
        50: "DIO_STATUS",
        51: "DIO_CMD",
        54: "DIO_SET",
        55: "DIO_CLEAR",
        58: "DUTPOWER12V_CURRENT_MIN",
        59: "DUTPOWER5V_CURRENT_MIN",
        60: "DUTPOWER3V3_CURRENT_MIN",
        61: "DUTPOWERNEG12V_CURRENT_MIN",
        62: "DUTPOWER12V_CURRENT_MAX",
        63: "DUTPOWER5V_CURRENT_MAX",
        64: "DUTPOWER3V3_CURRENT_MAX",
        65: "DUTPOWERNEG12V_CURRENT_MAX",
        66: "RGBB",
        68: "DAC_DCLEFT",
        69: "DAC_DCRIGHT",
        70: "PWM_CH0_FREQUENCY",
        72: "PWM_CH1_FREQUENCY",
        74: "PWM_CH2_FREQUENCY",
        76: "PWM_CH3_FREQUENCY",
        78: "PWM_CH0_DUTY",
        79: "PWM_CH1_DUTY",
        80: "PWM_CH2_DUTY",
        81: "PWM_CH3_DUTY",
        82: "CLOCKS",
        83: "PWM_LOWCLOCKDIV",
        84: "TIMER_CONFIG",
        86: "TIMER_COUNTER",
        88: "TIMER_CLOCK",
        92: "ADC_CH0_MIN",
        93: "ADC_CH1_MIN",
        94: "ADC_CH2_MIN",
        95: "ADC_CH3_MIN",
        96: "POWERSUPPLY_MIN_CORE_TEMP",
        97: "ADC_CH0_MAX",
        98: "ADC_CH1_MAX",
        99: "ADC_CH2_MAX",
        100: "ADC_CH3_MAX",
        101: "POWERSUPPLY_MAX_CORE_TEMP",
        102: "POWERSUPPLY_INPUT_VOLTAGE",
        103: "POWERSUPPLY_PMIC_TEMP",
        104: "POWERSUPPLY_12V_VOLTAGE",
        105: "POWERSUPPLY_12V_CURRENT",
        106: "POWERSUPPLY_12V_TEMP",
        107: "POWERSUPPLY_5V_VOLTAGE",
        108: "POWERSUPPLY_3V3_VOLTAGE",
        109: "POWERSUPPLY_3V3_CURRENT",
        110: "POWERSUPPLY_3V3_TEMP",
        111: "POWERSUPPLY_3V3STANDBY_VOLTAGE",
        112: "POWERSUPPLY_NEG12V_VOLTAGE",
        113: "POWERSUPPLY_MIN_INPUT_VOLTAGE",
        114: "POWERSUPPLY_MIN_PMIC_TEMP",
        115: "POWERSUPPLY_MIN_12V_VOLTAGE",
        116: "POWERSUPPLY_MIN_12V_CURRENT",
        117: "POWERSUPPLY_MIN_12V_TEMP",
        118: "POWERSUPPLY_MIN_5V_VOLTAGE",
        119: "POWERSUPPLY_MIN_3V3_VOLTAGE",
        120: "POWERSUPPLY_MIN_3V3_CURRENT",
        121: "POWERSUPPLY_MIN_3V3_TEMP",
        122: "POWERSUPPLY_MIN_3V3STANDBY_VOLTAGE",
        123: "POWERSUPPLY_MIN_NEG12V_VOLTAGE",
        124: "POWERSUPPLY_MAX_INPUT_VOLTAGE",
        125: "POWERSUPPLY_MAX_PMIC_TEMP",
        126: "POWERSUPPLY_MAX_12V_VOLTAGE",
        127: "POWERSUPPLY_MAX_12V_CURRENT",
        128: "POWERSUPPLY_MAX_12V_TEMP",
        129: "POWERSUPPLY_MAX_5V_VOLTAGE",
        130: "POWERSUPPLY_MAX_3V3_VOLTAGE",
        131: "POWERSUPPLY_MAX_3V3_CURRENT",
        132: "POWERSUPPLY_MAX_3V3_TEMP",
        133: "POWERSUPPLY_MAX_3V3STANDBY_VOLTAGE",
        134: "POWERSUPPLY_MAX_NEG12V_VOLTAGE",
        142: "RGBB_EXCEPTION_STATUS",
        143: "RGBB_EXCEPTION_SUPRESSION",
        144: "DUTPOWER_CONFIG12V",
        145: "DUTPOWER_CONFIG5V",
        146: "DUTPOWER_CONFIG3V3",
        147: "DUTPOWER_CONFIGNEG12V",
        148: "DIO0_CONFIG",
        149: "DIO1_CONFIG",
        150: "DIO2_CONFIG",
        151: "DIO3_CONFIG",
        152: "DIO4_CONFIG",
        153: "DIO5_CONFIG",
        154: "DIO6_CONFIG",
        155: "DIO7_CONFIG",
        156: "DIO8_CONFIG",
        157: "DIO9_CONFIG",
        158: "DIO10_CONFIG",
        159: "DIO_INTERRUPT_FILTER",
        160: "USERLEDS",
        161: "DAC_CONTROL",
        162: "ADC_CONTROL",
        178: "DAC_WAVE_RUNTIME",
        179: "DAC_WAVE_SAMPLECOUNT",
        180: "DAC_WAVE_SAMPLEPERIOD",
        182: "DAC_WAVE_SAMPLESINDEX",
        183: "DAC_WAVE_SAMPLES_LEFT",
        184: "DAC_WAVE_SAMPLES_RIGHT",
        185: "SPI_CONTROL",
        186: "SPI1_CONFIG",
        188: "SPI2_CONFIG",
        190: "SPI3_CONFIG",
        192: "SPI1_BUFFER",
        193: "SPI2_BUFFER",
        194: "SPI3_BUFFER",
        196: "CAN_CONFIG",
        197: "CAN_STATUS",
        198: "CAN_BAUD",
        200: "CAN_TXERRORCOUNT",
        201: "CAN_RXERRORCOUNT",
        202: "CAN_MAILBOX_CONTROL",
        203: "CAN_MAILBOX_SELECT",
        204: "CAN_MB_RXMASK",
        206: "CAN_MB_MID",
        208: "CAN_MB_CONFIG",
        209: "CAN_MB_TIMEMARK_TRIGGER",
        210: "CAN_MB_FID",
        212: "CAN_MB_DATALOW",
        214: "CAN_MB_DATAHIGH",
        216: "CAN_MB_CONTROL",
        217: "CAN_MB_TIMEMARK",
        221: "UART_CONFIG",
        222: "UART_BAUD",
        223: "UART_TXFIFO",
        224: "UART_RXFIFO",
        225: "UART_STATUS",
        230: "I2C_CONFIG",
        231: "I2C_STATUS",
        232: "I2C_BUFFER",
        233: "I2C_STREAM",
        234: "I2C_STREAM_STATUS",
        236: "I2C_STREAM_INTERVAL",
        238: "CAN_ERRORSTREAM_CONTROL",
        240: "CAN_ERRORSTREAM",
        251: "UART485_CONFIG",
        252: "UART485_BAUD",
        254: "UART485_TXFIFO",
        255: "UART485_RXFIFO",
        256: "UART485_STATUS",
        257: "UART485_RTSDELAY",
        258: "ENDREG",
        #/*%end_regaddr%*/
    }

    def __init__(self, device):
        self.dev = device
        #/*%reginst%*/
        self.REG0 = Register(0, device)
        self.KEY = Register(1, device)
        self.SERIALNUMBER = RegisterU32(2, device)
        self.FIRMWARE = RegisterU32(4, device)
        self.ASSEMBLY_NUMBER = Register(6, device)
        self.BUSADDRESS = Register(7, device)
        self.CONTROL = Register(8, device)
        self.STATUS = Register(9, device)
        self.CONFIG_ADDRESS = RegisterU32(10, device)
        self.CONFIG = RegisterU32(12, device)
        self.UPTIME = RegisterU32(14, device)
        self.BROADCAST_STATUS = Register(16, device)
        self.REQUEST_STATUS = Register(17, device)
        self.GUID = RegisterBytes(18, device)
        self.SESSION = RegisterBytes(19, device)
        self.GPR0 = Register(20, device)
        self.GPR1 = Register(21, device)
        self.GPR2 = Register(22, device)
        self.GPR3 = Register(23, device)
        self.TRIGGER = Register(24, device)
        self.FILE_PATH0 = RegisterBytes(25, device)
        self.FILE_PATH1 = RegisterBytes(26, device)
        self.FILE_CURSOR = RegisterU32(27, device)
        self.FILE_CONTROL = Register(29, device)
        self.FILE_BUFFER = RegisterBytes(30, device)
        self.TIMESTAMP = RegisterU32(32, device)
        self.TIMESTAMP_PS = RegisterU64(34, device)
        self.DUTPOWER12V_CURRENT = RTypes.Div1000(40, device)
        self.DUTPOWER5V_CURRENT = RTypes.Div1000(41, device)
        self.DUTPOWER3V3_CURRENT = RTypes.Div1000(42, device)
        self.DUTPOWERNEG12V_CURRENT = RTypes.Div1000(43, device)
        self.ADC_CH0 = RTypes.AdcRegister(44, device)
        self.ADC_CH1 = RTypes.AdcRegister(45, device)
        self.ADC_CH2 = RTypes.AdcRegister(46, device)
        self.ADC_CH3 = RTypes.AdcRegister(47, device)
        self.POWERSUPPLY_CORE_TEMP = RTypes.Celcius(48, device)
        self.DUTPOWER_CONTROL = Register(49, device)
        self.DIO_STATUS = Register(50, device)
        self.DIO_CMD = Register(51, device)
        self.DIO_SET = Register(54, device)
        self.DIO_CLEAR = Register(55, device)
        self.DUTPOWER12V_CURRENT_MIN = RTypes.Div1000(58, device)
        self.DUTPOWER5V_CURRENT_MIN = RTypes.Div1000(59, device)
        self.DUTPOWER3V3_CURRENT_MIN = RTypes.Div1000(60, device)
        self.DUTPOWERNEG12V_CURRENT_MIN = RTypes.Div1000(61, device)
        self.DUTPOWER12V_CURRENT_MAX = RTypes.Div1000(62, device)
        self.DUTPOWER5V_CURRENT_MAX = RTypes.Div1000(63, device)
        self.DUTPOWER3V3_CURRENT_MAX = RTypes.Div1000(64, device)
        self.DUTPOWERNEG12V_CURRENT_MAX = RTypes.Div1000(65, device)
        self.RGBB = RegisterU32(66, device)
        self.DAC_DCLEFT = RTypes.DacDC(68, device)
        self.DAC_DCRIGHT = RTypes.DacDC(69, device)
        self.PWM_CH0_FREQUENCY = RTypes.U32Div1000(70, device)
        self.PWM_CH1_FREQUENCY = RTypes.U32Div1000(72, device)
        self.PWM_CH2_FREQUENCY = RTypes.U32Div1000(74, device)
        self.PWM_CH3_FREQUENCY = RTypes.U32Div1000(76, device)
        self.PWM_CH0_DUTY = RTypes.Percentage(78, device)
        self.PWM_CH1_DUTY = RTypes.Percentage(79, device)
        self.PWM_CH2_DUTY = RTypes.Percentage(80, device)
        self.PWM_CH3_DUTY = RTypes.Percentage(81, device)
        self.CLOCKS = Register(82, device)
        self.PWM_LOWCLOCKDIV = Register(83, device)
        self.TIMER_CONFIG = Register(84, device)
        self.TIMER_COUNTER = RegisterU32(86, device)
        self.TIMER_CLOCK = RegisterU32(88, device)
        self.ADC_CH0_MIN = RTypes.AdcRegister(92, device)
        self.ADC_CH1_MIN = RTypes.AdcRegister(93, device)
        self.ADC_CH2_MIN = RTypes.AdcRegister(94, device)
        self.ADC_CH3_MIN = RTypes.AdcRegister(95, device)
        self.POWERSUPPLY_MIN_CORE_TEMP = RTypes.Celcius(96, device)
        self.ADC_CH0_MAX = RTypes.AdcRegister(97, device)
        self.ADC_CH1_MAX = RTypes.AdcRegister(98, device)
        self.ADC_CH2_MAX = RTypes.AdcRegister(99, device)
        self.ADC_CH3_MAX = RTypes.AdcRegister(100, device)
        self.POWERSUPPLY_MAX_CORE_TEMP = RTypes.Celcius(101, device)
        self.POWERSUPPLY_INPUT_VOLTAGE = RTypes.Div100(102, device)
        self.POWERSUPPLY_PMIC_TEMP = RTypes.Div100(103, device)
        self.POWERSUPPLY_12V_VOLTAGE = RTypes.Div1000(104, device)
        self.POWERSUPPLY_12V_CURRENT = RTypes.Div1000(105, device)
        self.POWERSUPPLY_12V_TEMP = RTypes.Div100(106, device)
        self.POWERSUPPLY_5V_VOLTAGE = RTypes.Div1000(107, device)
        self.POWERSUPPLY_3V3_VOLTAGE = RTypes.Div1000(108, device)
        self.POWERSUPPLY_3V3_CURRENT = RTypes.Div1000(109, device)
        self.POWERSUPPLY_3V3_TEMP = RTypes.Div100(110, device)
        self.POWERSUPPLY_3V3STANDBY_VOLTAGE = RTypes.Div1000(111, device)
        self.POWERSUPPLY_NEG12V_VOLTAGE = RTypes.Neg12(112, device)
        self.POWERSUPPLY_MIN_INPUT_VOLTAGE = RTypes.Div100(113, device)
        self.POWERSUPPLY_MIN_PMIC_TEMP = RTypes.Div100(114, device)
        self.POWERSUPPLY_MIN_12V_VOLTAGE = RTypes.Div1000(115, device)
        self.POWERSUPPLY_MIN_12V_CURRENT = RTypes.Div1000(116, device)
        self.POWERSUPPLY_MIN_12V_TEMP = RTypes.Div100(117, device)
        self.POWERSUPPLY_MIN_5V_VOLTAGE = RTypes.Div1000(118, device)
        self.POWERSUPPLY_MIN_3V3_VOLTAGE = RTypes.Div1000(119, device)
        self.POWERSUPPLY_MIN_3V3_CURRENT = RTypes.Div1000(120, device)
        self.POWERSUPPLY_MIN_3V3_TEMP = RTypes.Div100(121, device)
        self.POWERSUPPLY_MIN_3V3STANDBY_VOLTAGE = RTypes.Div1000(122, device)
        self.POWERSUPPLY_MIN_NEG12V_VOLTAGE = RTypes.Neg12(123, device)
        self.POWERSUPPLY_MAX_INPUT_VOLTAGE = RTypes.Div100(124, device)
        self.POWERSUPPLY_MAX_PMIC_TEMP = RTypes.Div100(125, device)
        self.POWERSUPPLY_MAX_12V_VOLTAGE = RTypes.Div1000(126, device)
        self.POWERSUPPLY_MAX_12V_CURRENT = RTypes.Div1000(127, device)
        self.POWERSUPPLY_MAX_12V_TEMP = RTypes.Div100(128, device)
        self.POWERSUPPLY_MAX_5V_VOLTAGE = RTypes.Div1000(129, device)
        self.POWERSUPPLY_MAX_3V3_VOLTAGE = RTypes.Div1000(130, device)
        self.POWERSUPPLY_MAX_3V3_CURRENT = RTypes.Div1000(131, device)
        self.POWERSUPPLY_MAX_3V3_TEMP = RTypes.Div100(132, device)
        self.POWERSUPPLY_MAX_3V3STANDBY_VOLTAGE = RTypes.Div1000(133, device)
        self.POWERSUPPLY_MAX_NEG12V_VOLTAGE = RTypes.Neg12(134, device)
        self.RGBB_EXCEPTION_STATUS = Register(142, device)
        self.RGBB_EXCEPTION_SUPRESSION = Register(143, device)
        self.DUTPOWER_CONFIG12V = Register(144, device)
        self.DUTPOWER_CONFIG5V = Register(145, device)
        self.DUTPOWER_CONFIG3V3 = Register(146, device)
        self.DUTPOWER_CONFIGNEG12V = Register(147, device)
        self.DIO0_CONFIG = Register(148, device)
        self.DIO1_CONFIG = Register(149, device)
        self.DIO2_CONFIG = Register(150, device)
        self.DIO3_CONFIG = Register(151, device)
        self.DIO4_CONFIG = Register(152, device)
        self.DIO5_CONFIG = Register(153, device)
        self.DIO6_CONFIG = Register(154, device)
        self.DIO7_CONFIG = Register(155, device)
        self.DIO8_CONFIG = Register(156, device)
        self.DIO9_CONFIG = Register(157, device)
        self.DIO10_CONFIG = Register(158, device)
        self.DIO_INTERRUPT_FILTER = Register(159, device)
        self.USERLEDS = Register(160, device)
        self.DAC_CONTROL = Register(161, device)
        self.ADC_CONTROL = Register(162, device)
        self.DAC_WAVE_RUNTIME = RTypes.Mul1000(178, device)
        self.DAC_WAVE_SAMPLECOUNT = Register(179, device)
        self.DAC_WAVE_SAMPLEPERIOD = RegisterU32(180, device)
        self.DAC_WAVE_SAMPLESINDEX = Register(182, device)
        self.DAC_WAVE_SAMPLES_LEFT = RTypes.DacWave(183, device)
        self.DAC_WAVE_SAMPLES_RIGHT = RTypes.DacWave(184, device)
        self.SPI_CONTROL = Register(185, device)
        self.SPI1_CONFIG = RegisterU32(186, device)
        self.SPI2_CONFIG = RegisterU32(188, device)
        self.SPI3_CONFIG = RegisterU32(190, device)
        self.SPI1_BUFFER = RegisterBytes(192, device)
        self.SPI2_BUFFER = RegisterBytes(193, device)
        self.SPI3_BUFFER = RegisterBytes(194, device)
        self.CAN_CONFIG = Register(196, device)
        self.CAN_STATUS = Register(197, device)
        self.CAN_BAUD = RegisterU32(198, device)
        self.CAN_TXERRORCOUNT = Register(200, device)
        self.CAN_RXERRORCOUNT = Register(201, device)
        self.CAN_MAILBOX_CONTROL = Register(202, device)
        self.CAN_MAILBOX_SELECT = Register(203, device)
        self.CAN_MB_RXMASK = RegisterU32(204, device)
        self.CAN_MB_MID = RegisterU32(206, device)
        self.CAN_MB_CONFIG = Register(208, device)
        self.CAN_MB_TIMEMARK_TRIGGER = Register(209, device)
        self.CAN_MB_FID = RegisterU32(210, device)
        self.CAN_MB_DATALOW = RegisterU32(212, device)
        self.CAN_MB_DATAHIGH = RegisterU32(214, device)
        self.CAN_MB_CONTROL = Register(216, device)
        self.CAN_MB_TIMEMARK = Register(217, device)
        self.UART_CONFIG = Register(221, device)
        self.UART_BAUD = RTypes.UartBaud(222, device)
        self.UART_TXFIFO = RegisterBytes(223, device)
        self.UART_RXFIFO = RegisterBytes(224, device)
        self.UART_STATUS = Register(225, device)
        self.I2C_CONFIG = Register(230, device)
        self.I2C_STATUS = Register(231, device)
        self.I2C_BUFFER = RegisterBytes(232, device)
        self.I2C_STREAM = RegisterBytes(233, device)
        self.I2C_STREAM_STATUS = RegisterU32(234, device)
        self.I2C_STREAM_INTERVAL = RegisterU32(236, device)
        self.CAN_ERRORSTREAM_CONTROL = Register(238, device)
        self.CAN_ERRORSTREAM = RegisterU32Array(240, device)
        self.UART485_CONFIG = Register(251, device)
        self.UART485_BAUD = RegisterU32(252, device)
        self.UART485_TXFIFO = RegisterBytes(254, device)
        self.UART485_RXFIFO = RegisterBytes(255, device)
        self.UART485_STATUS = Register(256, device)
        self.UART485_RTSDELAY = Register(257, device)
        self.ENDREG = Register(258, device)
        #/*%end_reginst%*/

    def __getitem__(self, addr):
        if type(addr) == int:
            addr = RegisterSet.Addresses[addr]
        return getattr(self, addr)


class Peripheral(object):
    """Base class for custom Device peripherals"""
    def __init__(self, device):
        self.dev = device
        """:type: Core"""
        self.regs = device.regs
        """:type: RegisterSet"""


class DutPower(Peripheral):
    """
    DUT (Device Under Test) Power

    DutPowers are available for the user as voltage supply rails for any devices
    that require +12V, +5V, +3.3V, or -12V supplies.
    """

    class CONTROL:
        ENABLE_12V = 1 << 0
        ENABLE_5V = 1 << 1
        ENABLE_3V3 = 1 << 2
        ENABLE_neg12V = 1 << 3
        FAULT_12V = 1 << 4
        FAULT_5V = 1 << 5
        FAULT_3V3 = 1 << 6
        # FAULT_neg12V = 1 << 7
        DISABLE_12V = 1 << 8
        DISABLE_5V = 1 << 9
        DISABLE_3V3 = 1 << 10
        DISABLE_neg12V = 1 << 11

    ILIMIT_0A25 = 0.25
    ILIMIT_0A75 = 0.75
    ILIMIT_1A0 = 1.0
    ILIMIT_1A5 = 1.5

    ILIMIT12V_0A5 = 0.5
    ILIMIT12V_1A4 = 1.4
    ILIMIT12V_2A5 = 2.5
    ILIMIT12V_3A2 = 3.2

    LATCH_OFF_FAULT = 0
    RETRY_ON_FAULT = 1

    ID_12V = 0
    ID_5V = 1
    ID_3V3 = 2
    ID_neg12V = 3

    ILIMIT_MIN_3V3 = 0.225
    ILIMIT_MIN_5V = 0.225
    ILIMIT_MIN_12V = 0.56
    ILIMIT_MAX_3V3 = 2.1
    ILIMIT_MAX_5V = 2.1
    ILIMIT_MAX_12V = 5

    def __init__(self, dev, dut_id):
        self._id = dut_id
        self.error_on_failure = False
        """If True, during (dis)engage procedures:
                -if a fault occurs or if the (dis)engage fails to verify expected status then an Exception is thrown"""
        Peripheral.__init__(self, dev)

    def _did_respond(self, on_not_off):
        status, faulted = self.get_status()
        return status == on_not_off, status, faulted

    def engage(self):
        """Engage the DutPower, also clear pre-existing faulted status

        :return: tuple(is_engaged, is_faulted)
        :rtype: (bool, bool)
        """
        self.regs.DUTPOWER_CONTROL.write(DutPower.CONTROL.ENABLE_12V << self._id)
        success, status, fault = do_for(0.05, self._did_respond, func_args_list=(True,), startdelay=0.025)
        if self.error_on_failure and (not success or fault):
            if fault:
                raise SubinitialException("DutPowerFault")
            if not success:
                raise SubinitialException("DutPower Failed: could not engage after 50ms")
        return status, fault

    def disengage(self):
        """Disengage the DutPower

        :return: tuple(is_engaged, is_faulted)
        :rtype: (bool, bool)
        """
        self.regs.DUTPOWER_CONTROL.write(DutPower.CONTROL.DISABLE_12V << self._id)
        success, status, fault = do_for(0.05, self._did_respond, func_args_list=(False,), startdelay=0.025)
        if self.error_on_failure and (not success or fault):
            if fault:
                raise SubinitialException("DutPowerFault")
            if not success:
                raise SubinitialException("DutPower Failed: could not disengage after 50ms")
        return status, fault

    def cmd(self, on_not_off):
        """Set on/off status

        :param on_not_off: True: enable dutpower and also resets fault status, False: disable dutpower
        :type on_not_off: bool
        """
        if on_not_off:
            self.engage()
        else:
            self.disengage()

    def get_status(self):
        """Get engaged status and fault status

        :return: tuple(is_engaged, is_faulted)
        :rtype: (bool, bool)
        """
        ctrl = self.regs.DUTPOWER_CONTROL.read()
        is_engaged = (ctrl & (DutPower.CONTROL.ENABLE_12V << self._id)) > 0
        is_faulted = (ctrl & (DutPower.CONTROL.FAULT_12V << self._id)) > 0
        return is_engaged, is_faulted

    def set_config(self, ilimit, retry_on_fault=False):
        """Set DutPower configuration parameters

        :param ilimit: current limit setpoint in amps. Accepts: 3.3V/5V range: 0.25 - 2.0. 12V range: 0.6 - 4.5.
        :type ilimit: float
        :param retry_on_fault: fault behavior configuration

            * True: continuously retry enabling DutPower on fault
            * False: on fault disable DutPower and latch disabled. Raises an RGBLED exception on the Stacks Core.
                    Color is yellow. 3 flashes for 3.3V, 5 flashes for 5V, 1 flash for 12V
                    Clear RGBLED exception by disengage() or engage() command.

        :type retry_on_fault: bool
        """
        cfg = self._i_to_counts(ilimit)
        if retry_on_fault:
            cfg |= 1 << 12
        self.regs[self.regs.DUTPOWER_CONFIG12V.address + self._id].write(cfg)

    def get_config(self):
        """Get DutPower configuration parameters

        :return: current limit setpoint in amps, retry_on_fault
        :rtype: (float, bool)
        """
        cfg = self.regs[self.regs.DUTPOWER_CONFIG12V.address + self._id].read()
        ilimit = self._counts_to_i(cfg & 0xFFF)
        return ilimit, (cfg >> 12) & 0xF  # (ilimit, mode)

    def get_current(self):
        """Get DutPower current. Current data on Core refreshed at about 10Hz

        :return: current, in amps
        :rtype: float
        """
        return self.regs[self.regs.DUTPOWER12V_CURRENT.address + self._id].read()

    def engage_inrush(self, retry_time=0.010):
        """WARNING: This method is subject to change in future library releases. It is for design reference only"""
        ilimit, mode = self.get_config()
        self.set_config(ilimit, retry_on_fault=True)
        self.engage()
        time.sleep(retry_time)
        self.set_config(ilimit, retry_on_fault=False)
        self.engage()

    def _i_to_counts(self, curr):
        """ Internal function, converts current in A to counts from 0-4095, clips output """
        if self._id == self.ID_12V:
            counts = int(((curr-self.ILIMIT_MIN_12V)/(self.ILIMIT_MAX_12V-self.ILIMIT_MIN_12V))*4095.0)
        elif self._id == self.ID_5V:
            counts = int(((curr-self.ILIMIT_MIN_5V)/(self.ILIMIT_MAX_5V-self.ILIMIT_MIN_5V))*4095.0)
        elif self._id == self.ID_3V3:
            counts = int(((curr-self.ILIMIT_MIN_3V3)/(self.ILIMIT_MAX_3V3-self.ILIMIT_MIN_3V3))*4095.0)
        else:
            return 0
        if counts > 4095:
            counts = 4095
        elif counts < 0:
            counts = 0
        return counts

    def _counts_to_i(self, counts):
        """ Internal function, converts current in A to counts from 0-4095, clips output """
        if self._id == self.ID_12V:
            curr = counts / 4095 * (self.ILIMIT_MAX_12V-self.ILIMIT_MIN_12V)+ self.ILIMIT_MIN_12V
            #counts = int(((I-self.ILIMIT_MIN_12V)/(self.ILIMIT_MAX_12V-self.ILIMIT_MIN_12V))*4095.0)
        elif self._id == self.ID_5V:
            curr = counts / 4095 * (self.ILIMIT_MAX_5V-self.ILIMIT_MIN_5V)+ self.ILIMIT_MIN_5V
        elif self._id == self.ID_3V3:
            curr = counts / 4095 * (self.ILIMIT_MAX_3V3-self.ILIMIT_MIN_3V3)+ self.ILIMIT_MIN_3V3
        else:
            return 0
        return curr



class Dio(Peripheral):
    """
    Digital Input Output
    """

    PIN_COUNT = 11
    PIN_MASK = 0x07FF

    PERIPH_DIO = 0
    PERIPH_A = 1
    PERIPH_B = 2
    PERIPH_C = 3
    PERIPH_D = 4
    PERIPH_ADC = 5

    PULL_NONE = 0
    PULL_UP = 1
    PULL_DOWN = 2
    PULL_NOCHANGE = 3

    INPUT = 1
    OUTPUT = 0

    def set_config(self, *pins, mask=0, peripheral=PERIPH_DIO,
                   direction=INPUT, pullupdown=PULL_NONE):
        """Configure DIO pins

        :param pins: arbitrary list of DIO pin indices. e.g. 0, 1
        :type pins: tuple[int]
        :param mask: optional bitmask of indices. e.g. 0b11
            See `Specifying Indices or Masks <http://subinitial.com/stacks/docs/indices>`_
        :type mask: int
        :param peripheral: selection from 0-5, peripheral mux selection. Refer to Dio.PERIPH_* options
        :type peripheral: int
        :param direction: 0: output, 1: input
        :type direction: int
        :param pullupdown: pin pull direction. Refer to Dio.PULL_* options
        :type pullupdown: int
        """
        pins = digest_indices(pins, mask, clearmask=self.PIN_MASK, return_mask_not_list=False)
        if len(pins) == 0:
            return
        cfgval = ((peripheral & 0b111) << 0) | ((direction & 0b1) << 3) | ((pullupdown & 0b11) << 4)

        self.dev.request.queue()
        for pin in pins:
            self.regs[self.regs.DIO0_CONFIG.address + pin].write(cfgval)
        self.dev.request.submit()

    def get_pin_config(self, pin):
        """Read DIO pin configuration

        :param pin: pin index
        :type pin: int
        :return: peripheral, direction, pullupdown
        :rtype: (int, int, int)
        """
        cfg = self.regs[self.regs.DIO0_CONFIG.address + pin].read()
        return (cfg >> 0) & 0b111,   (cfg >> 3) & 0b1, ((cfg >> 4) & 0b11)

    def get_status(self):
        """Read the input status of all DIO pins

        :return: 11-bit bitmask of DIO pin status (1: high, 0: low). MSB is DIO10 and LSB is DIO0
        :rtype: int | Bitmask
        """
        return Bitmask(self.regs.DIO_STATUS.read())

    def get_pin_status(self, pin):
        """Read the input status of a specified DIO pin

        :return: status of DIO pin status (True: high, False: low)
        :rtype: bool
        """
        return bool(self.get_status()[pin])

    def get_cmd(self):
        """Read the commanded output of all DIO pins

        :return: 11-bit bitmask of DIO pin command (1: high, 0: low). MSB is DIO10 and LSB is DIO0
        :rtype: int | Bitmask
        """
        return Bitmask(self.regs.DIO_CMD.read())

    def get_pin_cmd(self, pin):
        """Read the commanded output of a specified DIO pin

        :return: command of DIO pin (True: high, False: low)
        :rtype: bool
        """
        return bool(self.get_cmd()[pin])

    def cmd(self, *cmd, mask=0):
        """Set the DIO pins' output command to high state. (omitted pins will be commanded low)

        :param cmd: arbitrary list of DIO pin indices. e.g. 0, 1
        :type cmd: tuple[int]
        :param mask: optional bitmask of indices. e.g. 0b00100100000 for DIO8 and DIO5
            See `Specifying Indices or Masks <http://subinitial.com/stacks/docs/indices>`_
        :type mask: int
        """
        cmd = digest_indices(cmd, mask, clearmask=self.PIN_MASK)
        self.regs.DIO_CMD.write(cmd)

    def cmd_pin(self, pin, cmd):
        """Set the commanded output of the specified DIO pin

        :param pin: index of DIO pin
        :type pin: int
        :param cmd: commanded state of pin, 1: high, 0: low
        :type cmd: int
        """
        if cmd:
            self.set(pin)
        else:
            self.clear(pin)

    def set(self, *pins, mask=0):
        """Set the DIO pins' output command to high state

        :param pins: arbitrary list of DIO pin indices. e.g. 0, 1
        :type pins: tuple[int]
        :param mask: optional bitmask of indices. e.g. 0b00100100000 for DIO8 and DIO5
            See `Specifying Indices or Masks <http://subinitial.com/stacks/docs/indices>`_
        :type mask: int
        """
        pins = digest_indices(pins, mask, clearmask=self.PIN_MASK)
        self.regs.DIO_SET.write(pins)

    def clear(self, *pins, mask=0):
        """Clear the DIO pins' output command to low state

        :param pins: arbitrary list of DIO pin indices. e.g. 0, 1
        :type pins: tuple[int]
        :param mask: optional bitmask of indices. e.g. 0b00100100000 for DIO8 and DIO5
            See `Specifying Indices or Masks <http://subinitial.com/stacks/docs/indices>`_
        :type mask: int
        """
        pins = digest_indices(pins, mask, clearmask=self.PIN_MASK)
        self.regs.DIO_CLEAR.write(pins)


class RgbLed(Peripheral):
    """
    Red-Green-Blue LED
    """
    COLOR_RED = 0xFF000064
    COLOR_GREEN = 0x00FF0064
    COLOR_BLUE = 0x0000FF64
    COLOR_WHITE = 0xFFFFFF50
    COLOR_YELLOW = 0xFFAA0044
    COLOR_ORANGE = 0xFFFF0044
    COLOR_PURPLE = 0xFF00FF80

    def set(self, rgbb_val):
        """Set the RGB and Brightness value of the LED

        :param rgbb_val: 32 bit value where each octet represents MSB [Red, Green, Blue, Brightness] LSB
        :type rgbb_val: int
        """
        self.regs.RGBB.write(rgbb_val)

    def get(self):
        """Get the RGB and Brightness value of the LED

        :return: 32 bit value where each octet represents MSB [Red, Green, Blue, Brightness] LSB
        :rtype: int
        """
        return self.regs.RGBB.read()


class Power(Peripheral):
    """
    Power Supply Telemetry
    """
    # [r.POWERSUPPLY_CORE_TEMP] + \
    #     list(range(r.POWERSUPPLY_INPUT_VOLTAGE, r.POWERSUPPLY_NEG12V_VOLTAGE+1)) + \
    #     [r.POWERSUPPLY_MIN_CORE_TEMP] + \
    #     list(range(r.POWERSUPPLY_MIN_INPUT_VOLTAGE, r.POWERSUPPLY_MIN_NEG12V_VOLTAGE+1)) + \
    #     [r.POWERSUPPLY_MAX_CORE_TEMP] + \
    #     list(range(r.POWERSUPPLY_MAX_INPUT_VOLTAGE, r.POWERSUPPLY_MAX_NEG12V_VOLTAGE+1))
    #
    # def get_all_telemetry(self):
    #     """Read all powersupply and board telemetry data
    #
    #     :return: dictionary of power supply telemetry values by title
    #     :rtype: dict[str:float]
    #     """
    #     regs = self.deck.read(r.POWERSUPPLY_CORE_TEMP, r.POWERSUPPLY_MIN_CORE_TEMP, r.POWERSUPPLY_MAX_CORE_TEMP,
    #                           (r.POWERSUPPLY_INPUT_VOLTAGE, r.POWERSUPPLY_MAX_NEG12V_VOLTAGE))
    #     return {key: regs[key] for key in regs if type(key) is str and key.startswith("POWERSUPPLY")}
    #
    # @staticmethod
    # def get_all_telemetry_titles():
    #     """Get a list of power supply telemetry titles
    #
    #     :return: power supply telemetry titles
    #     :rtype: list[str]
    #     """
    #     return list(registernames[address] for address in Power._register_addresses)
    #
    # def print_all_telemetry(self):
    #     """Print telemetry data to stdout
    #
    #     :return: telemetry data in text form
    #     :rtype: str
    #     """
    #     description = self.deck.describe_dataset(
    #         self.deck.read(
    #             r.POWERSUPPLY_CORE_TEMP, r.POWERSUPPLY_MIN_CORE_TEMP, r.POWERSUPPLY_MAX_CORE_TEMP,
    #             (r.POWERSUPPLY_INPUT_VOLTAGE, r.POWERSUPPLY_MAX_NEG12V_VOLTAGE)))
    #     print(description)
    #     return description


    def get_input_voltage(self):
        """Read supply input voltage

        :return: supply input voltage in Volts
        :rtype: float
        """
        return self.regs.POWERSUPPLY_INPUT_VOLTAGE.read()

    def get_pmic_temperature(self):
        """Read power management IC temperature

        :return: power management IC temperature in Celsius
        :rtype: float
        """
        return self.regs.POWERSUPPLY_PMIC_TEMP.read()

    def get_12v_voltage(self):
        """Read 12V supply voltage

        :return: 12V supply voltage in Volts
        :rtype: float
        """
        return self.regs.POWERSUPPLY_12V_VOLTAGE.read()

    def get_12v_current(self):
        """Read 12V supply current

        :return: 12V supply current in Amps
        :rtype: float
        """
        return self.regs.POWERSUPPLY_12V_CURRENT.read()

    def get_12v_temperature(self):
        """Read 12V supply circuitry temperature

        :return: 12V supply circuitry temperature in Celsius
        :rtype: float
        """
        return self.regs.POWERSUPPLY_12V_TEMP.read()

    def get_5v_voltage(self):
        """Read 5V supply voltage

        :return: 5V supply voltage in Volts
        :rtype: float
        """
        return self.regs.POWERSUPPLY_5V_VOLTAGE.read()

    def get_3v3_voltage(self):
        """Read 3V3 supply voltage

        :return: 3V3 supply voltage in Volts
        :rtype: float
        """
        return self.regs.POWERSUPPLY_3V3_VOLTAGE.read()

    def get_3v3_current(self):
        """Read 3V3 supply current

        :return: 3V3 supply current in Amps
        :rtype: float
        """
        return self.regs.POWERSUPPLY_3V3_CURRENT.read()

    def get_3v3_temperature(self):
        """Read 3V3 supply circuitry temperature

        :return: 3V3 supply circuitry temperature in Celsius
        :rtype: float
        """
        return self.regs.POWERSUPPLY_3V3_TEMP.read()

    def get_3v3standby_voltage(self):
        """Read 3V3_STANDBY supply voltage

        :return: 3V3_STANDBY supply voltage in Volts
        :rtype: float
        """
        return self.regs.POWERSUPPLY_3V3STANDBY_VOLTAGE.read()

    def get_neg12v_voltage(self):
        """Read -12V supply voltage

        :return: -12V supply voltage in Volts
        :rtype: float
        """
        return self.regs.POWERSUPPLY_NEG12V_VOLTAGE.read()

    def get_core_temp(self):
        """Read Core micro-controller temperature

        :return: Core micro-controller temperature in Celsius
        :rtype: float
        """
        return self.regs.POWERSUPPLY_CORE_TEMP.read()

    def reset_all_telemetry(self):
        """Reset all powersupply telemetry max and min values"""
        self.dev.request.queue()
        self.regs.POWERSUPPLY_MIN_CORE_TEMP.write(0)
        self.regs.POWERSUPPLY_MAX_CORE_TEMP.write(0)
        count = self.regs.POWERSUPPLY_MAX_INPUT_VOLTAGE.address - self.regs.POWERSUPPLY_MIN_INPUT_VOLTAGE.address
        self.regs.POWERSUPPLY_MIN_INPUT_VOLTAGE.write(*([0]*count))
        self.dev.request.submit()

    def reset_telemetry(self, register_address):
        """Reset powersupply telemetry max and min values

        :param register_address: address of register to reset. (both MAX and MIN value registers will reset when \
            one or the other is reset)
        :type register_address: int
        """
        self.regs[register_address].write(0)


class Adc(Peripheral):
    """
    Analog To Digital Converter
    """
    CHANNEL_COUNT = 4
    _CHANNEL_MASK = 0xF
    # ADC channel to DIO number translation
    _CH0_DIO = 4
    _CH1_DIO = 0
    _CH2_DIO = 5
    _CH3_DIO = 1

    FULLSCALE_COUNTS = 65535
    HALFSCALE_COUNTS = FULLSCALE_COUNTS/2.0
    FULLSCALE_VOLTS = 2.5
    VOLTS_PER_COUNT = FULLSCALE_VOLTS / FULLSCALE_COUNTS

    def _counts_to_volts(self, channel_num, read, control):
        isdiff = control & (1 << (3 + (channel_num >> 2)))
        if isdiff:
            # return (counts - self.HALFSCALE_COUNTS) * self.VOLTS_PER_COUNT * 2.0
            return 2 * read - 2.5
        else:
            # return counts * self.VOLTS_PER_COUNT
            return read

    _channel_dios = [_CH0_DIO, _CH1_DIO, _CH2_DIO, _CH3_DIO]

    def enable(self, *channels, mask=0):
        """Enable ADC channel pins

        :param channels: arbitrary list of indices. e.g. 0, 1
        :type channels: tuple[int]
        :param mask: optional bitmask of indices. e.g. 0b1010 for ADC3 and ADC1
            See `Specifying Indices or Masks <http://subinitial.com/stacks/docs/indices>`_
        :type mask: int
        """
        channels = digest_indices(channels, mask, clearmask=Adc._CHANNEL_MASK, return_mask_not_list=False)
        for channel in channels:
            self.dev.dio.set_config(self._channel_dios[channel],
                                     peripheral=Dio.PERIPH_ADC,
                                     pullupdown=Dio.PULL_NOCHANGE)

    def disable(self, *channels, mask=0):
        """Disable ADC channel pins. (Set the pins to DIO mode)

        :param channels: arbitrary list of indices. e.g. 0, 1
        :type channels: tuple[int]
        :param mask: optional bitmask of indices. e.g. 0b1010 for ADC3 and ADC1
            See `Specifying Indices or Masks <http://subinitial.com/stacks/docs/indices>`_
        :type mask: int
        """
        channels = digest_indices(channels, mask, clearmask=Adc._CHANNEL_MASK, return_mask_not_list=False)
        for channel in channels:
            self.dev.dio.set_config(self._channel_dios[channel],
                                     peripheral=self.dev.dio.PERIPH_DIO,
                                     pullupdown=Dio.PULL_NOCHANGE)

    def get(self, channel):
        """Get measured ADC voltage at channel pin. Measures apx. 0.01V - 2.45V

        :param channel: channel index
        :type channel: int
        :return: voltage measured at ADC pin
        :rtype: float
        """
        with self.dev as (regs, r, control, res):
            res.read = regs[regs.ADC_CH0.address + channel].read()
            res.ctrl = regs.ADC_CONTROL.read()
        return self._counts_to_volts(channel, res.read, res.ctrl)

    def get_all(self):
        """Get measured ADC voltage at all channel pins. Measures apx. 0.01V - 2.45V

        :return: voltage measured at each ADC pin
        :rtype: (float, float, float, float)
        """
        res = self.dev.request.queue()
        res.reads = self.regs.ADC_CH0.read(self.CHANNEL_COUNT)
        res.ctrl = self.regs.ADC_CONTROL.read()
        self.dev.request.submit()
        return list(
            self._counts_to_volts(channel, res.reads[channel], res.ctrl)
            for channel in range(0, self.CHANNEL_COUNT))

    def get_min(self, channel):
        """Get minimum recorded measured ADC voltage at channel pin since last reset of minimum

        :param channel: channel index
        :type channel: int
        :return: minimum voltage measured at ADC pin
        :rtype: float
        """
        with self.dev as (regs, r, control, res):
            res.read = regs[regs.ADC_CH0_MIN.address + channel].read()
            res.ctrl = regs.ADC_CONTROL.read()
        return self._counts_to_volts(channel, res.read, res.ctrl)

    def get_all_min(self):
        """Get minimum recorded measured ADC voltage at all channel pins since last reset of minimum

        :return: minimum voltage measured at each ADC pin
        :rtype: (float, float, float, float)
        """
        with self.dev as (regs, r, control, res):
            res.reads = regs.ADC_CH0_MIN.read(self.CHANNEL_COUNT)
            res.ctrl = regs.ADC_CONTROL.read()
        return list(
            self._counts_to_volts(channel, res.reads[channel], res.ctrl)
            for channel in range(0, self.CHANNEL_COUNT)
        )

    def get_max(self, channel):
        """Get maximum recorded measured ADC voltage at channel pin since last reset of maximum

        :param channel: channel index
        :type channel: int
        :return: maximum voltage measured at ADC pin
        :rtype: float
        """
        with self.dev as (regs, r, control, res):
            res.read = regs[regs.ADC_CH0_MAX.address + channel].read()
            res.ctrl = regs.ADC_CONTROL.read()
        return self._counts_to_volts(channel, res.read, res.ctrl)

    def get_all_max(self):
        """Get maximum recorded measured ADC voltage at all channel pins since last reset of maximum

        :return: maximum voltage measured at each ADC pin
        :rtype: (float, float, float, float)
        """
        with self.dev as (regs, r, control, res):
            res.reads = regs.ADC_CH0_MAX.read(self.CHANNEL_COUNT)
            res.ctrl = regs.ADC_CONTROL.read()
        return list(
            self._counts_to_volts(channel, res.reads[channel], res.ctrl)
            for channel in range(0, self.CHANNEL_COUNT)
        )

    def reset_min(self, channel):
        """Reset recorded minimum measured ADC voltages at channel pin

        :param channel: channel index
        :type channel: int
        """
        self.regs[self.regs.ADC_CH0_MIN.address+channel].write(0)

    def reset_max(self, channel):
        """Reset recorded maximum measured ADC voltages at channel pin

        :param channel: channel index
        :type channel: int
        """
        self.regs[self.regs.ADC_CH0_MAX.address+channel].write(0)

    def reset_all_min(self):
        """Reset recorded minimum measured ADC voltages at all channel pins"""
        self.regs.ADC_CH0_MIN.write(0, 0, 0, 0)

    def reset_all_max(self):
        """Reset recorded maximum measured ADC voltages at all channel pins"""
        self.regs.ADC_CH0_MAX.write(0, 0, 0, 0)

    AVERAGE_NONE = 0
    AVERAGE_4 = 2
    AVERAGE_16 = 3
    AVERAGE_64 = 4
    AVERAGE_256 = 5

    def set_control(self, averaging=AVERAGE_256, diff_01=False, diff_23=False):
        ctrl = averaging & 0x7
        if diff_01:
            ctrl |= 0x8
        if diff_23:
            ctrl |= 0x10
        self.regs.ADC_CONTROL.write(ctrl)

    def get_control(self):
        return self.regs.ADC_CONTROL.read()


class Dac(Peripheral):
    """
    Digital To Analog Converter
    """
    CHANNEL_COUNT = 2
    CHANNEL_LEFT = 0
    CHANNEL_RIGHT = 1

    DIO_EXTERNALTRIGGER = 2

    _HALFSCALE_VOLTS = 3.3
    _HALFSCALE_COUNTS = 2047.5
    _FULLSCALE_COUNTS = 4095

    def _volts_to_counts(self, volts):
        return int(max(0, min(self._FULLSCALE_COUNTS,  # CLAMP between 0 and full-scale and convert to integer
                   round(volts / self._HALFSCALE_VOLTS * self._HALFSCALE_COUNTS + self._HALFSCALE_COUNTS, 0))))

    def _counts_to_volts(self, counts):
        return (counts - self._HALFSCALE_COUNTS) / self._HALFSCALE_COUNTS * self._HALFSCALE_VOLTS

    def set_dc(self, left=None, right=None):
        """Set the DAC DC left (DAC0) and right (DAC1) channel set voltages

        :param left: left channel (DAC0) DC setpoint in Volts, from -3.3 to +3.3. Pass None for no change.
        :type left: float | None
        :param right: right channel (DAC1) DC setpoint in Volts, from -3.3 to +3.3. Pass None for no change.
        :type right: float | None
        """
        if left is not None and right is not None:
            self.regs.DAC_DCLEFT.write(left, right)
        elif left is not None:
            self.regs.DAC_DCLEFT.write(left)
        elif right is not None:
            self.regs.DAC_DCRIGHT.write(right)

    def set_channel_dc(self, channel, volts):
        """Set the voltage specified to the DAC channel specified

        :param channel: Channel on which to set the "volts" parameter. 0 or CHANNEL_LEFT for left (DAC0), 1 or CHANNEL_RIGHT for right (DAC1)
        :type channel: int
        :param volts: DC setpoint in Volts, from -3.3 to +3.3
        :type volts: float
        """
        if channel == 0:
            self.set_dc(volts, None)
        elif channel == 1:
            self.set_dc(None, volts)
        else:
            raise SubinitialInputError("DAC channel parameter must be 0 or 1")

    def get_dc(self):
        """Read the DAC DC left (DAC0) and right (DAC1) channel set points

        :return: LeftCh (DAC0) Volts, RightCh (DAC1) Volts
        :rtype: (float, float)
        """
        return self.regs.DAC_DCLEFT.read(regcount=2)

    MODE_DC = 0
    MODE_WAVETRIGGERED = 1
    MODE_WAVEREPEAT = 2
    MODE_WAVETIMED = 3

    def set_control(self, mode=MODE_DC, dcbandwidth=0, wavebandwidth=1, exttrigger=False):
        """Set control options for the DAC

        :param mode: DAC operating mode. Refer to Dac.MODE_* options
        :type mode: int
        :param dcbandwidth: 0: low-bandwidth, 1: high-bandwidth when in DC operating mode
        :type dcbandwidth: int
        :param wavebandwidth: 0: low-bandwidth, 1: high-bandwidth when in waveform operating mode
        :type wavebandwidth: int
        :param exttrigger: configure the waveform to trigger each sample via True: external DAC pin, False: DAC samplerate
        :type exttrigger: bool
        """
        regval = 0
        regval |= mode & 0xF
        regval |= dcbandwidth << 4
        regval |= wavebandwidth << 5
        if exttrigger:
            regval |= 1 << 6
        self.regs.DAC_CONTROL.write(regval)

    class Status(object):
        def __init__(self, m, d, w, e, dsub):
            self.mode = m
            self.dc_bandwidth = d
            self.wave_bandwidth = w
            self.ext_trigger = e
            self.dsub_output = dsub

    def get_status(self):
        regval = self.regs.DAC_CONTROL.read()
        mode = regval & 0xF
        dcbandwidth = (regval >> 4) & 0x1
        wavebandwidth = (regval >> 5) & 0x1
        exttrigger = (regval >> 6) & 0x1
        dsub_output = ((regval >> 14) & 0x1) > 0
        return self.Status(mode, dcbandwidth, wavebandwidth, exttrigger, dsub_output)

    def enable_pin_externaltrigger(self):
        """Configure DAC external trigger pin to trigger mode (DIO-Pin:2, DA26-Pin:3)"""
        self.dev.dio.set_config(self.DIO_EXTERNALTRIGGER, peripheral=Dio.PERIPH_C)

    def disable_pin_externaltrigger(self):
        """Disable DAC external trigger pin to Dio mode (DIO-Pin:2, DA26-Pin:3)"""
        self.dev.dio.set_config(self.DIO_EXTERNALTRIGGER, peripheral=Dio.PERIPH_DIO)

    def enable_dsub_output(self):
        """Output DAC0 and DAC1 on CANH/DAC0 and CANL/DAC1 respectively. CANH/DAC0 and CANL/DAC1 no longer provide 
        CAN functionality"""
        self.regs.DAC_CONTROL.write(0xC000)
        time.sleep(0.025)  # wait for 25ms so request can propagate to Supervisor and read status

    def disable_dsub_output(self):
        """Disable output of DAC0 and DAC1 on CANH/DAC0 and CANL/DAC1, respectively. CANH/DAC0 and CANL/DAC1 now 
        provide CAN functionality"""
        self.regs.DAC_CONTROL.write(0x8000)
        time.sleep(0.025)  # wait for 25ms so request can propagate to Supervisor and read status

    WAVE_SAMPLECOUNT_MAX = 256

    def update_waveform(self, left_values=None, right_values=None, samplerate_hz=None, run_time_s=None):
        """Update the left (DAC0) and right (DAC1) channel arbitrary DAC waveform

        :param left_values: left channel (DAC0) sample values. Pass None for no change. Accepts up to 256 points.
        :type left_values: list[float] | None
        :param right_values: right channel (DAC1) sample values. Pass None for no change. Accepts up to 256 points.
        :type right_values: list[float] | None
        :param samplerate_hz: waveform samplerate in Hz. Pass None for no change. Accepts 0.01667 - 40000.
        :type samplerate_hz: float | None
        :param run_time_s: optional waveform runtime in seconds (the waveform will repeat for this
            duration when triggered). Pass None for no change. Accepts 0.001 - 65.
        :type run_time_s: float | None
        """
        if samplerate_hz is not None:
            sample_period_us = int(round(1e6 / samplerate_hz, 0))
            self.regs.DAC_WAVE_SAMPLEPERIOD.write(sample_period_us)

        if run_time_s is not None:
            run_time_ms = int(round(1000 * run_time_s))
            self.regs.DAC_WAVE_RUNTIME.write(run_time_ms)

        if left_values is not None:
            samplecount = min(len(left_values), self.WAVE_SAMPLECOUNT_MAX)
            # left_entries = list(self._volts_to_counts(value) for value in left_values[0:samplecount])
            with self.dev as (regs, r, control, res):
                regs.DAC_WAVE_SAMPLECOUNT.write(samplecount)
                regs.DAC_WAVE_SAMPLESINDEX.write(0)
                regs.DAC_WAVE_SAMPLES_LEFT.write(left_values[0:samplecount])
            # self.deck.write((r.DAC_WAVE_SAMPLECOUNT, samplecount), (r.DAC_WAVE_SAMPLESINDEX, 0),
            #                 (r.DAC_WAVE_SAMPLES_LEFT, left_entries))

        if right_values is not None:
            samplecount = min(len(right_values), self.WAVE_SAMPLECOUNT_MAX)
            # right_entries = list(self._volts_to_counts(value) for value in right_values[0:samplecount])
            # self.deck.write((r.DAC_WAVE_SAMPLECOUNT, samplecount), (r.DAC_WAVE_SAMPLESINDEX, 0),
            #                 (r.DAC_WAVE_SAMPLES_RIGHT, right_entries))
            with self.dev as (regs, r, control, res):
                regs.DAC_WAVE_SAMPLECOUNT.write(samplecount)
                regs.DAC_WAVE_SAMPLESINDEX.write(0)
                regs.DAC_WAVE_SAMPLES_RIGHT.write(right_values[0:samplecount])

    def reset(self):
        """Reset the DAC configuration to the default power-up state"""
        self.set_dc(0, 0)
        self.disable_dsub_output()
        self.set_control()
        self.update_waveform(left_values=[0], right_values=[0], samplerate_hz=1000)
        time.sleep(0.010)  # Settling time for the filter


class Pwm(Peripheral):
    """
    Pulse Width Modulator
    """

    CHANNEL_COUNT = 4
    # PMW-To-DA26 Dio Pin Translation
    CH_0 = 0
    CH_1 = 1
    CH_2 = 2
    CH_3 = 3
    CH_PWMH0 = 6
    CH_PWML0 = 5
    CH_PWML1 = 1
    CH_PWMH2 = 2
    CH_PWML3 = 7
    CH_PWMH3 = 4

    # _oc_to_hz = lambda valdict, regdesc: (valdict[regdesc.address] + (valdict[regdesc.address+1] << 16)) / 1000.0
    # _in_from_hz = lambda in_val, regdesc: stacks.uint32_to_shorts(int(round(in_val * 1000, 0)))
    #
    # _outconvert_to_percent = lambda valdict, regdesc: valdict[regdesc.address] / 32768.0
    # _inconvert_from_percent = lambda in_val, regdesc: [int(round(in_val * 32768.0, 0))]


    def _name_to_chnum(self, name):
        # converts so input can be a channel number or the provided channel names
        if name == self.CH_PWMH0 or name == self.CH_PWML0:
            return 0
        elif name == self.CH_PWML1:
            return 1
        elif name == self.CH_PWMH2:
            return 2
        elif name == self.CH_PWML3 or name == self.CH_PWMH3:
            return 3
        else:
            return name

    _chpin_peripherals = {
        CH_PWMH0: Dio.PERIPH_A,
        CH_PWML0: Dio.PERIPH_B,
        CH_PWML1: Dio.PERIPH_B,
        CH_PWMH2: Dio.PERIPH_A,
        CH_PWMH3: Dio.PERIPH_C,
        CH_PWML3: Dio.PERIPH_C
    }
    _chpins = [
        [CH_PWMH0, CH_PWML0],
        [CH_PWML1],
        [CH_PWMH2],
        [CH_PWML3, CH_PWMH3],
        [CH_PWMH3],
        [CH_PWML0],
        [CH_PWMH0],
        [CH_PWML3]
    ]

    def enable(self, *channels, mask=0):
        """Configure DIO pins as PWM outputs (Refer to CH_* constants for enable individual channel pins)

        :param channels: arbitrary list of indices. e.g. 0, 1, pwm.CH_PWMH0, pwm.CH_PWMH2
        :type channels: tuple[int]
        :param mask: optional bitmask of indices. e.g. 0b11
            See `Specifying Indices or Masks <http://subinitial.com/stacks/docs/indices>`_
        :type mask: int
        """
        for channel in digest_indices(channels, mask, clearmask=0xFF, return_mask_not_list=False):
            for pin in self._chpins[channel]:
                self.dev.dio.set_config(pin, peripheral=self._chpin_peripherals[pin])

    def disable(self, *channels, mask=0):
        """Configure PWM pins as DIO pins (Refer to CH_* constants for disabling individual channel pins)

        :param channels: arbitrary list of indices. e.g. 0, 1, pwm.CH_PWMH0, pwm.CH_PWMH2
        :type channels: tuple[int]
        :param mask: optional bitmask of indices. e.g. 0b11
            See `Specifying Indices or Masks <http://subinitial.com/stacks/docs/indices>`_
        :type mask: int
        """
        for channel in digest_indices(channels, mask, clearmask=0xFF, return_mask_not_list=False):
            for pin in self._chpins[channel]:
                self.dev.dio.set_config(pin, peripheral=Dio.PERIPH_DIO)

    def get_frequency(self, channel):
        """Get PWM channel frequency

        :param channel: channel index (0-3)
        :type channel: int
        :return: value in Hz from 1.0-4.2e6
        :rtype: float
        """
        channel = self._name_to_chnum(channel)
        # regname = "PWM_CH{0}_FREQUENCY".format(channel)
        return self.regs[self.regs.PWM_CH0_FREQUENCY.address+2*channel].read()

    def get_dutycycle(self, channel):
        """Get PWM channel dutycycle

        :param channel: channel index (0-3)
        :type channel: int
        :return: dutycycle from 0-1.0
        :rtype: float
        """
        channel = self._name_to_chnum(channel)
        return self.regs[self.regs.PWM_CH0_DUTY.address+channel].read()

    def set_frequency(self, channel, frequency):
        """Set PWM channel frequency

        :param channel: channel index (0-3)
        :type channel: int
        :param frequency: value in Hz from 1.0-4.2e6
        :type frequency: float
        """
        channel = self._name_to_chnum(channel)
        self.regs[self.regs.PWM_CH0_FREQUENCY.address+2*channel].write(frequency)

    def set_dutycycle(self, channel, dutycycle):
        """Set PWM channel dutycycle

        :param channel: channel index (0-3)
        :type channel: int
        :param dutycycle: value between 0-1.0
        :type dutycycle: float
        """
        channel = self._name_to_chnum(channel)
        self.regs[self.regs.PWM_CH0_DUTY.address+channel].write(dutycycle)


class Timer(Peripheral):
    """
    Event Timer / Frequency Counter
    """

    CHANNEL_COUNT = 2
    # TIO to DIO translation
    TIOA0 = 6
    TIOA1 = 7
    TCLK0 = 8

    # 3 bit width
    CLK_60M = 0
    CLK_15M = 1
    CLK_3M75 = 2
    CLK_937K5 = 3
    CLK_32K768 = 4

    # 1 bit width
    EDGE_RISING = 0
    EDGE_FALLING = 1

    _FREQUENCY_LIST = (60000000, 15000000, 3750000, 937500, 32768)

    def _channel_to_dio(self, channel):
        if channel == 0:
            return self.TIOA0
        elif channel == 1:
            return self.TIOA1
        else:
            return channel

    def _name_to_chnum(self, name):
        # converts so input can be a channel number or the provided channel names
        if (name == self.TIOA0) or (name == self.TCLK0):
            return 0
        elif name == self.TIOA1:
            return 1
        else:
            return name

    def _enable(self, channel):
        channel = self._channel_to_dio(channel)
        if channel == self.TIOA0:
            self.dev.dio.set_config(self.TIOA0, peripheral=self.dev.dio.PERIPH_B)  # PA0
        if channel == self.TIOA1:
            self.dev.dio.set_config(self.TIOA1, peripheral=self.dev.dio.PERIPH_B)  # PA15
        if channel == self.TCLK0:
            self.dev.dio.set_config(self.TCLK0, peripheral=self.dev.dio.PERIPH_B)  # PA4

    def _disable(self, channel):
        channel = self._channel_to_dio(channel)
        self.dev.dio.set_config(channel, peripheral=self.dev.dio.PERIPH_DIO)

    def enable_counter(self):
        self._enable(self.TCLK0)

    def disable_counter(self):
        self._disable(self.TCLK0)

    def restart(self, restart_clock=True, restart_counter=True):
        """Restarts the timer clock and counter values at 0

        :param restart_clock: True: clock value reset to 0, False: clock value not changed
        :type restart_clock: bool
        :param restart_counter: True: counter value reset to 0, False: counter value not changed
        :type restart_counter: bool
        """
        if restart_clock and restart_counter:
            self.regs.TIMER_COUNTER.write(0, 0)
        elif restart_clock:
            self.regs.TIMER_CLOCK.write(0)
        elif restart_counter:
            self.regs.TIMER_COUNTER.write(0)

    def get_clock_value(self):
        """Read present clock count value

        :return: timer clock's count value in counts
        :rtype: int
        """
        return self.regs.TIMER_CLOCK.read()

    def get_counter_value(self):
        """Read present event count value

        :return: timer counter's count value in counts
        :rtype: int
        """
        return self.regs.TIMER_COUNTER.read()

    def get_clock_and_counter_values(self):
        """Read both clock and counter values in the same instant

        :return: clock and counter values in counts
        :rtype: (int, int)
        """
        counter, clock = self.regs.TIMER_COUNTER.read(regcount=2)
        return clock, counter

    # _TRIGGER_EDGE_SHIFT = 0
    # _CLOCK_SPEED_SHIFT = 1
    # def get_config(self):
    #     regVal = self.deck.read(TIMER_CONFIG)
    #     trigger_edge = 0x0000001 & (regVal >> self._TRIGGER_EDGE_SHIFT)
    #     clock_config = 0x0000007 & (regVal >> self._CLOCK_SPEED_SHIFT)
    #     clock_speed = self.frequencies[clock_config]
    #     return clock_speed, trigger_edge
    #
    # def set_config(self, clock_speed=CLK_60M, trigger_edge=EDGE_RISING):
    #     regVal = ((clock_speed << self._CLOCK_SPEED_SHIFT) | (trigger_edge << self._TRIGGER_EDGE_SHIFT))
    #     self.deck.write(regVal, TIMER_CONFIG)

    def _get_minimum_counter_val(self, mincount):
        tclock, tcounter = self.get_clock_and_counter_values()
        return tcounter >= mincount, tclock, tcounter

    def measure_frequency(self, min_events=250, timeout=0.250):
        """Measure a frequency based on minimum events and a timeout

        :param min_events: minimum number of counter events
        :type min_events: int
        :param timeout: max time in seconds after which to calculate the measured frequency (no greater than 30s)
        :type timeout: float
        :return: measured frequency in Hz
        :rtype: float
        """
        timeout = min(timeout, 30)
        self.restart()
        notimeout, tclock, tcounter = do_for(timeout, self._get_minimum_counter_val, [min_events], repeatdelay=0.005)
        frequency = tcounter / (tclock / 60000000)
        return frequency


class Clock(Peripheral):
    """
    Programmable Clock (core.clock1 & core.clock2)
    """
    DIO_PCK1 = 4  # PA17
    DIO_PCK2 = 0  # PA18
    _channel_dios = {1: DIO_PCK1, 2: DIO_PCK2}

    HZ_NOCHANGE = 0
    HZ_OFF = 1
    HZ_30M = 2
    HZ_15M = 3
    HZ_12M = 4
    HZ_7M5 = 5
    HZ_6M = 6
    HZ_3M75 = 7
    HZ_3M = 8
    HZ_1M875 = 9
    HZ_1M5 = 10
    HZ_750K = 11
    HZ_375K = 12
    HZ_187K5 = 13
    HZ_32K768 = 14
    HZ_16K384 = 15
    HZ_8K192 = 16
    HZ_4K096 = 17
    HZ_2K048 = 18
    HZ_1K024 = 19
    HZ_0K512 = 20

    frequencies = (None, 0, 30000000, 15000000, 12000000, 7500000, 6000000, 3750000, 3000000, 1875000, 1500000,
                   750000, 375000, 187500, 32768, 16384, 8192, 4096, 2048, 1024, 512)

    def __init__(self, deck, channel):
        self.channel = channel
        super(Clock, self).__init__(deck)

    def enable(self):
        """Enable programmable clock pin. This disables other peripherals on the same pin."""
        self.dev.dio.set_config(Clock._channel_dios[self.channel], peripheral=Dio.PERIPH_B)

    def disable(self):
        """Disable programmable clock pin by switching the pin to Dio control."""
        self.dev.dio.set_config(Clock._channel_dios[self.channel], peripheral=Dio.PERIPH_DIO)

    def _mask_regval(self, regdata):
        if self.channel == 1:
            return (self.HZ_NOCHANGE << 8) | regdata
        else:
            return (regdata << 8) | self.HZ_NOCHANGE

    def set_config(self, frequency=HZ_NOCHANGE):
        """Set programmable clock frequency selection

        :param frequency: frequency selection, see Clock.frequencies
        :type frequency: int
        """
        # Prevent glitch
        regdata = self._mask_regval(self.HZ_OFF)
        self.regs.CLOCKS.write(regdata)

        # Set output frequency
        regdata = self._mask_regval(frequency)
        self.regs.CLOCKS.write(regdata)

    def get_config(self):
        """Get programmable clock frequency selection

        :return: frequency selection, see Clock.frequencies
        :rtype: int
        """
        regdata = self.regs.CLOCKS.read()
        if self.channel == 1:
            return 0x00FF & regdata
        elif self.channel == 2:
            return (0xFF00 & regdata) >> 8
        raise SubinitialInputError("Clock channel parameter must be 1 or 2")

    def get_frequency(self):
        """Get frequency of programmable clock in Hz

        :return: frequency in Hz
        :rtype: int
        """
        return self.frequencies[self.get_config()]


class CanErrorSample:
    def __init__(self, sample, timestamp, flags):
        self.rec = sample & 0xFF
        """Receive Error Counter"""
        self.tec = (sample >> 8) & 0xFF
        """Transmit Error Counter"""
        self.status = (sample >> 16) & 0xFFFF
        """CAN Error Status Flags"""
        self.timestamp = timestamp
        """:type: float"""
        self.overflow = flags & 0b1 > 0
        """Stream overflow boolean"""

    STATUS_ERROR_ACTIVE = 1 << 0
    STATUS_WARNING_LIMIT = 1 << 1
    STATUS_ERROR_PASSIVE = 1 << 2
    STATUS_BUS_OFF = 1 << 3
    STATUS_CRC_ERROR = 1 << 8
    STATUS_STUFFING_ERROR = 1 << 9
    STATUS_ACK_ERROR = 1 << 10
    STATUS_FORM_ERROR = 1 << 11
    STATUS_BIT_ERROR =  1 << 12

    def __repr__(self):
        return "time: {: .9f}, R/TEC: {:3}/{:3}, Status: {:6} {{{}}}".format(
            self.timestamp, self.rec, self.tec, hex(self.status), self.get_flags())

    @property
    def error_active(self):
        return self.status & self.STATUS_ERROR_ACTIVE > 0

    @property
    def error_warning(self):
        return self.status & self.STATUS_WARNING_LIMIT > 0

    @property
    def error_passive(self):
        return self.status & self.STATUS_ERROR_PASSIVE > 0

    @property
    def error_bus_off(self):
        return self.status & self.STATUS_BUS_OFF > 0

    @property
    def error_crc(self):
        return self.status & self.STATUS_CRC_ERROR > 0

    @property
    def error_stuffing(self):
        return self.status & self.STATUS_STUFFING_ERROR > 0

    @property
    def error_ack(self):
        return self.status & self.STATUS_ACK_ERROR > 0

    @property
    def error_form(self):
        return self.status & self.STATUS_FORM_ERROR > 0

    @property
    def error_bit(self):
        return self.status & self.STATUS_BIT_ERROR > 0

    def get_flags(self):
        flags = []
        if self.error_active:
            flags.append("BUS_ACTIVE")
        if self.error_warning:
            flags.append("WARNING_LIMIT")
        if self.error_passive:
            flags.append("BUS_PASSIVE")
        if self.error_bus_off:
            flags.append("BUS_OFF")
        if self.error_crc:
            flags.append("CRC_ERR")
        if self.error_stuffing:
            flags.append("STUFF_ERR")
        if self.error_ack:
            flags.append("ACK_ERR")
        if self.error_form:
            flags.append("FORM_ERR")
        if self.error_bit:
            flags.append("BIT_ERR")
        # if self.overflow:
        #     flags.append("STREAM_OVERFLOW")
        return "|".join(flags)

class Can(Peripheral):
    """
    Can Bus controller
    """
    # DeckRegister(r.CAN_CONFIG, "CAN controller configuration parameters")
    # DeckRegister(r.CAN_STATUS, "CAN status bits")
    # DeckRegister(r.CAN_BAUD, "CAN baudrate parameters", datatype=DataType.uint32)
    # DeckRegister(r.CAN_TXERRORCOUNT, "Count of receive errors on the CAN bus")
    # DeckRegister(r.CAN_RXERRORCOUNT, "Count of receive errors on the CAN bus")
    #
    # DeckRegister(r.CAN_MAILBOX_CONTROL, "CAN controller mailbox configuration bits")
    # DeckRegister(r.CAN_MAILBOX_SELECT, "CAN controller active mailbox selection index")
    #
    # DeckRegister(r.CAN_MB_RXMASK, "CAN Mailbox Receive Acceptance Mask", datatype=DataType.uint32)
    # DeckRegister(r.CAN_MB_MID, "CAN Mailbox Message Idk", datatype=DataType.uint32)
    # DeckRegister(r.CAN_MB_CONFIG, "CAN Mailbox configuration bits")
    # DeckRegister(r.CAN_MB_TIMEMARK_TRIGGER, "CAN Mailbox timemark trigger config")
    # DeckRegister(r.CAN_MB_FID, "CAN Mailbox received message Family ID", datatype=DataType.uint32)
    # DeckRegister(r.CAN_MB_DATALOW, "CAN Mailbox received message data bytes 0-3", datatype=DataType.uint32)
    # DeckRegister(r.CAN_MB_DATAHIGH, "CAN Mailbox received message data bytes 4-7", datatype=DataType.uint32)
    # DeckRegister(r.CAN_MB_CONTROL, "CAN Mailbox control bits")
    # DeckRegister(r.CAN_MB_TIMEMARK, "CAN Mailbox message timemark")

    _CAN_MODE_WRITE = 0 << 8
    _CAN_MODE_OR = 1 << 8

    MAILBOX_COUNT = 8

    def __init__(self, dev):
        super(Can, self).__init__(dev)
        self.mailbox = []
        """:type: list[Can.Mailbox]"""
        for i in range(0, Can.MAILBOX_COUNT):
            self.mailbox.append(Can.Mailbox(dev=dev, mailbox_idx=i))

    def enable_output(self):
        """Output CANH and CANL on CANH/DAC0 and CANL/DAC1, respectively. CANH/DAC0 and CANL/DAC1 no longer provide 
        DAC functionality"""
        self.regs.DAC_CONTROL.write(0x8000)
        time.sleep(0.025)  # wait for 25ms so request can propagate to Supervisor and read status

    def disable_output(self):
        """Disable output of CANH and CANL on CANH/DAC0 and CANL/DAC1, respectively. CANH/DAC0 and CANL/DAC1 now 
        provide DAC functionality"""
        self.regs.DAC_CONTROL.write(0xC000)
        time.sleep(0.025)  # wait for 25ms so request can propagate to Supervisor and read status

    def reset(self):
        """Reset the CAN controller state"""
        self.regs.CAN_CONFIG.write(256)

    def set_config(self, overload_frame=False, disable_repeat=False):
        """set CAN controller configuration parameters

        :param bool overload_frame: if True, an overload frame is generated after each successful reception for
            mailboxes configured in Receive with/without overwrite Mode, Producer, and Consumer.
        :param bool disable_repeat: if True, when a transmit mailbox loses bus arbitration, the transfer request is
            automatically aborted.
        """
        timestamp_sof_not_eof = False  # Timestamp start of frames for now
        time_triggered_mode = False  # Time stamping mode, not time triggered mailbox transfers
        timer_freeze = False  # Allow the timer to roll over from 0xFFFF to 0x0000 automatically
        low_power_mode = False  # don't enter low power mode when all pending messages have been transmitted

        regval = self._CAN_MODE_WRITE | (low_power_mode << 1) | (overload_frame << 3) | \
            (timestamp_sof_not_eof << 4) | (time_triggered_mode << 5) | (timer_freeze << 6) | (disable_repeat << 7)
        self.regs.CAN_CONFIG.write(regval)

    def disable(self):
        """Disables the internal CAN peripheral so it will not TX, RX, or ACK on the CAN bus"""
        self.regs.CAN_CONFIG.write(0)

    def get_config(self):
        """get CAN controller configuration parameters. Refer to :func:`set_config` for more details.

        :return: tuple(overload_frame, disable_repeat)
        :rtype: (bool, bool)
        """
        regval = self.regs.CAN_CONFIG.read()
        # low_power_mode = (regval & (1 << 1)) > 0
        overload_frame = (regval & (1 << 3)) > 0
        disable_repeat = (regval & (1 << 7)) > 0
        return overload_frame, disable_repeat

    # def auto_baud(self):
        # autobaud_mode = 1 << 2
        # self.deck.write(CAN_MODE, self._CAN_MODE_OR | autobaud_mode)

    def set_baudrate(self, baudrate_hz):
        """Set baudrate based on frequency using default segment values. A typical baudrate is 1e6

        :param float baudrate_hz: baudrate in Hz
        """
        baud = int(round(baudrate_hz / 1000.0, 0))
        baud_simple_flag = (1 << 31)
        self.regs.CAN_BAUD.write(baud_simple_flag | baud)

    def set_baudrate_details(self, baudrate_prescaler, sync_jump_width, propagation_segment,
                             phase1_segment, phase2_segment, oversample_x3):
        """Set detailed baudrate configuration parameters. For more details on these parameters see *The Configuration of the CAN Bit Timing* by Robert Bosch GmbH

        :param int baudrate_prescaler: specifies CAN system clock to determine the individual bit timing.

            *Input Range:* 2-128

            *Equation:* t_CSC (in seconds) = baudrate_prescaler / (120,000,000)

        :param int sync_jump_width: compensates for phase shifts between clock oscillators of different controllers
            on the bus. The Core's CAN controller must re-synchronize on any relevant signal edge of transmission.
            The sync_jump_width defines the maximum clock cycles a bit period may be shortened or lengthened by
            re-synchronization.

            *Input Range:* 0-3

            *Equation:* t_SJW (in seconds) = t_CSC * (sync_jump_width)

        :param int propagation_segment: compensates for physical delay times within the network

            *Input Range:* 1-8

            *Equation:* t_PRS (in seconds) = t_CSC * (propagation_segment)

        :param int phase1_segment: compensates for edge phase 1 error

            *Input Range:* 1-8

            *Equation:* t_PHS1 (in seconds) = t_CSC * (phase1_segment)

        :param int phase2_segment: compensates for edge phase 2 error

            *Input Range:* 2-8

            *Equation:* t_PHS2 (in seconds) = t_CSC * (phase2_segment)

        :param int oversample_x3: if 0, bit stream is sampled once. if 1, bit stream is sampled three times with a
            period of 8.3ns

        .. t_CSC replace:: tH\ :sub:`CSC`\

        """
        baudrate_prescaler = clamp(baudrate_prescaler, 2, 128) - 1
        sync_jump_width = clamp(sync_jump_width, 0, 3)
        propagation_segment = clamp(propagation_segment, 1, 8) - 1
        phase1_segment = clamp(phase1_segment, 1, 8) - 1
        phase2_segment = clamp(phase2_segment, 2, 8) - 1

        regval = (phase2_segment & 0b111) | ((phase1_segment & 0b111) << 4) | ((propagation_segment & 0b111) << 8) | \
                 ((sync_jump_width & 0b11) << 12) | ((baudrate_prescaler & 0b1111111) << 16) | \
                 ((oversample_x3 & 0b1) << 24)
        self.regs.CAN_BAUD.write(regval)

    def get_baudrate_details(self):
        """Get detailed baudrate configuration parameters. Refer to :func:`set_baudrate_details` parameter documentation for more info.

        :return: tuple of baudrate configuration values

            ========== ============== =============== =========== ====== ====== =============
              title    baud_prescaler sync_jump_width propagation phase1 phase2 oversample_x3
            ========== ============== =============== =========== ====== ====== =============
            **index:** 0              4               3           1      2      5
            **type:**  int            int             int         int    int    int
            **range:** 2-128          0-3             1-8         1-8    2-8    0 or 1
            ========== ============== =============== =========== ====== ====== =============
        :rtype: (int, int, int, int, int, int)
        """
        regval = self.regs.CAN_BAUD.read()
        phase2_segment = regval & 0b111
        phase1_segment = (regval >> 4) & 0b111
        propagation_segment = (regval >> 8) & 0b111
        sync_jump_width = (regval >> 12) & 0b11
        baudrate_prescaler = (regval >> 16) & 0b1111111
        oversample_x3 = (regval >> 24) & 0b1
        return baudrate_prescaler + 1, sync_jump_width, propagation_segment + 1, \
            phase1_segment + 1, phase2_segment + 1, oversample_x3

    def get_error_counters(self):
        """Get receieve and transmit error counts.

        :return: tuple(transmit errorcount, receive errorcount)
        :rtype: (int, int)
        """
        return self.regs.CAN_TXERRORCOUNT.read(regcount=2)

    def get_status(self):
        """Get CAN controller status bits.

        Bitmask Table:
        bit  0: error_active_mode
        bit  1: warning_limit
        bit  2: error_passive_mode
        bit  3: bus_off_mode
        bit  4: low_power_mode
        bit  5: wakeup
        bit  6: timer_overflow - (clear on read)
        bit  7: timestamp - (clear on read)
        bit  8: crc_error - (clear on read)
        bit  9: stuffing_error - (clear on read)
        bit 10: ack_error - (clear on read)
        bit 11: form_error - (clear on read)
        bit 12: bit_error - (clear on read)
        bit 13: receiver_busy
        bit 14: transmitter_busy
        bit 15: overload_busy

        :return: Bitmask of status bits
        :rtype: int | Bitmask
        """
        return Bitmask(self.regs.CAN_STATUS.read())

    def disable_all_mailboxes(self):
        """
        Disable all mailboxes from bus activity
        """
        self.dev.request.queue()
        for i in range(0, self.MAILBOX_COUNT):
            self.regs.CAN_MAILBOX_SELECT.write(i)
            self.regs.CAN_MB_CONFIG.write(0) # clear mode and mailbox priority
        self.dev.request.submit()

    # def get_all_mailbox_payloads(self):
    #     """Retrieves a list of the most recent payloads for all eight mailboxes.
    #
    #     :returns: list of payload objects for all eight mailboxes indexed 0 through 7
    #     :rtype: list(Can.Mailbox.Payload) | list(bytearray)
    #     """
    #     self.dev.request.queue()
    #     for i in range(0, self.MAILBOX_COUNT):
    #         self.regs.CAN_MAILBOX_SELECT.write(i)
    #         self.regs.CAN_MB_CONFIG.write(0) # clear mode and mailbox priority
    #     self.dev.request.submit()

    def trigger(self, *mailboxes, mask=0, reset_timer=False):
        """Triggeres transmission / next receive on specified mailbox indices

        :param mailboxes: arbitrary list of indices. e.g. 0, 1
        :type mailboxes: tuple[int]
        :param mask: optional bitmask of indices. e.g. 0b11
            See `Specifying Indices or Masks <http://subinitial.com/stacks/docs/indices>`_
        :type mask: int
        """
        ctrl = digest_indices(mailboxes, mask, clearmask=0xFF, return_mask_not_list=True)
        if reset_timer:
            ctrl |= 1 << 15
        self.regs.CAN_MAILBOX_CONTROL.write(ctrl)

    def abort(self, *mailboxes, mask=0, reset_timer=False):
        """Aborts transmission / next receive on specified mailbox indices

        :param mailboxes: arbitrary list of indices. e.g. 0, 1
        :type mailboxes: tuple[int]
        :param mask: optional bitmask of indices. e.g. 0b11
            See `Specifying Indices or Masks <http://subinitial.com/stacks/docs/indices>`_
        :type mask: int
        """
        ctrl = digest_indices(mailboxes, mask, clearmask=0xFF, return_mask_not_list=True)
        ctrl |= 1 << 9
        if reset_timer:
            ctrl |= 1 << 15
        self.regs.CAN_MAILBOX_CONTROL.write(ctrl)

    def errorstream_restart(self):
        """Resets and starts the Core CAN controller error stream capture process"""
        self.dev.regs.CAN_ERRORSTREAM_CONTROL.write(1)

    def errorstream_stop(self):
        """Stops the Core CAN controller error stream capture process"""
        self.dev.regs.CAN_ERRORSTREAM_CONTROL.write(0)

    def errorstream(self, raise_exception_on_overflow=False, timeout=5.0, pollingdelay=0.50, restart=True):
        """Yield error information as errors are detected on the CAN bus.

        :param raise_exception_on_overflow: raise a SubinitialResponseError if any buffer overflow occurs and some
            errorsamples were not able to be retrieved before being overwritten.
        :type raise_exception_on_overflow: bool

        :param timeout: timeout represents the maximum time to wait for the next error to be detected in seconds
            If this timeout is exceeded a SubinitialTimeoutError is raised.
        :type timeout: float

        :param pollingdelay: The polling delay represents the minimum time between reads for available buffered samples in seconds
        :type pollingdelay: int

        :return: CanErrorSample packets that indicate error flags and REC/TEC count values at the time errors were detected on the bus.
        :rtype: iter(CanErrorSample)
        """

        t_lastread = time.time()
        regs = self.dev.regs
        req = self.dev.request
        if restart:
            self.errorstream_restart()
        while True:
            res = req.queue()
            res.regs = regs.CAN_ERRORSTREAM.read(300)
            res.flags = regs.CAN_ERRORSTREAM_CONTROL.read()
            req.submit()

            samplecount = len(res.regs) if res.regs else 0
            if samplecount == 0:
                if timeout and time.time()-t_lastread > timeout:
                    # raise SubinitialTimeoutError("Can error stream timed out")
                    return
                time.sleep(pollingdelay)
                continue
            elif samplecount & 0b11:
                raise SubinitialResponseError("Can error stream returned fragmented response")
            elif res.flags & 0b1:
                if raise_exception_on_overflow:
                    raise SubinitialResponseError("Can error stream overflowed, flags: {}".format(res.flags))

            t_lastread = time.time()

            samplecount >>= 2
            idx = 0
            for i in range(0, samplecount):
                timestamp = res.regs[idx + 0] + (res.regs[idx + 1] << 32)
                counts = res.regs[idx+3]
                sample = (res.regs[idx+2] & 0xFFFF0000) + (counts & 0xFF) + \
                    ((counts >> 8) & 0xFF00)

                # print("timestamp", timestamp)
                timestamp *= 1e-12
                idx += 4
                yield CanErrorSample(sample, timestamp, res.flags)

            # Limit read frequency to a maximum of once every polling delay
            time.sleep(max(pollingdelay - (time.time() - t_lastread), 0))
        self.errorstream_stop()

    class Mailbox(object):
        """A CAN Mailbox that can be configured to independently transmit or receive messages

        .. Note:: The CAN controller contains 8 instances of :py:class:`Can.Mailbox` (indexed 0-7).
        """
        MODE_DISABLED = 0
        # Receive Modes
        MODE_RECEIVE = 1  # Mailbox receives first frame then is disabled until user reads the mailbox
        MODE_RECEIVE_WITH_OVERWRITE = 2  # Mailbox continues to receive frames and replaces existing frames
        MODE_CONSUMER = 4  # Mailbox sends remote frame, answer frame is received in this mailbox.
        # Transmit Modes
        MODE_TRANSMIT = 3  # Mailbox transmits frame
        MODE_PRODUCER = 5  # Mailbox responds to next remote frame by transmitting answer frame

        ACCEPTANCE_MASK_ID = 0x1FFFFFFF  # 29 bits work for extended and standard frame size
        ACCEPTANCE_MASK_PROMISCUOUS = 0

        def _startreq(self):
            res = self.dev.request.queue()
            self.dev.regs.CAN_MAILBOX_SELECT.write(self.mailbox_idx)
            return res

        def disable(self):
            """Disable this mailbox from bus activity."""
            self._startreq()
            self.regs.CAN_MB_CONFIG.write(0)
            self.dev.request.submit() # clear mode and mailbox priority

        def set_config(self, mode, message_id, acceptance_mask=ACCEPTANCE_MASK_ID, extended_frame=False,
                       mailbox_priority=0):
            """Set baudrate configuration parameters.
            For more details on these parameters see *The Configuration of the CAN Bit Timing* by Robert Bosch GmbH

            :param int mode: mailbox mode. See Mailbox.MODE_* constants for more options.

                :options:
                    *Mailbox.MODE_DISABLED* = 0

                    *Mailbox.MODE_RECEIVE* = 1

                    *Mailbox.MODE_RECEIVE_WITH_OVERWRITE* = 2

                    *Mailbox.MODE_TRANSMIT* = 3

                    *Mailbox.MODE_CONSUMER* = 4

                    *Mailbox.MODE_PRODUCER* = 5

            :param int message_id: CAN bus message id. Transmitted messages will use this identifier. Received messages
                must fit the acceptance criteria based on this message_id and the acceptance_mask parameter.
            :param int acceptance_mask: CAN bus receive bitmask used to filter receive message ids received in this
                mailbox. If the acceptance criteria is True below, the message will be received in this mailbox.

                **acceptance criteria** = mailbox_message_id & acceptance_mask == rx_message_id & acceptance_mask

                .. note:: The default value *Mailbox.ACCEPTANCE_MASK_ID* accepts messages exactly matching the message_id only.

                    The value *Mailbox.ACCEPTANCE_MASK_PROMISCUOUS* accepts any message_id.
            :param bool extended_frame: when False, use 11-bit message id, when True use 29-bit message id
            :param int mailbox_priority: if two mailboxes are configured to transmit at the same time, the mailbox with
                highest priority will transmit first. If mailboxes have the same priority, the lowest indexed mailbox
                will transmit first.

            """
            self._startreq()

            cfg = ((mode & 0b111) << 8) | (mailbox_priority & 0xF)
            self.regs.CAN_MB_CONFIG.write(cfg)  # set mode and mailbox transmit priority

            mam = acceptance_mask & self.ACCEPTANCE_MASK_ID
            mid = message_id & self.ACCEPTANCE_MASK_ID
            if extended_frame:
                mam |= 1 << 29
                mid |= 1 << 29
            else:
                mam = (mam & 0x7ff) << 18
                mid = (mid & 0x7ff) << 18
            self.regs.CAN_MB_RXMASK.write(mam)
            self.regs.CAN_MB_MID.write(mid)
            self.dev.request.submit()

        def get_config(self):
            """Get mailbox configuration parameters. Refer to :func:`set_config` for more details.

            :return: tuple(mode, message_id, acceptance_mask, extended_frame, mailbox_priority)
            :rtype: (int, int, int, bool, int)
            """
            res = self._startreq()
            res.cfg = self.regs.CAN_MB_CONFIG.read()
            res.mam = self.regs.CAN_MB_RXMASK.read()
            res.mid = self.regs.CAN_MB_MID.read()
            self.dev.request.submit()

            mode = (res.cfg >> 8) & 0b111
            mailbox_priority = res.cfg & 0xF

            acceptance_mask = res.mam & self.ACCEPTANCE_MASK_ID
            message_id = res.mid & self.ACCEPTANCE_MASK_ID
            extended_frame = res.mid & (1 << 29) == 1
            if not extended_frame:
                message_id >>= 18
                acceptance_mask >>= 18

            return mode, message_id, acceptance_mask, extended_frame, mailbox_priority

        def transmit(self, *payload, setup_only=False):  # , wait_for_trigger=False):
            """Transmit message of payload bytes from this mailbox.

            :param payload: payload data as a bytearray type, string, or variable length set of byte-sized integers
            :type payload: bytes | bytearray | str | list[int]
            """
            self._startreq()

            # Set the payload data
            if len(payload) > 0:
                payload_bytes = message_to_bytes(*payload)
                u32s = self.Payload(payload_bytes).get_u32array()

                self.regs.CAN_MB_DATALOW.write(u32s[0])
                if len(u32s) > 1:
                    self.regs.CAN_MB_DATAHIGH.write(u32s[1])
            else:
                payload_bytes = b''

            # Set payload data length and start the transfer
            ctrl = min(len(payload_bytes), 8)
            if not setup_only:
                ctrl |= 0x80  # MDLC | MTCR, message datalength | start transfer bit
            self.regs.CAN_MB_CONTROL.write(ctrl)
            self.dev.request.submit()

        class Payload(bytearray):
            """
            :py:class:`bytearray` type payload data bytes of a CAN Mailbox with additional status properties.

            :Properties:
                * **is_valid** *(bool)* - indicates that message was received and payload data is valid

                * **was_overflow** *(bool)* - inicates that some messages messages were ignored (multiple messages were received and only one was kept)
            """
            def _set_status(self, is_valid, was_overflow, family_id):
                self.is_valid = is_valid
                """:type: bool"""
                self.was_overflow = was_overflow
                """:type: bool"""
                self.family_id = Bitmask(family_id)
                """:type: int | Bitmask"""
                return self

            def get_u32array(self):
                """

                :return: representation of bytestring as list of little endian uint32 values
                """
                mylen = min(len(self), 8)
                acc = []
                idx = 0
                while True:
                    u32 = 0
                    for pos in range(0, 32, 8):
                        u32 |= self[idx] << pos
                        idx += 1
                        if idx >= mylen:
                            break
                    acc.append(u32)
                    if idx >= mylen:
                        break
                return acc

        @staticmethod
        def _regs_to_payload(cfg, tmtrig, fid, dlow, dhigh, control):
            # Parse received register data
            mode = (cfg >> 8) & 0b111

            family_id = fid

            ctrl = control
            payload_len = min(ctrl & 0x0F, 8)
            # is_remote_transmission_request = ctrl & (1 << 4) > 0
            # was_aborted = ctrl & (1 << 6)
            is_ready = (ctrl & (1 << 7)) > 0
            was_overflow = (ctrl & (1 << 8)) > 0

            is_valid = is_ready and \
                       (mode == Can.Mailbox.MODE_RECEIVE_WITH_OVERWRITE or
                        mode == Can.Mailbox.MODE_RECEIVE or
                        mode == Can.Mailbox.MODE_CONSUMER)

            datalow = Bitmask(dlow)
            datahigh = Bitmask(dhigh)
            databytes = datalow.to_uint8_array() + datahigh.to_uint8_array()

            return Can.Mailbox.Payload(databytes[0:payload_len])._set_status(
                family_id=family_id,
                is_valid=is_valid,
                was_overflow=was_overflow)

        def get_received(self):
            """Get received message in this mailbox if one has arrived.

            :return: payload data as bytearray with additional status properties
            :rtype: bytearray | Can.Mailbox.Payload
            """
            res = self._startreq()
            res.regs = self.regs.CAN_MB_CONFIG.read(regcount=9)
            self.dev.request.submit()
            return self._regs_to_payload(*res.regs)

        def trigger(self):
            """
            Trigger transmission / next receive on this mailbox
            """
            ctrl = (1 << self.mailbox_idx)
            self.regs.CAN_MAILBOX_CONTROL.write(ctrl)

        # def abort(self):
        #     """
        #     Aborts transmission / next receive on this mailbox
        #     """
        #     ctrl = (1 << self.mailbox_idx) | (1 << 9)
        #     self.deck.write(r.CAN_MAILBOX_CONTROL, ctrl)

        def __init__(self, dev, mailbox_idx):
            self.dev = dev
            """:type: Core"""
            self.regs = dev.regs
            self.mailbox_idx = mailbox_idx
            """:type: int"""


class Spi(Peripheral):
    """SPI Peripheral control object"""
    def __init__(self, dev):
        Peripheral.__init__(self, dev)
        self.channels = [None, self.dev.spi1, self.dev.spi2, self.dev.spi3]

    def __getitem__(self, item):
        """Gets spi by channel index

        :rtype: SpiChannel
        """
        if 1 > item > 3:
            raise SubinitialInputError("SPI Channel must be 1, 2, or 3")
        return self.channels[item]

    def enable(self):
        """Activate output of SPI lines SPI_MOSI, and SPI_CLK, and input of SPI_MISO"""
        self.regs.SPI_CONTROL.write(1)

    def disable(self):
        """Tri-state the SPI lines SPI_MOSI, SPI_CLK, and SPI_MISO"""
        self.regs.SPI_CONTROL.write(0)


class SpiChannel(Peripheral):
    """SPI Channel control object"""
    FIFO_LEN = 128

    pins = [None, (9, Dio.PERIPH_B), (10, Dio.PERIPH_B), (3, Dio.PERIPH_B)]

    def __init__(self, dev, cs):
        Peripheral.__init__(self, dev)
        self.cs = cs
        self._regcfg = self.regs[self.regs.SPI1_CONFIG.address+self.cs*2-2]
        self._regbuf = self.regs[self.regs.SPI1_BUFFER.address+self.cs-1]

    def enable(self):
        """Enable the SPI channel chip select pin muxing. Enable the SPI_MOSI and SPI_CLK lines as outputs."""
        pin, periph = self.pins[self.cs]
        self.dev.dio.set_config(pin, peripheral=periph)
        self.dev.spi.enable()

    def disable(self):
        """Disable the SPI channel chip select pin and resets the pin function to its default configuration.
        Does not effect SPI_MOSI, SPI_CLK, or SPI_MISO lines."""
        pin, periph = self.pins[self.cs]
        if pin == 9 or pin == 10:
            periph = Dio.PERIPH_A
        else:
            periph = Dio.PERIPH_DIO
        self.dev.dio.set_config(pin, peripheral=periph)

    def transmit(self, *message):
        """Transmit message of bytes.

        This will discard any existing data in the RX FIFO before transmitting.

        :param message: payload data as a bytearray type, string, or variable length set of byte-sized integers
        :type message: bytes | bytearray | str | list[int] | int
        """
        # with self.dev as (regs, r, control, res):
        #     regs.SPI_CHANNELSELECT.write(self.cs)
        self._regbuf.write(message_to_bytes(*message))

    def receive(self):
        """Get received data in the RX FIFO. Reading the FIFO clears the FIFO.

        :return: payload data as bytearray with additional status properties
        :rtype: bytearray
        """
        # with self.dev as (regs, r, control, res):
        #     regs.SPI_CHANNELSELECT.write(self.cs)
        #     res.data = regs.SPI_TXRXBUFFER.read(SpiChannel.FIFO_LEN)
        # return res.data
        return self._regbuf.read(SpiChannel.FIFO_LEN)

    def _rxblocking(self):
        rxdata = self.receive()
        return len(rxdata) > 0, rxdata

    def transmit_receive(self, *message):
        """Transmit message of bytes, and get received data in the RX FIFO after the transmission is complete.

        This will discard any existing data in the RX FIFO before transmitting.

        :param message: payload data as a bytearray type, string, or variable length set of byte-sized integers
        :type message: bytes | bytearray | str | list[int] | int
        :return: payload data as bytearray with additional status properties
        :rtype: bytearray
        """
        self.transmit(*message)
        valid, rxdata = do_for(1, self._rxblocking)
        if not valid:
            raise SubinitialTimeoutError("SpiChannel {} timed out waiting for transmit/receive".format(self.cs))
        return rxdata

    class Config:
        """SPI Channel Config Object"""

        def __init__(self):
            self.baudrate = 1e6
            """*(float)* sclk clockspeed / baudrate"""
            self.cpolarity = 0
            """*(int)* clock polarity"""
            self.ncphase = 0
            """*(int)* inverse clock phase"""
            self.bits_per_word = 8
            """*(int)* bits per word / transmission"""
            self.delay_before_sclk = 50e-9
            """*(float)* delay between asserting chip-select and sclk's first edge"""
            self.delay_between_words = 800e-9
            """*(float)* delay between each word transmitted on MOSI"""
            self.cs_strobe_each_word = False
            """*(bool)* if True, cs is de-asserted for a short duration between each word transmitted on MOSI"""

        def to_reg(self):
            bits_per_word = min(max(self.bits_per_word-8, 0), 8)
            bauddiv = int(round(min(max(120e6 / self.baudrate, 3), 255)))
            delay_before_sclk = int(round(min(max(self.delay_before_sclk * 120e6, 0), 255)))
            delay_between_words = int(round(min(max(self.delay_between_words * 120e6 / 32, 0), 255)))

            return (delay_between_words << 24) | (delay_before_sclk << 16) | \
              (bauddiv << 8) | (bits_per_word << 4) | (int(self.cs_strobe_each_word) << 2) | \
              (self.ncphase << 1) | self.cpolarity

        def from_reg(self, val):
            self.delay_between_words = ((val >> 24) & 0xFF) * 32 / 120e6
            self.delay_before_sclk = min(((val >> 16) & 0xFF) / 120e6, 4e-9)
            self.baudrate = ((val >> 8) & 0xFF) * 120e6
            self.bits_per_word = ((val >> 4) & 0xF) + 8
            self.cs_strobe_each_word = bool(val & 4)
            self.ncphase = (val >> 1) & 1
            self.cpolarity = val & 1

    def set_config(self, baudrate=1e6, cpolarity=0, ncphase=0, bits_per_word=8,
                   delay_before_sclk=1000e-9, delay_between_words=800e-9,
                   cs_strobe_each_word=False):
        """ Configures a user SPI Channel

        :param cpolarity: Controls the active/inactive state of SPCK (SPI Clock Pin)
            0 -> SPCK Active High
            1 -> SPCK Active Low

        :param ncphase: Controls which edge of SPCK is used to change vs capture data
            0 -> Data is changed on leading edge, captured on following edge of SPCK (SPI Clock Pin)
            1 -> Data is captured on leading edge, changed on following edge of SPCK

        :param bits_per_word: Controls how many bits per word transfer on the bus. Min: 8, Max 16
        :param baudrate: Sets the SPI baudrate in Hz. Min: 471kHz, Max: 10MHz
        :param delay_before_sclk: Sets delay between CS first asserted and SCLK in seconds. Min: 4ns, Max: 2.12us
        :param delay_between_words: Sets delay between consecutive word transfers in seconds. Min: 0s, Max: 68us
        :param cs_strobe_each_word: CS rises after each word
        """
        cfg = self.Config()
        cfg.cpolarity = cpolarity
        cfg.ncphase = ncphase
        cfg.bits_per_word  =bits_per_word
        cfg.baudrate = baudrate
        cfg.delay_before_sclk = delay_before_sclk
        cfg.delay_between_words = delay_between_words
        cfg.cs_strobe_each_word = cs_strobe_each_word
        # cfg.cs_always_asserted = cs_always_asserted
        # with self.dev as (regs, r, control, res):
        #     regs.SPI_CHANNELSELECT.write(self.cs)
        #     res.data = regs.SPI_CHANNELCONFIG.write(cfg.to_reg())
        self._regcfg.write(cfg.to_reg())

    def get_config(self):
        """Get spi channel configuration settings
        
        :return: Configuration of SPI
        :rtype: SpiChannel.Config
        """
        cfg = self.Config()
        # with self.dev as (regs, r, control, res):
        #     regs.SPI_CHANNELSELECT.write(self.cs)
        #     res.cfg = regs.SPI_CHANNELCONFIG.read()
        return cfg.from_reg(self._regcfg.read())


class Uart(Peripheral):
    """Universal Asynchronous Receiver/Transmitter Controller"""

    FIFO_LEN = 128

    def enable(self):
        """Enable the UART pins"""
        self.regs.UART_CONFIG.write(0x8A00)
        self.dev.dio.set_config(9, 10, peripheral=Dio.PERIPH_A)

    def disable(self):
        """Disable the UART pins"""
        self.dev.dio.set_config(9, 10, peripheral=Dio.PERIPH_DIO)
        self.regs.UART_CONFIG.write(0x0A00)

    def get_status(self):
        """Get UART status bits.

        Bitmask Table:
        bit 0: error_active mode

        :return: Bitmask of status bits
        :rtype: int | Bitmask
        """
        self.regs.UART_STATUS.read()

    PARITY_EVEN = 0
    PARITY_ODD = 1
    PARITY_0 = 2
    PARITY_1 = 3
    PARITY_NONE = 4
    PARITY_NOCHANGE = 5

    def set_config(self, baudrate=115200, parity=PARITY_NONE, cts_dio=None, enabled=True):
        """Set UART configuration.
        
        :param baudrate: UART baudrate
        :type baudrate: int
        :param parity: parity selection, see Uart.PARITY_* for options
        :type parity: int
        :param cts_dio: DIO pin to use to output CTS for RS485 transceivers.
        :type cts_dio: int
        """
        cfg = (0b111 & parity) << 9
        if cts_dio is not None:
            cfg |= 0xF & (cts_dio + 1)
        if enabled:
            cfg |= 0x8000
        self.dev.request.queue()
        self.regs.UART_CONFIG.write(cfg)
        self.regs.UART_BAUD.write(baudrate)
        self.dev.request.submit()

    def get_config(self):
        """Get UART configuration.

        :return: (baudrate, parity)
        :rtype: (int, int)
        """
        cfg, baudrate = self.regs.UART_CONFIG.read(regcount=2)
        parity = (cfg >> 9) & 0b111
        enabled = cfg & 0x8000 > 0

        return enabled, baudrate, parity

    class Payload(bytearray):
        """
        :py:class:`bytearray` type payload data bytes of a UART RX FIFO with additional status properties.

        :Properties:
            * **frame_error** *(bool)* - indicates a framing error occured while receiving this data.

            * **overrun_error** *(bool)* - inicates an overrun error occured while receiving this data. Overrun data was discarded.

            * **parity_error** *(bool)* - inicates a parity error occured while receiving this data.

            * **is_error** *(bool)* - inicates any of frame, overrun, or parity error occured while receiving this data.
        """
        def _set_status(self, status_bits):
            self.length = status_bits & 0x3FF
            self.frame_error = (status_bits & 0x8000) > 0
            """:type: bool"""
            self.overrun_error = (status_bits & 0x4000) > 0
            """:type: bool"""
            self.parity_error = (status_bits & 0x2000) > 0
            """:type: bool"""
            self.is_error = (status_bits & 0xE000) > 0
            """:type: bool"""
            return self

    def transmit(self, *message):
        """Transmit message of bytes. Starts transmitting message but does not wait for transmission to complete.

        :param message: payload data as a bytearray type, string, or variable length set of byte-sized integers
        :type message: bytes | bytearray | str | list[int]
        """
        self.regs.UART_TXFIFO.write(message_to_bytes(*message))
        # self.deck.write(r.UART_TXFIFO, payload_words)
        # return self.deck.read(r.UART_TXFIFO)[r.UART_TXFIFO] == 0

    def receive(self):
        """Get received data in the RX FIFO. Reading the FIFO clears the FIFO.

        :return: payload data as bytearray with additional status properties
        :rtype: bytearray | Uart.Payload
        """
        rxdata = self.regs.UART_RXFIFO.read(Uart.FIFO_LEN)
        # regs = self.deck.read((r.UART_RXFIFO, r.UART_RXFIFO+Uart.FIFO_LEN))
        # status = regs[r.UART_RXFIFO]
        # acc = []
        # try:
        #     for i in range(r.UART_RXFIFO+1, r.UART_RXFIFO+Uart.FIFO_LEN):
        #         val = regs[i]
        #         acc.append(val & 0xff)
        #         acc.append((val >> 8) & 0xff)
        # except KeyError:
        #     pass
        # length = status & 0x3FF
        return Uart.Payload(rxdata)
        # return Uart.Payload(acc[0:length])._set_status(status)

    def receive_blocking(self):
        """DEPRECATED: Wait for then return received data in the RX FIFO.

        :return: payload data as bytearray with additional status properties
        :rtype: bytearray | Uart.Payload
        """
        rxdata = self.receive()
        while rxdata.length == 0:
            rxdata = self.receive()
            time.sleep(0.005)

        return rxdata

    def on_receive(self, handler):
        """DISABLED: Register a callback function to handle Uart receive data asynchronously.

        :param handler: callback function
        :type handler: callable(bytearray | Uart.Payload)
        """
        raise Exception("Uart.on_receive is no longer supported in subinitail.stacks")
        # self.deck._network_connection.register_handler(self.deck.deck_id, r.UART_RXFIFO, handler)

    def unregister_on_receive(self, handler):
        """DISABLED: Unregister a callback function previously registered to receive data asynchronously.

        :param handler: callback function
        :type handler: callable(bytearray | Uart.Payload)
        """
        raise Exception("Uart.unregister_on_receive is no longer supported in subinitail.stacks")
        # self.deck._network_connection.unregister_handler(self.deck.deck_id, r.UART_RXFIFO, handler)


class Uart485(Peripheral):
    """RS485 Universal Asynchronous Receiver/Transmitter Controller"""

    FIFO_LEN = 128

    # def enable(self):
    #     """Enable the UART pins"""
    #     self.regs.UART485_CONFIG.write(0x8A00)

    def disable(self):
        """Disable the User controlled RS485 UART"""
        self.regs.UART485_CONFIG.write(0)

    def get_status(self):
        """Get UART status bits.

        Bitmask Table:
        bit 0: error_active mode

        :return: Bitmask of status bits
        :rtype: int | Bitmask
        """
        self.regs.UART485_STATUS.read()

    PARITY_EVEN = 0
    PARITY_ODD = 1
    PARITY_0 = 2
    PARITY_1 = 3
    PARITY_NONE = 4
    PARITY_NOCHANGE = 5

    STOPBITS_1 = 0
    STOPBITS_1_5 = 1
    STOPBITS_2 = 2

    def set_config(self, baudrate=1000000, parity=PARITY_NONE, stopbits=STOPBITS_1, # rts_dio=None, rts_polarity=False,
                   enabled=True):
        """Set RS485 UART configuration.

        :param baudrate: UART baudrate between 230Hz and 15MHz
        :type baudrate: int
        :param parity: parity selection, see Uart485.PARITY_* for options
        :type parity: int
        :param stopbits: stopbits selection, see Uart485.STOPBITS_* for options
        :type stopbits: int
        # :param rts_dio: DIO pin to use to output RTS for RS485 transceivers, use None for no automatic RTS pin drive.
        # :type rts_dio: int
        # :param rts_polarity: RTS polarity, if False (active low) RTS will be driven low when transmitting,
        #     if True (active high) RTS will be driven high when transmitting
        # :type rts_polarity: bool
        :param enabled: if True the RS485 output is enabled for user control, if False the RS485 is used for
            communicating with RS485 based Tracks products.
        :type enabled: bool
        """
        cfg = ((0b111 & parity) << 9) | ((0b11 & stopbits) << 12)

        rts_dio = None
        rts_polarity = False

        if rts_dio is not None:
            cfg |= 0xF & rts_dio
        else:
            cfg |= 0xF
        if enabled:
            cfg |= 0x8000
        if rts_polarity:
            cfg |= 0x10
        self.dev.request.queue()
        self.regs.UART485_CONFIG.write(cfg)
        self.regs.UART485_BAUD.write(int(baudrate))
        self.dev.request.submit()

    def get_config(self):
        """Get RS485 UART configuration.

        :return: {baudrate: int, parity: int, rts_dio: int or None, rts_polarity: bool, enabled: bool}
        :rtype: dict(string: int)
        """
        cfg, baudrate = self.regs.UART485_CONFIG.read(regcount=3)
        parity = (cfg >> 9) & 0b111
        enabled = cfg & 0x8000 > 0
        rts_polarity = cfg & 0x100 > 0
        rts_dio = cfg & 0xF
        if rts_dio > 10:
            rts_dio = None

        return {"enabled": enabled, "baudrate": baudrate, "parity": parity,
                "rts_dio": rts_dio, "rts_polarity": rts_polarity}

    def transmit(self, *message):
        """Transmit message of bytes.

        :param message: payload data as a bytearray type, string, or variable length set of byte-sized integers
        :type message: bytes | bytearray | str | list[int]
        """
        self.regs.UART485_TXFIFO.write(message_to_bytes(*message))
        # self.deck.write(r.UART_TXFIFO, payload_words)
        # return self.deck.read(r.UART_TXFIFO)[r.UART_TXFIFO] == 0

    def receive(self):
        """Get received data in the RX FIFO. Reading the FIFO clears the FIFO.

        :return: payload data as bytearray with additional status properties
        :rtype: bytearray | Uart.Payload
        """
        rxdata = self.regs.UART485_RXFIFO.read(Uart485.FIFO_LEN)
        # regs = self.deck.read((r.UART_RXFIFO, r.UART_RXFIFO+Uart.FIFO_LEN))
        # status = regs[r.UART_RXFIFO]
        # acc = []
        # try:
        #     for i in range(r.UART_RXFIFO+1, r.UART_RXFIFO+Uart.FIFO_LEN):
        #         val = regs[i]
        #         acc.append(val & 0xff)
        #         acc.append((val >> 8) & 0xff)
        # except KeyError:
        #     pass
        # length = status & 0x3FF
        return Uart.Payload(rxdata)
        # return Uart.Payload(acc[0:length])._set_status(status)


class I2C(Peripheral):
    """I2C Master Controller"""
    SPEED_100KHZ = 0
    SPEED_400KHZ = 1
    _TWCK0_DIO = 8
    _TWD0_DIO = 3

    BUFFERSIZE = 200

    def enable(self, pullup=True):
        """Enable the I2C pins to be used for I2C communication.

        :param pullup: True enables the internal pull-ups on these pins, false disables pull-ups.
        :type pullup: bool
        :return: None
        """
        if pullup:
            self.regs.I2C_CONFIG.write(0x8000 | 1)  # Enables pull-ups
        else:
            self.regs.I2C_CONFIG.write(0x8000 | 0)  # Disables pull-ups

        self.dev.dio.set_config(self._TWCK0_DIO, peripheral=Dio.PERIPH_A)
        self.dev.dio.set_config(self._TWD0_DIO, peripheral=Dio.PERIPH_A)

    def disable(self):
        """Disable the I2C pins and return them to DIO pins. Also disables the pull-ups.

        :return: None
        """
        self.dev.dio.set_config(self._TWCK0_DIO, peripheral=Dio.PERIPH_DIO)
        self.dev.dio.set_config(self._TWD0_DIO, peripheral=Dio.PERIPH_DIO)
        self.regs.I2C_CONFIG.write(0x8000 | 0)  # Disables pull-ups

    @staticmethod
    def _writereq(device_id, data, command=0, command_size=0,
                  speed=SPEED_400KHZ, use10bit_device_id=False):
        dev = device_id & 0x3FF if use10bit_device_id else device_id & 0x7F
        flag0 = (int(use10bit_device_id) << 10) | (1 << 11) | (speed << 13) | (command_size << 14)
        return struct.pack("<II", dev | flag0 | (len(data) << 24), command)[:4+command_size] + data

    @staticmethod
    def _readreq(device_id, length, command=0, command_size=0,
                 speed=SPEED_400KHZ, use10bit_device_id=False):
        dev = device_id & 0x3FF if use10bit_device_id else device_id & 0x7F
        flag0 = (int(use10bit_device_id) << 10) | (0b11 << 11) | (speed << 13) | (command_size << 14)
        return struct.pack("<II", dev | flag0 | (length << 24), command)[:4+command_size]

    class Packet(bytearray):
        """ I2C Transmission Packet (both Tx and Rx)
        
        :Attributes:
        
            * **device_id** *(int)* - I2C slave device id that this packet was addressed to

            * **command** *(int)* - SMBus command value
            
            * **command_size** *(int)* - SMBus command size in bytes
            
            * **flags** *(int)* - bitmask of error flags, refer to I2C.Packet.FLAGS_* for flag types
            
            * **speed** *(int)* - bus speed selection
            
            * **master_not_slave** *(bool)* - if True this packet was transmitted with the Core as I2C mater, if False core was I2C slave
            
            * **read_not_write** *(bool)* - if True this packet read data from the device, if False this packet wrote data to the device
            
            * **timestamp** *(float)* - timestamp of when this packet began transmitting in (s) since the Core was reset
            
        """

        FLAGS_NACK = 1 << 0
        """NACK was detected on the I2C bus during this packet"""
        FLAGS_BUSBUSY = 1 << 1
        """Packet failed due to bus being busy"""
        FLAGS_OVERRUN = 1 << 2
        """Packet failed due to data overrun"""
        FLAGS_TIMEOUT = 1 << 3
        """Packet failed due to device timeout"""
        FLAGS_PINCONFIG = 1 << 4
        """Packet failed due to Core's i2c pins muxed incorrectly"""
        FLAGS_SUCCESS = 0  # 1 << 4
        """Packet succeeded, no error flags were detected"""

        @classmethod
        def from_response(cls, core, resp, cursor, timestamp=0):
            if len(resp) < 4:
                return None
            # 4Byte Header
            hdr = struct.unpack_from("<I", resp, cursor)[0]
            use10bit_device_id = bool(hdr & (1 << 10))
            master_not_slave = bool(hdr & (1 << 11))
            read_not_write = bool(hdr & (1 << 12))
            speed = hdr & (1 << 13)
            command_size = (hdr >> 14) & 3
            device_id = hdr & 0x3FF if use10bit_device_id else hdr & 0x7F
            flags = (hdr >> 16) & 0x1F
            datalen = (hdr >> 24) & 0xFF
            cursor += 4

            if flags & cls.FLAGS_TIMEOUT:
                # suspicious, check the Core's pin muxing
                if core.dio.get_pin_config(3)[0] != 1 or core.dio.get_pin_config(8)[0] != 1:
                    flags = cls.FLAGS_PINCONFIG

            # Optional Command
            if command_size == 1:
                command = struct.unpack_from("B", resp, cursor)[0]
                cursor += 1
            elif command_size == 2:
                command = struct.unpack_from("<H", resp, cursor)[0]
                cursor += 2
            elif command_size == 3:
                hword, hibyte = struct.unpack_from("<HB", resp, cursor)
                command = hibyte << 16 + hword
                cursor += 3
            else:
                command = 0

            # Payload
            data = struct.unpack_from(str(datalen)+"s", resp, cursor)[0]
            packet = I2C.Packet(data)
            packet._set_stats(device_id, command, command_size, flags, speed, master_not_slave, read_not_write, timestamp)
            return packet, cursor + datalen

        def _set_stats(self, device_id, command, command_size, flags, speed, master_not_slave, read_not_write, timestamp):
            self.device_id = device_id
            self.command = command
            self.command_size = command_size
            self.flags = flags
            self.speed = speed
            self.master_not_slave = master_not_slave
            self.read_not_write = read_not_write
            self.timestamp = timestamp / 1000

        def __str__(self):
            return self.__repr__()

        def __repr__(self):
            stats = list()
            stats.append("I2CMaster" if self.master_not_slave else "I2CSlave")
            stats.append("Dev:" + hex(self.device_id))

            stats.append("Read" if self.read_not_write else "Write")

            if self.command_size:
                stats.append("Cmd:" + str(self.command))

            if len(self) > 10:
                stats.append(bytearray.__repr__(self)[:10] + "...")
            else:
                stats.append(bytearray.__repr__(self))

            if self.flags != self.FLAGS_SUCCESS:
                flagstats = []
                if self.flags & self.FLAGS_NACK:
                    flagstats.append("Nack")
                if self.flags & self.FLAGS_BUSBUSY:
                    flagstats.append("Busy")
                if self.flags & self.FLAGS_OVERRUN:
                    flagstats.append("Overrun")
                if self.flags & self.FLAGS_TIMEOUT:
                    flagstats.append("Timeout")
                if self.flags & self.FLAGS_PINCONFIG:
                    flagstats.append("Core DIO PinMux 3 or 8 misconfigured")
                stats.append("Error({})".format("|".join(flagstats)))
            return " ".join(stats)

    def write(self, device_id, *data, command=0, command_size=0, speed=SPEED_400KHZ, use10bit_device_id=False):
        """ Write packet to slave as master
        
        :param device_id:  I2C device bus identifier (slave id)
        :type device_id: int
        :param data: payload data as a bytearray type, string, or variable length set of byte-sized integers
        :type data: bytes | bytearray | str | list[int] | int
        :param command: SMBus/PMBus command if command_size > 0
        :type command: int
        :param command_size: SMBus/PMBus command size in bytes, set to 0 to disable SMBus transmissions 
            (classic I2C packets will be used instead)
        :type command_size: int
        :param speed: bus baudrate selection, use i2c.SPEED_400KHZ or i2c.SPEED_100KHZ
        :type speed: int
        :param use10bit_device_id: if False, device id must be between 0-127. True is reserved
        :type use10bit_device_id: bool
        :return: I2C Packet with status, flags, and timestamp
        :rtype: I2C.Packet
        """
        reqbuf = self._writereq(device_id, message_to_bytes(*data), command, command_size, speed, use10bit_device_id)
        with self.dev as (regs, r, control, res):
            res.time = regs.TIMESTAMP.read()
            regs.I2C_BUFFER.write(reqbuf)
            res.packet = regs.I2C_BUFFER.read(I2C.BUFFERSIZE)
        return self.Packet.from_response(self.dev, res.packet, 0, timestamp=res.time)[0]

    def read(self, device_id, length, command=0, command_size=0, speed=SPEED_400KHZ, use10bit_device_id=False):
        """ Read packet from slave as master
        
        :param device_id:  I2C device bus identifier (slave id)
        :type device_id: int
        :param length: length in bytes of data to read from I2C. Valid range: 1-127
        :type length: int
        :param command: SMBus/PMBus command if command_size > 0
        :type command: int
        :param command_size: SMBus/PMBus command size in bytes, set to 0 to disable SMBus transmissions 
            (classic I2C packets will be used instead)
        :type command_size: int
        :param speed: bus baudrate selection, use i2c.SPEED_400KHZ or i2c.SPEED_100KHZ
        :type speed: int
        :param use10bit_device_id: if False, device id must be between 0-127. True is reserved
        :type use10bit_device_id: bool
        :return: I2C Packet data bytearray with status, flags, and timestamp
        :rtype: I2C.Packet
        """
        reqbuf = self._readreq(device_id, length, command, command_size, speed=speed, use10bit_device_id=use10bit_device_id)
        with self.dev as (regs, r, control, res):
            res.time = regs.TIMESTAMP.read()
            regs.I2C_BUFFER.write(reqbuf)
            res.packet = regs.I2C_BUFFER.read(I2C.BUFFERSIZE)
        return self.Packet.from_response(self.dev, res.packet, 0, timestamp=res.time)[0]

    def stream(self, device_id, length, command=0, command_size=0, speed=SPEED_400KHZ, use10bit_device_id=False, interval=0.001):
        """Stream I2C data reads

        :param device_id:  I2C device bus identifier (slave id)
        :type device_id: int
        :param length: length in bytes of data to read from I2C. Valid range: 1-127
        :type length: int
        :param command: SMBus/PMBus command if command_size > 0
        :type command: int
        :param command_size: SMBus/PMBus command size in bytes, set to 0 to disable SMBus transmissions 
            (classic I2C packets will be used instead)
        :type command_size: int
        :param speed: bus baudrate selection, use i2c.SPEED_400KHZ or i2c.SPEED_100KHZ
        :type speed: int
        :param use10bit_device_id: if False, device id must be between 0-127. True is reserved
        :type use10bit_device_id: bool
        :param interval: I2C read period
        :type interval: float
        :return: an enumerable stream of I2C packets being read 
        :rtype: enumerable(I2C.Packet)
        """
        reqbuf = self._readreq(device_id, length, command, command_size, speed=speed, use10bit_device_id=use10bit_device_id)
        pollingdelay = 10 * interval
        timeout = 100 * interval
        with self.dev as (regs, r, control, res):
            interval = max(min(int(interval * 1e6), 0xFFFFFFFF), 0)
            regs.I2C_STREAM_INTERVAL.write(interval)
            regs.I2C_STREAM.write(reqbuf)

        t_lastread = time.time()
        while True:
            with self.dev as (regs, r, control, res):
                res.data = regs.I2C_STREAM.read(1000)
                res.status = regs.I2C_STREAM_STATUS.read()
            dlen = len(res.data)
            if dlen == 0:
                ellapsed_time = time.time() - t_lastread
                if ellapsed_time > timeout:
                    raise SubinitialTimeoutError("Error: I2C stream timed out {} > {}".format(ellapsed_time, timeout))
                time.sleep(pollingdelay)
                continue
            if (dlen % length) != 0:
                raise SubinitialException("Error: fragmented I2C stream data received")
            t_lastread = time.time()
            idx = 0
            while idx < dlen:
                packet = res.data[idx:idx+length]
                idx += length
                p = self.Packet(packet)
                p.flags = (res.status >> 16) & 0x1F
                yield p

            # Limit read frequency to a maximum of once every polling delay
            time.sleep(max(pollingdelay-(time.time()-t_lastread),0))


    def stream_stop(self):
        self.regs.I2C_STREAM.write(b'0')

    class _Device:
        """I2C Slave Device Handle, create this with core.i2c.Device(...)"""

        SPEED_100KHZ = 0
        SPEED_400KHZ = 1

        def __init__(self, i2c, device_id, command_size, speed=SPEED_400KHZ, use10bit_device_id=False, raise_exception_on_flag=False):
            """ Construct I2C slave device handle

            :param device_id:  I2C device bus identifier (slave id)
            :type device_id: int
            :param command_size: SMBus/PMBus command size in bytes, set to 0 to disable SMBus transmissions 
                (classic I2C packets will be used instead)
            :type command_size: int
            :param speed: bus baudrate selection, use i2c.SPEED_400KHZ or i2c.SPEED_100KHZ
            :type speed: int
            :param use10bit_device_id: if False, device id must be between 0-127. True is reserved
            :type use10bit_device_id: bool
            :param raise_exception_on_flag: if True, any error flags received will raise a Python exception
            :type raise_exception_on_flag: bool
            :return: I2C slave device handle that can be used to transmit/receive I2C packets
            :rtype: I2C._Device
            """
            self.i2c = i2c
            """:type: I2C"""
            if self.reconfigure(device_id, command_size, speed, use10bit_device_id, raise_exception_on_flag):
                return
            self.device_id = 0
            self.command_size = 0
            self.speed = 0
            self.use10bit_device_id = False
            self.raise_exception_on_flag = raise_exception_on_flag

        def reconfigure(self, device_id, command_size, speed=SPEED_400KHZ, use10bit_device_id=False, raise_exception_on_flag=False):
            """ Reconfigure I2C slave device handle with changed parameters

            :param device_id:  I2C device bus identifier (slave id)
            :type device_id: int
            :param command_size: SMBus/PMBus command size in bytes, set to 0 to disable SMBus transmissions 
                (classic I2C packets will be used instead)
            :type command_size: int
            :param speed: bus baudrate selection, use i2c.SPEED_400KHZ or i2c.SPEED_100KHZ
            :type speed: int
            :param use10bit_device_id: if False, device id must be between 0-127. True is reserved
            :type use10bit_device_id: bool
            :param raise_exception_on_flag: if True, any error flags received will raise a Python exception
            :type raise_exception_on_flag: bool
            """
            self.device_id = device_id
            self.command_size = max(min(command_size, 3), 0)
            self.speed = speed
            self.use10bit_device_id = use10bit_device_id
            self.raise_exception_on_flag = raise_exception_on_flag
            return True
            # dev = self.device_id & 0x3FF if self.use10bit_device_id else self.device_id & 0x7F
            # flag0 = (int(use10bit_device_id) << 10) | (1 << 11) | (speed << 13) | (command_size << 14)
            # self._cfgbits = dev | flag0

        def write(self, *data, command=0):
            """Write data to this slave device
            
            :param data: payload data as a bytearray type, string, or variable length set of byte-sized integers
            :type data: bytes | bytearray | str | list[int] | int
            :param command: SMBus/PMBus command register (if device command_length == 0 then this parameter is ignored)
            :type command: int
            :return: packet status, timestamp, and error flags
            :rtype: I2C.Packet | bytearray
            """
            resp = self.i2c.write(self.device_id, *data, command=command, command_size=self.command_size,
                                  speed=self.speed, use10bit_device_id=self.use10bit_device_id)
            if self.raise_exception_on_flag and resp.flags:
                raise SubinitialException("Flag raised {}".format(resp))
            return resp

        def read(self, length, command=0):
            """Read data from this slave device

            :param length: length in bytes of data to read from I2C. Valid range: 1-127
            :type length: int
            :param command: SMBus/PMBus command register (if device command_length == 0 then this parameter is ignored)
            :type command: int
            :return: packet status, timestamp, and error flags
            :rtype: I2C.Packet | bytearray
            """
            resp = self.i2c.read(self.device_id, length, command=command, command_size=self.command_size,
                                 speed=self.speed, use10bit_device_id=self.use10bit_device_id)
            if self.raise_exception_on_flag and resp.flags:
                raise SubinitialException("Flag raised {}".format(resp))
            return resp

    def Device(self, device_id, command_size, speed=SPEED_400KHZ, use10bit_device_id=False, raise_exception_on_flag=False):
        """ Create new I2C slave device handle

        :param device_id:  I2C device bus identifier (slave id)
        :type device_id: int
        :param command_size: SMBus/PMBus command size in bytes, set to 0 to disable SMBus transmissions 
            (classic I2C packets will be used instead)
        :type command_size: int
        :param speed: bus baudrate selection, use i2c.SPEED_400KHZ or i2c.SPEED_100KHZ
        :type speed: int
        :param use10bit_device_id: if False, device id must be between 0-127. True is reserved
        :type use10bit_device_id: bool
        :param raise_exception_on_flag:  if True, any error flags received will raise a Python exception. If False, no exception will be raised
        :type raise_exception_on_flag: bool
        :return: I2C slave device handle that can be used to transmit/receive I2C packets
        :rtype: I2C._Device
        """
        return I2C._Device(self, device_id, command_size, speed, use10bit_device_id, raise_exception_on_flag)


    """ SLAVE INTERFACE """

    # def slave_enable(self, device_id, command_size=0, use10bit_device_id=False):
    #     pass
    #
    # class SlaveMap:
    #     def __init__(self):
    #         pass
    #
    # def slave_getmap(self):
    #     return I2C.SlaveMap()
    #
    # def slave_setmap(self, map):
    #     pass
    #
    # def slave_updatemap(self, command, *data):
    #     databytes = message_to_bytes(*data)
    #
    # def slave_getpacketlog(self):
    #     return []
    #
    # def slave_disable(self):
    #     pass


class Leds(Peripheral):
    """
    Back Panel User LEDs
    """
    LED0 = 0  # Index of LED0
    LED1 = 1  # Index of LED1

    _LEDSMASK = 0x0003
    _MODE_SET = 0x0100
    _MODE_CLEAR = 0x0200
    _MODE_TOGGLE = 0x0300

    def set(self, *leds, mask=0):
        """Set LEDs to on state

        :param leds: arbitrary list of indices. e.g. 0, 1
        :type leds: tuple[int]
        :param mask: optional bitmask of indices. e.g. 0b11
            See `Specifying Indices or Masks <http://subinitial.com/stacks/docs/indices>`_
        :type mask: int
        """
        leds = digest_indices(leds, mask, clearmask=self._LEDSMASK)
        self.regs.USERLEDS.write(self._MODE_SET | leds)

    def clear(self, *leds, mask=0):
        """Clear LEDs to off state

        :param leds: arbitrary list of indices. e.g. 0, 1
        :type leds: tuple[int]
        :param mask: optional bitmask of indices. e.g. 0b11
            See `Specifying Indices or Masks <http://subinitial.com/stacks/docs/indices>`_
        :type mask: int
        """
        leds = digest_indices(leds, mask, clearmask=self._LEDSMASK)
        self.regs.USERLEDS.write(self._MODE_CLEAR | leds)

    def toggle(self, *leds, mask=0):
        """Toggles the LEDs' state

        :param leds: arbitrary list of indices. e.g. 0, 1
        :type leds: tuple[int]
        :param mask: optional bitmask of indices. e.g. 0b11
            See `Specifying Indices or Masks <http://subinitial.com/stacks/docs/indices>`_
        :type mask: int
        """
        leds = digest_indices(leds, mask, clearmask=self._LEDSMASK)
        self.regs.USERLEDS.write(self._MODE_TOGGLE | leds)

    def cmd(self, *cmd, mask=0):
        """Command LEDs to on, omitted LEDs will be turned off

        :param cmd: arbitrary list of indices. e.g. 0, 1
        :type cmd: tuple[int]
        :param mask: optional bitmask of indices. e.g. 0b11
            See `Specifying Indices or Masks <http://subinitial.com/stacks/docs/indices>`_
        :type mask: int
        """
        leds = digest_indices(cmd, mask, clearmask=self._LEDSMASK)
        self.regs.USERLEDS.write(leds)

    def get_cmd(self):
        """
        Get the commanded (on) LEDs

        :return: bitmask
        :rtype: int
        """
        return Bitmask(self.regs.USERLEDS.read())


class _Fop(object):
    NOOP = 0
    INVALID = 1
    CREATE = 2
    DELETE = 3
    COPY = 4
    TRUNCATE = 5
    MOVE = 6
    STATUS = 7
    INFO = 8
    LIST = 9
    RLIST = 10
    UPDATEFIRMWARE = 11

    # @staticmethod
    # def strToU16Array(astr):
    #     buf = astr.encode() + b'\0'
    #     return _Fop.bufToU16Array(buf)
    #
    # @staticmethod
    # def bufToU16Array(buf):
    #     if len(buf) % 2 != 0:
    #         buf = buf + b'\0'
    #     return struct.unpack("<{}H".format(len(buf)//2), buf)
    #
    # @staticmethod
    # def U16ArrayTobuf(array, length, startidx=0):
    #     buf = []
    #     for i in range(startidx, startidx + length):
    #         buf.append(array[i])
    #
    #     return struct.pack("<{}H", *buf)
    #
    # @staticmethod
    # def U16ArrayTostr(array, length, startidx=0):
    #     buf = _Fop.U16ArrayTobuf(array, length, startidx)
    #     return buf.decode().strip('\0')


class FileSystem(Peripheral):
    """
    FileSystem controller for sd-card, logs, web-site, and firmware-updates.
    """
    # DeckRegister(r.FILE_CURSOR, "file cursor for operations", datatype=DataType.uint32)
    BLOCKSIZE = 512*2

    def write(self, path, buffer, cursor=0):
        """Write file

        :param path: file path, e.g. '/bin/core.img'
        :type path: str
        :param buffer: data to write to the file
        :type buffer: bytes
        :param cursor: offset from start of file to start reading bytes.
        :type cursor: int
        """
        lenrem = len(buffer)
        if lenrem == 0:
            return 0

        start_cursor = cursor

        self.regs.FILE_PATH0.write(path.encode())

        # blkcount = int(lenrem/self.BLOCKSIZE+1)
        while lenrem:
            writelen = min(self.BLOCKSIZE, lenrem)
            res = self.dev.request.queue()
            self.regs.FILE_CURSOR.write(cursor)
            self.regs.FILE_BUFFER.write(buffer[cursor:cursor+writelen])
            res.cursor = self.regs.FILE_CURSOR.read()
            self.dev.request.submit()

            ncursor = res.cursor

            if cursor + writelen != ncursor:
                raise SubinitialException("File write failed to write {}B @{} to {}".format(
                    writelen, cursor, path)
                )

            cursor += writelen
            lenrem -= writelen

        return cursor - start_cursor

    DEFAULT_READLIMIT = 0x100000 * 5

    def read(self, path, cursor=0, limit=DEFAULT_READLIMIT):
        """Read file

        :param path: file path, e.g. '/bin/core.img'
        :type path: str
        :param cursor: offset from start of file to start reading bytes.
        :type cursor: int
        :param limit: maximum bytecount to read
        :type limit: int
        :return: bytes read from file
        :rtype: bytes
        """
        lenrem = limit
        if lenrem == 0:
            return b''

        start_cursor = cursor
        self.regs.FILE_PATH0.write(path.encode())

        rdata = bytearray()

        while lenrem:
            readlen = min(self.BLOCKSIZE, lenrem)
            res = self.dev.request.queue()
            self.regs.FILE_CURSOR.write(cursor)
            res.rdata = self.regs.FILE_BUFFER.read(readlen)
            res.cursor = self.regs.FILE_CURSOR.read()
            self.dev.request.submit()
            ncursor = res.cursor

            rdata += res.rdata

            br = len(res.rdata)
            cursor += br
            lenrem -= br

            if cursor != ncursor:
                raise SubinitialException("File read failed @{} in {}".format(
                    ncursor, path)
                )

            if br != readlen:
                # EOF
                break

        # totalbr = cursor - start_cursor
        return rdata

    def move(self, oldpath, newpath):
        """Move (or rename) file

        :param oldpath: file path, e.g. '/bin/core.img'
        :type oldpath: str
        :param newpath: file path, e.g. '/bin/core.img'
        :type newpath: str
        """
        ctrl = (_Fop.MOVE | 0x8000)
        res = self.dev.request.queue()
        self.regs.FILE_PATH0.write(oldpath.encode())
        self.regs.FILE_PATH1.write(newpath.encode())
        self.regs.FILE_CONTROL.write(ctrl)
        res.control = self.regs.FILE_CONTROL.read()
        self.dev.request.submit()
        return (res.control >> 8) & 0xFF


    def delete(self, path):
        """Delete file

        :param path: file path, e.g. '/bin/core.img'
        :type path: str
        """
        ctrl = (_Fop.DELETE | 0x8000)
        res = self.dev.request.queue()
        self.regs.FILE_PATH0.write(path.encode())
        self.regs.FILE_CONTROL.write(ctrl)
        res.control = self.regs.FILE_CONTROL.read()
        self.dev.request.submit()
        return (res.control >> 8) & 0xFF


class Updater(Peripheral):
    """
    Firmware Update controller
    """

    BUSADDRESS_CORE = 1
    BUSADDRESS_SUPERVISOR = 1

    def update(self, busaddress, image):
        """Update the application firmware of the device at busaddress with a signed .img file provided by Subinitial.

        .. Note:: at this time only the Stacks Core can receive firmware updates. In this case busaddress=1.

        :param busaddress: busaddress of device to receive the firmware update
        :type busaddress: int
        :param image: file bytes of Subinitial provided application binary image (*.img)
        :type image: bytes
        """
        d = self.dev
        imgpath = "/bin/urom.img" if busaddress != 1 else "/bin/ucore.img"
        _imgpath = "/bin/_urom.img"
        d.fs.delete(_imgpath)
        d.fs.move(imgpath, _imgpath)
        d.fs.write(imgpath, image)

        # start the firmware update on busaddress
        self.dev.request.queue()
        self.regs.FILE_PATH1.write(struct.pack("<I", busaddress))
        self.regs.FILE_CONTROL.write((_Fop.UPDATEFIRMWARE | 0x8000))
        self.dev.request.submit()