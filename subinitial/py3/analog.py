# -*- coding: UTF-8 -*-
# Analog Deck interface
# © 2012-2018 Subinitial LLC. All Rights Reserved
# Redistribution and use in source and binary forms, with or without modification, are permitted provided that the
# following conditions are met: Redistributions of source code and binary forms must retain the above copyright notice,
# these conditions, and the disclaimer below all in plain text. The name Subinitial may not be used to endorse or
# promote products derived from this software without prior written consent from Subinitial LLC. This software may only
# be redistributed and used with a Subinitial Stacks product.

# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
# warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. Refer to the included LICENSE.txt for more details.

from subinitial.py3.stacks import *
from subinitial.stacks.utils import digest_indices, do_for, mask_to_bits, Bitmask


class AnalogDeck(Device):
    ASSEMBLY = "SA13730"
    FIRMWARE_MIN = "v1.15.0"

    def __init__(self, core_deck, bus_address, verify_connection=True):
        """Construct an Analog Deck instance.

        .. code-block:: python

            # Python Example
            analog_deck = subinitial.stacks.AnalogDeck(core, bus_address=2)

        :param core_deck: Stacks Core instance that is controlling this Device
        :type core_deck: subinitial.py3.core1.Core
        :param bus_address: bus address from 2-31
        :type bus_address: int
        """
        Device.__init__(self, core_deck, bus_address, verify_connection=verify_connection)
        self.regs = RegisterSet(self)
        """:type: RegisterSet"""
        self.dmm = Dmm(self)
        """:type: Dmm"""
        self.wavegen = WaveformGenerator(self)
        """:type: WaveformGenerator"""
        self.sourcemeter = SourceMeter(self)
        """:type: SourceMeter"""
        self.powermeter0 = PowerMeter(self, 0)
        """:type: PowerMeter"""
        self.powermeter1 = PowerMeter(self, 1)
        """:type: PowerMeter"""
        self.acripple = ACRipple(self)
        """:type: ACRipple"""
        self.negative_powerdac = NegativePowerDac(self)
        """:type: NegativePowerDac"""
        self.rgbled = RgbLed(self)
        """:type: RgbLed"""
        self.dio = Dio(self)
        """:type: Dio"""
        self.isolatedinputs = IsolatedInputs(self)
        """:type: IsolatedInputs"""
        self.solenoiddrivers = SolenoidDrivers(self)
        """:type: SolenoidDrivers"""
        self.relays = Relays(self)
        """:type: Relays"""
        self.stepper = Stepper(self)
        """:type: Stepper"""

    def __enter__(self):
        return self.regs, self.r, self.control, self.request.queue()

    def __exit__(self, type, value, traceback):
        self.request.submit()

    POWERMETER_SAMPLERATE_357HZ = 0
    POWERMETER_SAMPLERATE_1429HZ = 1
    POWERMETER_SAMPLERATE_5714HZ = 2

    _rate_to_f = {
        0: 357.1428571,
        1: 1428.571429,
        2: 5714.285714
    }

    def powermeter_set_samplerate(self, samplerate=POWERMETER_SAMPLERATE_357HZ):
        """Set the samplerate for the SourceMeter, PowerMeter0, and PowerMeter1 sense Voltage, Current, and Power

        :param samplerate: sample rate selection. This samplerate is accurate to 60ppm. See AnalogDeck.SAMPLERATE_* for options.
        :type samplerate: int
        """
        self.regs.POWERMETER_SAMPLERATE.write(samplerate)

    def powermeter_get_samplerate(self, in_hz=True):
        """Get the samplerate for the SourceMeter, PowerMeter0, and PowerMeter1 sense Voltage, Current, and Powers

        :param in_hz: return value in floating point Hz or in integer selection
        :type in_hz: bool
        :return: samplerate hz, or samplerate selection intger
        :rtype: float | int
        """
        samplerate = self.regs.POWERMETER_SAMPLERATE.read()
        if in_hz:
            return self._rate_to_f[samplerate]
        return samplerate


    def powermeter_stream(self, raise_exception_on_overflow=False, timeout=500, pollingdelay=100):
        """Yield samples as they become available over the network. Samples are gathered at the samplerate

        :param raise_exception_on_overflow: raise a SubinitialResponseError if any buffer overflow occurs and some
            samples were not able to be retrieved before being overwritten.
        :type raise_exception_on_overflow: bool

        :param timeout: timeout represents the maximum time to wait for new samples to be available in sample periods
            If this timeout is exceeded a SubinitialTimeoutError is raised.
        :type timeout: int

        :param pollingdelay: The polling delay represents the minimum time between reads for available buffered samples in sample periods
        :type pollingdelay: int

        :return: (powermeter0_volts, powermeter0_current, powermeter1_volts, powermeter1_current) iterable
        :rtype: iter(PowerMeterStreamSample)
        """

        # Continually read samples from the DmmStream Fifo
        t_lastread = time.time()

        pm0v_conv = self.regs.POWERMETER0_VOLTAGE.convert
        pm0i_conv = self.regs.POWERMETER0_CURRENT.convert
        pm1v_conv = self.regs.POWERMETER1_VOLTAGE.convert
        pm1i_conv = self.regs.POWERMETER1_CURRENT.convert

        res = self.request.queue()
        res.rate = self.regs.POWERMETER_SAMPLERATE.read()
        self.regs.POWERMETER_STREAM_CONTROL.write(0)
        self.request.submit()

        # Predetermine configuration, timeouts and pollingdelays
        samperiod = 1 / ([425, 1701, 6803][res.rate])
        timeout *= samperiod
        pollingdelay *= samperiod
        timestamp = 0
        while True:
            res = self.request.queue()
            res.regs = self.regs.POWERMETER_STREAM.read(600)
            res.flags = self.regs.POWERMETER_STREAM_CONTROL.read()
            self.request.submit()

            samplecount = len(res.regs) if res.regs else 0
            if samplecount == 0:
                # If no samples are available yet then delay for a while
                if time.time()-t_lastread > timeout:
                    raise SubinitialTimeoutError("Analog Deck PowerMeter stream timed out")
                time.sleep(pollingdelay)
                continue
            elif samplecount & 0b11:
                raise SubinitialResponseError("Analog Deck PowerMeter Stream Partial Response")
            elif res.flags:
                if raise_exception_on_overflow:
                    raise SubinitialResponseError("Analog Deck PowerMeter Overflowed, flags: {}".format(res.flags))
                timestamp += 1.0  # add a whole second to denote the discontinuity in samples

            # Samples were received
            t_lastread = time.time()

            samplecount >>= 2
            idx = 0
            for i in range(0, samplecount):
                retval = PowerMeterStreamSample(
                    pm0v_conv(res.regs[idx+0]), pm0i_conv(res.regs[idx+1]),
                    pm1v_conv(res.regs[idx+2]), pm1i_conv(res.regs[idx+3]),
                    (res.flags & 1) > 0, timestamp)
                timestamp += samperiod
                idx += 4
                yield retval


            # Limit read frequency to a maximum of once every polling delay
            time.sleep(max(pollingdelay-(time.time()-t_lastread),0))


class RTypes:
#/*%regdef%*/
    class Div1000(RegisterU32):
        @staticmethod
        def convert(val):
            return 0.001 * val
    
        @staticmethod
        def revert(val):
            return round(val / 0.001)
    
    class PicoReg(RegisterU64):
        @staticmethod
        def convert(val):
            return 1e-12 * val
    
        @staticmethod
        def revert(val):
            return round(val / 1e-12)
    
    class WavegenDC(RegisterI16):
        @staticmethod
        def convert(val):
            return 0.00015259021896696422 * val
    
        @staticmethod
        def revert(val):
            return round(min(max(val / 0.00015259021896696422, -32768), 32767))
    
    class WavegenDCArray(RegisterI16Array):
        @staticmethod
        def convert(val):
            return 0.00015259021896696422 * val
    
        @staticmethod
        def revert(val):
            return round(min(max(val / 0.00015259021896696422, -32768), 32767))
    
    class SourceMeterVolts(Register):
        @staticmethod
        def convert(val):
            return 0.00018310826276035706 * val
    
        @staticmethod
        def revert(val):
            return round(val / 0.00018310826276035706)
    
    class SourceMeterCurrent(Register):
        @staticmethod
        def convert(val):
            return 3.1000228885328444e-05 * val
    
        @staticmethod
        def revert(val):
            return round(val / 3.1000228885328444e-05)
    
    class SourceMeterPower(Register):
        @staticmethod
        def convert(val):
            return 0.00037200274662394135 * val
    
        @staticmethod
        def revert(val):
            return round(val / 0.00037200274662394135)
    
    class PowerMeterVolts(RegisterI16):
        @staticmethod
        def convert(val):
            return 0.0004150453955901427 * val
    
        @staticmethod
        def revert(val):
            return round(val / 0.0004150453955901427)
    
    class PowerMeter0Current(RegisterI16):
        @staticmethod
        def convert(val):
            return 5.065995269703213e-06 * val
    
        @staticmethod
        def revert(val):
            return round(val / 5.065995269703213e-06)
    
    class PowerMeter0Power(RegisterI16):
        @staticmethod
        def convert(val):
            return 6.889753566796368e-05 * val
    
        @staticmethod
        def revert(val):
            return round(val / 6.889753566796368e-05)
    
    class PowerMeter1Current(RegisterI16):
        @staticmethod
        def convert(val):
            return 5.0659952697032115e-05 * val
    
        @staticmethod
        def revert(val):
            return round(val / 5.0659952697032115e-05)
    
    class PowerMeter1Power(RegisterI16):
        @staticmethod
        def convert(val):
            return 0.0006889753566796368 * val
    
        @staticmethod
        def revert(val):
            return round(val / 0.0006889753566796368)
    
    class DmmVoltage(RegisterI32):
        @staticmethod
        def convert(val):
            return 2.9802325940409415e-07 * val
    
        @staticmethod
        def revert(val):
            return round(min(max(val / 2.9802325940409415e-07, -2147483648), 2147483647))
    
    class DmmVoltageStream(RegisterI32Array):
        @staticmethod
        def convert(val):
            return 2.9802325940409415e-07 * val
    
        @staticmethod
        def revert(val):
            return round(val / 2.9802325940409415e-07)
    
    class DmmCurrent(RegisterI32):
        @staticmethod
        def convert(val):
            return 2.9802325940409415e-07 * val
    
        @staticmethod
        def revert(val):
            return round(min(max(val / 2.9802325940409415e-07, -2147483648), 2147483647))
    
    class DmmCurrentStream(RegisterI32Array):
        @staticmethod
        def convert(val):
            return 2.9802325940409415e-07 * val
    
        @staticmethod
        def revert(val):
            return round(val / 2.9802325940409415e-07)
    
    class BitmaskU16(Register):
        @staticmethod
        def convert(val):
            return Bitmask(val)
    
        @staticmethod
        def revert(val):
            return val
    
    class SourceMeterSource(RegisterI16):
        @staticmethod
        def convert(val):
            return 0.002442002442002442 * val
    
        @staticmethod
        def revert(val):
            return round(min(max(val / 0.002442002442002442, 0), 4095))
    
    class NegPowerDac(RegisterI16):
        @staticmethod
        def convert(val):
            return -0.002442002442002442 * val
    
        @staticmethod
        def revert(val):
            return round(min(max(val / -0.002442002442002442, 0), 4095))
    
#/*%end_regdef%*/


class RegisterSet(RegisterSetBase):
    Addresses = {
        #/*%regaddr%*/
        0: "REG0",
        1: "KEY",
        2: "SERIALNUMBER",
        4: "FIRMWARE",
        6: "ASSEMBLY_NUMBER",
        7: "BUSADDRESS",
        8: "CONTROL",
        9: "STATUS",
        10: "CONFIG_ADDRESS",
        12: "CONFIG",
        14: "UPTIME",
        16: "BROADCAST_STATUS",
        17: "REQUEST_STATUS",
        18: "GUID",
        19: "SESSION",
        20: "GPR0",
        21: "GPR1",
        22: "GPR2",
        23: "GPR3",
        24: "TRIGGER",
        26: "TIMESTAMP",
        28: "TIMESTAMP_PS",
        39: "WAVEGEN_DC",
        41: "SOURCEMETER_METERVOLTAGE",
        42: "SOURCEMETER_METERCURRENT",
        43: "SOURCEMETER_METERPOWER",
        44: "POWERMETER0_VOLTAGE",
        45: "POWERMETER0_CURRENT",
        46: "POWERMETER0_POWER",
        47: "POWERMETER1_VOLTAGE",
        48: "POWERMETER1_CURRENT",
        49: "POWERMETER1_POWER",
        50: "DMM_RESULT_COUNTER",
        51: "DMM_FLAGS",
        52: "DMM_VCH0_RESULT",
        54: "DMM_VCH1_RESULT",
        56: "DMM_VCH2_RESULT",
        58: "DMM_VCH3_RESULT",
        60: "DMM_VCH4_RESULT",
        62: "DMM_VCH5_RESULT",
        64: "DMM_ICH6_RESULT",
        66: "DMM_VCH7_HV_RESULT",
        68: "ISOLATEDINPUTS_STATUS",
        69: "DIO_STATUS",
        70: "DIO_CMD",
        71: "DIO_CONFIG",
        72: "SOLENOIDDRIVERS_CMD",
        73: "RELAYS_CMD",
        74: "DIO_SET",
        75: "DIO_CLEAR",
        76: "SOLENOIDDRIVERS_ENGAGE",
        77: "SOLENOIDDRIVERS_DISENGAGE",
        78: "RELAYS_ENGAGE",
        79: "RELAYS_DISENGAGE",
        83: "DMM_CONTROL",
        84: "DMM_RANGES",
        86: "SOURCEMETER_SOURCEVOLTAGE",
        87: "NEGATIVE_POWERDAC",
        88: "RGBB",
        90: "SOURCEMETER_METERVOLTAGE_MIN",
        91: "POWERMETER0_VOLTAGE_MIN",
        92: "POWERMETER1_VOLTAGE_MIN",
        93: "SOURCEMETER_METERCURRENT_MIN",
        94: "POWERMETER0_CURRENT_MIN",
        95: "POWERMETER1_CURRENT_MIN",
        96: "SOURCEMETER_METERPOWER_MIN",
        97: "POWERMETER0_POWER_MIN",
        98: "POWERMETER1_POWER_MIN",
        99: "SOURCEMETER_METERVOLTAGE_MAX",
        100: "POWERMETER0_VOLTAGE_MAX",
        101: "POWERMETER1_VOLTAGE_MAX",
        102: "SOURCEMETER_METERCURRENT_MAX",
        103: "POWERMETER0_CURRENT_MAX",
        104: "POWERMETER1_CURRENT_MAX",
        105: "SOURCEMETER_METERPOWER_MAX",
        106: "POWERMETER0_POWER_MAX",
        107: "POWERMETER1_POWER_MAX",
        108: "SOURCEMETER_CURRENTLIMIT",
        109: "SOURCEMETER_CONFIG",
        110: "POWERMETER0_CONFIG",
        111: "POWERMETER1_CONFIG",
        112: "DMM_VCH0_MIN",
        114: "DMM_VCH1_MIN",
        116: "DMM_VCH2_MIN",
        118: "DMM_VCH3_MIN",
        120: "DMM_VCH4_MIN",
        122: "DMM_VCH5_MIN",
        124: "DMM_ICH6_MIN",
        126: "DMM_VCH7_HV_MIN",
        128: "DMM_VCH0_MAX",
        130: "DMM_VCH1_MAX",
        132: "DMM_VCH2_MAX",
        134: "DMM_VCH3_MAX",
        136: "DMM_VCH4_MAX",
        138: "DMM_VCH5_MAX",
        140: "DMM_ICH6_MAX",
        142: "DMM_VCH7_HV_MAX",
        144: "WAVEGEN_CONTROL",
        156: "RGBB_EXCEPTION_STATUS",
        157: "RGBB_EXCEPTION_SUPRESSION",
        158: "ACMETER_TRIGGER_POINT",
        159: "ACMETER_CONTROL",
        160: "ACMETER_SAMPLES",
        161: "DMM_TRIGGER_CONFIG",
        162: "DMM_TRIGGER_POINT",
        164: "DMM_STREAM_SAMPLES",
        166: "DMM_TRACKTIME",
        167: "DMM_ADCTEMP",
        168: "DMM_MUXTEMP",
        169: "WAVEGEN_SAMPLECOUNT",
        170: "WAVEGEN_SAMPLEPERIOD",
        172: "WAVEGEN_SAMPLESINDEX",
        173: "WAVEGEN_SAMPLES",
        174: "STEPPER_STEPCOUNT",
        176: "STEPPER_FREQUENCYDIVIDER",
        177: "STEPPER_CONTROL",
        178: "DMM_VCH0_RELS",
        180: "DMM_VCH1_RELS",
        182: "DMM_VCH2_RELS",
        184: "DMM_VCH3_RELS",
        186: "DMM_VCH4_RELS",
        188: "DMM_VCH5_RELS",
        190: "DMM_ICH6_RELS",
        192: "DMM_VCH7_HV_RELS",
        200: "POWERMETER_SAMPLERATE",
        201: "POWERMETER_STREAM_CONTROL",
        202: "POWERMETER_STREAM",
        204: "DMM_RANGE_STATUS",
        206: "DMM_STREAM_CONTROL",
        207: "DMM_EXTRA_CONTROL",
        #/*%end_regaddr%*/
    }
    def __init__(self, device):
        self.dev = device
        #/*%reginst%*/
        self.REG0 = Register(0, device)
        self.KEY = Register(1, device)
        self.SERIALNUMBER = RegisterU32(2, device)
        self.FIRMWARE = RegisterU32(4, device)
        self.ASSEMBLY_NUMBER = Register(6, device)
        self.BUSADDRESS = Register(7, device)
        self.CONTROL = Register(8, device)
        self.STATUS = Register(9, device)
        self.CONFIG_ADDRESS = RegisterU32(10, device)
        self.CONFIG = RegisterU32(12, device)
        self.UPTIME = RegisterU32(14, device)
        self.BROADCAST_STATUS = Register(16, device)
        self.REQUEST_STATUS = Register(17, device)
        self.GUID = RegisterBytes(18, device)
        self.SESSION = RegisterBytes(19, device)
        self.GPR0 = Register(20, device)
        self.GPR1 = Register(21, device)
        self.GPR2 = Register(22, device)
        self.GPR3 = Register(23, device)
        self.TRIGGER = Register(24, device)
        self.TIMESTAMP = RTypes.Div1000(26, device)
        self.TIMESTAMP_PS = RTypes.PicoReg(28, device)
        self.WAVEGEN_DC = RTypes.WavegenDC(39, device)
        self.SOURCEMETER_METERVOLTAGE = RTypes.SourceMeterVolts(41, device)
        self.SOURCEMETER_METERCURRENT = RTypes.SourceMeterCurrent(42, device)
        self.SOURCEMETER_METERPOWER = RTypes.SourceMeterPower(43, device)
        self.POWERMETER0_VOLTAGE = RTypes.PowerMeterVolts(44, device)
        self.POWERMETER0_CURRENT = RTypes.PowerMeter0Current(45, device)
        self.POWERMETER0_POWER = RTypes.PowerMeter0Power(46, device)
        self.POWERMETER1_VOLTAGE = RTypes.PowerMeterVolts(47, device)
        self.POWERMETER1_CURRENT = RTypes.PowerMeter1Current(48, device)
        self.POWERMETER1_POWER = RTypes.PowerMeter1Power(49, device)
        self.DMM_RESULT_COUNTER = Register(50, device)
        self.DMM_FLAGS = Register(51, device)
        self.DMM_VCH0_RESULT = RTypes.DmmVoltage(52, device)
        self.DMM_VCH1_RESULT = RTypes.DmmVoltage(54, device)
        self.DMM_VCH2_RESULT = RTypes.DmmVoltage(56, device)
        self.DMM_VCH3_RESULT = RTypes.DmmVoltage(58, device)
        self.DMM_VCH4_RESULT = RTypes.DmmVoltage(60, device)
        self.DMM_VCH5_RESULT = RTypes.DmmVoltage(62, device)
        self.DMM_ICH6_RESULT = RTypes.DmmCurrent(64, device)
        self.DMM_VCH7_HV_RESULT = RTypes.DmmVoltage(66, device)
        self.ISOLATEDINPUTS_STATUS = RTypes.BitmaskU16(68, device)
        self.DIO_STATUS = RTypes.BitmaskU16(69, device)
        self.DIO_CMD = RTypes.BitmaskU16(70, device)
        self.DIO_CONFIG = RTypes.BitmaskU16(71, device)
        self.SOLENOIDDRIVERS_CMD = RTypes.BitmaskU16(72, device)
        self.RELAYS_CMD = RTypes.BitmaskU16(73, device)
        self.DIO_SET = RTypes.BitmaskU16(74, device)
        self.DIO_CLEAR = RTypes.BitmaskU16(75, device)
        self.SOLENOIDDRIVERS_ENGAGE = RTypes.BitmaskU16(76, device)
        self.SOLENOIDDRIVERS_DISENGAGE = RTypes.BitmaskU16(77, device)
        self.RELAYS_ENGAGE = RTypes.BitmaskU16(78, device)
        self.RELAYS_DISENGAGE = RTypes.BitmaskU16(79, device)
        self.DMM_CONTROL = Register(83, device)
        self.DMM_RANGES = RegisterU32(84, device)
        self.SOURCEMETER_SOURCEVOLTAGE = RTypes.SourceMeterSource(86, device)
        self.NEGATIVE_POWERDAC = RTypes.NegPowerDac(87, device)
        self.RGBB = RegisterU32(88, device)
        self.SOURCEMETER_METERVOLTAGE_MIN = RTypes.SourceMeterVolts(90, device)
        self.POWERMETER0_VOLTAGE_MIN = RTypes.PowerMeterVolts(91, device)
        self.POWERMETER1_VOLTAGE_MIN = RTypes.PowerMeterVolts(92, device)
        self.SOURCEMETER_METERCURRENT_MIN = RTypes.SourceMeterCurrent(93, device)
        self.POWERMETER0_CURRENT_MIN = RTypes.PowerMeter0Current(94, device)
        self.POWERMETER1_CURRENT_MIN = RTypes.PowerMeter1Current(95, device)
        self.SOURCEMETER_METERPOWER_MIN = RTypes.SourceMeterPower(96, device)
        self.POWERMETER0_POWER_MIN = RTypes.PowerMeter0Power(97, device)
        self.POWERMETER1_POWER_MIN = RTypes.PowerMeter1Power(98, device)
        self.SOURCEMETER_METERVOLTAGE_MAX = RTypes.SourceMeterVolts(99, device)
        self.POWERMETER0_VOLTAGE_MAX = RTypes.PowerMeterVolts(100, device)
        self.POWERMETER1_VOLTAGE_MAX = RTypes.PowerMeterVolts(101, device)
        self.SOURCEMETER_METERCURRENT_MAX = RTypes.SourceMeterCurrent(102, device)
        self.POWERMETER0_CURRENT_MAX = RTypes.PowerMeter0Current(103, device)
        self.POWERMETER1_CURRENT_MAX = RTypes.PowerMeter1Current(104, device)
        self.SOURCEMETER_METERPOWER_MAX = RTypes.SourceMeterPower(105, device)
        self.POWERMETER0_POWER_MAX = RTypes.PowerMeter0Power(106, device)
        self.POWERMETER1_POWER_MAX = RTypes.PowerMeter1Power(107, device)
        self.SOURCEMETER_CURRENTLIMIT = RTypes.SourceMeterCurrent(108, device)
        self.SOURCEMETER_CONFIG = Register(109, device)
        self.POWERMETER0_CONFIG = Register(110, device)
        self.POWERMETER1_CONFIG = Register(111, device)
        self.DMM_VCH0_MIN = RTypes.DmmVoltage(112, device)
        self.DMM_VCH1_MIN = RTypes.DmmVoltage(114, device)
        self.DMM_VCH2_MIN = RTypes.DmmVoltage(116, device)
        self.DMM_VCH3_MIN = RTypes.DmmVoltage(118, device)
        self.DMM_VCH4_MIN = RTypes.DmmVoltage(120, device)
        self.DMM_VCH5_MIN = RTypes.DmmVoltage(122, device)
        self.DMM_ICH6_MIN = RTypes.DmmCurrent(124, device)
        self.DMM_VCH7_HV_MIN = RTypes.DmmVoltage(126, device)
        self.DMM_VCH0_MAX = RTypes.DmmVoltage(128, device)
        self.DMM_VCH1_MAX = RTypes.DmmVoltage(130, device)
        self.DMM_VCH2_MAX = RTypes.DmmVoltage(132, device)
        self.DMM_VCH3_MAX = RTypes.DmmVoltage(134, device)
        self.DMM_VCH4_MAX = RTypes.DmmVoltage(136, device)
        self.DMM_VCH5_MAX = RTypes.DmmVoltage(138, device)
        self.DMM_ICH6_MAX = RTypes.DmmCurrent(140, device)
        self.DMM_VCH7_HV_MAX = RTypes.DmmVoltage(142, device)
        self.WAVEGEN_CONTROL = Register(144, device)
        self.RGBB_EXCEPTION_STATUS = Register(156, device)
        self.RGBB_EXCEPTION_SUPRESSION = Register(157, device)
        self.ACMETER_TRIGGER_POINT = Register(158, device)
        self.ACMETER_CONTROL = Register(159, device)
        self.ACMETER_SAMPLES = Register(160, device)
        self.DMM_TRIGGER_CONFIG = Register(161, device)
        self.DMM_TRIGGER_POINT = RTypes.DmmVoltage(162, device)
        self.DMM_STREAM_SAMPLES = RegisterI32Array(164, device)
        self.DMM_TRACKTIME = Register(166, device)
        self.DMM_ADCTEMP = Register(167, device)
        self.DMM_MUXTEMP = Register(168, device)
        self.WAVEGEN_SAMPLECOUNT = Register(169, device)
        self.WAVEGEN_SAMPLEPERIOD = RegisterU32(170, device)
        self.WAVEGEN_SAMPLESINDEX = Register(172, device)
        self.WAVEGEN_SAMPLES = RTypes.WavegenDCArray(173, device)
        self.STEPPER_STEPCOUNT = RegisterU32(174, device)
        self.STEPPER_FREQUENCYDIVIDER = Register(176, device)
        self.STEPPER_CONTROL = Register(177, device)
        self.DMM_VCH0_RELS = RTypes.DmmVoltage(178, device)
        self.DMM_VCH1_RELS = RTypes.DmmVoltage(180, device)
        self.DMM_VCH2_RELS = RTypes.DmmVoltage(182, device)
        self.DMM_VCH3_RELS = RTypes.DmmVoltage(184, device)
        self.DMM_VCH4_RELS = RTypes.DmmVoltage(186, device)
        self.DMM_VCH5_RELS = RTypes.DmmVoltage(188, device)
        self.DMM_ICH6_RELS = RTypes.DmmCurrent(190, device)
        self.DMM_VCH7_HV_RELS = RTypes.DmmVoltage(192, device)
        self.POWERMETER_SAMPLERATE = Register(200, device)
        self.POWERMETER_STREAM_CONTROL = Register(201, device)
        self.POWERMETER_STREAM = RegisterI16Array(202, device)
        self.DMM_RANGE_STATUS = RegisterU32(204, device)
        self.DMM_STREAM_CONTROL = Register(206, device)
        self.DMM_EXTRA_CONTROL = Register(207, device)
        #/*%end_reginst%*/

    def __getitem__(self, addr):
        if type(addr) == int:
            addr = RegisterSet.Addresses[addr]
        return getattr(self, addr)


class Peripheral(object):
    """Base class for custom Device peripherals"""
    def __init__(self, device):
        self.dev = device
        """:type: AnalogDeck"""
        self.regs = device.regs
        """:type: RegisterSet"""


class Dmm(Peripheral):
    """Digital Multimeter"""

    CHANNEL_COUNT = 8
    CHANNEL_COUNT_VOLTAGE = 6

    # _voltspercount = 2.5/((2 << 23-1)-1)
    # _ampspervolt = 1.0
    # _ampspercount = _voltspercount/_ampspervolt
    # _description_result = "ADC Counts{Bits 31:1} , OutOfRange Flag{Bit 0}"
    # _units_vresult = "V"
    # _units_cresult = "A"
    # _vres_tostr = lambda x: "Counts: {0}, {1}".format(x >> 1, stacks.to_eng(Dmm._voltspercount * (x >> 1)))
    # _cres_tostr = lambda x: "Counts: {0}, {1}".format(x >> 1, stacks.to_eng(Dmm._ampspercount * (x >> 1)))
    # _description_min = "ADC minimum recorded result"
    # _description_max = "ADC maximum recorded result"
    # _description_rels = "User relative offset to be applied to channel result voltage"
    # _vaux_tostr = lambda x: "Counts: {0}, {1}".format(x, stacks.to_eng(Dmm._voltspercount * x))
    # _caux_tostr = lambda x: "Counts: {0}, {1}".format(x, stacks.to_eng(Dmm._ampspercount * x))
    # _description_channelconfig = "Channel Configuration Bits"
    # _channelconfig_tostr = lambda x: "Val(Hex):{0:0X}".format(x)
    # _description_auxconfig = "Channel Aux Configuration Bits"
    # _channelauxconfig_tostr = lambda x: "Val(Hex):{0:0X}".format(x)
    # _control_tostr = lambda x: "Val(Hex):{0:0X}".format(x)
    #
    # _resultregs = []
    # _cfgregs = []
    # _minregs = []
    # _maxregs = []
    # for i in range(0, CHANNEL_COUNT):
    #     _resultregs.append(registernames[r.DMM_VCH0_RESULT+i*2])
    #     _minregs.append(registernames[r.DMM_VCH0_MIN+i*2])
    #     _maxregs.append(registernames[r.DMM_VCH0_MAX+i*2])

    SAMPLERATE_FREQUENCIES = (10.0, 16.6, 50.0, 60.0, 400.0, 1200.0, 3600.0, 14400.0, 2.5, 1.0)
    SAMPLERATE_1HZ = 9
    SAMPLERATE_2HZ5 = 8
    SAMPLERATE_10HZ = 0
    SAMPLERATE_16HZ6 = 1
    SAMPLERATE_50HZ = 2
    SAMPLERATE_60HZ = 3
    SAMPLERATE_400HZ = 4
    SAMPLERATE_1200HZ = 5
    SAMPLERATE_3600HZ = 6
    SAMPLERATE_14400HZ = 7
    SAMPLERATE_NOCHANGE = 0xF

    RANGE_AUTO = 0
    RANGE_LOW_2V5 = 1
    RANGE_MED_25V = 2
    RANGE_HIGH_250V = 3
    NO_CHANGE = 4

    CHANNELS_ALL = -1
    CHANNEL_CURRENT = 6
    CHANNEL_HIGHVOLTAGE = 7
    CHANNEL_OFFSETCAL = 8

    def __init__(self, deck):
        """:param deck: Device"""
        super(Dmm, self).__init__(deck)

        self._stream_config = None, None
        self._stream_triggered = False

        # self.deck.clear_queued()
        # self.deck.queue_read((r.DMM_VCH0_RESULT, r.DMM_RESULT_COUNTER), r.DMM_CONTROL)
        # self._all_results_req = self.deck.save_queued()

    # @staticmethod
    # def _reg_to_result(results, regname):
    #         if regname == "DMM_CCH0_RESULT":
    #             return Dmm._ampspercount * (results[regname] >> 1)
    #         else:
    #             return Dmm._voltspercount * (results[regname] >> 1)

    # @staticmethod
    # def _reg_to_vresult(lowword, hiword):
    #     return Dmm._voltspercount * (stacks.to_int32((hiword << 16) + lowword) >> 1)
    #
    # @staticmethod
    # def _reg_to_cresult(lowword, hiword):
    #     return Dmm._ampspercount * (stacks.to_int32((hiword << 16) + lowword) >> 1)

    def _action(self, action, channels, channelmask, samplerate):
        channelbits = digest_indices(channels, channelmask, clearmask=0xFF)
        self._set_control(action=action, enabled=channelbits, samplerate=samplerate)

    def freerun(self, *channels, mask=0, samplerate=SAMPLERATE_NOCHANGE, range_=NO_CHANGE):
        """Configure the dmm to continuously perform result measurements on the selected channels.

        :param channels: arbitrary list of indices. e.g. 0, 1
        :type channels: tuple[int]
        :param mask: optional bitmask of indices. e.g. 0b11
            See `Specifying Indices or Masks <http://subinitial.com/stacks/docs/indices>`_
        :type mask: int

        :param samplerate: samplerate selection. refer to Dmm.SAMPLERATE_* options
        :type samplerate: int

        :param range_: range selection. refer to Dmm.RANGE_* options
        :type range_: int
        """
        self.dev.request.queue()
        if range_ != self.NO_CHANGE:
            self.set_ranges(*channels, range_=range_)
        self._action(self.MODE_FREERUN, channels, mask, samplerate)
        self.dev.request.submit()

    def trigger(self, *channels, mask=0, samplerate=SAMPLERATE_NOCHANGE):
        """Trigger the dmm to perform a result measurement on the selected channels.

        :param channels: arbitrary list of indices. e.g. 0, 1
        :type channels: tuple[int]
        :param mask: optional bitmask of indices. e.g. 0b11
            See `Specifying Indices or Masks <http://subinitial.com/stacks/docs/indices>`_
        :type mask: int

        :param samplerate: samplerate selection. refer to Dmm.SAMPLERATE_* options
        :type samplerate: int
        """
        self._action(self.MODE_TRIGGER, channels, mask, samplerate=samplerate)

    def stop(self):
        """Stop dmm result measurements"""
        self._set_control(action=self.MODE_STOPPED, samplerate=self.SAMPLERATE_NOCHANGE)

    def reset(self):
        """Reset dmm configuration to defaults (Dmm will run in freerun mode on channels 0-6)"""
        self._set_control(action=self.MODE_RESET, samplerate=self.SAMPLERATE_NOCHANGE)

    def configure_auto_offsetcal(self, auto_offsetcal_on_not_off=True):
        """Configure automatic offset calibration mode

        :param auto_offsetcal_on_not_off: True: refresh offset calibration values automatically.
            False: use previous offset calibration value on all future measurement results.
        :type auto_offsetcal_on_not_off: bool
        """
        self._set_control(action=self.MODE_AUTOZERO, autozero_flag=auto_offsetcal_on_not_off)

    class Results(tuple):
        """:py:class:`tuple` of 8 float values representing DMM channel results 0-7 with additional status properties.

            :Properties:
                * **cyclecount** *(int)* - number of result cycles that have been completed since the last trigger or dmm reset

                * **out_of_range** *(int | Bitmask)* - bitmask of out-of-range flags indicating if the specified channel (0-7) result was measured out of range.

                * **last_channel** *(int)* - indicates which channel in this result dataset has most recently completed a result
            """
        def set_status(self, cyclecount, out_of_range, last_channel):
            self.cyclecount = cyclecount
            self.out_of_range = Bitmask(out_of_range)
            self.last_channel = last_channel
            return self

    class Result(float):
        """:py:class:`int` value representing DMM channel result with additional status properties.

            :Properties:
                * **cyclecount** *(int)* - number of result cycles that have been completed since the last trigger or dmm reset

                * **out_of_range** *(int | Bitmask)* - bitmask of out-of-range flags indicating if the specified channel (0-7) result was measured out of range.

                * **last_channel** *(int)* - indicates which channel in this result dataset has most recently completed a result
            """

        def set_status(self, cyclecount, out_of_range, last_channel):
            self.cyclecount = cyclecount
            self.out_of_range = Bitmask(out_of_range)
            self.last_channel = last_channel
            return self

    def measure_channel(self, channel, samplerate=SAMPLERATE_NOCHANGE, timeout=4.0, pollingdelay=0.05, range_=NO_CHANGE):
        """Trigger dmm to perform a measurement result on the specified channel.

        This method times out if the result takes too long.

        :param channels: channel index 0-7
        :type channels: int

        :param samplerate: samplerate selection. refer to Dmm.SAMPLERATE_* options
        :type samplerate: int

        :param timeout: seconds to wait for all requested measurement results to complete.
            Configure this based on the enabled channels, their samplerates, and their averaged samplecount requirements
        :type timeout: float

        :param pollingdelay: seconds to wait between polling the dmm for cycle-complete status
        :type pollingdelay: float

        :param range_: range selection. refer to Dmm.RANGE_* options
        :type range_: int

        :return: measurement result in Volts or Amps (If channel==6 the result is in Amps) with additional status properties

            ========== ===== ===== ===== ===== ===== ===== ===== =======
              title    VCH0  VCH1  VCH2  VCH3  VCH4  VCH5  CCH6  VCH7_HV
            ========== ===== ===== ===== ===== ===== ===== ===== =======
            **units:** Volts Volts Volts Volts Volts Volts Amps  Volts
            **index:** 0     1     2     3     4     5     6     7
            **type:**  float float float float float float float float
            ========== ===== ===== ===== ===== ===== ===== ===== =======
        :rtype: float | Dmm.Result
        """
        results = self.measure(channel, samplerate=samplerate, timeout=timeout, pollingdelay=pollingdelay, range_=range_)
        return self.Result(results[channel]).set_status(results.cyclecount, results.out_of_range, results.last_channel)


    def measure(self, *channels, mask=0, samplerate=SAMPLERATE_NOCHANGE, timeout=4.0, pollingdelay=0.05, range_=NO_CHANGE):
        """Trigger dmm to perform a measurement result on the selected channels.

        This method times out if the result takes too long.

        :param channels: arbitrary list of indices. e.g. 0, 1
        :type channels: tuple[int]
        :param mask: optional bitmask of indices. e.g. 0b11
            See `Specifying Indices or Masks <http://subinitial.com/stacks/docs/indices>`_
        :type mask: int

        :param samplerate: samplerate selection. refer to Dmm.SAMPLERATE_* options
        :type samplerate: int

        :param timeout: seconds to wait for all requested measurement results to complete.
            Configure this based on the enabled channels, their samplerates, and their averaged samplecount requirements
        :type timeout: float

        :param pollingdelay: seconds to wait between polling the dmm for cycle-complete status
        :type pollingdelay: float

        :param range_: range selection. refer to Dmm.RANGE_* options
        :type range_: int

        :return: measurement results list with additional status properties. See :py:class:`Results` for details.

            ========== ===== ===== ===== ===== ===== ===== ===== =======
              title    VCH0  VCH1  VCH2  VCH3  VCH4  VCH5  CCH6  VCH7_HV
            ========== ===== ===== ===== ===== ===== ===== ===== =======
            **units:** Volts Volts Volts Volts Volts Volts Amps  Volts
            **index:** 0     1     2     3     4     5     6     7
            **type:**  float float float float float float float float
            ========== ===== ===== ===== ===== ===== ===== ===== =======
        :rtype: (float, float, float, float, float, float, float, float) | Dmm.Results
        """

        self.dev.request.queue()
        if range_ != self.NO_CHANGE:
            self.set_ranges(*channels, range_=range_)
        self.regs.DMM_RESULT_COUNTER.write(0)  # reset the result counter
        self.trigger(*channels, mask=mask, samplerate=samplerate)
        self.dev.request.submit()
        if not do_for(timeout, lambda: self.get_cyclecounter() > 0, repeatdelay=pollingdelay):
            raise SubinitialException(
                "Timeout: Analog Deck DMM failed to perform measurement results before {0}s".format(timeout))
        return self.get_results()

    SINC1 = 0
    SINC2 = 1
    def configure_filter(self, filter=SINC2):
        """Configure the DMM digital sinc filter

        :param filter: filter selection Dmm.SINC*
        :type filter: int
        """
        self.regs.DMM_EXTRA_CONTROL.write(filter & 1)

    def stream_arm(self, channel, samplerate=SAMPLERATE_3600HZ, range_=RANGE_LOW_2V5, timeout=4.0, pollingdelay=0.010, startdelay=None, filter=SINC1, **kwargs):
        """Arms the Dmm in a streaming configuration, ready for a stream_trigger()

        :param channel: channel index to stream on
        :type channel: int

        :param samplerate: samplerate selection. refer to Dmm.SAMPLERATE_* options
        :type samplerate: int

        :param range_: range selection. refer to Dmm.RANGE_* options
        :type range_: int

        :param timeout: seconds to wait for all requested measurement results to complete.
            Configure this based on the enabled channels, their samplerates, and their averaged samplecount requirements
        :type timeout: float

        :param pollingdelay: seconds to wait between polling the dmm for cycle-complete status
        :type pollingdelay: float

        :param filter: filter selection Dmm.SINC*
        :type filter: int
        """
        if startdelay is None:
            startdelay = 0

        # Shim for backwards compatibility with 1.9.3 etc
        if "range" in kwargs:
            range_ = kwargs["range"]

        if samplerate > self.SAMPLERATE_14400HZ:
            raise SubinitialInputError("Invalid Samplerate: a streaming samplerate above 1HZ must be specified")

        regval = (channel & 0b111) | ((samplerate & 0b1111) << 4) | \
            (self.MODE_STREAM << 8) | ((range_ & 0b111) << 11) | ((filter & 1) << 14)
        self.regs.DMM_STREAM_CONTROL.write(regval)

        if not do_for(timeout, lambda: self.get_status()[2] == self.MODE_STREAM_TRIGGER, repeatdelay=pollingdelay,
                      startdelay=startdelay):
            raise SubinitialTimeoutError(
                "Timeout: Analog Deck DMM failed to prepare for streaming mode within {0}s".format(timeout))
        self._stream_config = channel, samplerate
        self._stream_triggered = False

    def stream_trigger(self):
        """Starts the Dmm stream process.

        Samples will be buffered as they are measured"""
        self.regs.DMM_STREAM_CONTROL.write(self.MODE_STREAM_TRIGGER << 8)
        self._stream_triggered = True

    def stream_stop(self):
        """Stops a running Dmm stream.

        Remaining buffered samples will be available until read with Dmm.stream()
        """
        self.stop()
        time.sleep(0.002)
        self._stream_triggered = False

    def stream(self, timeout=1000, pollingdelay=200):
        """Yield samples as they become available over the network.

        The samples are measured at the samplerate specified in dmm.stream_arm with a typical sample-period accuracy of 0.3%
        The dmm can store up to 4500 samples before its internal buffer becomes full.
        dmm.stream() must read samples often enough to prevent the buffer from becoming full if a continuous stream is desired.
        The Dmm will stop sampling new stream data if the stream buffer fills up (due to samples accumulating without
        being read) or if the user calls dmm.stream_stop().

        :param timeout: timeout represents the maximum time to wait for new samples to be available.
            If this timeout is exceeded a SubinitialTimeoutError is raised.
        :type timeout: int

        :param pollingdelay: The polling delay represents the minimum time between reads for available buffered samples.
            The minimum value of pollingdelay is limited by your network latency.
            Typical roundtrip request/response on a small LAN will take between 1 and 10ms.
        :type pollingdelay: int

        :return: float iterable
        """

        # Predetermine configuration, timeouts and pollingdelays
        channel, samplerate = self._stream_config
        upercount = 2.5/0x7FFFFF  # if channel != 6 else 1/0x7FFFFF
        samperiod = 1/self.SAMPLERATE_FREQUENCIES[samplerate]
        pollingdelay = min(pollingdelay * samperiod, 0.5)
        timeout = min(timeout * samperiod, 2.0)

        if not self._stream_triggered:
            self.stream_trigger()

        # Continually read samples from the DmmStream Fifo
        t_lastread = time.time()
        while True:
            regs = self.regs.DMM_STREAM_SAMPLES.read(350)
            word0 = regs[0]
            samplecount = len(regs) # (word0 >> 16) & 0xFFFF
            flags = (word0 & 0xFFFF)
            if samplecount == 1:
                # If no samples are available yet then delay for a while
                if time.time()-t_lastread > timeout:
                    raise SubinitialTimeoutError("Analog Deck DMM stream timed out")
                time.sleep(pollingdelay)
                continue

            # Samples were received
            t_lastread = time.time()
            idx = 1
            while idx < samplecount:
                counts = regs[idx]
                idx += 1
                # Yield each sample result to the caller individually
                yield upercount * counts

            # Limit read frequency to a maximum of once every polling delay
            time.sleep(max(pollingdelay-(time.time()-t_lastread),0))

    def get_results(self):
        """Get all previously sampled dmm channel measurements.

        :return: measurement results list with additional status properties. See :py:class:`Results` for details.

            ========== ===== ===== ===== ===== ===== ===== ===== =======
              title    VCH0  VCH1  VCH2  VCH3  VCH4  VCH5  CCH6  VCH7_HV
            ========== ===== ===== ===== ===== ===== ===== ===== =======
            **units:** Volts Volts Volts Volts Volts Volts Amps  Volts
            **index:** 0     1     2     3     4     5     6     7
            **type:**  float float float float float float float float
            ========== ===== ===== ===== ===== ===== ===== ===== =======
        :rtype: (float, float, float, float, float, float, float, float) | Dmm.Results
        """
        with self.dev as (regs, r, control, res):
            res.reads = regs.DMM_VCH0_RESULT.read(8)
            res.counter = regs.DMM_RESULT_COUNTER.read()
            res.flags = regs.DMM_FLAGS.read()
        return Dmm.Results(res.reads).set_status(res.counter >> 4, res.flags, res.counter & 0xF)

    def get_result(self, channel):
        """Get one dmm channel measurement result

        :param channel: channel index between 0-7
        :type channel: int
        :return: channel measurement result in Volts unless channel is 6 then Amps
        :rtype: float
        """
        return self.regs[self.regs.DMM_VCH0_RESULT.address + channel*2].read()


    def get_next_results(self, timeout=4.0, pollingdelay=0.05, wait_cycles=1):
        """Wait for measurements to complete then get sampled dmm channel measurements.

        Use this method to retrieve results as they become available while in MODE_FREERUN
        This method times out if the result takes too long.

        :param timeout: seconds to wait for all requested measurement results to complete.
            Configure this based on the enabled channels, their samplerates, and their averaged samplecount requirements
        :type timeout: float

        :param pollingdelay: seconds to wait between polling the dmm for cycle-complete status
        :type pollingdelay: float

        :param wait_cycles: number of full cycles to wait before returning results.
            When 0 this will return data as soon as the present cycle is finished
            When 1 this will return data 1 full cycle after the present cycle is finished
            (guaranteed fresh data from the time of calling this method)
        :type wait_cycles: int

        Data includes measurement results, the running result count, a bitmask of outofrange flags, and an
        indicator of the last channel result obtained

        :return: measurement results list with additional status properties. See :py:class:`Results` for details.

            ========== ===== ===== ===== ===== ===== ===== ===== =======
              title    VCH0  VCH1  VCH2  VCH3  VCH4  VCH5  CCH6  VCH7_HV
            ========== ===== ===== ===== ===== ===== ===== ===== =======
            **units:** Volts Volts Volts Volts Volts Volts Amps  Volts
            **index:** 0     1     2     3     4     5     6     7
            **type:**  float float float float float float float float
            ========== ===== ===== ===== ===== ===== ===== ===== =======
        :rtype: (float, float, float, float, float, float, float, float) | Dmm.Results
        """
        start_result = self.get_cyclecounter()
        next_result = start_result + wait_cycles

        def is_next_result():
            current_result = self.get_cyclecounter()
            if current_result < start_result:  # Handle wrap around
                current_result += 65536
            return current_result > next_result

        if not do_for(timeout, is_next_result, repeatdelay=pollingdelay):
            raise SubinitialException("Timeout: Analog Deck DMM failed to perform measurement results before {0}s"
                                      .format(timeout))

        return self.get_results()

    def set_all_channelranges(self, range_=RANGE_AUTO):
        """Set range selection on all Dmm channels

        :param range_: range selection. refer to Dmm.RANGE_* options
        :type range_: int
        """
        rn = range_ & 0b11
        self.regs.DMM_RANGES.write(rn << 0 | rn << 2 | rn << 4 | rn << 6 | rn << 8 | rn << 10 | rn << 14)

    def set_ranges(self, *channels, mask=0, range_=NO_CHANGE):
        """Set range selections on Dmm channels

        :param channels: arbitrary list of indices. e.g. 0, 1
        :type channels: tuple[int]
        :param mask: optional bitmask of indices. e.g. 0b11
            See `Specifying Indices or Masks <http://subinitial.com/stacks/docs/indices>`_
        :type mask: int
        :param range: range selection. refer to Dmm.RANGE_* options
        :type range: int
        """
        regval = 0
        set_channels = digest_indices(channels, mask, clearmask=0xBF)
        for i in range(0, 8):
            if set_channels & (1<<i) > 0:
                regval |= (range_ << (i*3))
            else:
                regval |= (self.NO_CHANGE << (i*3))
        self.regs.DMM_RANGES.write(regval)

    def get_ranges(self, autorange_not_actualrange=True):
        """Get range configurations for all channels

        :param autorange_not_actualrange: If False, the present range state (low:1, medium:2, or high:3)
            will be shown for channels set to auto-range instead of RANGE_AUTO:0
        :type autorange_not_actualrange: bool
        :return: A list of all 8 channel range configurations.
        :rtype: list[int]
        """
        if autorange_not_actualrange:
            rangemask = self.regs.DMM_RANGES.read()
        else:
            rangemask = self.regs.DMM_RANGE_STATUS.read()
        ranges = []
        for i in range(0, 8):
            r = (rangemask >> i*3) & 0b111
            ranges.append(r)
        return ranges


    def set_channelranges(self, range0=NO_CHANGE, range1=NO_CHANGE, range2=NO_CHANGE,
                          range3=NO_CHANGE, range4=NO_CHANGE, range5=NO_CHANGE, range7=NO_CHANGE):
        """Set range selection on each Dmm channel

        :param range0: range selection. refer to Dmm.RANGE_* options
        :type range0: int
        :param range1: range selection. refer to Dmm.RANGE_* options
        :type range1: int
        :param range2: range selection. refer to Dmm.RANGE_* options
        :type range2: int
        :param range3: range selection. refer to Dmm.RANGE_* options
        :type range3: int
        :param range4: range selection. refer to Dmm.RANGE_* options
        :type range4: int
        :param range5: range selection. refer to Dmm.RANGE_* options
        :type range5: int
        :param range7: range selection. refer to Dmm.RANGE_* options
        :type range7: int
        """
        cfg = 0
        cfg |= (range0 & 0b111) << 0
        cfg |= (range1 & 0b111) << 3
        cfg |= (range2 & 0b111) << 6
        cfg |= (range3 & 0b111) << 9
        cfg |= (range4 & 0b111) << 12
        cfg |= (range5 & 0b111) << 15
        # cfg |= (range6 & 0b11) << 18
        cfg |= (range7 & 0b11) << 21
        self.regs.DMM_RANGES.write(cfg)

    # This method is slated for 1.3.1 release
    # def set_channelrange(self, channel, range_=RANGE_AUTO):
    #     """
    #     Set range configuration for Dmm channel
    #     :param channel: int (0-5 & 7) Dmm channel index
    #     :param range: int, RANGE_xx selection between 0 and 3
    #     """
    #     cfg = 0b01 << 12  # flag that this is an individual channel range config
    #     cfg |= (channel & 0b111) << 8  # indicate which channel we'd like to configure
    #     cfg |= range_ & 0b11  # indicate the configuration to set
    #     self.deck.write(DMM_RANGES, cfg)

    def get_channelrange(self, channel):
        """Get range selection for a given channel

        :param channel: channel index between 0-7
        :type channel: int
        :return: range selection between 0-3. refer to Dmm.RANGE_* options
        :rtype: int
        """
        cfgval = self.regs.DMM_RANGES.read()
        return (cfgval >> (channel * 3)) & 0b111

    def get_cyclecounter(self):
        """Get DMM result cycle counter

        :return: number of result cycles that have been completed since the last trigger or dmm reset
        :rtype: int
        """
        return self.regs.DMM_RESULT_COUNTER.read() >> 4

    MODE_STOPPED = 0
    MODE_STREAM = 1
    MODE_TRIGGER = 2
    MODE_FREERUN = 3
    MODE_RESET = 4
    MODE_AUTOZERO = 5
    MODE_STREAM_TRIGGER = 6

    DEFAULT_ENABLED_CHANNELS = 0x17F

    def _set_control(self, enabled=DEFAULT_ENABLED_CHANNELS, action=MODE_FREERUN, autozero_flag=False,
                     samplerate=SAMPLERATE_NOCHANGE,):
        ctrl = 0
        if enabled:
            ctrl |= enabled & 0xFF
        if autozero_flag:
            ctrl |= 0x100
        if action:
            ctrl |= (action & 0b111) << 9
        if samplerate:
            ctrl |= (samplerate & 0xF) << 12
        self.regs.DMM_CONTROL.write(ctrl)

    STATUS_LASTCHANNEL_MASK = 0xF
    STATUS_MODE_MASK = 3 << 4
    STATUS_ENABLED_CHANNELS_MASK = 0xFF << 7
    STATUS_AUTO_OFFSETCAL_MASK = 0x8000

    def get_status(self):
        """Get the current control status of the DMM

        :return: (enabled_channels mask, auto_offsetcal, mode, samplerate)
        :rtype: (int, bool, int, int)
        """
        ctrl = self.regs.DMM_CONTROL.read()
        enabled_channels = 0xFF & ctrl
        auto_offsetcal = (0x100 & ctrl) > 0
        mode = (0xE00 & ctrl) >> 9
        samplerate = (0xF000 & ctrl) >> 12

        return enabled_channels, auto_offsetcal, mode, samplerate

    # def get_adctemp(self):
    #     return self.deck.read(DMM_ADCTEMP)[DMM_ADCTEMP]

    # def get_muxtemp(self):
    #     return self.deck.read(DMM_MUXTEMP)[DMM_MUXTEMP]

    # def get_chiptemp(self):
    #     return self.deck.read(CHIPTEMP)[CHIPTEMP]

    def rels_channels(self, *channels, mask=0):
        """Perform an auto-relative offset on the selected channels

        :param channels: arbitrary list of indices. e.g. 0, 1
        :type channels: tuple[int]
        :param mask: optional bitmask of indices. e.g. 0b11
            See `Specifying Indices or Masks <http://subinitial.com/stacks/docs/indices>`_
        :type mask: int
        """
        channel_list = digest_indices(channels, mask, clearmask=0xFF, return_mask_not_list=False)
        for i in channel_list:
            if 0 < i < self.CHANNEL_COUNT:
                self.regs[self.regs.DMM_VCH0_RELS.address+i*2].write(self.regs.DMM_VCH0_RELS.convert(0x7FFFFFFF))

    def set_rels(self, ch0=None, ch1=None, ch2=None, ch3=None, ch4=None, ch5=None, ch6=None, ch7=None):
        """Set each channel relative offset voltage per each Dmm channel

        :param float ch0: relative offset voltage (Pass None to specify no change)
        :param float ch1: relative offset voltage (Pass None to specify no change)
        :param float ch2: relative offset voltage (Pass None to specify no change)
        :param float ch3: relative offset voltage (Pass None to specify no change)
        :param float ch4: relative offset voltage (Pass None to specify no change)
        :param float ch5: relative offset voltage (Pass None to specify no change)
        :param float ch6: relative offset voltage (Pass None to specify no change)
        :param float ch7: relative offset voltage (Pass None to specify no change)
        """
        for chidx, chval in enumerate((ch0, ch1, ch2, ch3, ch4, ch5, ch6, ch7)):
            if chval is not None:
                self.regs[self.regs.DMM_VCH0_RELS.address+chidx*2].write(chval)


    def configure_tracktime(self, tracktime):
        """Configure the DMM channel tracktime

        :param tracktime: tracktime in seconds (e.g. 0.001 or 0.118)
        :type tracktime: float
        """
        regval = int(min(tracktime*1000, 118) * 500)
        self.regs.DMM_TRACKTIME.write(regval)

    def get_tracktime(self):
        """Get DMM channel tracktime in seconds

        :return: tracktime in seconds
        :rtype: float
        """
        regval = self.regs.DMM_TRACKTIME.read()
        return regval / 500000


class WaveformGenerator(Peripheral):
    """
    Arbitrary Waveform Generator and 16-bit Digital To Analog Converter
    """
    MODE_DC = 1
    MODE_WAVEFREERUN = 2
    MODE_WAVETRIGGERED = 4

    MAX_SAMPLECOUNT = 1000.0

    _HALFSCALE = 32768.0
    _MAXVOLTS = 5.0
    _COUNTS_PER_VOLT = _HALFSCALE / _MAXVOLTS

    def _volts_to_counts(self, volts):
        return max(min(int(round(volts * self._COUNTS_PER_VOLT, 0)), 32767), -32767)

    def set_control(self, mode=MODE_DC):
        """Set the mode among DC, Freerun mode, and Triggered mode.
        DC outputs a DC level
        Freerun mode produces an arbitrary waveform based on the data provided by wavegen.update_waveform(). The
        waveform is repeated infinitely.
        Triggered mode produces an arbitrary waveform based on the data provided by wavegen.update_waveform(). The
        waveform is only produced once when waveform.trigger() is called. The waveform's last sample is held as the
        DC value after the waveform completes.

        :param mode: refer to WaveformGenerator.MODE_* options: MODE_DC, MODE_WAVEFREERUN, or MODE_WAVETRIGGERED
        :type mode: int
        """
        self.regs.WAVEGEN_CONTROL.write(mode & 0x7)

    def trigger(self, trigger_dmm_stream=False):
        """Send a trigger command the the waveform generator

        :param trigger_dmm_stream: if true, triggers the armed DMM stream simultaneously with the wavegen trigger.
            Similar to calling dmm.stream_trigger(), except this synchronizes the wavegen and dmm triggers.
            If false, no DMM trigger is sent.
        :type trigger_dmm_stream: bool
        """
        with self.dev as (regs, r, control, res):
            if trigger_dmm_stream:
                regs.DMM_CONTROL.write(Dmm.MODE_STREAM_TRIGGER << 9)
            regs.WAVEGEN_CONTROL.write(self.MODE_WAVETRIGGERED & 0x7)

    def update_waveform(self, samplerate_hz=None, samples=None):
        """Update the Wavegen's arbitrary waveform

        :param samplerate_hz: samplerate in Hz between 7.6 and 1000000. pass None for no change. Note: sample rate is
            quantized internally.
        :type samplerate_hz: float
        :param samples: list of sample voltages between -5.0 and +5.0. Can pass 1-500 samples. pass None for no change
        :type samples: list[float]
        """
        if samplerate_hz is not None:
            sampleperiod_ns = int(round(1e9 / samplerate_hz, 0))
            self.regs.WAVEGEN_SAMPLEPERIOD.write(sampleperiod_ns)

        if samples is not None:
            samplecount = len(samples)
            if samplecount > self.MAX_SAMPLECOUNT:
                raise SubinitialInputError("Analog Deck Wavegen's arbitrary wave must be less than {0} samples".format(
                    self.MAX_SAMPLECOUNT))
            # entries = list(self._volts_to_counts(value) + 32767 for value in samples)  # Offset added for 1.4.1
            with self.dev as (regs, r, control, res):
                regs.WAVEGEN_SAMPLECOUNT.write(samplecount)
                regs.WAVEGEN_SAMPLESINDEX.write(0)
                regs.WAVEGEN_SAMPLES.write(samples)

    def set_dc(self, volts):
        """Configure the DC voltage of the Wavegen when in DC mode

        :param volts: voltage command from -5.0 to 5.0V
        :type volts: float
        """
        self.regs.WAVEGEN_DC.write(volts)

    def get_dc(self):
        """Retrieve the existing DC voltage of the Wavegen when in DC mode

        :return: volts: voltage from -5.0 to 5.0V
        :rtype: float
        """
        return self.regs.WAVEGEN_DC.read()


class SourceMeter(Peripheral):
    _DAC_MINRANGE = 0.0
    _DAC_MAXRANGE = 10.0
    _DAC_MAXCOUNTS = 4095.0

    def set_sourcevoltage(self, volts):
        """Set commanded source voltage

        :param volts: voltage command between 0-10.0
        :type volts: float
        """
        self.regs.SOURCEMETER_SOURCEVOLTAGE.write(volts)

    def get_sourcevoltage(self):
        """Get commanded source voltage

        :return: voltage command between 0-10.0
        :rtype: float
        """
        return self.regs.SOURCEMETER_SOURCEVOLTAGE.read()


    def get_metervoltage(self):
        """Get measured voltage being provided by sourcemeter

        :return: measured Volts between 0-12.0
        :rtype: float
        """
        return self.regs.SOURCEMETER_METERVOLTAGE.read()

    def get_metercurrent(self):
        """Get measured current being drawn from sourcemeter

        :return: measured Amps between 0-2.002A
        :rtype: float
        """
        return self.regs.SOURCEMETER_METERCURRENT.read()

    def get_meterpower(self):
        """Get measured power being provided by sourcemeter

        :return: measured Watts between 0-24.0W
        :rtype: float
        """
        return self.regs.SOURCEMETER_METERPOWER.read()


class PowerMeter(Peripheral):
    """
    Power Meter (Simultaneous Voltage, Current, and Power Measurements)
    """
    SAMPLERATE_357HZ = 0
    SAMPLERATE_1429HZ = 1
    SAMPLERATE_5714HZ = 2

    def __init__(self, deck, meter_id):
        self.meter_id = meter_id
        super(PowerMeter, self).__init__(deck)

    def get_voltage(self):
        """Get measured voltage

        :return: measured Volts
        :rtype: float
        """
        if self.meter_id == 0:
            return self.regs.POWERMETER0_VOLTAGE.read()
        else:
            return self.regs.POWERMETER1_VOLTAGE.read()

    def get_current(self):
        """Get measured current

        :return: measured Amps
        :rtype: float
        """
        if self.meter_id == 0:
            return self.regs.POWERMETER0_CURRENT.read()
        else:
            return self.regs.POWERMETER1_CURRENT.read()

    def get_power(self):
        """Get measured power

        :return: measured Watts
        :rtype: float
        """
        if self.meter_id == 0:
            return self.regs.POWERMETER0_POWER.read()
        else:
            return self.regs.POWERMETER1_POWER.read()


class PowerMeterStreamSample:
    """
    A single sample of the PowerMeter containing timestamp, voltage, current, and power data for both channel 0 and 1
    """
    def __init__(self, v0, i0, v1, i1, overflow, timestamp):
        self.overflow = overflow
        """Overflow flag (bool): indicates if the stream buffer overflowed"""
        self.timestamp = timestamp
        """Overflow flag (bool): starts at 0s, increments by sample-period each sample.
            If an overflow occurs this increments by 1.0s to indicate the discontinuity"""
        self.voltage0 = v0
        """Powermeter0 Voltage (float): Volts"""
        self.current0 = i0
        """Powermeter0 Current (float): Amps"""
        self.voltage1 = v1
        """Powermeter1 Voltage (float): Volts"""
        self.current1 = i1
        """Powermeter1 Current (float): Amps"""

    @property
    def power0(self):
        """Powermeter0 Power (float): Watts"""
        return self.voltage0 * self.current0

    @property
    def power1(self):
        """Powermeter1 Power (float): Watts"""
        return self.voltage1 * self.current1


class NegativePowerDac(Peripheral):
    """
    Buffered Negative Digital To Analog Converter
    """
    _MAXRANGE = -10.0
    _MAXCOUNTS = 4095.0

    def set_voltage(self, volts):
        """Set output voltage

        :param volts: Voltage between 0 and -10.0
        :type volts: float
        """
        self.regs.NEGATIVE_POWERDAC.write(-abs(volts))

    def get_voltage(self):
        """Get output voltage

        :return: Voltage between 0 and -10.0
        :rtype: float
        """
        return self.regs.NEGATIVE_POWERDAC.read()


class RgbLed(Peripheral):
    """
    Red-Green-Blue LED
    """
    COLOR_RED = 0xFF000064
    COLOR_GREEN = 0x00FF0064
    COLOR_BLUE = 0x0000FF64
    COLOR_WHITE = 0xFFFFFF50
    COLOR_YELLOW = 0xFFAA0044
    COLOR_ORANGE = 0xFFFF0044
    COLOR_PURPLE = 0xFF00FF80

    def set(self, rgbb_val):
        """Set the RGB and Brightness value of the LED

        :param rgbb_val: 32 bit value where each octet represents MSB [Red, Green, Blue, Brightness] LSB
        :type rgbb_val: int
        """
        self.regs.RGBB.write(rgbb_val)

    def get(self):
        """Get the RGB and Brightness value of the LED

        :return: 32 bit value where each octet represents MSB [Red, Green, Blue, Brightness] LSB
        :rtype: int
        """
        return self.regs.RGBB.read()


class Dio(Peripheral):
    """
    Digital Input Output
    """

    PIN_COUNT = 8
    PIN_MASK = 0xFF

    def get_status(self):
        """Read the input status of all DIO pins

        :return: 8-bit bitmask of DIO pin status (1: high, 0: low). MSB is DIO7 and LSB is DIO0
        :rtype: int | Bitmask
        """
        return self.regs.DIO_STATUS.read()

    def get_pin_status(self, pin):
        """Read the input status of a specified DIO pin

        :return: status of DIO pin status (True: high, False: low)
        :rtype: bool
        """
        return bool(self.get_status()[pin])

    def get_cmd(self):
        """Read the commanded output of all DIO pins

        :return: 8-bit bitmask of DIO pin command (1: high, 0: low). MSB is DIO7 and LSB is DIO0
        :rtype: int | Bitmask
        """
        return self.regs.DIO_CMD.read()

    def get_pin_cmd(self, pin):
        """Read the commanded output of a specified DIO pin

        :return: command of DIO pin (True: high, False: low)
        :rtype: bool
        """
        return bool(self.get_cmd()[pin])

    def cmd(self, *cmd, mask=0):
        """Set the DIO pins' output command to high state. (omitted pins will be commanded low)

        :param cmd: arbitrary list of DIO pin indices. e.g. 0, 1
        :type cmd: tuple[int]
        :param mask: optional bitmask of indices. e.g. 0b01100000 for DIO6 and DIO7
            See `Specifying Indices or Masks <http://subinitial.com/stacks/docs/indices>`_
        :type mask: int
        """
        cmd = digest_indices(cmd, mask, clearmask=self.PIN_MASK)
        self.regs.DIO_CMD.write(cmd)

    def cmd_pin(self, pin, cmd):
        """Set the commanded output of the specified DIO pin

        :param pin: index of DIO pin
        :type pin: int
        :param cmd: commanded state of pin, 1: high, 0: low
        :type cmd: int
        """
        if cmd:
            self.set(pin)
        else:
            self.clear(pin)

    def set(self, *pins, mask=0):
        """Set the DIO pins' output command to high state

        :param pins: arbitrary list of DIO pin indices. e.g. 0, 1
        :type pins: tuple[int]
        :param mask: optional bitmask of indices. e.g. 0b01100000 for DIO6 and DIO7
            See `Specifying Indices or Masks <http://subinitial.com/stacks/docs/indices>`_
        :type mask: int
        """
        pins = digest_indices(pins, mask, clearmask=self.PIN_MASK)
        self.regs.DIO_SET.write(pins)

    def clear(self, *pins, mask=0):
        """Clear the DIO pins' output command to low state

        :param pins: arbitrary list of DIO pin indices. e.g. 0, 1
        :type pins: tuple[int]
        :param mask: optional bitmask of indices. e.g. 0b01100000 for DIO6 and DIO7
            See `Specifying Indices or Masks <http://subinitial.com/stacks/docs/indices>`_
        :type mask: int
        """
        pins = digest_indices(pins, mask, clearmask=self.PIN_MASK)
        self.regs.DIO_CLEAR.write(pins)

    _CONFIG_VOLTAGE_KEY = 0x1500

    def set_config(self, dio0_3_innotout=True, dio4_7_innotout=True, logic5v_not3v3=None):
        """Configure the DIO pin directions and logic level

        :param dio0_3_innotout: True: DIO 0-3 set to input, False: DIO 0-3 set to outputs
        :type dio0_3_innotout: bool
        :param dio4_7_innotout: True: DIO 4-7 set to input, False: DIO 4-7 set to outputs
        :type dio4_7_innotout: bool
        :param logic5v_not3v3: True: 5V logic, False: 3V3 logic
        :type logic5v_not3v3: bool
        """
        cfg = 0
        if logic5v_not3v3 is not None:
            cfg |= Dio._CONFIG_VOLTAGE_KEY
            if logic5v_not3v3:
                cfg |= 0x8000
        if dio0_3_innotout:
            cfg |= 1 << 0
        if dio4_7_innotout:
            cfg |= 1 << 4
        self.regs.DIO_CONFIG.write(cfg)

    def get_config(self):
        """Get the existing DIO configuration of DIO pin directions and logic level

        :return: (dio0_3_innotout, dio4_7_innotout, logic5v_not3v3)
        :rtype: (bool, bool, bool)
        """
        cfg = self.regs.DIO_CONFIG.read()
        dio4_7_innotout = (cfg & (1 << 4)) > 0
        dio0_3_innotout = (cfg & (1 << 0)) > 0
        logic5v_not3v3 = (cfg & 0x8000) > 0
        return dio0_3_innotout, dio4_7_innotout, logic5v_not3v3


class IsolatedInputs(Peripheral):
    """
    Opto-Isolated Input Pins
    """

    def get_status(self):
        """Get the input status of the isolated inputs

        :return: Bitmask of isolated inputs' status
        :rtype: int | Bitmask
        """
        return self.regs.ISOLATEDINPUTS_STATUS.read()


class SolenoidDrivers(Peripheral):
    """
    Low-Side-Switched Solenoid Drivers
    """
    DRIVER_MASK = 0xF

    def get_cmd(self):
        """Read the command status of the solenoid drivers

        :return: Bitmask of solenoid driver commands (1: engaged, 0: disengaged)
        :rtype: int | Bitmask
        """
        return self.regs.SOLENOIDDRIVERS_CMD.read()

    def cmd(self, *cmd, mask=0):
        """Write solenoid driver commands

        :param cmd: arbitrary list of indices. e.g. 0, 1
        :type cmd: tuple[int]
        :param mask: optional bitmask of indices. e.g. 0b11
            See `Specifying Indices or Masks <http://subinitial.com/stacks/docs/indices>`_
        :type mask: int
        """
        cmdbits = digest_indices(cmd, mask, clearmask=self.DRIVER_MASK)
        self.regs.SOLENOIDDRIVERS_CMD.write(cmdbits)

    def engage(self, *drivers, mask=0):
        """Engage solenoid drivers

        :param drivers: arbitrary list of indices. e.g. 0, 1
        :type drivers: tuple[int]
        :param mask: optional bitmask of indices. e.g. 0b11
            See `Specifying Indices or Masks <http://subinitial.com/stacks/docs/indices>`_
        :type mask: int
        """
        driverbits = digest_indices(drivers, mask, clearmask=self.DRIVER_MASK)
        self.regs.SOLENOIDDRIVERS_ENGAGE.write(driverbits)

    def disengage(self, *drivers, mask=0):
        """Disengage solenoid drivers

        :param drivers: arbitrary list of indices. e.g. 0, 1
        :type drivers: tuple[int]
        :param mask: optional bitmask of indices. e.g. 0b11
            See `Specifying Indices or Masks <http://subinitial.com/stacks/docs/indices>`_
        :type mask: int
        """
        driverbits = digest_indices(drivers, mask, clearmask=self.DRIVER_MASK)
        self.regs.SOLENOIDDRIVERS_DISENGAGE.write(driverbits)


class Relays(Peripheral):
    """
    Solid-State Relays
    """

    RELAY_MASK = 0b11

    def get_cmd(self):
        """Read the command status of the relays

        :return: Bitmask of relay commands (1: engaged, 0: disengaged)
        :rtype: int | Bitmask
        """
        return self.regs.RELAYS_CMD.read()

    def cmd(self, *cmd, mask=0):
        """Write relays commands

        :param cmd: arbitrary list of indices. e.g. 0, 1
        :type cmd: tuple[int]
        :param mask: optional bitmask of indices. e.g. 0b11
            See `Specifying Indices or Masks <http://subinitial.com/stacks/docs/indices>`_
        :type mask: int
        """
        cmd = digest_indices(cmd, mask, clearmask=self.RELAY_MASK)
        self.regs.RELAYS_CMD.write(cmd)

    def engage(self, *relays, mask=0):
        """Engage relays

        :param relays: arbitrary list of indices. e.g. 0, 1
        :type relays: tuple[int]
        :param mask: optional bitmask of indices. e.g. 0b11
            See `Specifying Indices or Masks <http://subinitial.com/stacks/docs/indices>`_
        :type mask: int
        """
        relaybits = digest_indices(relays, mask, clearmask=self.RELAY_MASK)
        self.regs.RELAYS_ENGAGE.write(relaybits)

    def disengage(self, *relays, mask=0):
        """Disengage relays

        :param relays: arbitrary list of indices. e.g. 0, 1
        :type relays: tuple[int]
        :param mask: optional bitmask of indices. e.g. 0b11
            See `Specifying Indices or Masks <http://subinitial.com/stacks/docs/indices>`_
        :type mask: int
        """
        relaybits = digest_indices(relays, mask, clearmask=self.RELAY_MASK)
        self.regs.RELAYS_DISENGAGE.write(relaybits)


class ACRipple(Peripheral):
    pass


class Stepper(Peripheral):
    pass