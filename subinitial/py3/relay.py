# -*- coding: UTF-8 -*-
# Relay Device interface
# © 2012-2018 Subinitial LLC. All Rights Reserved
# Redistribution and use in source and binary forms, with or without modification, are permitted provided that the
# following conditions are met: Redistributions of source code and binary forms must retain the above copyright notice,
# these conditions, and the disclaimer below all in plain text. The name Subinitial may not be used to endorse or
# promote products derived from this software without prior written consent from Subinitial LLC. This software may only
# be redistributed and used with a Subinitial Stacks product.

# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
# warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. Refer to the included LICENSE.txt for more details.

from subinitial.py3.stacks import *
from subinitial.stacks.utils import digest_indices, mask_to_bits, Bitmask

VERSION_MAJOR = 1


class RelayDeck(Device):
    ASSEMBLY = "SA13731"
    FIRMWARE_MIN = "v1.15.0"

    def __init__(self, core_deck, bus_address, verify_connection=True):
        """Construct Relay Deck instance

        :param core_deck: Stacks Core instance that is controlling this Device
        :type core_deck: subinitial.py3.core1.Core
        :param bus_address: bus address from 2-31
        :type bus_address: int
        """
        Device.__init__(self, core_deck, bus_address, verify_connection=verify_connection)
        self.regs = RegisterSet(self)
        """:type: RegisterSet"""
        self.relay0 = SpdtRelay(self, 0)
        """:type: SpdtRelay"""
        self.relay1 = SpdtRelay(self, 1)
        """:type: SpdtRelay"""
        self.relay2 = DpdtRelay(self, 2)
        """:type: DpdtRelay"""
        self.relay3 = DpdtRelay(self, 3)
        """:type: DpdtRelay"""
        self.relay4 = SolidStateRelay(self, 4)
        """:type: SolidStateRelay"""
        self.relay5 = SolidStateRelay(self, 5)
        """:type: SolidStateRelay"""
        self.relays = RelayCollection((self.relay0, self.relay1, self.relay2, self.relay3, self.relay4, self.relay5))
        """:type: RelayCollection"""
        self.relays._init_deck(self)
        """:type: list[RelayBase]"""

        self.isolatedinputs = IsolatedInputs(self)
        """:type: IsolatedInputs"""
        self.rgbled = RgbLed(self)
        """:type: RgbLed"""

    def __enter__(self):
        return self.regs, self.r, self.control, self.request.queue()

    def __exit__(self, type, value, traceback):
        self.request.submit()


class RTypes:
#/*%regdef%*/
    class Bitmask16(Register):
        @staticmethod
        def convert(val):
            return Bitmask(val)
    
        @staticmethod
        def revert(val):
            return val
    
    class IReg(RegisterI16):
        @staticmethod
        def convert(val):
            return 0.002 * val
    
        @staticmethod
        def revert(val):
            return round(val / 0.002)
    
    class IRmsReg(RegisterI16):
        @staticmethod
        def convert(val):
            return 0.001 * val
    
        @staticmethod
        def revert(val):
            return round(val / 0.001)
    
    class TReg(RegisterI16):
        @staticmethod
        def convert(val):
            return 0.001 * val
    
        @staticmethod
        def revert(val):
            return round(val / 0.001)
    
    class Reg5V5(RegisterI16):
        @staticmethod
        def convert(val):
            return 0.001 * val
    
        @staticmethod
        def revert(val):
            return round(val / 0.001)
    
#/*%end_regdef%*/

class RegisterSet(RegisterSetBase):
    Addresses = {
        #/*%regaddr%*/
        0: "REG0",
        1: "KEY",
        2: "SERIALNUMBER",
        4: "FIRMWARE",
        6: "ASSEMBLY_NUMBER",
        7: "BUSADDRESS",
        8: "CONTROL",
        9: "STATUS",
        10: "CONFIG_ADDRESS",
        12: "CONFIG",
        14: "UPTIME",
        16: "BROADCAST_STATUS",
        17: "REQUEST_STATUS",
        18: "GUID",
        19: "SESSION",
        20: "GPR0",
        21: "GPR1",
        22: "GPR2",
        23: "GPR3",
        24: "TRIGGER",
        33: "RELAYS_CMD",
        34: "RELAYS_STATUS",
        35: "RELAYS_FAULTS",
        36: "INPUT_STATUS",
        37: "RELAY0_CURRENT",
        38: "RELAY1_CURRENT",
        39: "RELAY2A_CURRENT",
        40: "RELAY2B_CURRENT",
        41: "RELAY3A_CURRENT",
        42: "RELAY3B_CURRENT",
        43: "RELAYS_ENGAGE",
        44: "RELAYS_DISENGAGE",
        45: "RELAY0_CURRENT_MAX",
        46: "RELAY1_CURRENT_MAX",
        47: "RELAY2A_CURRENT_MAX",
        48: "RELAY2B_CURRENT_MAX",
        49: "RELAY3A_CURRENT_MAX",
        50: "RELAY3B_CURRENT_MAX",
        51: "RELAY0_CURRENT_MIN",
        52: "RELAY1_CURRENT_MIN",
        53: "RELAY2A_CURRENT_MIN",
        54: "RELAY2B_CURRENT_MIN",
        55: "RELAY3A_CURRENT_MIN",
        56: "RELAY3B_CURRENT_MIN",
        57: "RELAY0_CURRENT_RMS",
        58: "RELAY1_CURRENT_RMS",
        59: "RELAY2A_CURRENT_RMS",
        60: "RELAY2B_CURRENT_RMS",
        61: "RELAY3A_CURRENT_RMS",
        62: "RELAY3B_CURRENT_RMS",
        63: "RELAY4_TEMP",
        64: "RELAY5_TEMP",
        65: "BOARD_TEMP",
        66: "CHIP_TEMP",
        67: "RELAY4_TEMP_MAX",
        68: "RELAY5_TEMP_MAX",
        69: "BOARD_TEMP_MAX",
        70: "CHIP_TEMP_MAX",
        71: "RELAY4_TEMP_MIN",
        72: "RELAY5_TEMP_MIN",
        73: "BOARD_TEMP_MIN",
        74: "CHIP_TEMP_MIN",
        75: "SENSE_5V5",
        76: "LEDS",
        78: "RGBB",
        80: "RGBB_EXCEPTION_STATUS",
        81: "RGBB_EXCEPTION_SUPRESSION",
        82: "INPUT0_RISE_ACTION",
        83: "INPUT0_FALL_ACTION",
        84: "INPUT1_RISE_ACTION",
        85: "INPUT1_FALL_ACTION",
        86: "RELAY0_CURRENT_TRIP",
        87: "RELAY1_CURRENT_TRIP",
        88: "RELAY2A_CURRENT_TRIP",
        89: "RELAY2B_CURRENT_TRIP",
        90: "RELAY3A_CURRENT_TRIP",
        91: "RELAY3B_CURRENT_TRIP",
        92: "RELAY4_TEMP_TRIP",
        93: "RELAY5_TEMP_TRIP",
        94: "RELAY0_CURRENT_RELS",
        95: "RELAY1_CURRENT_RELS",
        96: "RELAY2A_CURRENT_RELS",
        97: "RELAY2B_CURRENT_RELS",
        98: "RELAY3A_CURRENT_RELS",
        99: "RELAY3B_CURRENT_RELS",
        100: "RELAY0_CURRENT_WARNING",
        101: "RELAY1_CURRENT_WARNING",
        102: "RELAY2A_CURRENT_WARNING",
        103: "RELAY2B_CURRENT_WARNING",
        104: "RELAY3A_CURRENT_WARNING",
        105: "RELAY3B_CURRENT_WARNING",
        106: "RELAY4_TEMP_WARNING",
        107: "RELAY5_TEMP_WARNING",
        #/*%end_regaddr%*/
    }
    def __init__(self, device):
        self.dev = device
        #/*%reginst%*/
        self.REG0 = Register(0, device)
        self.KEY = Register(1, device)
        self.SERIALNUMBER = RegisterU32(2, device)
        self.FIRMWARE = RegisterU32(4, device)
        self.ASSEMBLY_NUMBER = Register(6, device)
        self.BUSADDRESS = Register(7, device)
        self.CONTROL = Register(8, device)
        self.STATUS = Register(9, device)
        self.CONFIG_ADDRESS = RegisterU32(10, device)
        self.CONFIG = RegisterU32(12, device)
        self.UPTIME = RegisterU32(14, device)
        self.BROADCAST_STATUS = Register(16, device)
        self.REQUEST_STATUS = Register(17, device)
        self.GUID = RegisterBytes(18, device)
        self.SESSION = RegisterBytes(19, device)
        self.GPR0 = Register(20, device)
        self.GPR1 = Register(21, device)
        self.GPR2 = Register(22, device)
        self.GPR3 = Register(23, device)
        self.TRIGGER = Register(24, device)
        self.RELAYS_CMD = RTypes.Bitmask16(33, device)
        self.RELAYS_STATUS = RTypes.Bitmask16(34, device)
        self.RELAYS_FAULTS = RTypes.Bitmask16(35, device)
        self.INPUT_STATUS = RTypes.Bitmask16(36, device)
        self.RELAY0_CURRENT = RTypes.IReg(37, device)
        self.RELAY1_CURRENT = RTypes.IReg(38, device)
        self.RELAY2A_CURRENT = RTypes.IReg(39, device)
        self.RELAY2B_CURRENT = RTypes.IReg(40, device)
        self.RELAY3A_CURRENT = RTypes.IReg(41, device)
        self.RELAY3B_CURRENT = RTypes.IReg(42, device)
        self.RELAYS_ENGAGE = Register(43, device)
        self.RELAYS_DISENGAGE = Register(44, device)
        self.RELAY0_CURRENT_MAX = RTypes.IReg(45, device)
        self.RELAY1_CURRENT_MAX = RTypes.IReg(46, device)
        self.RELAY2A_CURRENT_MAX = RTypes.IReg(47, device)
        self.RELAY2B_CURRENT_MAX = RTypes.IReg(48, device)
        self.RELAY3A_CURRENT_MAX = RTypes.IReg(49, device)
        self.RELAY3B_CURRENT_MAX = RTypes.IReg(50, device)
        self.RELAY0_CURRENT_MIN = RTypes.IReg(51, device)
        self.RELAY1_CURRENT_MIN = RTypes.IReg(52, device)
        self.RELAY2A_CURRENT_MIN = RTypes.IReg(53, device)
        self.RELAY2B_CURRENT_MIN = RTypes.IReg(54, device)
        self.RELAY3A_CURRENT_MIN = RTypes.IReg(55, device)
        self.RELAY3B_CURRENT_MIN = RTypes.IReg(56, device)
        self.RELAY0_CURRENT_RMS = RTypes.IRmsReg(57, device)
        self.RELAY1_CURRENT_RMS = RTypes.IRmsReg(58, device)
        self.RELAY2A_CURRENT_RMS = RTypes.IRmsReg(59, device)
        self.RELAY2B_CURRENT_RMS = RTypes.IRmsReg(60, device)
        self.RELAY3A_CURRENT_RMS = RTypes.IRmsReg(61, device)
        self.RELAY3B_CURRENT_RMS = RTypes.IRmsReg(62, device)
        self.RELAY4_TEMP = RTypes.TReg(63, device)
        self.RELAY5_TEMP = RTypes.TReg(64, device)
        self.BOARD_TEMP = RTypes.TReg(65, device)
        self.CHIP_TEMP = RTypes.TReg(66, device)
        self.RELAY4_TEMP_MAX = RTypes.TReg(67, device)
        self.RELAY5_TEMP_MAX = RTypes.TReg(68, device)
        self.BOARD_TEMP_MAX = RTypes.TReg(69, device)
        self.CHIP_TEMP_MAX = RTypes.TReg(70, device)
        self.RELAY4_TEMP_MIN = RTypes.TReg(71, device)
        self.RELAY5_TEMP_MIN = RTypes.TReg(72, device)
        self.BOARD_TEMP_MIN = RTypes.TReg(73, device)
        self.CHIP_TEMP_MIN = RTypes.TReg(74, device)
        self.SENSE_5V5 = RTypes.Reg5V5(75, device)
        self.LEDS = Register(76, device)
        self.RGBB = RegisterU32(78, device)
        self.RGBB_EXCEPTION_STATUS = Register(80, device)
        self.RGBB_EXCEPTION_SUPRESSION = Register(81, device)
        self.INPUT0_RISE_ACTION = Register(82, device)
        self.INPUT0_FALL_ACTION = Register(83, device)
        self.INPUT1_RISE_ACTION = Register(84, device)
        self.INPUT1_FALL_ACTION = Register(85, device)
        self.RELAY0_CURRENT_TRIP = RTypes.IReg(86, device)
        self.RELAY1_CURRENT_TRIP = RTypes.IReg(87, device)
        self.RELAY2A_CURRENT_TRIP = RTypes.IReg(88, device)
        self.RELAY2B_CURRENT_TRIP = RTypes.IReg(89, device)
        self.RELAY3A_CURRENT_TRIP = RTypes.IReg(90, device)
        self.RELAY3B_CURRENT_TRIP = RTypes.IReg(91, device)
        self.RELAY4_TEMP_TRIP = RTypes.TReg(92, device)
        self.RELAY5_TEMP_TRIP = RTypes.TReg(93, device)
        self.RELAY0_CURRENT_RELS = RTypes.IReg(94, device)
        self.RELAY1_CURRENT_RELS = RTypes.IReg(95, device)
        self.RELAY2A_CURRENT_RELS = RTypes.IReg(96, device)
        self.RELAY2B_CURRENT_RELS = RTypes.IReg(97, device)
        self.RELAY3A_CURRENT_RELS = RTypes.IReg(98, device)
        self.RELAY3B_CURRENT_RELS = RTypes.IReg(99, device)
        self.RELAY0_CURRENT_WARNING = RTypes.IReg(100, device)
        self.RELAY1_CURRENT_WARNING = RTypes.IReg(101, device)
        self.RELAY2A_CURRENT_WARNING = RTypes.IReg(102, device)
        self.RELAY2B_CURRENT_WARNING = RTypes.IReg(103, device)
        self.RELAY3A_CURRENT_WARNING = RTypes.IReg(104, device)
        self.RELAY3B_CURRENT_WARNING = RTypes.IReg(105, device)
        self.RELAY4_TEMP_WARNING = RTypes.TReg(106, device)
        self.RELAY5_TEMP_WARNING = RTypes.TReg(107, device)
        #/*%end_reginst%*/

    def __getitem__(self, addr):
        if type(addr) == int:
            addr = RegisterSet.Addresses[addr]
        return getattr(self, addr)


class Peripheral(object):
    """Base class for custom Device peripherals"""
    def __init__(self, device):
        self.dev = device
        """:type: RelayDeck"""
        self.regs = device.regs
        """:type: RegisterSet"""


class RelayCollection(list):
    """A collection of relays in the Relay Deck by index"""
    def _init_deck(self, device):
        self.dev = device
        """:type: RelayDeck"""
        self.regs = device.regs
        """:type: RegisterSet"""

    def cmd(self, *relays, mask=0):
        """Command specified relays to engaged (and disengage omitted relays).

        :param channels: arbitrary list of indices. e.g. 0, 1, 12, 13, "F3", "A2"
        :type channels: tuple[int|str]
        :param mask: optional bitmask of indices. e.g. 0b11
            See `Specifying Indices or Masks <http://subinitial.com/stacks/docs/indices>`_
        :type mask: int
        """
        bits = digest_indices(relays, mask)
        self.regs.RELAYS_CMD.write(bits)

    def engage(self, *relays, mask=0):
        """Engage relays.

        :param channels: arbitrary list of indices. e.g. 0, 1, 12, 13, "F3", "A2"
        :type channels: tuple[int|str]
        :param mask: optional bitmask of indices. e.g. 0b11
            See `Specifying Indices or Masks <http://subinitial.com/stacks/docs/indices>`_
        :type mask: int
        """
        bits = digest_indices(relays, mask)
        self.regs.RELAYS_ENGAGE.write(bits)

    def get_commanded(self):
        """Get commanded status bitmask of the all relays that are engaged

        Note: Mirrors get_status.

        :returns: bitmask of relays commanded status by index
        :rvalue: int | Bitmask
        """
        return self.regs.RELAYS_CMD.read()

    def get_status(self):
        """Get relay status bitmask of the all relays

        :returns: bitmask of relays status by index
        :rvalue: int | Bitmask
        """
        return self.regs.RELAYS_STATUS.read()

    def get_faults(self):
        """Get relay fault status bitmask of the all relays

        bit0: relay 0 fuse fault
        bit1: relay 1 fuse fault
        bit2: relay 2A fuse fault
        bit3: relay 2B fuse fault
        bit4: relay 3A fuse fault
        bit5: relay 3B fuse fault
        bit6: relay 4 over-temp fault
        bit7: relay 5 over-temp fault

        A fuse fault indicates detection of a failed or missing fuse.

        An over-temperature fault indicates the temperature of the relay was measured abnormally high
        and the relay was disabled. The temperature of the relay must fall prior to further operation.

        :returns: bitmask of relays status by index
        :rvalue: int | Bitmask
        """
        return self.regs.RELAYS_FAULTS.read()

    def clear_faults(self):
        """Clear all fault status of all relays"""
        self.regs.RELAYS_FAULTS.write(0xFFFF)

    def disengage(self, *channels, mask=0):
        """Disengage relay channels

        :param channels: arbitrary list of indices. e.g. 0, 1, 12, 13, "F3", "A2"
        :type channels: tuple[int|str]
        :param mask: optional bitmask of indices. e.g. 0b11
            See `Specifying Indices or Masks <http://subinitial.com/stacks/docs/indices>`_
        :type mask: int
        """
        bits = digest_indices(channels, mask)
        self.regs.RELAYS_DISENGAGE.write(bits)

_temp_board_desc = "±600 dC PC Board Temp (12 bit)"
_temp_chip_desc = "±600 dC uC Temp (12 bit)"

class Util(object):

    # RELAY_ENABLE/DISABLE/STATUS
    Mask_RELAY0 = (0x1 << 0)
    Mask_RELAY1 = (0x1 << 1)
    Mask_RELAY2 = (0x1 << 2)
    Mask_RELAY3 = (0x1 << 3)
    Mask_RELAY4 = (0x1 << 4)
    Mask_RELAY5 = (0x1 << 5)


    def to_mask(*args):
        bitset = 0x0
        for relayId in args:
            bitset |= (0x1 << relayId)
        return bitset

    # RELAYS_FAULTS_MASKS
    Mask_RELAY0_FUSE = (0x1 << 0)
    Mask_RELAY1_FUSE = (0x1 << 1)
    Mask_RELAY2A_FUSE = (0x1 << 2)
    Mask_RELAY2B_FUSE = (0x1 << 3)
    Mask_RELAY3A_FUSE = (0x1 << 4)
    Mask_RELAY3B_FUSE = (0x1 << 5)
    Mask_RELAY4_OVERTEMP = (0x1 << 6)
    Mask_RELAY5_OVERTEMP = (0x1 << 7)
    Mask_RELAY0_OVERCURRENT = (0x1 << 8)
    Mask_RELAY1_OVERCURRENT = (0x1 << 9)
    _maskRelays = [Mask_RELAY0,
                   Mask_RELAY1,
                   Mask_RELAY2,
                   Mask_RELAY3,
                   Mask_RELAY4,
                   Mask_RELAY5]
    _maskFuses = [Mask_RELAY0_FUSE,
                  Mask_RELAY1_FUSE,
                  Mask_RELAY2A_FUSE,
                  Mask_RELAY2B_FUSE,
                  Mask_RELAY3A_FUSE,
                  Mask_RELAY3B_FUSE]

    _maskTemps = [Mask_RELAY4_OVERTEMP, Mask_RELAY5_OVERTEMP]
    _maskOverCurrent = [Mask_RELAY0_OVERCURRENT, Mask_RELAY1_OVERCURRENT]


    _relaysBitToText = ['0', '1', '2', '3', '4', '5']


def _relays_tostr(relays):
    return bits_to_csv("Relays: ", relays, "Relays: none", Util._relaysBitToText)

_maskFaults_Fuse = 0b111111              
_maskFaults_OverTemp = 0b11 << 6         
_maskFaults_OverCurrent = 0b111111 << 8  

_faultBitToText = ['0',   # RELAY0  Fuse
                   '1',   # RELAY1  Fuse
                   '2A',  # RELAY2A Fuse
                   '2B',  # RELAY2B Fuse
                   '3A',  # RELAY3A Fuse
                   '3B',  # RELAY3B Fuse
                   '4',   # RELAY4  Overtemp
                   '5',   # RELAY5  Overtemp
                   '0',   # RELAY0  Overcurrent
                   '1']   # RELAY1  Overcurrent


def _faults_tostr(faults):
    text = bits_to_csv("Fuses Blown: ", faults & _maskFaults_Fuse, "Fuses: O.K.", _faultBitToText)
    text += bits_to_csv(" / OverTemp: ", faults & _maskFaults_OverTemp, " / Temperatures: O.K.", _faultBitToText)
    text += bits_to_csv(" / OverCurrent: ", faults & _maskFaults_OverCurrent, " / Currents: O.K.",
                               _faultBitToText)
    return text


def _contains(mask, bitset):
    return (mask & bitset) > 0


def _currents_tostr(current):
    return "{:,}".format(current)


class IsolatedInputs(Peripheral):
    """
    Isolated Inputs
    """
    def get_status(self):
        """Get bitwise status of input 0 and 1

        :returns: bitwise input status of 0 (bit0) and 1 (bit1)
        :rtype: int | Bitmask
        """
        return self.regs.INPUT_STATUS.read()

    def get_input0_status(self):
        """Get status of input 0

        :returns: True indicates the input is asserted. False indicates the input is not asserted.
        :rtype: bool
        """
        status = self.regs.INPUT_STATUS.read()
        return bool(0x1 & status)

    def get_input1_status(self):
        """Get status of input 0

        :returns: True indicates the input is asserted. False indicates the input is not asserted.
        :rtype: bool
        """
        status = self.regs.INPUT_STATUS.read()
        return bool(0x2 & status)

    def clear_all_actions(self):
        """Clear all assigned actions for isolated inputs 0 and 1"""
        self.regs.INPUT0_RISE_ACTION.write(0, 0, 0, 0)
        self.regs.INPUT1_RISE_ACTION.write(0, 0, 0, 0)
        self.regs.INPUT0_FALL_ACTION.write(0, 0, 0, 0)
        self.regs.INPUT1_FALL_ACTION.write(0, 0, 0, 0)

    _inputActionToStr = (lambda x: '0x{0:04X}'.format(x))


class RelayBase(Peripheral):
    """
    Common Relay Functionality
    """
    def __init__(self, deck, id_):
        self._id = id_
        self._mask = 1 << id_
        super(RelayBase, self).__init__(deck)

    def engage(self):
        """Engage the relay"""
        self.regs.RELAYS_ENGAGE.write(self._mask)

    def disengage(self):
        """Disengage the relay"""
        self.regs.RELAYS_DISENGAGE.write(self._mask)

    def cmd(self, cmd):
        """Command the relay state to on or off

        :param cmd: commanded state on or off
        :type cmd: bool
        """
        if cmd:
            self.engage()
        else:
            self.disengage()

    def get_commanded(self):
        """Get commanded status of the relay

        Note: Mirrors get_status.

        :return: is_commanded
        :rtype: bool
        """

        return (self.regs.RELAYS_CMD.read() & self._mask) > 0

    def get_status(self):
        """Get engaged status of the relay

        :return: is_engaged
        :rtype: bool
        """
        return (self.regs.RELAYS_STATUS.read() & self._mask) > 0

    def _set_new_input_reg(self, reg, state):
        val = reg.read()
        val &= ~((1<<self._id) | (256<<self._id))
        if state is True:
            val |= 1 << self._id
        elif state is False:
            val |= 256 << self._id
        reg.write(val)

    ENGAGE = True
    DISENGAGE = False

    def input_rising_action(self, input0_relaystate=None, input1_relaystate=None):
        """Configure the relay's state to react to an Isolated Input pin rising high

        :param input0_relaystate: If True relay state will be engaged when input 0 pin rises high.
                If False relay state will be disengaged when input pin rises high.
                If None then relay state will not change from input pin rising high.
        :type relay_state: bool || None
        :param input1_relaystate: If True relay state will be engaged when input 1 pin rises high.
                If False relay state will be disengaged when input pin rises high.
                If None then relay state will not change from input pin rising high.
        :type relay_state: bool || None
        """
        self._set_new_input_reg(self.regs.INPUT0_RISE_ACTION, input0_relaystate)
        self._set_new_input_reg(self.regs.INPUT1_RISE_ACTION, input1_relaystate)

    def input_falling_action(self, input0_relaystate=None, input1_relaystate=None):
        """Configure the relay's state to react to an Isolated Input pin falling low

        :param input0_relaystate: If True relay state will be engaged when input 0 pin falls low.
                If False relay state will be disengaged when input pin falls low.
                If None then relay state will not change from input pin falling low.
        :type relay_state: bool || None
        :param input1_relaystate: If True relay state will be engaged when input 1 pin falls low.
                If False relay state will be disengaged when input pin falls low.
                If None then relay state will not change from input pin falling low.
        :type relay_state: bool || None
        """
        self._set_new_input_reg(self.regs.INPUT0_FALL_ACTION, input0_relaystate)
        self._set_new_input_reg(self.regs.INPUT1_FALL_ACTION, input1_relaystate)

    def inputs_clear_all_actions(self):
        """Clear all actions associated with both Isolated Input pins 0 and 1"""
        self._set_new_input_reg(self.regs.INPUT0_FALL_ACTION, None)
        self._set_new_input_reg(self.regs.INPUT1_FALL_ACTION, None)
        self._set_new_input_reg(self.regs.INPUT0_RISE_ACTION, None)
        self._set_new_input_reg(self.regs.INPUT1_RISE_ACTION, None)


class SpdtRelay(RelayBase):
    """
    Single-Pole Double-Throw (SPDT) Relay
    """
    _current_desc = "±25,000mA Relay Channel Current (12 bit)"
    AUTO_RELS_VALUE = RTypes.IReg.convert(-0x8000)
    _current_offset_desc = "Relay Channel Current Offset: Write {0} to auto-zero".format(AUTO_RELS_VALUE)
    _current_trip_desc = "±25,000mA Relay Channel Current 1Hz TRIP (12 bit)"
    _current_warn_desc = "Relay Current Warning Level"

    def get_fusefault(self):
        """Get fuse fault status

        A fuse fault indicates detection of a failed or missing fuse.

        :return: fault status, True is faulted, False is not faulted
        :rtype: bool
        """
        return (self.regs.RELAYS_FAULTS.read() & self._mask) > 0

    def clear_fusefault(self):
        """Clear a faulted fuse status"""
        return self.regs.RELAYS_FAULTS.write(self._mask)

    def get_current(self):
        """Get instantaneous current

        :return: current (Amps)
        :rtype: float
        """
        return float(round(self.regs[self.regs.RELAY0_CURRENT.address+self._id].read(), 3))

    def get_rms_current(self):
        """Get RMS current

        The RMS is taken over a 1second sample window.
        The RMS value is updated once per second.

        :return: current (Amps)
        :rtype: float
        """
        return float(round(self.regs[self.regs.RELAY0_CURRENT_RMS.address+self._id].read(), 3))

    def get_max_current(self):
        """Get maximum relay current

        The maximum relay current is recorded based on continuous monitoring of the instantaneous current.

        :return: current (Amps)
        :rtype: float
        """
        return float(round(self.regs[self.regs.RELAY0_CURRENT_MAX.address+self._id].read(), 3))

    def get_min_current(self):
        """Get minimum relay current

        The minimum relay current is recorded based on continuous monitoring of the instantaneous current.

        :return: current (Amps)
        :rtype: float
        """
        return float(round(self.regs[self.regs.RELAY0_CURRENT_MIN.address+self._id].read(), 3))

    def rels_current(self, value=AUTO_RELS_VALUE):
        """Set the relative offset for this current measurement channel

        Setting a relative offset will apply an offset to the instantaneous current value and to the RMS
        value. For RMS, the offset is applied to each measurement prior to the RMS calculation.

        Min and max currents are always recorded with no offset applied.

        :param value: The value by which to offset the measurement. (By default the offset zeros the present reading)
        :type value: float
        """
        self.regs[self.regs.RELAY0_CURRENT_RELS.address+self._id].write(value)

    def reset_max_current(self):
        """Reset the previously recorded max channel current to 0"""
        self.regs[self.regs.RELAY0_CURRENT_MAX.address+self._id].write(0)

    def reset_min_current(self):
        """Reset the previously recorded min channel current to 0"""
        self.regs[self.regs.RELAY0_CURRENT_MIN.address+self._id].write(0)


class DpdtRelay(RelayBase):
    """
    Double-Pole Double-Throw (DPDT) Relay
    """
    def get_fusefaults(self):
        """Get fuse fault status of both channels

        A fuse fault indicates detection of a failed or missing fuse.

        :return: fault status of both channels, True is faulted, False is not faulted
        :rtype: (bool, bool)
        """
        faults = self.regs.RELAYS_FAULTS.read()
        mask = faults >> 2 if self._id == 2 else faults >> 4
        return (mask & 1) > 0, (mask & 2) > 0

    def clear_fusefaults(self):
        """Clear a faulted fuse status"""
        mask = 0b1100 if self._id == 2 else 0b110000
        return self.regs.RELAYS_FAULTS.write(mask)

    def get_current(self):
        """Get instantaneous current of channel A current, channel B not available

        :return: current (Amps)
        :rtype: float
        """
        offset = self._id if self._id == 2 else 4
        return float(round(self.regs[self.regs.RELAY0_CURRENT.address+offset].read(), 3))

    def get_rms_current(self):
        """Get RMS channel A current, channel B not available

        The RMS is taken over a 1second sample window.
        The RMS value is updated once per second.

        :return: current (Amps)
        :rtype: float
        """
        offset = self._id if self._id == 2 else 4
        return float(round(self.regs[self.regs.RELAY0_CURRENT_RMS.address+offset].read(), 3))

    def get_max_current(self):
        """Get max channel A current, channel B not available

        The maximum relay current is recorded based on continuous monitoring of the instantaneous current.

        :return: current (Amps)
        :rtype: float
        """
        offset = self._id if self._id == 2 else 4
        return float(round(self.regs[self.regs.RELAY0_CURRENT_MAX.address+offset].read(), 3))

    def get_min_current(self):
        """Get min channel A current, channel B not available

        The minimum relay current is recorded based on continuous monitoring of the instantaneous current.

        :return: current (Amps)
        :rtype: float
        """
        offset = self._id if self._id == 2 else 4
        return float(round(self.regs[self.regs.RELAY0_CURRENT_MIN.address+offset].read(), 3))

    AUTO_RELS_VALUE = RTypes.IReg.convert(-0x8000)
    def rels_current(self, value=AUTO_RELS_VALUE):
        """Set the relative offset for this current measurement channel

        Setting a relative offset will apply an offset to the instantaneous current value and to the RMS
        value. For RMS, the offset is applied to each measurement prior to the RMS calculation.

        Min and max currents are always recorded with no offset applied.

        :param value: The value by which to offset the measurement. (By default the offset zeros the present reading)
        :type value: float
        """
        offset = self._id if self._id == 2 else 4
        self.regs[self.regs.RELAY0_CURRENT_RELS.address+offset].write(value)

    def reset_max_current(self):
        """Reset the previously recorded max channel A current to 0, channel B not available"""
        offset = self._id if self._id == 2 else 4
        self.regs[self.regs.RELAY0_CURRENT_MAX.address+offset].write(0)

    def reset_min_current(self):
        """Reset the previously recorded min channel A current to 0, channel B not available"""
        offset = self._id if self._id == 2 else 4
        self.regs[self.regs.RELAY0_CURRENT_MIN.address+offset].write(0)


_temp_units = "°C"
_temp_desc = "±600 dC Solid State Relay Temp (12 bit)"


class SolidStateRelay(RelayBase):
    """
    Solid State Relay
    """

    def get_tempfault(self):
        """Get over-temperature fault status

        An over-temperature fault indicates the temperature of the relay was measured abnormally high
        and the relay was disabled. The temperature of the relay must fall prior to further operation.

        :return: fault status
        :rtype: bool
        """
        faults = self.regs.RELAYS_FAULTS.read()
        mask = 1 << (self._id + 2)
        return (faults & mask) > 0

    def clear_tempfault(self):
        """Clear an overtemperature fault"""
        self.regs.RELAYS_FAULTS.write(1 << (self._id+2))


class Board(Peripheral):
    def get_sense5v5(self):
        return self.regs.SENSE_5V5.read()

class RgbLed(Peripheral):
    """
    Red-Green-Blue LED
    """
    COLOR_RED = 0xFF000064
    COLOR_GREEN = 0x00FF0064
    COLOR_BLUE = 0x0000FF64
    COLOR_WHITE = 0xFFFFFF50
    COLOR_YELLOW = 0xFFAA0044
    COLOR_ORANGE = 0xFFFF0044
    COLOR_PURPLE = 0xFF00FF80

    def set(self, rgbb_val):
        """Set the RGB and Brightness value of the front LED

        :param rgbb_val: 32 bit value where each octet represents MSB [Red, Green, Blue, Brightness] LSB
        :type rgbb_val: int
        """
        self.regs.RGBB.write(rgbb_val)

    def get(self):
        """Get the RGB and Brightness value of the front LED

        :return: 32 bit value where each octet represents MSB [Red, Green, Blue, Brightness] LSB
        :rtype: int
        """
        return self.regs.RGBB.read()