# -*- coding: UTF-8 -*-
# Subinitial Thermocouple Reader (STCR)
# © 2012-2018 Subinitial LLC. All Rights Reserved
# Redistribution and use in source and binary forms, with or without modification, are permitted provided that the
# following conditions are met: Redistributions of source code and binary forms must retain the above copyright notice,
# these conditions, and the disclaimer below all in plain text. The name Subinitial may not be used to endorse or
# promote products derived from this software without prior written consent from Subinitial LLC. This software may only
# be redistributed and used with a Subinitial Stacks product.

# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
# warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. Refer to the included LICENSE.txt for more details.

from subinitial.py3.landevice import *


class _ChannelDebug:
    def __init__(self, reprstr):
        (self.idx,
            self.hjunc,
            self.delta,
            self.cjunc,
            self.adc0,
            self.adc1,
            self.adc2,
            self.status,
            self.sensor_config,
            self.dev_config) = tuple(int(x) for x in reprstr.split(","))

    @property
    def adc(self):
        return (self.adc0 << 16) + (self.adc1 << 8) + self.adc2

    def __repr__(self):
        return "{}={}{{{}+{}}}-[{}]-{}:{}:{}".format(
            self.idx,
            self.hjunc/16, self.delta/16, self.cjunc/16,
            self.adc,
            hex(self.status), hex(self.sensor_config), hex(self.dev_config)
        )


class ChannelResults(list):
    """
        Thermocouple Channel Result Set
    """

    def __init__(self):
        list.__init__(self)
        self.error = None
        """Error (string or None): indicates if an error occurred during the acquisition of this result set"""
        self.timestamp = 0.0
        """Result Timestamp (float): indicates the time in seconds when the measurement was taken"""
        self.total_acquisition_time = 0.0
        """:type: float"""

    @classmethod
    def _new(cls, error, timestamp, total_acquisition_time):
        self = cls()
        for i in range(8):
            self.append(None)
        self.error = error
        self.timestamp = timestamp
        self.total_acquisition_time = total_acquisition_time
        return self


class ChannelResult(float):
    """
    Thermocouple Channel Result

    This result object is a floating point value of the temperature measured in degrees Celcius with additional attributes.
    """
    def __init__(self, arg):
        # float.__init__(self)
        self.idx = 0
        """Channel Index (int): indicates what channel this result is from"""
        self._coldjunction = 0.0
        """:type: float"""
        # self.error = False
        # """Error (bool): indicates if a timeout error occurred during the acquisition of this result"""
        self.is_open = False
        """Is TC Open (bool): indicates if the thermocouple is detected as being 
            disconnected/open-circuit"""
        self.is_alerted = False
        """Is TC alerted (bool): indicates if the thermocouple reading is in the user specified alert limit 
            conditions"""
        self.is_relay_engaged = False
        """Is TC relay engaged (bool): indicates if the channel's relay is engaged during 
            this sample due to alert conditions"""
        # self.timestamp = 0.0
        # """Result Timestamp (float): indicates the time in seconds when the measurement was taken"""
        self._acquisition_time = 0.0
        """:type: float"""
        self._volts = 0.0
        """:type: float"""
        # self._total_acquisition_time = 0.0
        # """:type: float"""

    @classmethod
    def _new(cls, respstr):
        if respstr:
            parts = respstr.split(",")
            if len(parts) < 8:
                raise Exception("invalid response data")
            self = cls(parts[1])
            self.idx = int(parts[0])
            self._coldjunction = float(parts[2])
            self._volts = float(parts[3])
            self._acquisition_time = float(parts[4])
            self.is_open = parts[5] != "0"
            self.is_alerted = parts[6] != "0"
            self.is_relay_engaged = parts[7] != "0"
        else:
            self = cls(-999)
        return self

    def to_string(self):
        retval = "Temperature: {}C @{}s".format(float.__repr__(self), self.timestamp)
        pieces = []
        if self.error:
            pieces.append("Error:{}".format(self.error))
        if self.is_open:
            pieces.append("OpenDetected")
        if self.is_alerted:
            pieces.append("Alerted")
        if self.is_relay_engaged:
            pieces.append("RelayEngaged")
        if len(pieces):
            return "{} [{}]".format(retval, "|".join(pieces))
        return retval


class Stcr(LanDevice):
    ASSEMBLY = "SA13931"
    FIRMWARE_MIN = "v1.0.0"

    def __init__(self, host, port=9221, autoconnect=True,  # raise_response_exceptions=True,
                 lan_retries=3, lan_timeout=5, lan_handler=None, verify_connection=False, logger=None, pause_background_reads=True):
        """Construct a Stacks Thermocouple Reader instance.
        """

        self.rgbled = RgbLed(self)
        # """:type: RgbLed"""
        self.ch0 = Channel(self, 0)
        """Channel 0 (Channel): instance ch0"""
        self.ch1 = Channel(self, 1)
        """Channel 1 (Channel): instance ch1"""
        self.ch2 = Channel(self, 2)
        """Channel 2 (Channel): instance ch2"""
        self.ch3 = Channel(self, 3)
        """Channel 3 (Channel): instance ch3"""
        self.ch4 = Channel(self, 4)
        """Channel 4 (Channel): instance ch4"""
        self.ch5 = Channel(self, 5)
        """Channel 5 (Channel): instance ch5"""
        self.ch6 = Channel(self, 6)
        """Channel 6 (Channel): instance ch6"""
        self.ch7 = Channel(self, 7)
        """Channel 7 (Channel): instance ch7"""
        self.channels = (self.ch0, self.ch1, self.ch2, self.ch3, self.ch4, self.ch5, self.ch6, self.ch7)
        """Channel 7 (list[Channel]): ch0-ch7 instances"""

        self._pause_background_reads = pause_background_reads

        LanDevice.__init__(self, host, port, autoconnect, False,  # raise_response_exceptions,
                           lan_retries, lan_timeout, lan_handler, verify_connection, logger)

    # def connect(self, host=None):
    #     LanDevice.connect(self, host)
    #     if self._pause_background_reads:
    #         self.pause_background_reads = self._pause_background_reads
    #
    # @property
    # def pause_background_reads(self):
    #     return self._pause_background_reads
    #
    # @pause_background_reads.setter
    # def pause_background_reads(self, val):
    #     resp = self.request("register_priority {}".format(to_bool(val)))
    #     if not resp:
    #         raise SubinitialResponseError("Modifying background temperature reading state failed")
    #     self.pause_background_reads = val

    PRECISION_18BIT = 0
    PRECISION_16BIT = 1
    # PRECISION_14BIT = 2
    # PRECISION_12BIT = 3

    class MeasureError(Exception):
        def __init__(self, tstamp, error):
            self.timestamp = tstamp
            self.error = error
            Exception.__init__(self, error)

        def __repr__(self):
            return "{} @t={}".format(self.error, self.timestamp)

    def measure(self, *channels, precision=PRECISION_18BIT, opendetect=True, raise_error=True):
        """Perform a temperature measurement on multiple \*channels and return a list of results

        :param channels: arbitrary list of indices (0-7). e.g. 0, 1
        :type channels: tuple[int]
        :param precision:  Precision selection.
            Higher precision requires a longer sample time. Refer to Stcr.PRECISION_* options
        :type precision: int
        :param opendetect: if True the channels will be checked for disconnected / open state at
            the cost of a longer sample time.
        :type opendetect: bool
        :return: a list of eight ChannelResults that act as floating point values. Only indices requested in
            \*channels will have valid results in the returned list; all other channel indices not requested
            will be None
        :rtype: ChannelResults or list[ChannelResult, ChannelResult, ChannelResult, ChannelResult,
            ChannelResult, ChannelResult, ChannelResult, ChannelResult]
        """
        chanstr = ",".join(str(int(x)) for x in channels)
        if not chanstr:
            raise SubinitialInputError("Channel selection must not be empty")
        if precision == Stcr.PRECISION_18BIT and opendetect is True:
            resp = self.request("meas? {}".format(chanstr))
        else:
            resp = self.request("meas? {} {} {}".format(chanstr, int(precision), int(opendetect)))

        respstrs = resp.split()
        tstamp = float(respstrs[0])
        aqtime = float(respstrs[1])
        error = respstrs[2]
        if error == "None":
            error = None
        results = ChannelResults._new(error, tstamp, aqtime)
        """:type: ChannelResults or list[ChannelResult]"""

        if error:
            if raise_error:
                raise self.MeasureError(tstamp, error)
            else:
                err_res = ChannelResult._new(None)
                for idx in channels:
                    results[idx] = err_res
        else:
            for respstr in respstrs[3:]:
                result = ChannelResult._new(respstr)
                results[result.idx] = result
        return results

    def _measure_debug(self):
        """debug use only

        :rtype: list[_ChannelDebug]
        """
        respstrs = self.request("meas_dbg?").split()
        tstamp = float(respstrs[0])
        aqtime = float(respstrs[1])
        error = respstrs[2]
        results = [None] * 8
        if error == "None":
            error = None
        for respstr in respstrs[3:]:
            result = _ChannelDebug(respstr)
            results[result.idx] = result
        return results

    DEFAULT = None
    """use default value"""

    NO_CHANGE = None
    """do not change device value"""

    TYPE_K = "K"
    TYPE_J = "J"
    TYPE_T = "T"
    TYPE_N = "N"
    TYPE_S = "S"
    TYPE_E = "E"
    TYPE_B = "B"
    TYPE_R = "R"

    def set_alert_config(self, idx, low_limit=NO_CHANGE, high_limit=NO_CHANGE, hysteresis=NO_CHANGE,
                         latching=NO_CHANGE):
        """Set the channel's alert conditions

            Alert conditions specify at what temperature range a channel will report an active alert status.
            During active alert status the channel's front LED will turn RED, and the channel relay can optionally
            engage to (dis)connect external circuitry. When a channel's temperature is no longer in alert range by more than
            the hysteresis(C) the alert status will deactivate. If the channel is configured to latching, then an
            active alert status remains true even after the temperature settles, until the user calls clear_alert() on
            the channel.

            Alert status is only evaluated each time a measurement is commanded.

        :param idx: channel index (0-7)
        :type idx: int
        :param low_limit: alert condition low limit temperature setpoint (Celcius). Use None for NO_CHANGE
        :type low_limit: float or None
        :param high_limit: alert condition high limit temperature setpoint (Celcius). Use None for NO_CHANGE
        :type high_limit: float or None
        :param hysteresis: recovery threshold in Celcius past the high/low limits before alert condition
            disengages while the channel is not in latching mode. Use None for NO_CHANGE
        :type hysteresis: float or None
        :param latching: if True the channel enters latching mode where alert is latched upon reaching the high/low
            limits and remains in alert state until a call to clear_alert() is made.
            (hysteresis has no effect in this mode). Use None for NO_CHANGE
        :type latching: bool or None
        """
        self.request("alert {} {} {} {} {}".format(idx, low_limit, high_limit, hysteresis, latching))

    def get_alert_config(self, idx):
        """Get the channel's alert conditions

            Alert conditions specify at what temperature range a channel will report an active alert status.
            During active alert status the channel's front LED will turn RED, and the channel relay can optionally
            engage to (dis)connect external circuitry. When a channel's temperature is no longer in alert range by more than
            the hysteresis(C) the alert status will deactivate. If the channel is configured to latching, then an
            active alert status remains true even after the temperature settles, until the user calls clear_alert() on
            the channel.

            Alert status is only evaluated each time a measurement is commanded.

        :param idx: channel index (0-7)
        :type idx: int
        :return: dictionary {low_limit: float, high_limit: float, hysteresis: float, latching: bool}
        :rtype: dict(str:float)
        """
        low_limit, high_limit, hysteresis, latching = self.request("alert? {}".format(idx)).split()
        return {"low_limit": float(low_limit), "high_limit": float(high_limit), "hysteresis": float(hysteresis),
                "latching": tobool(latching)}

    def clear_alert(self, idx):
        """Clear channel's latched alert status

        :param idx: channel index (0-7)
        :type idx: int
        """
        self.request("alertreset {}".format(idx))

    def set_relay_config(self, idx, engage_on_alert=NO_CHANGE, engage_on_open=NO_CHANGE):
        """Set the channel's relay configuration

        :param idx: channel index (0-7)
        :type idx: int
        :param engage_on_alert: if True engage relay when alert status is True. Use None for NO_CHANGE
        :type engage_on_alert: bool or None
        :param engage_on_open: if True engage relay when disconnected/open is detected. Use None for NO_CHANGE
        :type engage_on_open: bool or None
        """
        self.request("relay {} {} {}".format(idx, engage_on_alert, engage_on_open))

    def get_relay_config(self, idx):
        """Get the channel's relay configuration

        :param idx: channel index (0-7)
        :type idx: int
        :return: dictionary {engage_on_alert: bool, engage_on_open: bool}
        :rtype: dict(str:bool)
        """
        engage_on_alert, engage_on_open = self.request("relay? {}".format(idx)).split()
        return {"engage_on_alert": tobool(engage_on_alert), "engage_on_open": tobool(engage_on_open)}

    def set_tctype(self, idx, tctype):
        """Set the channel's thermocouple type

        :param idx: channel index (0-7)
        :type idx: int
        :param tctype: thermocouple letter type: K, J, T, N, S, E, B, R
        :type tctype: str
        """
        self.request("tctype {} {}".format(idx, tctype))

    def get_tctype(self, idx):
        """Get the channel's thermocouple type

        :param idx: channel index (0-7)
        :type idx: int
        :return: thermocouple letter type: K, J, T, N, S, E, B, R
        :rtype: str
        """
        return self.request("tctype? {}".format(idx))

    def _set_filter(self, threshold, timeout, adcwarnmin, adcwarnmax):
        self.request("xfilter {} {} {} {}".format(threshold, timeout, adcwarnmin, adcwarnmax))

    def _get_filter(self):
        threshold, timeout, warnmin, warnmax = self.request("xfilter?").split()
        return {"threshold": int(threshold), "timeout": float(timeout),
                "warnmin": int(warnmin), "warnmax": int(warnmax)}


class Channel:
    PRECISION_18BIT = 0
    PRECISION_16BIT = 1
    # PRECISION_14BIT = 2
    # PRECISION_12BIT = 3

    DEFAULT = None
    """use default value"""

    NO_CHANGE = None
    """do not change device value"""

    TYPE_K = "K"
    TYPE_J = "J"
    TYPE_T = "T"
    TYPE_N = "N"
    TYPE_S = "S"
    TYPE_E = "E"
    TYPE_B = "B"
    TYPE_R = "R"

    def __init__(self, dev, idx):
        self.dev = dev  # type: Stcr
        self.idx = idx
        """Channel Index (int): indicates what channel this result is from"""
        self.default_precision = self.PRECISION_18BIT
        """Default Precision (int): this value is the channel specific default that is passed to measure 
            when measure is called with measure(precision=None). This value is set to PRECISION_18BIT on init."""
        self.default_opendetect = True
        """Default OpenDetect (bool): this value is the channel specific default that is passed to measure 
            when measure is called with measure(opendetect=None). This field is set to True on init."""

    def measure(self, precision=DEFAULT, opendetect=DEFAULT):
        """Perform a temperature measurement on this channel

        :param precision: Precision selection.
            Higher precision requires a longer sample time. Refer to Stcr.PRECISION_* options.
            If None is passed this function will use self.default_precision instead
        :type precision: int
        :param opendetect: if True the channels will be checked for disconnected / open state at
            the cost of a longer sample time. If None is passed this function will use self.default_precision instead
        :type opendetect: bool
        :return: one ChannelResult of the temperature data
        :rtype: ChannelResult or float
        """
        res = self.dev.measure(self.idx, precision=precision or self.default_precision,
                               opendetect=opendetect or self.default_opendetect)
        return res[self.idx]

    def set_alert_config(self, low_limit=NO_CHANGE, high_limit=NO_CHANGE, hysteresis=NO_CHANGE, latching=NO_CHANGE):
        """Set the alert conditions for a channel.

            Alert conditions specify at what temperature range a channel will report an active alert status.
            During active alert status the channel's front LED will turn RED, and the channel relay can optionally
            engage to (dis)connect external circuitry. When a channel's temperature is no longer in alert range by more than
            the hysteresis(C) the alert status will deactivate. If the channel is configured to latching, then an
            active alert status remains true even after the temperature settles, until the user calls clear_alert() on
            the channel.

            Alert status is only evaluated each time a measurement is commanded.

        :param idx: channel index (0-7)
        :type idx: int
        :param low_limit: alert condition low limit temperature setpoint (Celcius). Use None for NO_CHANGE
        :type low_limit: float or None
        :param high_limit: alert condition high limit temperature setpoint (Celcius). Use None for NO_CHANGE
        :type high_limit: float or None
        :param hysteresis: recovery threshold in Celcius past the high/low limits before alert condition
            disengages while the channel is not in latching mode. Use None for NO_CHANGE
        :type hysteresis: float or None
        :param latching: if True the channel enters latching mode where alert is latched upon reaching the high/low
            limits and remains in alert state until a call to clear_alert() is made.
            (hysteresis has no effect in this mode). Use None for NO_CHANGE
        :type latching: bool or None
        :return:
        """
        self.dev.set_alert_config(self.idx, low_limit, high_limit, hysteresis, latching)

    def get_alert_config(self):
        """Get the channel's alert conditions

            Alert conditions specify at what temperature range a channel will report an active alert status.
            During active alert status the channel's front LED will turn RED, and the channel relay can optionally
            engage to (dis)connect external circuitry. When a channel's temperature is no longer in alert range by more than
            the hysteresis(C) the alert status will deactivate. If the channel is configured to latching, then an
            active alert status remains true even after the temperature settles, until the user calls clear_alert() on
            the channel.

            Alert status is only evaluated each time a measurement is commanded.

        :return: dictionary {low_limit: float, high_limit: float, hysteresis: float, latching: bool}
        :rtype: dict(str:float)
        """
        return self.dev.get_alert_config(self.idx)

    def clear_alert(self):
        """Clear channel's latched alert status"""
        self.dev.clear_alert(self.idx)

    def set_relay_config(self, engage_on_alert=NO_CHANGE, engage_on_open=NO_CHANGE):
        """Set the channel's relay configuration

        :param engage_on_alert: if True engage relay when alert status is True. Use None for NO_CHANGE
        :type engage_on_alert: bool or None
        :param engage_on_open: if True engage relay when disconnected/open is detected. Use None for NO_CHANGE
        :type engage_on_open: bool or None
        """
        self.dev.set_relay_config(self.idx, engage_on_alert, engage_on_open)

    def get_relay_config(self):
        """Get the channel's relay configuration

        :return: dictionary {engage_on_alert: bool, engage_on_open: bool}
        :rtype: dict(str:bool)
        """
        return self.dev.get_relay_config(self.idx)

    def set_tctype(self, tctype):
        """Set the channel's thermocouple type

        :param tctype: thermocouple letter type: K, J, T, N, S, E, B, R
        :type tctype: str
        """
        self.dev.set_tctype(self.idx, tctype)

    def get_tctype(self):
        """Get the channel's thermocouple type

        :return: thermocouple letter type: K, J, T, N, S, E, B, R
        :rtype: str
        """
        return self.dev.get_tctype(self.idx)


class RgbLed:
    """
    Red-Green-Blue LED
    """
    COLOR_RED = 0xFF000064
    COLOR_GREEN = 0x00FF0064
    COLOR_BLUE = 0x0000FF64
    COLOR_WHITE = 0xFFFFFF50
    COLOR_YELLOW = 0xFFAA0044
    COLOR_ORANGE = 0xFFFF0044
    COLOR_PURPLE = 0xFF00FF80

    def __init__(self, dev):
        self.dev = dev

    def set(self, rgbb_val):
        """Set the RGB and Brightness value of the front LED

        :param rgbb_val: 32 bit value where each octet represents MSB [Red, Green, Blue, Brightness] LSB
        :type rgbb_val: int
        """
        self.dev.request("rgbled {}".format(rgbb_val))

    def get(self):
        """Get the RGB and Brightness value of the front LED

        :return: 32 bit value where each octet represents MSB [Red, Green, Blue, Brightness] LSB
        :rtype: int
        """
        res = self.dev.request("rgbled?")
        return int(res)
