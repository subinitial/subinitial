# -*- coding: UTF-8 -*-
# Stacks internal library
# © 2012-2018 Subinitial LLC. All Rights Reserved
# Redistribution and use in source and binary forms, with or without modification, are permitted provided that the
# following conditions are met: Redistributions of source code and binary forms must retain the above copyright notice,
# these conditions, and the disclaimer below all in plain text. The name Subinitial may not be used to endorse or
# promote products derived from this software without prior written consent from Subinitial LLC. This software may only
# be redistributed and used with a Subinitial Stacks product.

# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
# warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. Refer to the included LICENSE.txt for more details.

import time
import socket
import threading
import queue
import math
import logging
import struct
import json
unpack = struct.unpack_from
pack = struct.pack_into

from subinitial.stacks.utils import *

AUTO = None
# Type = (Signed, RegLength)
UINT16 = (1, False)
INT16 = (1, True)
UINT32 = (2, False)
INT32 = (2, True)


exceptions = {
    0: "OK",
    1: "Bad Function Code",
    2: "Bad Register Address",
    3: "Bad Register Value",
    4: "Slave Failure",
    5: "Operation Acknowledged",
    6: "Operation in Process",
    7: "Operation Not-Acknowledged",
    8: "CRC Error",
    10: "Device ID not found",
    11: "Device failed to respond",
    0x45: "Operation is Async",
    0x46: "Async Operation Failed"
}


class ResultsArchive(object):
    pass


class ResultsContainer(object):
    # def __setattr__(self, key, value):
    #     self.__dict__[key] = value

    def _load(self):
        all = ResultsArchive()
        for key, container in list(self.__dict__.items()):
            if len(container):
                if len(container) == 1:
                    setattr(self, key, container[0])
                else:
                    setattr(self, key, container)
            else:
                setattr(self, key, None)
            # setattr(self, key, container[-1] if len(container) else None)
            setattr(all, key, container)
        setattr(self, "all", all)


class Register(object):
    length = 1
    size = 2
    consumes = False
    signed = False

    __slots__ = ['dev', 'address']

    def __init__(self, address, device=None):
        self.dev = device
        self.address = address

    def __int__(self):
        return self.address

    def read(self, regcount=1):
        """:rtype: Container"""
        return self.dev.request.read(self.address, regcount*self.length)

    def write(self, *vals):
        return self.dev.request.write(self.address, *vals)

    def equals(self, *expression):
        return self.dev.request.math(self, *expression, datatype=(self.length, self.signed))

    def _get(self, start, end, cursor, bytebuf, offset, vals):
        vals.append(self.convert(unpack("<H", bytebuf, offset)[0]))
        return cursor + 1, offset + 2

    def _set(self, start, cursor, bytebuf, offset, val):
        pack("<H", bytebuf, offset, self.revert(val))
        return cursor + 1, offset + 2

    def _reqlen(self, lastreg):
        return lastreg.addr + lastreg.length

    @staticmethod
    def revert(val):
        return val

    @staticmethod
    def convert(val):
        return val


class LittleR(object):
    AUTO = AUTO

    __slots__ = ['dev', 'index']

    def __init__(self, index, device):
        self.index = index
        self.dev = device

    def __int__(self):
        return self.index

    def read(self, rcount=1):
        # Note: these are signed 32 bit reads
        return self.dev.request.read_r(self.index, rcount)

    def write(self, *vals):
        return self.dev.request.write_r(self.index, *vals)

    def load(self, address, hword_count=AUTO):
        return self.dev.request.load_r(self.index, address, hword_count)

    def store(self, address, hword_count=AUTO):
        return self.dev.request.store_r(self.index, address, hword_count)

    def copy(self, src_index, rcount=1):
        # Note: these are 32 bit copies
        return self.dev.request.copy_r(self.index, src_index, rcount)

    def equals(self, *expression, datatype=AUTO):
        return self.dev.request.math(self, *expression, datatype=datatype)


class LittleRSet(object):
    COUNT = 14

    def __init__(self, device):
        self.dev = device
        self._r = []
        """:type: list[LittleR]"""
        for i in range(0, self.COUNT+2):
            self._r.append(LittleR(i, device))
        self.x = self._r[-1]
        """:type: LittleR"""
        self.y = self._r[-2]
        """:type: LittleR"""

    def __getitem__(self, item):
        """:rtype: LittleR"""
        return self._r[item]

    def _coerce_x(self, arg):
        if type(arg) == LittleR: # no coercion necessary
            return arg
        # coercion
        self.x.write(arg) if type(arg) == int else self.x.load(arg)
        return self.x

    def _coerce_y(self, arg):
        if type(arg) == LittleR: # no coercion necessary
            return arg
        # coercion
        self.y.write(arg) if type(arg) == int else self.y.load(arg)
        return self.y


class Control(object):
    def __init__(self, device):
        self.dev = device
        self.req = device.request

    def label(self, name):
        self.req.label(name)

    def branch(self, label):
        self.req.branch(0, None, label)

    def branch_if(self, r_index, label):
        self.req.branch(r_index, True, label)

    def branch_ifz(self, r_index, label):
        self.req.branch(r_index, False, label)

    def delay_ms(self, ms):
        self.req.delay_ms(ms)

    def event_wait(self, event):
        self.req.event_wait(event)

    def event_raise(self, event):
        self.req.event_raise(event)


class Op:
    Read = 0 << 12
    ReadZ = 1 << 12
    Write = 2 << 12
    Write16 = 3 << 12

    rOp_Mask = 0xF0E0
    rOp = 4 << 12
    rOpLoad = rOp | (0 << 5)
    rOpStore = rOp | (1 << 5)
    rOpRead = rOp | (2 << 5)
    rOpWrite = rOp | (3 << 5)
    rOpWrite16 = rOp | (4 << 5)
    rOpWrite16i = rOp | (5 << 5)
    rOpCopy = rOp | (6 << 5)

    Math32 = 5 << 12
    Math16 = 6 << 12

    Control = 7 << 12
    ControlBranch = Control | (0 << 8)
    ControlBranchIf = Control | (1 << 8)
    ControlBranchIfZ = Control | (2 << 8)
    ControlDelayMs = Control | (3 << 8)
    ControlDelayManyMs = Control | (4 << 8)
    ControlEventWait = Control | (5 << 8)
    ControlEventRaise = Control | (6 << 8)

    Exception = 0xF << 12

# class rOp:
#     Load = (0 << 5)
#     Store = 1
#     Read = 2
#     Write = 3
#     Write16 = 4
#     Write16i = 5
#     Copy = 6

class Branch:
    def __init__(self, r_index, state, label, headerloc):
        self.r_index = int(r_index)
        self.state = state
        self.label = label
        self.headerloc = headerloc

    def compile(self, reqbuf, labelloc):
        if self.state is None:
            pack("<HH", reqbuf, self.headerloc, labelloc - 6, Op.ControlBranch)
        else:
            op = Op.ControlBranchIf if self.state else Op.ControlBranchIfZ
            pack("<HH", reqbuf, self.headerloc, labelloc - 6, op | self.r_index)


class Request67:
    AUTO = AUTO

    def __init__(self, device, opener=None):
        self.dev = device
        self.opener = opener
        self.connection = device.connection

        self.pending = False
        self.subrequests = []
        self.results = []
        self.labels = {}
        """:type: {str: int}"""
        self.branches = []
        """:type: list[Branch]"""

        self.resultref = 0
        """:type: list[Container]"""
        self._rc = None
        """:type: ResultsContainer"""
        self.reqbuf = bytearray()
        self.offset = 0

        self.reset()

    def reset(self):
        self.pending = False
        self.subrequests = []
        self.branches = []
        self.resultref = 0
        self.results = []
        self.labels.clear()
        self.reqbuf = bytearray(1460)
        self.reqbuf[6:8] = self.dev.bus_address, 67  # fn67 RTU header
        self.offset = 8  # skip past TCP MBap and RTU headers

    def submit(self):
        if self.opener:
            opener = self.opener
            self.opener = None
            opener()
        try:
            # compile branches
            for branch in self.branches:
                branch.compile(self.reqbuf, self.labels[branch.label])

            # go get the data
            response = self.connection.request(self.reqbuf, self.offset)
            results = self._parse(response)

            # unpack result container if there is one
            if self._rc:
                self._rc._load()
                self._rc = None

        except Exception as ex:
            raise ex

        finally:
            # reset the request context
            self.reset()

        if len(results) == 0:
            return None  # nullify
        if len(results) == 1:
            return results[0]  # unpack
        return results

    def _parseread(self, vals, start, count, bytebuf, offset):
        cursor = start
        offset += 2
        end = start + count
        while cursor < end:
            reg = self.dev.regs[cursor]
            if reg:
                cursor, offset = reg._get(
                    start, end, cursor, bytebuf, offset, vals)
            else:
                cursor += 1
                offset += 2
        return offset

    def _parse(self, respbuf):
        # if (respbuf[6] != self.dev.bus_address) or (respbuf[7] != 67):
        #     raise Exception("invalid bus address: {} or function code: {}".format(response[6], response[7])

        offset = 8
        remlen = len(respbuf) - offset

        # check for protocol exception
        if unpack("B", respbuf, 7)[0] & 0x80:
            code = -1 if remlen < 1 else unpack("B", respbuf, 8)[0]
            raise Exception("A protocol exception occured: {:02} '{}'. Response Buffer: {}".format(code, exceptions.get(code, "Unknown"), respbuf))

        # we expect 1 of 3 Opcodes: Read, ReadZ, and Exception
        while remlen >= 4:  # All fn67 subrequests are a minimum of 4Bytes
            hword0, hword1 = unpack("<HH", respbuf, offset)
            opcode = hword1 & 0xF000
            if opcode == Op.ReadZ:
                count = hword1 & 0xFFF
                ref = (hword0 & 0xF000) >> 12
                start = hword0 & 0xFFF
                vals = self.results[ref]
                offset = self._parseread(vals, start, count, respbuf, offset+2)

            elif opcode == Op.Read:
                ref = hword1 & 0xFFF
                start = hword0
                count = unpack("<H", respbuf, offset + 4)[0]
                vals = self.results[ref]
                offset = self._parseread(vals, start, count, respbuf, offset+4)

            elif (hword1 & 0xF0F0) == Op.rOpRead:
                # start = (hword0 >> 8) & 0xF
                ref = hword0
                count = hword1 & 0xF
                vals = self.results[ref]
                offset += 4
                for i in range(0, count):
                    vals.append(unpack("<i", respbuf, offset)[0])
                    offset += 4

            # check for subrequest exception
            elif opcode == Op.Exception:
                excode = hword0 & 0xFFF
                hword2, hword3 = unpack("<HH", respbuf, offset + 2)
                failed_op = (hword2 & 0xF000)
                offset = len(respbuf)  # close out the parsing on first failure
                raise Exception("exception code {} caught on operation: {}".format(excode, failed_op >> 12))
            else:
                raise Exception("unknown opcode {} in response".format(opcode >> 12))
            remlen = len(respbuf) - offset
        if len(self.results) == 1:
            return self.results[0]
        return self.results

    def queue(self):
        self.pending = True
        rc = ResultsContainer()
        self._rc = rc
        return rc

    def read(self, address, size):
        start, reqbuf, offset = int(address), self.reqbuf, self.offset
        assert(size != 0)

        if self.resultref < 16 and start < 4096:
            pack("<HH", reqbuf, offset, self.resultref << 12 | start, Op.ReadZ | (size & 0xFFF))
            self.offset = offset + 4
        else:
            pack("<HHH", reqbuf, offset, start, Op.Read | (self.resultref & 0xFFF), size)
            self.offset = offset + 6

        cont = []
        self.resultref += 1
        self.results.append(cont)

        if self.pending:
            return cont  # supply a data container to be filled later with response values
        else:
            return self.submit()

    def read_r(self, index, rcount):
        start, reqbuf, offset = int(index), self.reqbuf, self.offset
        if start + rcount > 16:
            raise Exception("Reading {} words from r[{}] will overflow".format(rcount, start))
        if rcount == 0 or rcount > 0x1F:
            raise Exception("LittleR read count must an integer from 1 to 31")

        pack("<HH", reqbuf, offset, self.resultref, Op.rOpRead | (index << 8) | rcount)
        self.offset += 4

        cont = []
        self.resultref += 1
        self.results.append(cont)

        if self.pending:
            return cont  # supply a data container to be filled later with response values
        else:
            return self.submit()

    def write(self, address, *vals):
        """writes to a fn67 reqbuf"""
        # if single value, defer to writeone
        if len(vals) == 1:
            return self._write_single(address, vals[0])

        # locals
        start, reqbuf, offset = int(address), self.reqbuf, self.offset
        # reserve space for write header
        headoffset = offset
        offset += 4
        # serialize values
        cursor = start
        for val in vals:
            reg = self.dev.regs[cursor]
            cursor, offset = reg._set(start, cursor, reqbuf, offset, val)
        # fill in header
        pack("<HH", reqbuf, headoffset, start, Op.Write | ((cursor-start) & 0xFFF))
        self.offset = offset

        if not self.pending:
            return self.submit()  # go out and write the vals to address immediately

    def _write_single(self, address, val):
        address, reqbuf, offset = int(address), self.reqbuf, self.offset
        headoffset = offset
        reg = self.dev.regs[address]

        cursor, offset = reg._set(address, address, reqbuf, offset+4, val)
        if offset - headoffset == 6:
            pack("<BBH", reqbuf, headoffset, reqbuf[headoffset+4], reqbuf[headoffset+5],
                 Op.Write16 | (address & 0xFFF))
            self.offset = headoffset + 4
        else:
            pack("<HH", reqbuf, headoffset, address, Op.Write | ((cursor-address) & 0xFFF))
            self.offset = offset

        if not self.pending:
            return self.submit()  # go out and write the vals to address immediately

    def write_r(self, index, *vals):
        if not self.pending:
            raise Exception("Little-R operations must be performed in a subprocedure")

        count = len(vals)
        if count == 1:
            return self._write_r_single(index, vals[0])

        start, reqbuf, offset = int(index), self.reqbuf, self.offset
        if index + count > 16:
            raise Exception("Writing {} words to r[{}] will overflow".format(count, index))
        buf_format = "<HH"
        for val in vals:
            buf_format += "i" if val < 0 else "I"
        pack(buf_format, reqbuf, offset, 0, Op.rOpWrite | (index << 8) | len(vals), *vals)
        self.offset += 4 + len(vals) * 4

    def copy_r(self, dst, src, rcount):
        if not self.pending:
            raise Exception("Little-R operations must be performed in a subprocedure")

        dst, src, reqbuf, offset = int(dst), int(src), self.reqbuf, self.offset
        if dst + rcount > 16 or src + rcount > 16:
            raise Exception("Copying {} registers from r[{}] to r[{}] will overflow".format(rcount, src, dst))
        pack("<HH", reqbuf, offset, dst, Op.rOpCopy | (src << 8) | rcount)
        self.offset += 4

    def _write_r_single(self, index, val):
        index, reqbuf, offset = int(index), self.reqbuf, self.offset
        if index > 0xF:
            raise Exception("LittleR index must be between 0 and 15")
        if 0 <= val:
            if val <= 65535:  # write16u
                pack("<HH", reqbuf, offset, val, Op.rOpWrite16 | (index << 8))
                self.offset += 4
            else:  # write32u
                pack("<HHI", reqbuf, offset, 0, Op.rOpWrite | (index << 8) | 1, val)
                self.offset += 8
        elif -32767 <= val:  # write16i
            pack("<hH", reqbuf, offset, val, Op.rOpWrite16i | (index << 8))
            self.offset += 4
        else:  # write32i
            pack("<HHi", reqbuf, offset, 0, Op.rOpWrite | (index << 8) | 1, val)
            self.offset += 8

    def load_r(self, index, address, hword_count):
        if not self.pending:
            raise Exception("Little-R operations must be performed in a subprocedure")
        index, address = int(index), int(address)
        if not hword_count:
            hword_count = self.dev.regs[address].length

        if (index * 2) + hword_count > 32:
            raise Exception("Loading {} hwords into r[{}] will overflow".format(hword_count, index))
        pack("<HH", self.reqbuf, self.offset, address, Op.rOpLoad | (int(index) << 8) | hword_count)
        self.offset += 4

    def store_r(self, index, address, hword_count):
        if not self.pending:
            raise Exception("Little-R operations must be performed in a subprocedure")

        index, address = int(index), int(address)
        if not hword_count:
            hword_count = self.dev.regs[address].length

        if (index * 2) + hword_count > 32:
            raise Exception("Storing {} hwords from r[{}] will overflow".format(hword_count, index))
        pack("<HH", self.reqbuf, self.offset, address, Op.rOpStore | (int(index) << 8) | hword_count)
        self.offset += 4

    @staticmethod
    def _determine_type(args):
        length = -1
        signed = -1
        for token in args:
            if type(token) == int:
                if token > 65535:
                    length = max(length, 2)
                elif token < -32768:
                    length = max(length, 2)
                    signed = max(signed, 1)
                elif token < 0:
                    signed = max(signed, 1)
            elif type(token) != LittleR:
                length = max(length, token.length)
                signed = max(signed, token.signed)
        if length == -1:
            length = 2
        signed = signed != 0
        return length, signed

    def math(self, lvalue, *expression, datatype=AUTO):
        if not self.pending:
            raise Exception("Math operations must be performed in a subprocedure")

        args = []
        operation = None

        # extract args and operation
        for token in expression:
            if type(token) == str:
                operation = Operation.get(token)
            else:
                args.append(token)

        if operation is None:
            self._assign(lvalue, args[0])
            return

        # Determine datatype by override, lvalue, or args
        if datatype:
            length, signed = datatype
        elif type(lvalue) != LittleR:
            length, signed = lvalue.length, lvalue.signed  # auto type based on lvalue
        else:
            length, signed = self._determine_type(args)  # auto type based on args

        mainop = Op.Math16 if length == 1 else Op.Math32
        signedflag = 0x80 if signed else 0

        self.offset = operation.compile(self.dev, mainop, signedflag, lvalue, args)

    def _assign(self, lvalue, arg):
        if type(lvalue) == LittleR:
            if type(arg) != LittleR and type(arg) != int:
                lvalue.load(arg)
            else:
                arg = self.dev.r._coerce_x(arg)
                lvalue.copy(arg)
        else:
            arg = self.dev.r._coerce_x(arg)
            arg.store(lvalue)

    def label(self, labelname):
        self.labels[labelname] = self.offset

    def branch(self, r_index, state, labelname):
        self.branches.append(Branch(r_index, state, labelname, self.offset))
        self.offset += 4

    def delay_ms(self, ms):
        if ms < 0:
            raise Exception("delay must be positive")
        if ms < 16777216:
            pack("<I", self.reqbuf, self.offset, (Op.ControlDelayMs << 16) | ms)
            self.offset += 4
        else:
            pack("<HHI", self.reqbuf, self.offset, 0, Op.ControlDelayManyMs, ms)
            self.offset += 8

    def event_wait(self, event):
        pack("<HH", self.reqbuf, self.offset, event, Op.ControlEventWait)
        self.offset += 4

    def event_raise(self, event):
        pack("<HH", self.reqbuf, self.offset, event, Op.ControlEventRaise)
        self.offset += 4


class Format:
    # binary
    A_is_B_op_C = 0 << 4
    A_is_B_op_IMM12 = 1 << 4
    A_is_IMM12_op_B = 2 << 4
    A_is_B_op_REG = 3 << 4
    A_is_REG_op_B = 4 << 4
    REG_is_A_op_B = 5 << 4
    # unary
    B_is_op_C = 0 << 4
    B_is_op_IMM12 = 1 << 4
    B_is_op_REG = 2 << 4
    REG_is_op_B = 3 << 4


class Operation:
    operations = {}

    @staticmethod
    def get(text):
        op = Operation.operations.get(text.lower().strip())
        if op:
            return op
        else:
            raise Exception("unknown operator: {}".format(text))

    def __init__(self, operator, opcode, descr, extcode=0):
        self.operator = operator
        self.opcode = opcode << 8
        self.extcode = extcode
        self.descr = descr
        self.operations[operator] = self

    def compile(self, dev, mainop, signedflag, lvalue, args):
        if len(args) != 2:
            raise Exception("binary operation: {} requires 2 arguments, {} received".format(self.operator, len(args)))
        reqbuf, offset = dev.request.reqbuf, dev.request.offset
        arg1 = args[0]
        arg2 = args[1]
        t_arg1 = type(arg1)
        t_arg2 = type(arg2)

        # determine lvalue
        if type(lvalue) != LittleR:  # Register Equals
            # Register = rA op rB
            arg1 = dev.r._coerce_x(arg1)
            arg2 = dev.r._coerce_y(arg2)

            pack("<HH", reqbuf, offset,
                 arg2.index << 12 | lvalue.address,
                 mainop | self.opcode | signedflag | Format.REG_is_A_op_B | arg1.index)

        # lvalue is now assumed to be a LittleR
        elif t_arg1 == int and (0 <= arg1 < 4096):
            # rA = Immediate(12) op rB
            arg2 = dev.r._coerce_x(arg2)
            pack("<HH", reqbuf, offset,
                 arg2.index << 12 | arg1,
                 mainop | self.opcode | signedflag | Format.A_is_IMM12_op_B | lvalue.index)


        elif t_arg2 == int and (0 <= arg2 < 4096):
            # rA = rB op Immediate(12)
            arg1 = dev.r._coerce_x(arg1)
            pack("<HH", reqbuf, offset,
                 arg1.index << 12 | arg2,
                 mainop | self.opcode | signedflag | Format.A_is_B_op_IMM12 | lvalue.index)

        elif t_arg1 != LittleR and t_arg1 != int and (0 <= int(arg1) < 4096):
            # rA = Register op rB asdf
            arg2 = dev.r._coerce_x(arg2)
            pack("<HH", reqbuf, offset,
                 arg2.index << 12 | int(arg1),
                 mainop | self.opcode | signedflag | Format.A_is_REG_op_B | lvalue.index)

        elif t_arg2 != LittleR and t_arg2 != int and (0 <= int(arg2) < 4096):
            # rA = rB op Register
            arg1 = dev.r._coerce_x(arg1)
            pack("<HH", reqbuf, offset,
                 arg1.index << 12 | int(arg2),
                 mainop | self.opcode | signedflag | Format.A_is_B_op_REG | lvalue.index)

        else:
            # rA = rB op rC
            arg1 = dev.r._coerce_x(arg1)
            arg2 = dev.r._coerce_y(arg2)
            pack("<HH", reqbuf, offset,
                 arg1.index << 12 | arg2.index,
                 mainop | self.opcode | signedflag | Format.A_is_B_op_C | lvalue.index)

        return offset + 4


class FlippedOperation(Operation):
    def compile(self, dev, mainop, signedflag, lvalue, args):
        args[1], args[0] = args[0], args[1]
        return Operation.compile(self, dev, mainop, signedflag, lvalue, args)


class UnaryOperation(Operation):
    def compile(self, dev, mainop, signedflag, lvalue, args):
        if len(args) != 1:
            raise Exception("unary operation: {} requires 1 argument, {} received".format(self.operator, len(args)))
        reqbuf, offset, arg = dev.request.reqbuf, dev.request.offset, args[0]

        # determine lvalue
        if type(lvalue) != LittleR:  # Register Equals
            # Register = op rB
            arg = dev.r._coerce_x(arg)
            pack("<HH", reqbuf, offset,
                 arg.index << 12 | lvalue.address,
                 mainop | self.opcode | signedflag | Format.REG_is_op_B | self.extcode)

        # lvalue is now assumed to be a LittleR
        elif type(arg) == int and (0 <= arg < 4096):
            # rB = op Immediate(12)
            # arg = dev.r._coerce_x(arg)  # this is not needed since we type checked the arg
            pack("<HH", reqbuf, offset,
                 lvalue.index << 12 | arg,
                 mainop | self.opcode | signedflag | Format.B_is_op_IMM12 | self.extcode)

        elif type(arg) != LittleR and type(arg) != int and (0 <= int(arg) < 4096):
            # rB = op Register
            pack("<HH", reqbuf, offset,
                 lvalue.index << 12 | int(arg),
                 mainop | self.opcode | signedflag | Format.B_is_op_REG | self.extcode)
        else:
            # rB = op rC
            arg = dev.r._coerce_x(arg)
            pack("<HH", reqbuf, offset,
                 lvalue.index << 12 | arg.index,
                 mainop | self.opcode | signedflag | Format.B_is_op_C | self.extcode)

        return offset + 4


class ExtendedOperation(Operation):
    def compile(self, dev, mainop, signedflag, lvalue, args):
        if len(args) != 2:
            raise Exception("binary operation: {} requires 2 arguments, {} received".format(self.operator, len(args)))
        reqbuf, offset = dev.request.reqbuf, dev.request.offset
        # rA = rB op rC
        arg1 = dev.r._coerce_x(args[0])
        arg2 = dev.r._coerce_y(args[1])
        pack("<HH", reqbuf, offset,
             (lvalue.index << 8) | (arg1.index << 4) | arg2.index,
             mainop | self.opcode | (self.extcode << 4))
        return offset + 2


class Operations:
    # binary
    Operation('+', 0, "add")
    Operation('-', 1, "sub")
    Operation('*', 2, "mul")
    Operation('/', 3, "div")
    Operation('%', 4, "mod")
    Operation('<<', 5, "lshift")
    Operation('>>', 6, "rshift")
    Operation('&', 7, "and")
    Operation('^', 8, "xor")
    Operation('|', 9, "or")
    # binary comparison
    Operation('<', 10, 'less than')
    FlippedOperation('>', 10, 'greater than')
    Operation('<=', 11, 'less or equal')
    FlippedOperation('>=', 11, 'greater or equal')
    Operation('==', 12, 'equal')
    Operation('!=', 13, 'not equal')
    # unary
    UnaryOperation('abs', 14, "absolute value", extcode=0)
    UnaryOperation('~', 14, "invert", extcode=1)
    UnaryOperation('clz', 14, "count leading zeros", extcode=2)
    UnaryOperation('ctz', 14, "count trailing zeros", extcode=3)
    UnaryOperation('rev', 14, "reverse endian 32b", extcode=4)
    UnaryOperation('rev16', 14, "reverse endian 2x16b", extcode=5)
    # extended
    ExtendedOperation('clr', 15, 'clear', extcode=0)  # & ~(bitmask) opposite of | bitmask


class MockConnection(object):
    """Used for unit-testing / offline testing"""
    def __init__(self, host="192.168.1.49", port=502, *args):
        self.host = host
        self.port = port
        self.buffer_size = 2048
        self.connection_state = "disconnected"
        self.logger = logging.getLogger()
        self.args = args
        self.tid = 0

    def connect(self):
        self.connection_state = "connected"

    def close(self):
        self.connection_state = "closed"

    def listen(self, bus_address, register, handler):
        pass

    def unlisten(self, bus_address, register, handler=None):
        pass

    def _transmit(self, req):
        pass

    def _receive(self):
        pass

    def request(self, reqbuf, size):
        pack(">HHH", reqbuf, 0, self.tid, 0, size-6)  # Modbus TCP MBAP
        self._transmit(reqbuf[:size])
        return self._receive()


class ReceiveThread(threading.Thread):
    def __init__(self, target):
        super(ReceiveThread, self).__init__()
        self.daemon = True
        self.target = target
        self._stop = threading.Event()

    def run(self):
        while True:
            self.target()
            if self.stopped():
                return

    def stopped(self):
        return self._stop.isSet()

    def stop(self):
        self._stop.set()


class Connection(object):
    FAILUREMODE_RECONNECT = 0
    FAILUREMODE_SESSIONID_CHANGED = 1
    FAILUREMODE_NO_RETRIES_REMAIN = 2

    _last_contime = {}
    _last_contime_lock = threading.Lock()

    tid = 0

    @property
    def is_connected(self):
        return self._is_open

    def __init__(self, host="192.168.1.49", port=502,
                 retries=3, timeout=5.0, on_networkfailure=None):
        self.host = host
        self.port = port
        self._buffer_size = 2048
        self.retries = retries
        self.timeout = timeout
        self.on_networkfailure = on_networkfailure
        self.logger = logging.getLogger()
        self._is_open = False
        self._is_dead = False
        self._response_queue = None
        self._request_lock = threading.Lock()
        self._active_request = None
        self.socket = None
        self._async_handlers = {}

        # Router Task
        self._rx_thread = None
        """:type: ReceiveThread"""

    def connect(self):
        with self._last_contime_lock:
            # self._limit_connections_per_second()
            self._response_queue = queue.Queue()

            self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            # Don't set a socket timeout here
            self.socket.settimeout(self.timeout)
            try:
                resp = self.socket.connect((self.host, self.port))
            except:
                raise SubinitialTimeoutError("Failed to connect to Stacks Core at host: \"{}\","
                                             " timed out after {} seconds".format(self.host, self.timeout))
            self.socket.settimeout(None)
            self._is_open = True
            # Router Task
            if self._rx_thread:
                self._rx_thread.stop()
            self._rx_thread = ReceiveThread(target=self._rx_task)
            self._rx_thread.start()

    def _reconnect(self, tries=1):
        # self._request_lock.release()
        while True:
            if self.on_networkfailure:
                retval = self.on_networkfailure(self, self.FAILUREMODE_RECONNECT)
                if type(retval) == int:
                    tries = retval
                if tries == 0:
                    return 0
            try:
                self.close()
                self.connect()
                return tries
            except Exception as e:
                self.logger.warning("Reconnecting to Stacks failed: {0}".format(e))
                if tries is None:
                    continue
                tries -= 1
                if tries <= 0:
                    return 0

    def _txrx_reqresp(self, req):
        with self._request_lock:
            self._active_request = req
            self.socket.send(req)
            resp = self._response_queue.get(timeout=self.timeout)
            req = self._active_request
            # Make sure the TID of the resp and req match
            while (len(resp) < 2) or (resp[0] != req[0]) or (resp[1] != req[1]):
                resp = self._response_queue.get_nowait()
            return resp

    # def __transmit_request(self, req):
    #     self._request_lock.acquire()
    #     self._active_request = req
    #     self.socket.send(req)
    #
    # def __receive_response(self):
    #     resp = self._response_queue.get(timeout=self.timeout)
    #     req = self._active_request
    #     # Make sure the TID of the resp and req match
    #     while (resp[0] != req[0]) or (resp[1] != req[1]):
    #         resp = self._response_queue.get_nowait()
    #     self._request_lock.release()
    #     return resp

    def request(self, req, size):
        pack(">HHH", req, 0, self.tid, 0, size-6)
        req = req[:size]
        self.tid += 1
        if self.tid > 65535:
            self.tid = 1

        ex = None

        # First Attempt
        try: return self._txrx_reqresp(req)
        except Exception as e:
            self.logger.warning("Stacks transmission failed: {0}".format(e))
            ex = e
            # raise e

        # Retries
        retries = self.retries
        while True:

            # Try to reconnect if retries remain
            if retries is None or retries > 0:
                self.logger.warning("Reconnecting to Stacks...")
                retries = self._reconnect(retries)  # reconnecting may have consumed some retries

            # Notify upper layer of network failure
            if retries <= 0 and self.on_networkfailure:
                retval = self.on_networkfailure(self, self.FAILUREMODE_NO_RETRIES_REMAIN)
                if type(retval) == int:
                    retries = retval
                retries = self._reconnect(retries)
                if retries:  # if the handler has modified the retry count then try to reconnect
                    continue

            # We have run out of retries, time to quit
            if retries <= 0:
                break

            # Now that we are reconnected to Stacks retry the transmission
            try: return self._txrx_reqresp(req)
            except Exception as e:
                self.logger.warning("Stacks transmission failed: {0}".format(e))
                ex = e
            retries -= 1  # the above transmission failed, consume one retry

        raise ex  # SubinitialException("Stacks network connection failed")

    def _rx_task(self):
        try:
            while self._is_open:
                resp = self.socket.recv(self._buffer_size)
                if resp == b'':
                    self._rx_thread.stop()
                    self._rx_thread = None
                    # print("stopping rx_thread")
                    return
                if len(resp) >= 12 and resp[3] == 2:
                    self.on_async(resp)
                else:
                    self._response_queue.put(resp)
        except Exception as ex:
            self.ex = ex
        time.sleep(0.5)

    def close(self):
        if self._is_open:
            self._is_open = False
            if self.socket:
                self.socket.shutdown(socket.SHUT_RDWR)
                self.socket.close()
            return True
        return False

    def kill(self):
        ret = self.close()
        if self._rx_thread:
            self._rx_thread.stop()
            self._rx_thread = None
            ret = True
        return ret


    # Async Handling

    def register_handler(self, bus_address, register, handler):
        device = getcreate(self._async_handlers, bus_address, {})
        handlers = getcreate(device, register, [])
        handlers.append(handler)

    def unregister_handler(self, bus_address, register, handler):
        device = getcreate(self._async_handlers, bus_address, {})
        handlers = getcreate(device, register, [])
        handlers.remove(handler)

    def _call_handlers(self):
        bus_address, register, response, handlers = self.async_response
        # print("async{ bus:", bus_address, "register:", register, "}")
        for handler in handlers:
            handler(response)

    def on_async(self, response):
        # get bus address and register from response
        if len(response) < 12:
            return

        bus_address = response[8]
        register = response[9] + (response[10] << 8)
        device = self._async_handlers.get(bus_address, {})
        handlers = device.get(register, [])

        if len(handlers):
            self.async_response = (bus_address, register, response, handlers)
            hthread = threading.Thread(target=self._call_handlers)
            hthread.start()


class Version:
    def __init__(self, vstr):
        self.major = 0
        self.minor = 0
        self.patch = 0
        self._rep = 0
        try:
            nums = vstr.lower().strip().strip("v").split(".")
            self.major = int(nums[0])
            self.minor = int(nums[1])
            self.patch = int(nums[2])
            self._rep = (self.major << 32) + (self.minor << 16) + self.patch
        except:
            pass

    def __eq__(self, other):
        return self._rep == other._rep

    def __lt__(self, other):
        return self._rep < other._rep

    def __le__(self, other):
        return self._rep <= other._rep

    def __gt__(self, other):
        return self._rep > other._rep

    def __ge__(self, other):
        return self._rep >= other._rep


class Device:
    TYPE_DECK = "Deck"
    TYPE_ACCESSORY = "Accessory"
    VERSION = "v1.17.0"

    def _get_devicetype(self):
        return self.TYPE_DECK

    def __init__(self, core, bus_address, raise_response_exceptions=True, verify_connection=True):
        self.bus_address = bus_address
        self.core = core
        self.connection = core.connection
        self.raise_response_exceptions = raise_response_exceptions

        self.request = Request67(self)
        self.r = LittleRSet(self)
        self.control = Control(self)

        if verify_connection:
            if self._get_devicetype() == self.TYPE_DECK and self.connection._is_open:
                self.verify_connection()
            else:
                self.request.opener = self.verify_connection

        # commented members below are defined in child classes
        # self.regs = RegisterSet(self)

    def verify_connection(self):
        """Verifies network connection to the Stacks device
        Also verifies if firmware version and library version are compatible.
        This prompts the user when incompatibilities are detected.

        :returns: True if connection verified, False if connection could not be verified
        :rtype: bool
        """
        tries = 1
        while True:
            was_error = False
            try:
                info = self.console("info 0")  # request device info JSON
                break
            except:
                was_error = True
            finally:
                if was_error:
                    tries -= 1
                    if tries <= 0:
                        raise SubinitialResponseError("Connection verification failed for {} at busaddress {}".format(
                            type(self).__name__, self.bus_address))
                    else:
                        self.core.console("busping")
                        time.sleep(1.0)


        # Try to parse the info
        try:
            info = json.loads(info)
            badversion = Version(info["firmware"]) < Version(self.FIRMWARE_MIN)
            badinfo = False
        except:
            badversion = True
            badinfo = True

        if badversion:
            print("\nWARNING: {} device (bus_address {})\nFirmware should be updated to version >= {}.".format(
                type(self).__name__, self.bus_address, self.FIRMWARE_MIN))
            time.sleep(0.01)
            resp = input("Open the StacksManager firmware update GUI? (y or n):")
            if 'y' in resp.lower():
                import subinitial.tools.stacksmanager as stacksmanager
                stacksmanager.run(default_host=self.core.connection.host, auto_connect=True)
                exit(1)

        # if they insist on continuing with unknown firmware and bad info then admit that the connection is valid
        if badinfo:
            return True

        if Version(info["lib_min"]) > Version(self.VERSION):
            print("\nWARNING: {} device (bus_address {}) requires library version >= {}".format(
                type(self).__name__, self.bus_address, info["lib_min"]))
            input("Pull the latest updates from https://bitbucket.org/subinitial/subinitial.git\nPress enter to continue anyway...")
        if info["assembly"] != self.ASSEMBLY:
            print("\nWARNING: {} device expected assembly number: {}, read: {} instead.".format(
                type(self).__name__, self.ASSEMBLY, info["assembly"]))
        return True

    def console(self, cmd, print_=False):
        if print_:
            print(">"+cmd)

        if cmd[-1] != '\n' and cmd[-1] != 'r':
            cmd += "\n"

        cmdlen = len(cmd)
        size = 8 + cmdlen
        req = bytearray(size)
        pack("BB{}s".format(cmdlen), req, 6, self.bus_address, 70, cmd.encode())

        resp = self.connection.request(req, size)

        # check for protocol exception
        remlen = len(resp) - 8
        if unpack("B", resp, 7)[0] & 0x80:
            code = -1 if remlen < 1 else unpack("B", resp, 8)[0]
            raise Exception("A protocol exception occured: {:02} '{}'. Response Buffer: {}".format(code, exceptions.get(code, "Unknown"), resp))

        resplen = unpack(">H", resp, 4)[0] - 2
        fncode, bus_address, response = unpack("BB{}s".format(resplen), resp, 6)
        response = response.decode().strip()

        if print_:
            print(response)

        return response

    def print_console(self, cmd):
        """Deprecated"""
        return self.console(cmd, print_=True)

    def update_busaddress(self, new_busaddress):
        if 30 < new_busaddress < 2:
            raise Exception("Bus address: {} must be between 2 and 30".format(new_busaddress))

        try:
            res = self.console("setbusaddr {}".format(new_busaddress))
        except Exception as ex:
            # print(ex)
            time.sleep(1)
            str(ex)
            pass

        try:
            res = self.console("getbusaddr ")
            return json.loads(res)["bus_address"] == new_busaddress
        except:
            return False

    def reset(self):
        try:
            self.console("ctrl 1 69")
        except:
            # console command will time out as expected
            pass
        time.sleep(2)


    def ident(self):
        if not hasattr(self, "rgbled"):
            raise Exception("Device: {} has no identification procedure".format(self.__class__.__name__))

        off_rgbb = 0xFFFFFF22
        on_rgbb = 0x00000000
        ori_rgbb = self.rgbled.get()
        flash_count = 12
        flash_duration = 0.08
        for i in range(0, flash_count):
            self.rgbled.set(on_rgbb)
            time.sleep(flash_duration)
            self.rgbled.set(off_rgbb)
            time.sleep(flash_duration)

        self.rgbled.set(ori_rgbb)


class RegisterI16(Register):
    signed = True

    def _get(self, start, end, cursor, bytebuf, offset, vals):
        vals.append(self.convert(unpack("<h", bytebuf, offset)[0]))
        return cursor + 1, offset + 2

    def _set(self, start, cursor, bytebuf, offset, val):
        pack("<h", bytebuf, offset, self.revert(val))
        return cursor + 1, offset + 2


class RegisterU32(Register):
    length = 2
    size = 4

    def _get(self, start, end, cursor, bytebuf, offset, vals):
        vals.append(self.convert(unpack("<I", bytebuf, offset)[0]))
        return cursor + 2, offset + 4

    def _set(self, start, cursor, bytebuf, offset, val):
        pack("<I", bytebuf, offset, self.revert(val))
        return cursor + 2, offset + 4


class RegisterI32(Register):
    length = 2
    size = 4
    signed = True

    def _get(self, start, end, cursor, bytebuf, offset, vals):
        vals.append(self.convert(unpack("<i", bytebuf, offset)[0]))
        return cursor + 2, offset + 4

    def _set(self, start, cursor, bytebuf, offset, val):
        pack("<i", bytebuf, offset, self.revert(val))
        return cursor + 2, offset + 4


class RegisterU64(Register):
    length = 4
    size = 8

    def _get(self, start, end, cursor, bytebuf, offset, vals):
        vals.append(self.convert(unpack("<Q", bytebuf, offset)[0]))
        return cursor + 4, offset + 8

    def _set(self, start, cursor, bytebuf, offset, val):
        pack("<Q", bytebuf, offset, self.revert(val))
        return cursor + 4, offset + 8


class RegisterI64(Register):
    length = 4
    size = 8
    signed = True

    def _get(self, start, end, cursor, bytebuf, offset, vals):
        vals.append(self.convert(unpack("<q", bytebuf, offset)[0]))
        return cursor + 4, offset + 8

    def _set(self, start, cursor, bytebuf, offset, val):
        pack("<q", bytebuf, offset, self.revert(val))
        return cursor + 4, offset + 8


class RegisterFloat(Register):
    length = 2
    size = 4

    def _get(self, start, end, cursor, bytebuf, offset, vals):
        vals.append(self.convert(unpack("<f", bytebuf, offset)[0]))
        return cursor + 2, offset + 4

    def _set(self, start, cursor, bytebuf, offset, val):
        pack("<f", bytebuf, offset, self.revert(val))
        return cursor + 2, offset + 4


class RegisterBytes(Register):
    length = 1
    size = 2
    consumes = True

    def _get(self, start, end, cursor, bytebuf, offset, vals):
        length = unpack("<h", bytebuf, offset)[0]
        if cursor == start:  # remaining data belongs to the array
            # if length > (end-cursor-1)*2:
            # 	raise Exception("bad array format")
            val = unpack(str(length)+"s", bytebuf, offset+2)[0] if length else b''
            vals.append(self.convert(val))
            return end, offset + (end-cursor)*2  # close out the read

    def _set(self, start, cursor, bytebuf, offset, val):
        length = len(val)
        if cursor == start:
            pack("<h{}s".format(length), bytebuf, offset, length, self.revert(val))
            doffset = ((length + 3) & 0xFFFE)  # HWord-Align and add space for length-header
            return cursor + (doffset >> 1), offset + doffset
        else:
            pack("<h", bytebuf, offset, length)
            return cursor + 1, offset + 2

    def read(self, bytecount=0):
        # in this case, a RegisterBytes will intepret x register reads as x byte reads and return the
        # appropriate read response in an hword aligned array
        return self.dev.request.read(self.address, bytecount)


class RegisterU16Array(Register):
    length = 1
    size = 2
    consumes = True
    format = "H"

    def _get(self, start, end, cursor, bytebuf, offset, vals):
        if cursor == start:  # remaining data belongs to the array
            length = end - cursor
            val = unpack("<{}{}".format(length, self.format), bytebuf, offset) if length else tuple()
            vals.append(tuple(self.convert(x) for x in val))
            return end, offset + length*2  # close out the read
        else:
            vals.append(unpack("<h", bytebuf, offset)[0])
            return cursor + 1, offset + 2

    def _set(self, start, cursor, bytebuf, offset, val):
        if cursor == start:
            length = len(val)

            pack("<{}{}".format(length, self.format), bytebuf, offset, *tuple(self.revert(x) for x in val))
            return cursor + length, offset + length*2
        else:
            pack("<H", bytebuf, offset, 0)
            return cursor + 1, offset + 2

    def read(self, itemcount=1):
        return self.dev.request.read(self.address, (itemcount*self.length))


class RegisterI16Array(RegisterU16Array):
    format = 'h'
    signed = True


class RegisterU32Array(Register):
    length = 2
    size = 4
    consumes = True
    format = "I"

    def _get(self, start, end, cursor, bytebuf, offset, vals):
        if cursor == start:  # remaining data belongs to the array
            length = (end - cursor) >> 1
            val = unpack("<{}{}".format(length, self.format), bytebuf, offset) if length else tuple()
            vals.append(tuple(self.convert(x) for x in val))
            return end, offset + (end-start) * 2  # close out the read
        else:
            vals.append(unpack("<i", bytebuf, offset)[0])
            return cursor + 2, offset + 4

    def _set(self, start, cursor, bytebuf, offset, val):
        if cursor == start:
            length = len(val)
            pack("<{}{}".format(length, self.format), bytebuf, offset, *tuple(self.revert(x) for x in val))
            return cursor + length*2, offset + length*4
        else:
            pack("<i", bytebuf, offset, 0)
            return cursor + 2, offset + 4

    def read(self, itemcount=1):
        return self.dev.request.read(self.address, itemcount*self.length)

    def _reqlen(self, lastreg):
        return lastreg.addr


class RegisterI32Array(RegisterU32Array):
    format = "i"
    signed = True


class RegisterSetBase(object):
    def read_multiple(self, firstreg, lastreg):
        lastidx = int(lastreg)
        firstidx = int(firstreg)
        regcount = lastidx - firstidx + self[lastidx].length
        return self.dev.request.read(firstidx, regcount)

    def __getitem__(self, addr):
        if type(addr) == int:
            addr = self.Addresses.get(addr)
            if addr is None:
                return None
        return getattr(self, addr)


""" BELOW IS AUTOGENERATED, ONLY EXISTS HERE FOR EXAMPLE """


# class Core:
#     def __init__(self, host, port=502, autoconnect=True):
#         self.connection = MockConnection(host, port, autoconnect)
#
#
# class AnalogDeck(Device):
#     def __init__(self, core, bus_address):
#         super(AnalogDeck, self).__init__(core, bus_address)
#         self.regs = RegisterSet(self)
#         self.r = LittleRSet(self)
#         self.control = Control(self)
#         self.request = Request67(self)
#
#     def __enter__(self):
#         return self.regs, self.r, self.control, self.request.queue()
#
#     def __exit__(self, type, value, traceback):
#         self.request.submit()

#
# def main():
#     # structtest()
#     # devtest()
#     trymock()
#
#
# def trymock():
#     connection = MockJsonConnection()
#
#     connection._transmit(b'1234')
#     resp = connection._receive()
#     print(resp)
#     print(connection.response)
#
#
# def devtest():
#     core = Core("192.168.1.49")
#     analog = AnalogDeck(core, 2)
#
#     print(analog.regs.ADC0.read())
#     analog.regs.ADC1.write(1)
#
#     with analog as (regs, r, ctrl, resp):
#         resp.adc0 = regs.ADC0
#         regs.ADC1.write(2)
#
#
# def structtest():
#     r = RegisterBytes(0,0)
#     buf = bytearray(1500)
#
#     cursor, offset = r._set(0,0,buf,0,b'123')
#     print(cursor, offset)
#     print(buf)
#
#     vals = []
#     print(r._get(0, cursor, 0, buf, 0, vals))
#     print(vals)
#
#     r = RegisterU32Array(0,0)
#     r._set(0, 0, buf, 0, [1,2,3,65530])
#
#     print(buf)
#
#     vals = []
#     print(r._get(0, 4, 0, bytearray(8), 0, vals))
#     print(r._get(0, 4, 0, bytearray([2,0,  1,0,2,0,0,0,4,0,5,0]), 0, vals))
#     print(vals)
#
#
# if __name__ == "__main__":
#     main()
