import sys

VERSION_MAJOR = 1  # deprecated

VERSION_STACKS = (1, 19, 0)

if sys.version_info < (3, 0):
    from subinitial.py2.core import Core
    from subinitial.py2.analog import AnalogDeck
    from subinitial.py2.relay import RelayDeck
    from subinitial.py2.tracks import Tracks, LanTracks
    from subinitial.py2.nanoamp import Nanoammeter
    from subinitial.py2.stcr import Stcr
else:
    from subinitial.py3.core import Core
    from subinitial.py3.analog import AnalogDeck
    from subinitial.py3.relay import RelayDeck
    from subinitial.py3.tracks import Tracks, LanTracks
    from subinitial.py3.nanoamp import Nanoammeter
    from subinitial.py3.stcr import Stcr

# Backwards Compatibility
NanoampMeter = Nanoammeter

class _Equipment:
    def __init__(self):
        self.objects = {}

    def get(self, name, ctor):
        obj = self.objects.get(name, None)
        if obj is None:
            obj = ctor()
            self.objects[name] = obj
        return obj

    def get_core(self, name=None):
        """ Retrieves Core object by name. Creates a new unconnected Core object if name not found.

        :returns: Core object
        :rtype: subinitial.stacks.Core
        """
        core = self.get(name, lambda: Core(host="stacks", autoconnect=False))
        """:type: subinitial.stacks.Core"""
        return core

    def get_analogdeck(self, core, bus_address):
        """ Retrieves an Analog Deck object by its Core and bus_address
        :param core: Stacks Core that controls the Analog Deck
        :type core: subinitial.stacks.Core
        :param bus_address: bus address of the Analog Deck
        :type bus_address: int
        :rtype: subinitial.stacks.AnalogDeck
        """
        name = "__analogdeck{}".format(bus_address)
        analogdeck = self.get(name, lambda: AnalogDeck(core, bus_address=bus_address, verify_connection=False))
        """:type: subinitial.stacks.AnalogDeck"""
        return analogdeck

    def get_relaydeck(self, core, bus_address):
        """ Retrieves an Relay Deck object by its Core and bus_address
        :param core: Stacks Core that controls the Relay Deck
        :type core: subinitial.stacks.Core
        :param bus_address: bus address of the Relay Deck
        :type bus_address: int
        :rtype: subinitial.stacks.RelayDeck
        """
        name = "__relaydeck{}".format(bus_address)
        relaydeck = self.get(name, lambda: RelayDeck(core, bus_address=bus_address, verify_connection=False))
        """:type: subinitial.stacks.RelayDeck"""
        return relaydeck

    def get_tracks(self, core, bus_address):
        """ Retrieves an Tracks object by its Core and bus_address
        :param core: Stacks Core that controls the Tracks
        :type core: subinitial.stacks.Core
        :param bus_address: bus address of the Tracks
        :type bus_address: int
        :rtype: subinitial.stacks.Tracks
        """
        name = "__tracks{}".format(bus_address)
        tracks = self.get(name, lambda: Tracks(core, bus_address=bus_address, verify_connection=False))
        """:type: subinitial.stacks.Tracks"""
        return tracks

    def get_nanoammeter(self, core, bus_address):
        """ Retrieves an Nanoammeter object by its Core and bus_address
        :param core: Stacks Core that controls the Nanoammeter
        :type core: subinitial.stacks.Core
        :param bus_address: bus address of the Tracks
        :type bus_address: int
        :rtype: subinitial.stacks.Nanoammeter
        """
        name = "__nanoammeter{}".format(bus_address)
        nanoammeter = self.get(name, lambda: Nanoammeter(core, bus_address=bus_address, verify_connection=False))
        """:type: subinitial.stacks.Nanoammeter"""
        return nanoammeter

equipment = _Equipment()
