# -*- coding: UTF-8 -*-
# Stacks Firmware Update Script
# © 2012-2018 Subinitial LLC. All Rights Reserved
# Redistribution and use in source and binary forms, with or without modification, are permitted provided that the
# following conditions are met: Redistributions of source code and binary forms must retain the above copyright notice,
# these conditions, and the disclaimer below all in plain text. The name Subinitial may not be used to endorse or
# promote products derived from this software without prior written consent from Subinitial LLC. This software may only
# be redistributed and used with a Subinitial Stacks product.

# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
# warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. Refer to the included LICENSE.txt for more details.

import time
import sys
import inspect
import os
import argparse
import json
import subinitial.stacks as stacks

def get_execfile():
    return inspect.stack()[0][1]

def get_execdir():
    return os.path.dirname(get_execfile())

def wait(seconds, dots=20):
    for i in range(0, dots):
        time.sleep(seconds/dots)
        sys.stdout.write(".")
        sys.stdout.flush()
        # print(".", end="", flush=True)
    sys.stdout.write("\n")
    sys.stdout.flush()


def update(host, busaddressstr, assembly, img):
    print(">>UPDATE>>")
    os.chdir(os.path.abspath(get_execdir()))

    try:
        print("Connecting to Stacks Core: {}".format(host))
        core = stacks.Core(host=host, verify_connection=False)
        print("Connection Validation - response to 'id'")
        core.print_console("id")
    except Exception as ex:
        err = "Failed to connect to Core"
        print(err)
        return 1, err

    # check busaddress
    try:
        busaddress = int(busaddressstr)
    except Exception as ex:
        err = "Invalid bus address: {}, must be an integer between 1 and 250".format(busaddressstr)
        print(err)
        return 2, err

    # get image data
    retcode, imgpath = get_imgpath(img, assembly)
    if retcode > 0:
        return retcode, imgpath
    with open(imgpath, mode="rb") as f:
        imgdata = f.read()

    print("Checking Update Target's ID:")
    device = stacks.Tracks(core, busaddress, verify_connection=False)
    try: device.print_console("id")
    except: print("checking target id failed...")

    print("Uploading {} to core for firmware update at busaddress {}".format(imgpath, busaddress))
    core.updater.update(busaddress, imgdata)

    if busaddress == 1:
        core.close()

    wait(7, 20)

    if busaddress == 1:
        core = stacks.Core(host=host, verify_connection=False)
        device = stacks.Tracks(core, busaddress, verify_connection=False)
        device.print_console("id")

    print("Checking Stacks Core Updater Log:")
    core.print_console("info 1")  # print updater logs

    print("Checking Update Target's ID:")
    device = stacks.Tracks(core, busaddress, verify_connection=False)
    try: device.print_console("id")
    except: print("checking target id failed...")
    return 0, "Update Finished"

def get_imgpath(imagepath, assembly):
    if type(imagepath) == str:
        return 0, imagepath

    assemblytext = assembly.strip().lower()
    images = {
        "bin/ucore.img": [
            "core",
            "13729",
        ],
        "bin/analog.img": [
            "analog",
            "13730",
        ],
        "bin/relay.img": [
            "relay",
            "13731",
        ],
        "bin/nanoamp.img": [
            "nanoamp",
            "13787",
        ],
        "bin/tracks.img": [
            "tracks",
            "13732",
            "13755"
        ],
    }
    for imgpath, matches in images.items():
        for match in matches:
            if match in assemblytext:
                return 0, imgpath

    err = "No firmware image found for assembly: {}".format(assembly)
    print(err)
    return 1, err


default_lanconfig = {
    "host": "stacks",
    "ip": "192.168.1.49",
    "subnet": "255.255.255.0",
    "gateway": "192.168.0.1"
}


def _parselanconfig(lanconfig_text):
    info = {}
    try:
        lines = lanconfig_text.strip().splitlines()
        info["host"] = lines[1].split("=")[1].strip().lower()
        info["ip"] = lines[2].split("=")[1].strip()
        info["subnet"] = lines[3].split("=")[1].strip()
        info["gateway"] = lines[4].split("=")[1].strip()
    except:
        pass
    return info


def ident(host, busaddr, assembly):
    try:
        print("Connecting to Stacks Core: {}".format(host))
        core = stacks.Core(host=host, verify_connection=False)
        if busaddr == 1:
            dev = core
        else:
            Device = {
                "SA13730": stacks.AnalogDeck,
                "SA13731": stacks.RelayDeck
            }.get(assembly, stacks.AnalogDeck)

            dev = Device(core, busaddr, verify_connection=False)
        dev.ident()
    except Exception as ex:
        err = "Failed to connect to Core+Busaddr {}".format(busaddr)
        print(err)
        return 1, err


def query_stack(host="192.168.1.49", engage_dutpower=True):
    print(">>QUERY_STACK>>")

    try:
        print("Connecting to Stacks Core: {}".format(host))
        core = stacks.Core(host=host, verify_connection=False)
        print("Connection Validation - response to 'id'")
        core.print_console("id")
    except Exception as ex:
        err = "Failed to connect to Core"
        print(err)
        return 1, err

    if engage_dutpower:
        print("Engaging Dutpowers 12V and 5V for Accessory Power")
        try:
            dut12_5_bits = (1<<0) | (1<<1)# 12V and 5V dutpower bits
            status = core.print_console("ctrl 2 0 {}".format(dut12_5_bits))  # Engage bits
            status = int(status.split(":")[1][:-1].strip('"'), 16)
            if status & dut12_5_bits != dut12_5_bits:
                wait(1.2, 5)
        except:
            pass

    core.print_console("busping")
    wait(1.2, 5)


    busconfigtxt = core.print_console("busconfig")
    busconfig = json.loads(busconfigtxt)

    stackinfo = {}

    for deck in busconfig["decks"]:
        addr = deck["id"]
        namespace = deck["ns"]


        info = {
            "busaddress": addr,
            "name": "N/A",
            "assembly": "N/A",
            "rev": "N/A",
            "firmware": "N/A",
            "lib_min": "1.5.0",
        }

        if namespace == "bootload":
            info["name"] = namespace
            stackinfo[addr] = info
            continue

        try:
            device = stacks.Tracks(core, addr, verify_connection=False)
            id = device.print_console("id")
            infotext = device.print_console("info 0")
        except Exception as ex:
            print("error using console id and info 0", ex)
            continue

        # try to load info json
        try:
            info.update(json.loads(infotext))
            stackinfo[addr] = info
            continue
        except Exception as ex:
            print("error loading info json", ex)

        # second chance, try to parse id string
        try:
            lines = id.splitlines()
            name, nsassy = lines[0].split("(")
            assy = nsassy.split(" ")[1]
            info["name"] = name.strip()
            info["assembly"] = assy[:-1]
            firmware = lines[1].split("_")[0]
            info["firmware"] =firmware.lstrip("v")
        except Exception as ex:
            print("error parsing console id string", ex)
        finally:
            stackinfo[addr] = info

    try:
        info = stackinfo[1]
        info.update(default_lanconfig)
        lanconfig = core.print_console("lanconfig")
        info.update(_parselanconfig(lanconfig))
    except:
        pass

    return 0, stackinfo


def search_for_core():
    try:
        lanconfig = stacks.Core.broadcast("lanconfig", print_=True)
        return _parselanconfig(lanconfig)
    except:
        return {}


def save_lanconfig(host, netbios, ip, subnet, gateway, is_broadcast=False):
    try:
        if is_broadcast:
            cons = stacks.Core.broadcast
        else:
            core = stacks.Core(host=host, verify_connection=False, lan_retries=0, lan_timeout=2)
            cons = core.console

        cons("lanconfig i {}".format(ip), print_=True)
        cons("lanconfig s {}".format(subnet), print_=True)
        cons("lanconfig g {}".format(gateway), print_=True)
        cons("lanconfig h {}".format(netbios), print_=True)
        cons("cfgsave", print_=True)
    except:
        return 1, "LAN Config Failed"

    try:
        cons("reset", print_=True)
    except:
        print("Reset command sent...core connection failed as expected")
    wait(3, 10)
    return 0, "Finished, reconnecting to {}".format(ip)


def save_busaddr(host, old_busaddr, new_busaddr):
    try:
        core = stacks.Core(host=host, verify_connection=False, lan_retries=0, lan_timeout=2)
        dev = stacks.AnalogDeck(core, old_busaddr, False)
        if not dev.update_busaddress(new_busaddr):
            return 1, "Bus Address Update Failed"
        dev.reset()
    except:
        return 1, "Bus Address Update Failed"

    wait(3, 10)
    return 0, "Finished, reconnecting to {}".format(host)

if __name__ == "__main__":
    # code, info = query_stack("192.168.1.49")
    # print(info)
    # exit()

    parser = argparse.ArgumentParser(description='Stacks Firmware Update Script')
    parser.add_argument('-n', '--host', default="stacks")
    parser.add_argument('-b', '--busaddress')
    parser.add_argument('-a', '--assembly', default=None)
    parser.add_argument('-i', '--img', default=None)
    args = parser.parse_args()

    code, msg = update(args.host, args.busaddress, args.assembly, args.img)
    exit(code)