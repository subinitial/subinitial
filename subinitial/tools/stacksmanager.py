#!/usr/bin/env python3
# -*- coding: UTF-8 -*-
# Stacks Manager GUI
# © 2012-2018 Subinitial LLC. All Rights Reserved
# Redistribution and use in source and binary forms, with or without modification, are permitted provided that the
# following conditions are met: Redistributions of source code and binary forms must retain the above copyright notice,
# these conditions, and the disclaimer below all in plain text. The name Subinitial may not be used to endorse or
# promote products derived from this software without prior written consent from Subinitial LLC. This software may only
# be redistributed and used with a Subinitial Stacks product.

# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
# warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. Refer to the included LICENSE.txt for more details.

import sys
import time
import os

if sys.version_info < (3, 0):
    raise Exception("Stacks Manager only supports Python3 as of this time")
    import ttk
    from Tkinter import *
else:
    from tkinter import *
    from tkinter import messagebox, filedialog
    from tkinter import ttk


import subinitial.tools.updater as updater
import subinitial.stacks as stacks
from subinitial.py3.stacks import Version

lib_location = os.path.dirname(stacks.__file__)
print("subinitial.stacks loaded from:")
print("\t"+lib_location)

root = Tk()
root.title("Stacks Manager / Stacks Python Library v{}.{}.{}".format(*stacks.VERSION_STACKS))

class Controller:
    def __init__(self, host="192.168.1.49", autoconnect=False):
        # model
        self.host = host
        self.stackinfo = {}
        self.autoconnect = autoconnect

        self.view = View(root, self)
        self.view.host.set(self.host)

        self.search_for_core()

        if autoconnect:
            self.reconnect()

        self.view.mainloop()

    def reconnect(self, host=None, engage_dutpower=True):
        if host:
            self.host = host
        print("Querying", host)
        code, stackinfo = updater.query_stack(self.host, engage_dutpower)
        if code != 0:
            messagebox.showerror("Error", str(stackinfo))
            return
        else:
            self.stackinfo = stackinfo
            self.view.update_devicetree(stackinfo)
            coreinfo = stackinfo.get(1, updater.default_lanconfig)
            self.view.update_lanconfig(coreinfo)

    def update(self, host, busaddr, assy, filename):
        self.host = host

        core = "SA13729"
        analog = "SA13730"
        relay = "SA13731"

        if assy in [core, analog, relay]:
            result = messagebox.askokcancel("Warning",
                                "WARNING: Disconnect all DSUB cables & 3.5mm Jack before continuing.\n\nThis will prevent potential damage.")
            if not result:
                return

        code, msg = updater.update(self.host, busaddr, assy, filename)

        if code == 0:
            messagebox.showinfo("Result", msg)
        else:
            messagebox.showerror('Update Failed', msg + "\nPower-cycle the Stack then click ok.")
        self.reconnect(engage_dutpower=self.view.dutpower.get())

    def save_lanconfig(self, host, netbios, ip, subnet, gateway, is_broadcast=False):
        code, msg = updater.save_lanconfig(host, netbios, ip, subnet, gateway, is_broadcast)
        if code:
            messagebox.showerror('LAN Config Failed', msg + "\nPower-cycle the Stack then retry.")
        else:
            messagebox.showinfo("LAN Config", msg)
            self.view.host.set(ip)
            self.reconnect(ip, engage_dutpower=self.view.dutpower.get())

    def save_busaddr(self, host, old_busaddr, new_busaddr, busaddresses):
        if old_busaddr == new_busaddr:
            messagebox.showerror('Already Set', "The selected device is already set to Bus Address = {}".format(new_busaddr))
            return

        if new_busaddr in busaddresses:
            messagebox.showerror('Address Conflict', 'Bus Address {} is already in use!'.format(new_busaddr))
            return

        if 2 > new_busaddr or new_busaddr > 30:
            messagebox.showerror('Invalid Address', 'Bus Address must be an integer from 2 to 30')
            return

        if old_busaddr == 1:
            messagebox.showerror('Invalid Device', "The Stacks Core must always have a Bus Address = 1")
            return



        result = messagebox.askokcancel("Warning",
                                        "WARNING: Disconnect all DSUB cables & 3.5mm Jack before continuing.\n\nThis will prevent potential damage.")
        if not result:
            return

        code, msg = updater.save_busaddr(host, old_busaddr, new_busaddr)
        if code:
            messagebox.showerror('Bus Address Update Failed', msg + "\nPower-cycle the Stack then retry.")
        else:
            messagebox.showinfo("Bus Address Update", msg)
            self.reconnect(host, engage_dutpower=self.view.dutpower.get())
            updater.ident(host, new_busaddr, self.stackinfo[new_busaddr]["assembly"])

    def search_for_core(self):
        info = updater.search_for_core()
        self.view.host.set(info.get("ip", self.view.host.get()))
        self.view.update_lanconfig(info)


class View(ttk.Frame):
    TREECOLS = ('busaddress', 'firmware', 'assembly', 'rev', 'serial', 'minimum library version')

    def __init__(self, master, controller):
        self.devicelist = []
        self.devicedict = {}
        self.controller = controller
        ttk.Frame.__init__(self, master, padding="5 5 5 5")
        self.grid(sticky=(N, W, E, S))
        self.columnconfigure(0, weight=0)
        self.columnconfigure(1, weight=1)
        self.rowconfigure(0, weight=1)
        self.rowconfigure(1, weight=0)

        ttk.Label(text="subinitial.stacks loaded from: " +lib_location + "  ").grid(column=0, row=1, sticky=E)

        top = self.winfo_toplevel()
        top.columnconfigure(0, weight=1)
        top.rowconfigure(0, weight=1)
        top.update_idletasks()
        w = top.winfo_screenwidth()
        h = top.winfo_screenheight()
        size = 1050, 550
        x, y = 200, 200  # y = h/2 - size[1]/2  # x = w/2 - size[0]/2
        x = max(min(x, w-size[0]), 0)
        y = max(min(y, h-size[1]), 0)
        top.geometry("{}x{}+{}+{}".format(size[0], size[1], int(x), int(y)))

        treeframe = ttk.Frame(self, padding="5 5 5 5")
        treeframe.grid(column=1, row=0, sticky=(N, W, E, S))
        treeframe.columnconfigure(0, weight=1)
        treeframe.rowconfigure(0, weight=1)



        self.devicetree = ttk.Treeview(treeframe, columns=self.TREECOLS)
        ysb = ttk.Scrollbar(treeframe, orient='vertical', command=self.devicetree.yview)
        xsb = ttk.Scrollbar(treeframe, orient='horizontal', command=self.devicetree.xview)
        self.devicetree.configure(yscroll=ysb.set, xscroll=xsb.set)
        self.devicetree.column('#0', width=120)
        self.devicetree.heading('#0', text='Device', anchor='w')
        #  ('busaddress', 'firmware', 'assembly', 'minimum library version')
        self.devicetree.column("busaddress", width=3)
        self.devicetree.heading("busaddress", text="Address")
        self.devicetree.column("firmware", width=6)
        self.devicetree.heading("firmware",  text="Version")
        self.devicetree.column("assembly", width=20)
        self.devicetree.heading("assembly",  text="Assembly")
        self.devicetree.column("rev", width=3)
        self.devicetree.heading("rev",  text="Rev")
        self.devicetree.column("serial", width=20)
        self.devicetree.heading("serial", text="Serial")
        self.devicetree.column("minimum library version", width=6)
        self.devicetree.heading("minimum library version", text="Minimum Library Version")

        self.devicetree.grid(column=0, row=0, sticky=(N, W, E, S))
        ysb.grid(row=0, column=1, sticky='ns')
        xsb.grid(row=1, column=0, sticky='ew')

        panelframe = ttk.Frame(self, padding="5 5 5 5")
        panelframe.grid(column=0, row=0, sticky=(N, W))

        hostframe = ttk.Frame(panelframe)
        hostframe.grid(row=0, column=0, sticky=EW)
        hostframe.columnconfigure(1, weight=1)
        ttk.Label(hostframe, text="Stacks Core IP/Hostname:").grid(row=0, column=0, sticky=W)
        ttk.Button(hostframe, text="Search", command=lambda: self.controller.search_for_core()).grid(row=0, column=1, sticky=E)
        self.host = StringVar()
        self.host.set(controller.host)
        # s=ttk.Style()
        # s.configure('Blue.TEntry', background='blue')
        ttk.Entry(panelframe, textvariable=self.host).grid(row=1, sticky=EW)

        connectFrame = ttk.Frame(panelframe)
        connectFrame.grid(row=3, sticky=EW)

        ttk.Button(connectFrame, text="Connect / Scan", command=lambda: self.controller.reconnect(self.host.get(), self.dutpower.get())).grid(row=0, column=0, sticky=W)
        self.dutpower = BooleanVar()
        self.dutpower.set(True)
        ttk.Checkbutton(connectFrame, text="Engage Accessory\nDutpowers", padding="3 3 3 3", variable=self.dutpower).grid(column=1, row=0, sticky=E)

        ttk.Separator(panelframe, orient=HORIZONTAL).grid(row=4, sticky=EW, pady=10)

        update_lblframe = ttk.Labelframe(panelframe, text='Firmware Update', padding="5 5 5 5")
        update_lblframe.grid(column=0, row=5, columnspan=2, sticky=EW)
        update_lblframe.columnconfigure(0, weight=1)
        ttk.Button(update_lblframe, text="Update Selected", command=lambda: self.on_update(), padding="5 10 5 10").grid(column=0, sticky=EW)

        fromfile_lblframe = ttk.Labelframe(panelframe, text='Firmware Update From File', padding="5 5 5 5")
        fromfile_lblframe.grid(column=0, row=7, sticky=EW)
        self.busaddr = StringVar()
        self.busaddr.set("1")
        ttk.Label(fromfile_lblframe, text="Bus Address:").grid(column=0, row=0, sticky=E)
        ttk.Entry(fromfile_lblframe, textvariable=self.busaddr, width=5).grid(column=1, row=0, sticky=W)
        ttk.Button(fromfile_lblframe, text="Start", command=lambda: self.on_openimg()).grid(column=2, row=0, sticky=EW, padx=5)

        identify_lblframe = ttk.Labelframe(panelframe, text='Identify Device', padding="5 5 5 5")
        identify_lblframe.grid(column=0, row=8, sticky=EW)
        ttk.Button(identify_lblframe, text="Flash Selected RGB LED", command=lambda: self.on_ident()).grid(sticky=EW)
        busaddr_lblframe = ttk.Labelframe(panelframe, text='Assign Bus Address', padding="5 5 5 5")
        busaddr_lblframe.grid(column=0, row=9, sticky=EW)
        self.new_busaddr = IntVar()
        self.new_busaddr.set(30)
        ttk.Label(busaddr_lblframe, text="New Address:").grid(column=0, row=0, sticky=E)
        ttk.Entry(busaddr_lblframe, textvariable=self.new_busaddr, width=5).grid(column=1, row=0, sticky=W)
        ttk.Button(busaddr_lblframe, text="Update Selected", command=lambda: self.on_savebusaddr()).grid(column=2, row=0,
                                                                                                 sticky=EW, padx=5)


        lancfg_lblframe = ttk.Labelframe(panelframe, text='Configure IP Settings', padding="5 5 5 5")
        lancfg_lblframe.grid(column=0, row=11, sticky=EW)
        self.new_host = StringVar()
        self.new_host.set("stacks")
        self.new_ip = StringVar()
        self.new_ip.set("192.168.1.49")
        self.new_subnet = StringVar()
        self.new_subnet.set("255.255.255.0")
        self.new_gateway = StringVar()
        self.new_gateway.set("192.168.1.0")
        self.broadcast = BooleanVar()
        self.broadcast.set(False)

        ttk.Label(lancfg_lblframe, text="IP Address:").grid(column=0, row=0, sticky=E)
        ttk.Entry(lancfg_lblframe, textvariable=self.new_ip).grid(column=1, row=0, sticky=W)
        ttk.Label(lancfg_lblframe, text="Subnet Mask:").grid(column=0, row=1, sticky=E)
        ttk.Entry(lancfg_lblframe, textvariable=self.new_subnet).grid(column=1, row=1, sticky=W)
        ttk.Label(lancfg_lblframe, text="Gateway:").grid(column=0, row=2, sticky=E)
        ttk.Entry(lancfg_lblframe, textvariable=self.new_gateway).grid(column=1, row=2, sticky=W)
        ttk.Label(lancfg_lblframe, text="NetBIOS Name:").grid(column=0, row=3, sticky=E)
        ttk.Entry(lancfg_lblframe, textvariable=self.new_host).grid(column=1, row=3, sticky=W)
        ttk.Checkbutton(lancfg_lblframe, text="Broadcast", variable=self.broadcast).grid(column=0, row=4, sticky=W)
        ttk.Button(lancfg_lblframe, text="Save & Reset", command=lambda: self.on_savelan()).grid(column=1, row=4, sticky=E, pady=5)

        for child in panelframe.winfo_children(): child.grid_configure(pady=3)

    def on_ident(self):
        sdev = self.get_selected_device()
        if sdev is None:
            print("no device selected!")
            messagebox.showerror('No Device Selected', 'Select a device from the device tree')
            return

        updater.ident(self.host.get(), sdev["busaddress"], sdev["assembly"])

    def on_savebusaddr(self):
        sdev = self.get_selected_device()
        if sdev is None:
            print("no device selected!")
            messagebox.showerror('No Device Selected', 'Select a device from the device tree')
            return

        busaddr = sdev["busaddress"]

        vmin = Version("1.15.0")
        vx = Version(sdev["firmware"])

        if vx < vmin:
            messagebox.showerror('Firmware Update Required', 'Device firmware must be at least v1.15.0')
            return

        busaddresses = set(x["busaddress"] for x in self.devicelist)


        self.controller.save_busaddr(self.host.get(), busaddr,
                                     self.new_busaddr.get(), busaddresses)

    def on_savelan(self):
        self.controller.save_lanconfig(self.host.get(),
            self.new_host.get(), self.new_ip.get(), self.new_subnet.get(),
            self.new_gateway.get(), self.broadcast.get())



    def on_openimg(self):
        filename = filedialog.askopenfilename()
        if filename:
            self.controller.update(self.host.get(), self.busaddr.get(), None, filename)
        else:
            print("openimg: no filename selected")

    def update_lanconfig(self, coreinfo):
        if "host" in coreinfo:
            self.new_host.set(coreinfo["host"])
        if "ip" in coreinfo:
            self.new_ip.set(coreinfo["ip"])
        if "subnet" in coreinfo:
            self.new_subnet.set(coreinfo["subnet"])
        if "gateway" in coreinfo:
            self.new_gateway.set(coreinfo["gateway"])

    def update_devicetree(self, stackinfo):
        self.devicelist = []
        self.devicedict = stackinfo
        self.devicetree.delete(*self.devicetree.get_children())  # clear
        for busaddr in sorted(stackinfo.keys()):
            info = stackinfo[busaddr]
            self.devicelist.append(info)
            node = self.devicetree.insert('', 'end', text=info["name"], open=True, values=(
                info["busaddress"], info["firmware"], info["assembly"], info["rev"], info.get("serial", ""), info["lib_min"]))


    def get_selected_device(self):
        try:
            vals = self.devicetree.item(self.devicetree.focus())['values']
            busaddr = vals[0]
            return self.devicedict[busaddr]
        except:
            return None

    def on_update(self):
        print("Update")
        sdev = self.get_selected_device()
        if sdev is None:
            print("no device selelcted!")
            messagebox.showerror('No Device Selected', 'Select a device from the device tree')
            return
        busaddr = sdev["busaddress"]
        assy = sdev["assembly"]
        # if busaddr == 4:
        #     assy = "SA13731"

        try:
            version = sdev["firmware"]
            major, minor, patch = version.split(".")
            major = int(major)
            minor = int(minor)

        except:
            print("No valid firmware version could be determined.")
            use_old_updater = busaddr == 1

        self.controller.update(self.host.get(), busaddr, assy, None)


def run(default_host="192.168.1.49", auto_connect=False):
    Controller(default_host, autoconnect=auto_connect)


if __name__ == "__main__":
    run()
