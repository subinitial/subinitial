# -*- coding: UTF-8 -*-
# Tracks Accessory interface
# © 2012-2018 Subinitial LLC. All Rights Reserved
# Redistribution and use in source and binary forms, with or without modification, are permitted provided that the
# following conditions are met: Redistributions of source code and binary forms must retain the above copyright notice,
# these conditions, and the disclaimer below all in plain text. The name Subinitial may not be used to endorse or
# promote products derived from this software without prior written consent from Subinitial LLC. This software may only
# be redistributed and used with a Subinitial Stacks product.

# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
# warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. Refer to the included LICENSE.txt for more details.

from subinitial.py2.stacks import *
from subinitial.stacks.utils import digest_indices, mask_to_bits, Bitmask

VERSION_MAJOR = 1


class Nanoammeter(Device):
    ASSEMBLY = "SA13787"
    FIRMWARE_MIN = "v1.13.0"

    def __init__(self, core_deck, bus_address, verify_connection=True):
        """Construct a Nanoamp Meter Accessory instance.

        .. code-block:: python

            # Python Example
            nanoamp = subinitial.stacks.Nanoammeter(core, bus_address=8)

        :param core_deck: Stacks Core instance that is controlling this Device
        :type core_deck: subinitial.py3.core1.Core
        :param bus_address: bus address from 2-31
        :type bus_address: int
        """
        Device.__init__(self, core_deck, bus_address, verify_connection)
        self.last_result_status = "OK"
        self.VERSION_MAJOR = VERSION_MAJOR
        self.regs = RegisterSet(self)
        self._last_result = [0, 0, 0]


    def _get_devicetype(self):
        return self.TYPE_ACCESSORY

    def __enter__(self):
        return self.regs, self.r, self.control, self.request.queue()

    def __exit__(self, type, value, traceback):
        self.request.submit()

    RANGE_AUTO = 0
    RANGE_3U = 1
    RANGE_30U = 2
    RANGE_300U = 3
    RANGE_3M = 4
    RANGE_30M = 5
    RANGE_300M = 6

    FILTER_0 = 0
    FILTER_1 = 1
    FILTER_2 = 2
    FILTER_3 = 3
    FILTER_4 = 4
    FILTER_5 = 5
    FILTER_6 = 6
    FILTER_7 = 7

    results = ["OK", "OUT_OF_RANGE", "ERROR", "BUSFAULT", "TIMEOUT"]

    def _get_result(self):
        regs = self.regs.METER_CONTROL.read(regcount=2+4)
        self._last_result = regs
        return regs[0] & 1 == 0

    def get_last_result_status(self, as_integer=False):
        ret = self._last_result[0] >> 8
        if as_integer:
            return ret
        return self.results[ret]

    def get_last_range(self):
        return (self._last_result[0] >> 4) & 0xF

    def measure(self, range_=RANGE_AUTO, filter_=FILTER_6, expected_measurement=0, raise_exception_on_error=True,
                timeout=4.0, pollingdelay=0.035):
        """
        @return: current measured in amps
        """
        if expected_measurement:
            expected_measurement = abs(expected_measurement)
            if expected_measurement > 30e-3:
                expected_measurement = self.RANGE_300M
            elif expected_measurement > 3e-3:
                expected_measurement = self.RANGE_30M
            elif expected_measurement > 300e-6:
                expected_measurement = self.RANGE_3M
            elif expected_measurement > 30e-6:
                expected_measurement = self.RANGE_300U
            elif expected_measurement > 3e-6:
                expected_measurement = self.RANGE_30U
            else:
                expected_measurement = self.RANGE_3U
        # txtmeas = self.console("read")
        # val, result, status = (int(x, 16) for x in txtmeas.split(","))
        self.request.queue()
        self.regs.METER_CONFIG.write(range_ | (filter_ << 8))
        self.regs.METER_CONTROL.write(1 | (int(expected_measurement) << 8))
        self.request.submit()

        if not do_for(timeout, self._get_result, repeatdelay=pollingdelay):
            raise SubinitialException(
                "Timeout: Nanoammeter failed to perform measurement results before {0}s".format(timeout))
        status, config, result = self._last_result
        status = (status >> 8) & 0xFF
        if status and raise_exception_on_error:
            raise Exception("Nanoammeter Result Error: {} {}".format(status, self.results[status]))
        return result / 1e12



    CHANNEL_COUNT = 13

    # def _get_task_result(self):
    #     while True:
    #         text = self.console("gettask")
    #         task, val, result, status, zero_offset = (int(x, 16) for x in text.split(","))
    #         if task == 0:
    #             if val > 0x7FFFFFFF:
    #                 val = -((0xFFFFFFFF - val) - 1)
    #             return val, result, status
    #         time.sleep(0.010)
    #
    # def get_last_zero_offset(self):
    #     text = self.console("gettask")
    #     task, val, result, status, zero_offset = (int(x, 16) for x in text.split(","))
    #     return zero_offset
    #
    # def set_range(self, range=RANGE_3M):
    #     if range < self.RANGE_150N or range > self.RANGE_AUTO:
    #         raise SubinitialInputError("Invalid range, use integer from 0 to 13")
    #     # self.console("range {}".format(range))
    #     self.console("settask 1 {}".format(range))
    #     self._get_task_result()
    #
    #
    # def _convert_result(self, counts, range):
    #     mult = [165e-9,
    #             3.3e-6,
    #             33e-6,
    #             330e-6,
    #             3.3e-3,
    #             33e-3,
    #             330e-3][range]
    #     return counts / 0x7FFFFFFF * mult

    # def measure2(self, raise_exception_on_error=True):
    #     """
    #     @return: current measured in amps
    #     """
    #     # txtmeas = self.console("read")
    #     # val, result, status = (int(x, 16) for x in txtmeas.split(","))
    #     self.console("settask 2 0")
    #     val, result, status = self._get_task_result()
    #     self.last_result_status = self.results[result]
    #     # print("val, result, status", hex(val), hex(result), hex(status))
    #     if result != 0 and raise_exception_on_error:
    #         raise SubinitialException("Nanoamp Meter Measure: {} status: {}, error_reg: {}".format(self.results[result], hex(status), hex(val)))
    #     return self._convert_result(val, status & 0xF)
    #
    # def measure_counts(self):
    #     """
    #     @return: uncalibrated current measurement counts value
    #     """
    #     # txtmeas = self.console("readcounts")
    #     # val, result, status = (int(x, 16) for x in txtmeas.split(","))
    #     self.console("settask 3 0")
    #     val, result, status = self._get_task_result()
    #     if result != 0:
    #         raise SubinitialException("Nanoamp Meter MeasureCounts: {} status: 0x{}".format(self.results[result], hex(status)))
    #     return val
    #



class RTypes:
#/*%regdef%*/
    pass
#/*%end_regdef%*/

class RegisterSet(RegisterSetBase):
    Addresses = {
        #/*%regaddr%*/
        0: "REG0",
        1: "KEY",
        2: "SERIALNUMBER",
        4: "FIRMWARE",
        6: "ASSEMBLY_NUMBER",
        7: "BUSADDRESS",
        8: "CONTROL",
        9: "STATUS",
        10: "CONFIG_ADDRESS",
        12: "CONFIG",
        14: "UPTIME",
        16: "BROADCAST_STATUS",
        17: "REQUEST_STATUS",
        18: "GUID",
        19: "SESSION",
        20: "GPR0",
        21: "GPR1",
        22: "GPR2",
        23: "GPR3",
        24: "TRIGGER",
        48: "METER_CONTROL",
        49: "METER_CONFIG",
        50: "METER_RESULT",
        60: "RGBB",
        62: "RGBB_EXCEPTION_STATUS",
        63: "RGBB_EXCEPTION_SUPRESSION",
        #/*%end_regaddr%*/
    }

    def __init__(self, device):
        self.dev = device
        #/*%reginst%*/
        self.REG0 = Register(0, device)
        self.KEY = Register(1, device)
        self.SERIALNUMBER = RegisterU32(2, device)
        self.FIRMWARE = RegisterU32(4, device)
        self.ASSEMBLY_NUMBER = Register(6, device)
        self.BUSADDRESS = Register(7, device)
        self.CONTROL = Register(8, device)
        self.STATUS = Register(9, device)
        self.CONFIG_ADDRESS = RegisterU32(10, device)
        self.CONFIG = RegisterU32(12, device)
        self.UPTIME = RegisterU32(14, device)
        self.BROADCAST_STATUS = Register(16, device)
        self.REQUEST_STATUS = Register(17, device)
        self.GUID = RegisterBytes(18, device)
        self.SESSION = RegisterBytes(19, device)
        self.GPR0 = Register(20, device)
        self.GPR1 = Register(21, device)
        self.GPR2 = Register(22, device)
        self.GPR3 = Register(23, device)
        self.TRIGGER = Register(24, device)
        self.METER_CONTROL = Register(48, device)
        self.METER_CONFIG = Register(49, device)
        self.METER_RESULT = RegisterI64(50, device)
        self.RGBB = RegisterU32(60, device)
        self.RGBB_EXCEPTION_STATUS = Register(62, device)
        self.RGBB_EXCEPTION_SUPRESSION = Register(63, device)
        #/*%end_reginst%*/

    def __getitem__(self, addr):
        if type(addr) == int:
            addr = RegisterSet.Addresses[addr]
        return getattr(self, addr)