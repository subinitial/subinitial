# -*- coding: UTF-8 -*-
# LAN Device Base Class
# © 2012-2018 Subinitial LLC. All Rights Reserved
# Redistribution and use in source and binary forms, with or without modification, are permitted provided that the
# following conditions are met: Redistributions of source code and binary forms must retain the above copyright notice,
# these conditions, and the disclaimer below all in plain text. The name Subinitial may not be used to endorse or
# promote products derived from this software without prior written consent from Subinitial LLC. This software may only
# be redistributed and used with a Subinitial Stacks product.

# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
# warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. Refer to the included LICENSE.txt for more details.

import sys
import socket
import time
import threading
import json

from subinitial.stacks import VERSION_STACKS
from subinitial.exceptions import *


def to_bool(astr):
    if astr is None:
        return 0
    if type(astr) == bytes:
        astr = astr.decode("utf-8")
    if type(astr) != str:
        return bool(astr)
    astr = astr.lower().strip()
    if len(astr) == 0 or astr in ("0", "false", "n", "no", "none", "null"):
        return False
    return True


class LanDevice:
    NO_CHANGE = None
    ASSEMBLY = "SA00XXX"
    FIRMWARE_MIN = (1, 0, 0)
    DEFAULT = None

    FAILUREMODE_RECONNECT = 0
    FAILUREMODE_SESSIONID_CHANGED = 1
    FAILUREMODE_NO_RETRIES_REMAIN = 2

    class NullLogger:
        def warning(self, str):
            pass

    class WarnLogger:
        def warning(self, str):
            sys.stderr.write(str)
            sys.stderr.write("\n")

    def __init__(self, host, port=9221, autoconnect=True, raise_response_exceptions=True,
                 lan_retries=3, lan_timeout=5, lan_handler=None, verify_connection=True, logger=None):
        """Construct a Subinitial Lan Device instance.

        .. code-block:: python

            # Python Example connect to two different devices
            dev0 = subinitial.stacks.LanDevice("192.168.1.50")
            # Connecting to device using its mDNS name (hostname + ".local")
            # The hostname is configurable with set_hostname(...)
            dev1 = subinitial.stacks.LanDevice("landevs000440.local")

        :param host: mDNS hostname, NETBIOS hostname, or ip address of the device
        :type host: str

        :param port: tcp port of the device server
        :type port: int

        :param autoconnect: if True a TCP connection is made to the device on object initialization
        :type autoconnect: bool

         :param raise_response_exceptions: if True any Modbus Exceptions are raised in Python as
        SubinitialModbusExceptions
        :type raise_response_exceptions: bool

        :param lan_retries: number of reconnection attempts (per each request) to perform after a network failure
            with the device. Set this to None for an infinite number of retries
        :type lan_retries: int

        :param lan_timeout: time in seconds after which a socket operation is considered timed out thereby prompting a
            reconnection attempt or raising a SubinitialNetworkException if no retries remain
        :type lan_timeout: float

        :param lan_handler: An optional event handler which is called during network failures to allow user
            intervention and custom fault tolerant applications

                * Param 1 (NetworkConnection): A reference to the failing network connection is supplied.
                  Use this to modify the NetworkConnection host, port, or timeout fields before continuing with
                  reconnection attempts.
                * Param 2 (int) FailureMode: A flag that notifies what failure mode is to be handled.

                    * Mode 0: A network timeout has occured and a reconnection attempt is about to be made.

                        * return None to continue recovering from the network failure
                        * return 0 to cancel the attempt and raise a SubinitialNetworkException
                        * return 1+ to specify the number of additional attempts remaining, thereby prolonging the
                          reconnection attempt procedure.

                    * Mode 1: A reconnection attempt has succeeded, but the device's session ID has changed
                      changed indicating possible state resets.

                        * return None to continue recovering from the network failure
                        * return 0 to cancel the attempt and raise a SubinitialNetworkException

                    * Mode 2: No reconnection attempts remain.

                        * return None to proceed with raising a SubinitialNetworkException
                        * return 1+ to specify a number of additional reconnection attempts, thereby prolonging the
                          reconnection attempt procedure.
        :type lan_handler: (NetworkConnection, int)->int|None
        """
        self.retries = lan_retries
        self.timeout = lan_timeout
        self.verify_on_connect = verify_connection
        self.on_networkfailure = lan_handler
        self.host = host
        self.port = port
        self._is_open = False
        self._last_contime = {}
        self._last_contime_lock = threading.Lock()
        self.logger = logger
        if self.logger is None:
            self.logger = self.WarnLogger()
        self.raise_response_exceptions = raise_response_exceptions

        self.socket = None
        self._buffer_size = 2048

        if autoconnect:
            self.connect()

    def __enter__(self):
        return self

    def __exit__(self, type_, value, traceback):
        self.close()

    @property
    def is_connected(self):
        return self._is_open

    def connect(self, host=None):
        host = host or self.host
        if self._is_open:
            if host == self.host:
                return
            else:
                self.close()
        self.host = host

        with self._last_contime_lock:
            # self._limit_connections_per_second()

            self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            # Don't set a socket timeout here
            self.socket.settimeout(self.timeout)
            try:
                resp = self.socket.connect((self.host, self.port))
            except:
                raise SubinitialTimeoutError("Failed to connect to {} at host: \"{}\","
                                             " timed out after {} seconds".format(self.__class__.__name__,
                                                                                  self.host, self.timeout))
            self._is_open = True
            if self.verify_on_connect:
                if not self.verify_connection():
                    self.close()
                    raise SubinitialException("device verification failed")

    def _reconnect(self, tries=1):
        # self._request_lock.release()
        while True:
            if self.on_networkfailure:
                retval = self.on_networkfailure(self, self.FAILUREMODE_RECONNECT)
                if type(retval) == int:
                    tries = retval
                if tries == 0:
                    return 0
            try:
                self.close()
                self.connect()
                return tries
            except Exception as e:
                self.logger.warning("Reconnecting to {} failed: {0}".format(self.__class__.__name__, e))
                if tries is None:
                    continue
                tries -= 1
                if tries <= 0:
                    return 0

    def close(self):
        if self._is_open:
            self._is_open = False
            if self.socket:
                self.socket.shutdown(socket.SHUT_RDWR)
                self.socket.close()
                self.socket = None
            return True
        return False

    def _txrx(self, msg):
        # First Attempt
        try:
            self.socket.send(msg)
            return bytearray(self.socket.recv(self._buffer_size))
        except Exception as e:
            self.logger.warning("{} transmission failed: {}".format(self.__class__.__name__, e))
            ex = e

        # Retries
        retries = self.retries
        while True:
            # Try to reconnect if retries remain
            if retries is None or retries > 0:
                self.logger.warning("Reconnecting to {}...".format(self.__class__.__name__))
                retries = self._reconnect(retries)  # reconnecting may have consumed some retries

            # Notify upper layer of network failure
            if retries <= 0 and self.on_networkfailure:
                retval = self.on_networkfailure(self, self.FAILUREMODE_NO_RETRIES_REMAIN)
                if type(retval) == int:
                    retries = retval
                retries = self._reconnect(retries)
                if retries:  # if the handler has modified the retry count then try to reconnect
                    continue

            # We have run out of retries, time to quit
            if retries <= 0:
                break

            # Now that we are reconnected retry the transmission
            try:
                self.socket.send(msg)
                return bytearray(self.socket.recv(self._buffer_size))
            except Exception as e:
                self.logger.warning("{} transmission failed: {}".format(self.__class__.__name__, e))
                ex = e
            retries -= 1  # the above transmission failed, consume one retry

        raise ex  # SubinitialException("network connection failed")

    def request(self, req):
        if type(req) != str:
            req = json.dumps(req)
        msg = req.encode('utf-8')
        rsp = self._txrx(msg)
        if rsp and len(rsp):
            if rsp == b'_':  # "_" character denotes empty responses
                return None
            resp = rsp.decode()
            if resp.startswith("error: ") and self.raise_response_exceptions:
                raise SubinitialResponseError(resp[7:])
            elif resp.startswith("{"):
                return json.loads(resp)
            return resp
        raise SubinitialResponseError("Remote LANDevice closed the network connection")

    def get_version(self):
        return self.request("version?")

    def get_config(self):
        return self.request("config?")

    def verify_connection(self):
        """Verifies network connection to the device
        Also verifies if firmware version and library version are compatible.
        This prompts the user when incompatibilities are detected.

        :returns: True if connection verified, False if connection could not be verified
        :rtype: bool
        """
        try:
            version = self.get_version()

            if version["assembly"] != self.ASSEMBLY:
                print("\nWARNING: Python is expecting {} with part-number: {}\nDevice is reporting part-number: {}"
                      .format(self.__class__.__name__, self.ASSEMBLY, version["assembly"]))
                time.sleep(0.01)
                resp = raw_input("Continue anyway? (y or n):")
                if 'y' in resp.lower():
                    return True
                else:
                    return False

            if tuple(version["firmware"]) < self.FIRMWARE_MIN:
                vstr = "v{}.{}.{}".format(*version["firmware"])
                fminstr = "v{}.{}.{}".format(*self.FIRMWARE_MIN)
                print("\nWARNING: {}\nFirmware should be updated to version >= {}".format(
                    self.__class__.__name__, fminstr))
                time.sleep(0.01)
                resp = raw_input("Update this device's firmware from {} to {}? (y or n):".format(vstr, fminstr))
                if 'y' in resp.lower():
                    if not self._upgrade(fminstr):
                        print("ERROR: firmware upgrade failed")
                        return False
                    version = self.request("version?")

            if VERSION_STACKS < tuple(version["lib_min"]):
                vstr = "v{}.{}.{}".format(*VERSION_STACKS)
                fminstr = "v{}.{}.{}".format(*version["lib_min"])
                print("\nWARNING: {}\nyour Python library should be updated from {} to version >= {}.".format(
                    self.__class__.__name__, vstr, fminstr))
                time.sleep(0.01)
                resp = raw_input("Continue anyway? (y or n):")
                if 'y' in resp.lower():
                    pass
                else:
                    return False
        except Exception as ex:
            print("An error occured while verifying connection to {}:\n\t{}".format(self.__class__.__name__, ex))
            return False
        return True

    def set_hostname(self, hostname=DEFAULT):
        """Set the device's hostname, netbios name, and zeroconf/mdns name
            Note: the mDNS hostname == hostname + ".local"
                unless there are mDNS hostname conflicts on your LAN.
                In which case the device will be hostname + "-#.local"
                where # is a positive integer such that no conflicts exist on your LAN.
            Note: the NETBIOS name == hostname.
        :param hostname: desired hostname
        :type: str
        """
        self.request("hostname {}".format(hostname))

    def get_hostname(self):
        """Get the device's hostname.
            Note: the mDNS hostname == hostname + ".local"
                unless there are mDNS hostname conflicts on your LAN.
                In which case the device will be hostname + "-#.local"
                where # is a positive integer such that no conflicts exist on your LAN.
            Note: the NETBIOS name == hostname.

        :return: device hostname
        :rtype: str
        """
        return self.request("hostname?")

    def get_idn(self):
        """Get the device's IDN

        :return: device IDN
        :rtype: str
        """
        return self.request("idn?")
    
    def identify(self, state, off_delay=None):
        """Command the device identification led to flash, or stop flashing

        :param state: if True then identify LED will start flashing, if False then identify LED will stop flashing
        :type state: bool
        :param off_delay: if None, the LED will continue flashing until commanded False, if off_delay is a positive
            number then the LED will continue flashing for off_delay seconds before it stops flashing.
        :type off_delay: None or float
        """
        if off_delay is not None:
            self.request("identify {} {}".format(state, off_delay))
        else:
            self.request("identify {}".format(state))

    def get_identify(self):
        """Get the device's identify state

        :return: True if device's identify LED is flashing
        :rtype: bool
        """
        return to_bool(self.request("identify?"))

    def reset(self):
        """Perform a complete system reboot, the device will go offline momentarily and Python will
            attempt reconnection as it becomes available"""
        retval = self.request("reset")
        if retval["error"]:
            self.close()
            raise SubinitialResponseError(retval["error"])
        etc = retval["etc"]
        if etc:
            self.close()
            self.logger.warning("waiting {}s for reset".format(etc))
            time.sleep(etc)
            self.connect()
        return

    def _shutdown(self):
        """Shutdown device, after which a power-cycle will be required for the device to resume operations"""
        resp = self.request("shutdown")
        self.close()

    def _upgrade(self, version=""):
        """Perform a software upgrade"""
        self.socket.settimeout(10)
        retval = self.request("upgrade {}".format(version))
        if retval["error"]:
            self.close()
            raise SubinitialResponseError(retval["error"])
        etc = retval["etc"]
        if etc:
            etc += 4
            self.logger.warning("closing")
            self.close()
            self.logger.warning("waiting {}s".format(etc))
            time.sleep(etc)
            self.logger.warning("reconnecting")
            self.connect()
            self.logger.warning("connection succeeded")

        return retval

    def reset_settings(self, *domains):
        """Reset the device configuration to factory defaults. This is equivalent to holding the RESET button on the device.

        :param domains: optional set of reset domain strings: "network", "usb", and/or "hostname".
            If ommitted, all domains are reset.
        :type domains: iter(str)
        """
        self.request("reset_settings {}".format(" ".join(domains)))

    def set_netconfig(self, dhcp=NO_CHANGE, ip=NO_CHANGE, subnet=NO_CHANGE, gateway=NO_CHANGE, dns=NO_CHANGE,
                      hostname=NO_CHANGE):
        """Set the device's network configuration to use automatic DHCP or static IP/Subnet/Gateway/DNS server
            the configuration will be applied immediately, and Python will attempt to connect to the new configuration
            parameters. If dhcp==True then Python will look for the hostname using mDNS and netbios
        :param dhcp: if True, use DHCP, if False, use static ip/subnet/gateway/dns
        :type dhcp: bool
        :param ip: static ipv4 ip address used if no DHCP, e.g. "192.168.1.51"
        :type ip: str
        :param subnet: static ipv4 subnet mask used if no DHCP, e.g. "255.255.255.0"
        :type subnet: str
        :param gateway: static gateway ipv4 address used if no DHCP, e.g. "192.168.1.1"
        :type gateway: str
        :param dns: static dns ip address used if no DHCP, e.g. "8.8.8.8"
        :type dns: str
        :param hostname: hostname, e.g. "mydevice"
            Note: the mDNS hostname == hostname + ".local"
                unless there are mDNS hostname conflicts on your LAN.
                In which case the device will be hostname + "-#.local"
                where # is a positive integer such that no conflicts exist on your LAN.
            Note: the NETBIOS name == hostname.
        :type hostname: str
        """
        self.request("netconfig {} {} {} {} {} {}".format(dhcp, ip, subnet, gateway, dns, hostname))

    def get_netconfig(self):
        """Gets the saved network settings applied at next device boot.

        :return: dictionary with the stored network settings for the device. These may not be the actual
            state of the device's network interface, check get_netstatus for that.
        """
        dhcp, ip, subnet, gateway, dns, hostname = self.request("netconfig?")
        return {"ip": ip, "subnet": subnet, "gateway": gateway, "dhcp": to_bool(dhcp), "dns": dns,
                "hostname": hostname}

    def get_netstatus(self):
        """Gets the current network interface status.

        :return: dictionary with the current network interface status. These may change when DHCP
            leases expire, or mDNS hostname conflicts are resolved.
        """
        dhcp, ip, subnet, gateway, dns, mdns_hostname = self.request("netstatus?")
        return {"ip": ip, "subnet": subnet, "gateway": gateway, "dhcp": to_bool(dhcp),
                "dns": dns, "mdns_hostname": mdns_hostname}

    USB_OFF = "off"
    USB_MULTI = "g_multi"
    USB_ETHERNET = "g_ether"
    USB_MASS_STORAGE = "g_mass_storage"
    USB_SERIAL = "g_serial"

    def set_usbmode(self, mode=USB_MULTI):
        """Set the device's usb mode configuration
        :param mode: usb device operating mode
            USB_OFF: the device will not enumerate as any usb device on your PC
            USB_MULTI: the device will enumerate as a usb-serial device, usb-ethernet device, and usb-mass-storage device
            USB_ETHERNET: the device will enumerate as just a usb-ethernet device
            USB_MASS_STORAGE: the device will enumerate as just a usb-mass-storage device
            USB_SERIAL: the device will enumeraet as just a usb-serial device
        :type mode: str
        """
        self.request("usbmode {}".format(mode))

    def get_usbmode(self):
        """Get teh device's usb mode state

        :return: dictionary containing the current usb mode status
        """
        usb_mode, usb_dhcp_server, usb_ip = self.request("usbmode?")
        return {"usb_mode": usb_mode, "usb_dhcp_server": usb_dhcp_server, "usb_ip": usb_ip}